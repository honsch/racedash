#include "mouse.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <linux/input.h>
#include "config/config.h"

#include "log/log.h"

//----------------------------------------------------------------------------------
static double EventTimeToSystemTime(const struct input_event *e)
{
   double st = ((double) e->time.tv_sec) + (((double)e->time.tv_usec) / 1000000.0);
   // I have no idea if event times are based on CLOCK_REALTIME
   return st;
}

//----------------------------------------------------------------------------------
Mouse::Mouse(void)
{
   mDeviceName = NULL;
   mDevFD = -1;
   mCallback = NULL;
   mCallbackContext = NULL;

   mPreviousEvent = { 0, 0, 0.5F, 0.5F, 0, 0 };

   mTotalEvents = 0;
}

//----------------------------------------------------------------------------------
Mouse::~Mouse()
{
   if (mDeviceName != NULL) free(mDeviceName);
   if (mDevFD > 0) close(mDevFD);
   mDeviceName = NULL;
   mDevFD = -1;
}

//----------------------------------------------------------------------------------
int Mouse::Init(const char *device)
{
   if (device == NULL) return -1;
   if (mDeviceName != NULL) free(mDeviceName);
   mDeviceName = strdup(device);

   return InitInternal();
}

//----------------------------------------------------------------------------------
int Mouse::InitInternal(void)
{
   if (mDeviceName == NULL) return -1;

   if (mDevFD > 0) close(mDevFD);
   mDevFD = open(mDeviceName, O_RDONLY | O_NONBLOCK);
   if (mDevFD < 0)
   {
      //log_warn("Mouse device open failure!  %d", mDevFD);
      mDevFD = -1;
      return errno;
   }
   log_info("Mouse device open success.");
   return 0;
}

//----------------------------------------------------------------------------------
void Mouse::RegisterCallback(InputEventCallback callback, void *context)
{
   mCallback        = callback;
   mCallbackContext = context;
}

//----------------------------------------------------------------------------------
int Mouse::Update(void)
{
   if (mDevFD < 0)
   {
      if (InitInternal() < 0) return -1;
   }

   int count = 0;
   do
   {
      struct input_event e;
      ssize_t bytes = read(mDevFD, &e, sizeof(e));

      // file error of some sort, close device and reopen next time
      if (bytes < 0)
      {
         if (errno != EAGAIN)
         {
            close(mDevFD);
            mDevFD = -1;
         }
         return (int) bytes;
      }

      if (bytes == 0) return count;         // no more data
      if (bytes != sizeof(e)) return count; // data error of some sort

      switch (e.type)
      {
         case EV_SYN:
            mPreviousEvent.time = EventTimeToSystemTime(&e);
            ++mTotalEvents;
            if (mCallback != NULL) mCallback(mCallbackContext, &mPreviousEvent);
            ++count;
            break;

         case EV_KEY:
            if (e.code == BTN_LEFT) mPreviousEvent.pressed = e.value;
            break;


         case EV_REL:
            if (e.code == REL_X)
            {
               mPreviousEvent.screenX += (0.001F * (float) e.value);
               if (mPreviousEvent.screenX < 0.0F)      mPreviousEvent.screenX = 0.0F;
               else if (mPreviousEvent.screenX > 1.0F) mPreviousEvent.screenX = 1.0F;
               mPreviousEvent.rawX    = (unsigned) e.value;
            }

            else if (e.code == REL_Y)
            {
               mPreviousEvent.screenY += (0.001F * (float) e.value);
               if (mPreviousEvent.screenY < 0.0F)      mPreviousEvent.screenY = 0.0F;
               else if (mPreviousEvent.screenY > 1.0F) mPreviousEvent.screenY = 1.0F;
               mPreviousEvent.rawY    = (unsigned) e.value;
            }
            else if (e.code == REL_WHEEL) // Two finger up/down drag
            {
            }
            else if (e.code == REL_WHEEL_HI_RES) // Also Two finger up/down drag
            {
            }
            else if (e.code == REL_HWHEEL) // Two finger left/right drag
            {
            }
            else if (e.code == REL_HWHEEL_HI_RES) // Also Two finger left/right drag
            {
            }
            else
            {
               printf("Mouse EV_REL code; %d 0x%X\n", e.code, e.code);
            }
            break;

         default:
            break;
      }
   } while (1);

   return count;
}
