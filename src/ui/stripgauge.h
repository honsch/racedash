#ifndef STRIPGAUGE_H
#define STRIPGAUGE_H

#include "gauge.h"
#include <vector>

//------------------------------------------------------
class StripGauge : public Gauge
{
  public:

    struct Presentation
    {
        bool                        showLabel;
        bool                        showUnits;
        bool                        showValue;
        unsigned                    wholeDigits;
        unsigned                    decimalDigits;

        unsigned                    tickSpacing; //number of CoordPairs to skip for drawing ticks

        ScreenElement::VisualStyle *labelStyle;
        ScreenElement::VisualStyle *unitsStyle;

        struct CoordPair
        {
            float x1;
            float y1;
            float x2;
            float y2;
            float value; // 0-1
            uint32_t color;
            const char *label;
            float labelX;
            float labelY;
            float scaledValue; // scaled by range internally
        };

        // need to be sorted by value
        std::vector<CoordPair> outline;
    };

    StripGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation);
   ~StripGauge();

    void SetRange(double minimum, double maximum);

    void SetVisualStyle(VisualStyle *style);

    void SetPresentation(StripGauge::Presentation *presentation);

    virtual void Draw(DisplayController *display);

  private:
    void RecalcSizes(void);

    Presentation  mPresentation;

    char          mValueFormat[10];

    float         mValueToRads;
    float         mMinRangeRad;

    float         mCX;
    float         mCenterY;
    float         mSizeX;
    float         mSizeY;

    static Presentation sDefaultPresentation;
};

#endif //STRIPGAUGE_H