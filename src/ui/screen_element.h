#ifndef SCREEN_ELEMENT_H
#define SCREEN_ELEMENT_H

#include <stdint.h>
#include "ui/ui.h"
#include "display.h"


#define SE_FLAG_DIRTY  0
#define SE_FLAG_USER   8  // Up to 15 is valid, bottom 8 are reserved

//-----------------------------------------------------------------------
class ScreenElement
{
 public:

    // Types used by ScreenElements that are externally accessible
    enum FillStyle { FillNone, FillBackgroundColor, FillSelectColor };
    struct VisualStyle
    {
        uint32_t    foregroundColor;
        uint32_t    backgroundColor;
        uint32_t    borderColor;
        uint32_t    selectColor;
        float       fontsize;
        int         fontFace;
        float       borderSize;
        float       cornerRadius;
    };

    ScreenElement(const char *name, uint16_t id, VisualStyle *style);
    virtual ~ScreenElement();

    // Valid return values from a Element UI update
    enum ElementUIResult { Yep, Handled };

    virtual ElementUIResult HandleUIEvent(UIEvent *message) = 0;
    virtual void            Draw(DisplayController *display) = 0;

    virtual void SetName(const char *name);
    virtual void SetPosition(float x, float y);
    virtual void SetSize(float width, float height);

    virtual void SetVisualStyle(VisualStyle *style);

    // These overwrite the local values inside the internal VisualStyle
    virtual void SetForegroundColor(uint32_t color);
    virtual void SetBackgroundColor(uint32_t color);

    bool Intersects(UIEvent *event, UIEvent *windowSpace);

    void SetFlag(uint8_t flag);
    void ClearFlag(uint8_t flag);
    bool TestFlag(uint8_t flag) const;
    bool TestAndClearFlag(uint8_t flag);

    float X(void)      const { return mX;      }
    float Y(void)      const { return mY;      }
    float Width(void)  const { return mWidth;  }
    float Height(void) const { return mHeight; }

 protected:
    float mX;
    float mY;
    float mWidth;
    float mHeight;

    VisualStyle    mVisualStyle;
    char          *mName;
    uint16_t       mID;
    uint16_t       mFlags;

    void DrawBorder(DisplayController *display, FillStyle fillStyle);

    static VisualStyle sDefaultVisualStyle;
};

#endif // SCREEN_ELEMENT_H
