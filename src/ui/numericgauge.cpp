#include "numericgauge.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

NumericGauge::Presentation NumericGauge::sDefaultPresentation =
{
    NumericGauge::Presentation::LP_Left, // labelPos
    NumericGauge::Presentation::LJ_Center, // labelJustify
    true, // showUnits
    0,    // nameAlignLength
    3,    // wholeDigits
    0,    // decimalDigits
    NULL, // nameStyle
    NULL  // unitsStyle
};


//------------------------------------------------------------------------------------------------------
NumericGauge::NumericGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation) :
    Gauge(variableName, units, id, visualStyle)
{
    SetPresentation(presentation);
}

//------------------------------------------------------------------------------------------------------
NumericGauge::~NumericGauge()
{
    if (mLabel != NULL) free(mLabel);
}

//------------------------------------------------------------------------------------------------------
void NumericGauge::SetVisualStyle(VisualStyle *style)
{
    if (style == NULL) mVisualStyle = sDefaultVisualStyle;
    else               mVisualStyle = *style;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void NumericGauge::SetPresentation(NumericGauge::Presentation *presentation)
{
    if (presentation == NULL) memcpy(&mPresentation, &sDefaultPresentation, sizeof(mPresentation));
    else                      memcpy(&mPresentation,  presentation,         sizeof(mPresentation));

    if (mPresentation.labelStyle == NULL) mPresentation.labelStyle = &mVisualStyle;
    if (mPresentation.unitsStyle == NULL) mPresentation.unitsStyle = &mVisualStyle;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void NumericGauge::RecalcSizes(void)
{
    // Make a new format string
    mValueFormat[0] = '%';
    mValueFormat[1] = 0;

    if (mPresentation.wholeDigits != 0)
    {
        sprintf(mValueFormat + 1, "%d", mPresentation.wholeDigits);
    }

    unsigned len = strlen(mValueFormat);
    sprintf(mValueFormat + len, ".%df", mPresentation.decimalDigits);

    // make the display offsets
    float labelWidth  = 0;
    float labelHeight = 0;
    float textWidth, textHeight;

    mLabelPosX = 0;
    mLabelPosY = 0;
    mValuePosX = 0;
    mValuePosY = 0;
    mUnitsPosX = 0;
    mUnitsPosY = 0;

    // get how wide the value can be
    len = ClampDigits();
    if (mPresentation.decimalDigits > 0)
    {
        //The +1 is for the decimal point
        len += 1 + mPresentation.decimalDigits;
    }
    // Always leave room for the minus sign
    if (1 || mMinRange < 0.0F)
    {
        //The +1 is for the minus sign
        len++;
    }

    gDisplay->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);
    mValueWidth = textWidth * len;

    // If I'm showing the label figure out how wide it is
    if (mPresentation.labelPos != NumericGauge::Presentation::LP_None)
    {
        if (mPresentation.nameAlignLength != 0)
        {
            gDisplay->GetFontSize(mPresentation.labelStyle->fontsize, textWidth, textHeight, mPresentation.labelStyle->fontFace);
            labelWidth  = (textWidth * mPresentation.nameAlignLength);
            labelHeight = textHeight;
        }
        else
        {
            gDisplay->GetStringExtents(mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace, mLabel, labelWidth, labelHeight);
        }

        // Deal with Label pos Y
        switch (mPresentation.labelPos)
        {
            case NumericGauge::Presentation::LP_Left:
            case NumericGauge::Presentation::LP_Right:
                switch (mPresentation.labelJustify)
                {
                    case NumericGauge::Presentation::LJ_LeftTop:
                        mLabelPosY = 0;
                        break;

                    case NumericGauge::Presentation::LJ_Center:
                        mLabelPosY = Center(mHeight, labelHeight);
                        break;

                    case NumericGauge::Presentation::LJ_RightBottom:
                        mLabelPosY = mHeight - labelHeight;
                        break;
                }
                break;

            case NumericGauge::Presentation::LP_Top:
                mLabelPosY = 0;
                break;

            case NumericGauge::Presentation::LP_Bottom:
                mLabelPosY = mHeight - labelHeight;
                break;

            default:
                break;
        }

        // Deal with Label pos X
        switch (mPresentation.labelPos)
        {
            case NumericGauge::Presentation::LP_Left:
                mLabelPosX = 0;
                break;

            case NumericGauge::Presentation::LP_Right:
                mLabelPosX = mWidth - labelWidth;
                break;

            case NumericGauge::Presentation::LP_Top:
            case NumericGauge::Presentation::LP_Bottom:
                switch (mPresentation.labelJustify)
                {
                    case NumericGauge::Presentation::LJ_LeftTop:
                        mLabelPosX = 0;
                        break;

                    case NumericGauge::Presentation::LJ_Center:
                        mLabelPosX = Center(mWidth, labelWidth);
                        break;

                    case NumericGauge::Presentation::LJ_RightBottom:
                        mLabelPosX = mWidth - labelWidth;
                        break;
                }
                break;

            default:
                break;
        }
    }

    // locate Value relative to label
    switch (mPresentation.labelPos)
    {
        case NumericGauge::Presentation::LP_Left:
            mValuePosX = mLabelPosX + labelWidth;
            mValuePosY = 0;
            break;

        case NumericGauge::Presentation::LP_Right:
            mValuePosX = 0;
            mValuePosY = 0;
            break;

        case NumericGauge::Presentation::LP_Top:
            mValuePosX = Center(mWidth, mValueWidth);
            mValuePosY = mLabelPosY + labelHeight;
            break;

        case NumericGauge::Presentation::LP_Bottom:
            mValuePosX = Center(mWidth, mValueWidth);
            mValuePosY = 0;
            break;

        default:
            break;
    }

    mUnitsPosX = mValuePosX + mValueWidth;
    mUnitsPosY = mValuePosY;
}


//-------------------------------------------------------------------
void NumericGauge::Draw(DisplayController *display)
{
    if (mVisualStyle.borderSize != 0)
    {
        DrawBorder(display, FillBackgroundColor);
    }

    if (mPresentation.labelPos != NumericGauge::Presentation::LP_None)
    {
        display->DrawString(mLabel, mX + mLabelPosX, mY + mLabelPosY, mPresentation.labelStyle->foregroundColor, mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace);
    }

    char s[80];
    snprintf(s, 80, mValueFormat, mCurrentValueClamped);
    float textWidth, textHeight;
    display->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, s, textWidth, textHeight);
    float offsetX = mValuePosX + (mValueWidth - textWidth);
    //printf("OffsetX of %s %d  SizeX %d   textWidth %d\n", mName, offsetX, mValueSizeX, textWidth);

    display->DrawString(s, mX + offsetX, mY + mValuePosY, mVisualStyle.foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);

    if (mPresentation.showUnits)
    {
        display->DrawString(mUnits, mX + mUnitsPosX, mY + mUnitsPosY, mPresentation.unitsStyle->foregroundColor, mPresentation.unitsStyle->fontsize, mPresentation.unitsStyle->fontFace);
    }
}

