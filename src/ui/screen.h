#ifndef SCREEN_H
#define SCREEN_H

#include <vector>

class ScreenElement;
class DisplayController;
class UIEvent;
class UI;
//--------------------------
class ScreenBase
{
  public:
   ScreenBase(void);
   virtual ~ScreenBase();

   // Valid return values from a ScreenBase UI update
   enum ScreenResult { ScreenOk, ScreenPop };

   virtual ScreenBase::ScreenResult Update(void) { return ScreenOk; }
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event) = 0;

   void ProcessElementUIEvent(UIEvent *e);

  protected:

   void AddElement(ScreenElement *e);
   void RemoveElement(ScreenElement *e);

  private:

   std::vector<ScreenElement *> mElements;

};

#endif // SCREEN_H