#ifndef DIALGAUGE_H
#define DIALGAUGE_H

#include "gauge.h"

//------------------------------------------------------
class DialGauge : public Gauge
{
  public:

    struct Presentation
    {
        bool                        showLabel;
        bool                        showUnits;
        bool                        showValue;
        unsigned                    wholeDigits;
        unsigned                    decimalDigits;

        float                       sweep;    // in degrees
        float                       topValue; // value at top of gauge, error if value isn't between min and max
        float                       tickSpacing;
        unsigned                    numMinorTicks;

        ScreenElement::VisualStyle *labelStyle;
        ScreenElement::VisualStyle *unitsStyle;
    };

    DialGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation);
   ~DialGauge();

    void SetVisualStyle(VisualStyle *style);

    void SetPresentation(DialGauge::Presentation *presentation);

    virtual void Draw(DisplayController *display);

  private:
    void RecalcSizes(void);

    void ValueToSinCos(float val, float &s, float &c);

    Presentation  mPresentation;

    char          mValueFormat[10];

    float         mValueToRads;
    float         mMinRangeRad;

    float         mCenterX;
    float         mCenterY;
    float         mSizeX;
    float         mSizeY;

    static Presentation sDefaultPresentation;
};

#endif //DIALGAUGE_H