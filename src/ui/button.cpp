#include "button.h"
#include "util/util.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define BN_FLAG_TOGGLE_STATE (SE_FLAG_USER + 0)


//-------------------------------------------------------------------
Button::Button(const char *text, uint16_t id, ScreenElement::VisualStyle *visualStyle) :
    ScreenElement(text, id, visualStyle)
{
    mWidth  = 0.0875;
    mHeight = 0.0875;
    mStyle  = Momentary;
    gDisplay->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, mName, mTextWidth, mTextHeight);
}


//-------------------------------------------------------------------
Button::~Button()
{

}

//-------------------------------------------------------------------
void Button::SetVisualStyle(VisualStyle *style)
{
    gDisplay->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, mName, mTextWidth, mTextHeight);
}

//-------------------------------------------------------------------
void Button::SetName(const char *name)
{
    ScreenElement::SetName(name);
    gDisplay->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, mName, mTextWidth, mTextHeight);
}

//-------------------------------------------------------------------
void Button::SetSizeToContent(float extraW, float extraH)
{
    gDisplay->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, mName, mTextWidth, mTextHeight);
    mWidth = (mVisualStyle.borderSize * 4) / gDisplay->DisplayWidth();
    if (mName != NULL) mWidth += mTextWidth;

    mHeight = mTextHeight + ((mVisualStyle.borderSize * 4) / gDisplay->DisplayHeight());
    mWidth  += extraW;
    mHeight += extraH;
}

//-------------------------------------------------------------------
void Button::SetStyle(Button::Style style)
{
    mStyle = style;
    ClearFlag(BN_FLAG_TOGGLE_STATE);
}

//-------------------------------------------------------------------
Button::State Button::GetState(void)
{
    if (TestFlag(BN_FLAG_TOGGLE_STATE)) return On;

    return Off;
}

//-------------------------------------------------------------------
void Button::SetState(Button::State state, bool postMessage)
{
    if (mStyle != Toggle) return;
    if (GetState() == state)  return;

    if (state == On) SetFlag(BN_FLAG_TOGGLE_STATE);
    else             ClearFlag(BN_FLAG_TOGGLE_STATE);

    if (postMessage)
    {
        UIEvent e;
        e.message = ButtonClicked;
        e.when    = SystemTimeUTC();
        e.bn.id   = mID;
        e.bn.newState = state;
        UI::QueueMessage(&e);
    }
}

//-------------------------------------------------------------------
void Button::Draw(DisplayController *display)
{
    float tx = mX + Center(mWidth, mTextWidth);
    float ty = mY + Center(mHeight, mTextHeight);

    if (GetState() == On)
    {
        DrawBorder(display, ScreenElement::FillSelectColor);
        display->DrawStringRect(mName, tx, ty, mWidth, mHeight, mVisualStyle.foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);
    }
    else
    {
        DrawBorder(display, ScreenElement::FillBackgroundColor);
        display->DrawStringRect(mName, tx, ty, mWidth, mHeight, mVisualStyle.foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);
    }
}

//-------------------------------------------------------------------
ScreenElement::ElementUIResult Button::HandleUIEvent(UIEvent *message)
{
    if (message == NULL) return Yep;

    UIEvent e;
    e.when    = message->when;
    e.bn.id   = mID;

    if ((message->message == Click) || (message->message == DoubleClick))
    {
        if (message->message == DoubleClick) e.message = ButtonDoubleClicked;
        else                                 e.message = ButtonClicked;
        e.bn.newState = 1;

        if (mStyle == Toggle)
        {
            if (TestFlag(BN_FLAG_TOGGLE_STATE)) ClearFlag(BN_FLAG_TOGGLE_STATE);
            else                                SetFlag(BN_FLAG_TOGGLE_STATE);

            e.bn.newState = (TestFlag(BN_FLAG_TOGGLE_STATE)) ? On : Off;
        }
        UI::QueueMessage(&e);
    }
    else if (message->message == Holding)
    {
        e.message = ButtonHolding;
        e.bn.newState = 1;
        UI::QueueMessage(&e);
    }
    else if (message->message == HoldDone)
    {
        e.message = ButtonHoldDone;
        e.bn.newState = 0;
        UI::QueueMessage(&e);
    }

    return Yep;
}


