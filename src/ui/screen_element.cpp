#include "ui/screen_element.h"
#include "display.h"

#include <stddef.h>
#include <string.h>
#include <malloc.h>

ScreenElement::VisualStyle ScreenElement::sDefaultVisualStyle = {   YELLOW,         // FG Color
                                                                    BLACK,          // BG Color
                                                                    GREY,           // Border Color
                                                                    PURPLE,         // Select Color
                                                                    Font_Regular,   // PixelFont style
                                                                    Font_Mono,      // Default font
                                                                    2,              // Border Size
                                                                    0.003F          // Corner Radius
                                                                };

//---------------------------------------------------------
ScreenElement::ScreenElement(const char *name, uint16_t id, VisualStyle *visualStyle)
{
    mX = 0;
    mY = 0;
    mWidth  = 0;
    mHeight = 0;

    mID = id;
    SetVisualStyle(visualStyle);
    mName = strdup(name);

    mFlags = 0;
}

//---------------------------------------------------------
ScreenElement::~ScreenElement()
{
    if (mName != NULL) free(mName);
    mName = NULL;
}

//-------------------------------------------------------------------
void ScreenElement::SetName(const char *name)
{
    if (name == NULL) return;

    if (mName != NULL) free(mName);
    mName = strdup(name);
}

//---------------------------------------------------------
void ScreenElement::SetPosition(float x, float y)
{
    mX = x;
    mY = y;
}

//---------------------------------------------------------
void ScreenElement::SetSize(float width, float height)
{
    mWidth  = width;
    mHeight = height;
}

//---------------------------------------------------------
bool ScreenElement::Intersects(UIEvent *event, UIEvent *windowSpace)
{
    if (event == NULL) return 0;

    if ((event->message != Click) &&
        (event->message != DoubleClick) &&
        (event->message != Dragging) &&
        (event->message != DragDone) &&
        (event->message != Holding) &&
        (event->message != HoldDone))
    {
        return 0;
    }

    float xDist = event->click.x - mX;
    float yDist = event->click.y - mY;

    // We only want an intersect on dragging when the START is inside the element
    // Since the dragStart X and Y are aliased to the Click X & Y, just test against Click's pos
    if (xDist > mWidth)  return false;
    if (yDist > mHeight) return false;
    if (xDist < 0.0F)    return false;
    if (yDist < 0.0F)    return false;

    if (windowSpace != NULL)
    {
         *windowSpace = *event;
         // put a copy of the message in to window local space
         switch (windowSpace->message)
         {
             case DoubleClick:
             case Dragging:
             case DragDone:
                 windowSpace->drag.end_x -= mX;
                 windowSpace->drag.end_y -= mY;
                 // No break;
             case Holding:
             case HoldDone:
             case Click:
                 windowSpace->click.x -= mX;
                 windowSpace->click.y -= mY;
                 break;
             default:
                 break;
         }
    }

    return true;
}

//---------------------------------------------------------
void ScreenElement::DrawBorder(DisplayController *display, FillStyle fillStyle)
{
    // Clear the area if asked
    if      (fillStyle == FillBackgroundColor) display->RectFilled(mX, mY, mWidth, mHeight, mVisualStyle.backgroundColor);
    else if (fillStyle == FillSelectColor)     display->RectFilled(mX, mY, mWidth, mHeight, mVisualStyle.selectColor);

    // Draw the borders
    display->RectRounded(mX, mY, mWidth, mHeight, mVisualStyle.cornerRadius, mVisualStyle.borderSize, mVisualStyle.borderColor);
}

//---------------------------------------------------------
void ScreenElement::SetVisualStyle(VisualStyle *visualStyle)
{
    if (visualStyle == NULL) mVisualStyle = sDefaultVisualStyle;
    else                     mVisualStyle = *visualStyle;
}

//---------------------------------------------------------
void ScreenElement::SetForegroundColor(uint32_t color)
{
    mVisualStyle.foregroundColor = color;
}

//---------------------------------------------------------
void ScreenElement::SetBackgroundColor(uint32_t color)
{
    mVisualStyle.backgroundColor = color;
}
//---------------------------------------------------------
void ScreenElement::SetFlag(uint8_t flag)
{
    mFlags |= (1 << flag);
}

//---------------------------------------------------------
void ScreenElement::ClearFlag(uint8_t flag)
{
    mFlags &= ~(1 << flag);
}

//---------------------------------------------------------
bool ScreenElement::TestFlag(uint8_t flag) const
{
    if ((mFlags & (1 << flag)) != 0) return true;
    return false;
}

//---------------------------------------------------------
bool  ScreenElement::TestAndClearFlag(uint8_t flag)
{
    uint16_t test = mFlags & (1 << flag);
    mFlags &= ~(1 << flag);

    if (test != 0) return true;
    return false;
}

