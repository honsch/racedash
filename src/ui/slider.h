#ifndef SLIDER_H
#define SLIDER_H

#include <stdint.h>
#include "ui/screen.h"
#include "ui/screen_element.h"

//-----------------------------------------
class Slider : public ScreenElement
{
  public:
   enum SliderType { Vertical, Horizontal };

   Slider(const char *name, uint16_t id, ScreenElement::VisualStyle *visualStyle, SliderType type);
   virtual ~Slider();

   virtual void SetPosition(float x, float y);
   virtual void SetSize(float width, float height);

   void SetRange(float topLeft, float bottomRight);
   void GetRange(float &topLeft, float &sbottomRight);

   void  SetStepSize(float stepSize);
   void  SetValue(float value);
   float GetValue(void);

   virtual ElementUIResult HandleUIEvent(UIEvent *message);
   virtual void            Draw(DisplayController *display);

  private:
   SliderType    mType;
   float         mTopLeft;
   float         mBottomRight;
   float         mValue;
   float         mStepSize;
   float         mThumbSize;
   float         mThumbPos;

    void UpdateWindowSettings(void);
};


#endif // SLIDER_H
