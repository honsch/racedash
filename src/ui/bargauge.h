#ifndef BARGAUGE_H
#define BARGAUGE_H

#include "gauge.h"

//------------------------------------------------------
class BarGauge : public Gauge
{
  public:

    // Direction for INCREASING value
    enum Orientation { Left, Right, Up, Down };


    enum TickOrientation { LeftTop, RightBottom };

    struct Presentation
    {
        bool                        showLabel;
        bool                        showUnits;
        bool                        showValue;
        unsigned                    wholeDigits;
        unsigned                    decimalDigits;
        Orientation                 orientation;

        float                       tickSpacing;
        unsigned                    numMinorTicks;
        unsigned                    numSubTicks;
        TickOrientation             tickOrientation;
        bool                        showTickValue;

        ScreenElement::VisualStyle *labelStyle;
        ScreenElement::VisualStyle *unitsStyle;
    };

    BarGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation);
   ~BarGauge();

    void SetVisualStyle(VisualStyle *style);

    void SetPresentation(BarGauge::Presentation *presentation);

    virtual void Draw(DisplayController *display);

  private:
    void RecalcSizes(void);

    float ValueToOffset(float val);
    void CacheTicks(void);

    Presentation  mPresentation;

    char          mValueFormat[10];

    float         mValueToOffset;
    float         mMinRangeRad;

    float         mOrigin;
    float         mSize;

    struct TickValue
    {
        float x;
        float y;
        double value;
    };

    unsigned                     mTickCount;
    DisplayController::LineData *mTicks;
    TickValue                   *mTickValues;

    static Presentation sDefaultPresentation;
};

#endif //BARGAUGE_H