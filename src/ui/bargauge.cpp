#include "bargauge.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

BarGauge::Presentation BarGauge::sDefaultPresentation =
{
    false,  // showLabel
    false,  // showUnits
    false,  // showValue
    3,      // wholeDigits
    0,      // decimalDigits
    Right,  // Orientation
    1.0F,   // tickSpacing
    0,      // numMinorTicks
    0,      // numSubTicks
    RightBottom, // tickOrientation
    false,       // showTickValue
    NULL,   // labelStyle
    NULL    // unitsStyle
 };


//------------------------------------------------------------------------------------------------------
BarGauge::BarGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation) :
    Gauge(variableName, units, id, visualStyle),
    mTickCount(0),
    mTicks(NULL)
{
    SetPresentation(presentation);
}

//------------------------------------------------------------------------------------------------------
BarGauge::~BarGauge()
{
    if (mLabel != NULL) free(mLabel);
}

//------------------------------------------------------------------------------------------------------
void BarGauge::SetVisualStyle(VisualStyle *style)
{
    if (style == NULL) mVisualStyle = sDefaultVisualStyle;
    else               mVisualStyle = *style;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void BarGauge::SetPresentation(BarGauge::Presentation *presentation)
{
    if (presentation == NULL) memcpy(&mPresentation, &sDefaultPresentation, sizeof(mPresentation));
    else                      memcpy(&mPresentation,  presentation,         sizeof(mPresentation));

    if (mPresentation.labelStyle == NULL) mPresentation.labelStyle = &mVisualStyle;
    if (mPresentation.unitsStyle == NULL) mPresentation.unitsStyle = &mVisualStyle;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void BarGauge::RecalcSizes(void)
{
    // Make a new format string
    mValueFormat[0] = '%';
    mValueFormat[1] = 0;

    if (mPresentation.wholeDigits != 0)
    {
        sprintf(mValueFormat + 1, "%d", mPresentation.wholeDigits);
    }

    unsigned len = strlen(mValueFormat);
    sprintf(mValueFormat + len, ".%df", mPresentation.decimalDigits);

    switch (mPresentation.orientation)
    {
        case Left:
            mOrigin =  mX + mWidth;
            mSize   = -mWidth;
            break;

        case Right:
            mOrigin = mX;
            mSize   = mWidth;
            break;

        case Up:
            mOrigin =  mY + mHeight;
            mSize   = -mHeight;
            break;

        case Down:
            mOrigin = mY;
            mSize   = mHeight;
            break;
    }

    float range = mMaxRange - mMinRange;
    mValueToOffset = mSize / range;

    CacheTicks();
}

//------------------------------------------------------------------------------------------------------
void BarGauge::CacheTicks(void)
{
    // recompute ticks
    if (mPresentation.tickSpacing <= 0.0)
    {
        mTickCount = 0;
        if (mTicks != NULL) delete[] mTicks;
        if (mTickValues != NULL) delete[] mTickValues;
        mTicks = NULL;
        mTickValues = NULL;
        return;
    }

    unsigned tickCount = ((mMaxRange - mMinRange) / mPresentation.tickSpacing) + 1;
    if (tickCount != mTickCount)
    {
        if (mTicks != NULL) delete[] mTicks;
        if (mTickValues != NULL) delete[] mTickValues;
        mTicks = new DisplayController::LineData[tickCount];
        mTickValues = new TickValue[tickCount];
    }
    mTickCount = tickCount;
    unsigned minorCycle = (mPresentation.numSubTicks + 1);
    unsigned majorCycle = minorCycle * (mPresentation.numMinorTicks + 1);


    unsigned tick = 0;
    float val = mMinRange;
    while(val <= mMaxRange)
    {
        bool drawNumber = false;
        float tickScale = 0.125F;
        if ((tick % majorCycle) == 0)
        {
            tickScale = 0.40F;
            drawNumber = true;
        }
        else if ((tick % minorCycle) == 0)
        {
            tickScale = 0.25F;
            //drawNumber = true;
        }
        float x1=0, y1=0, x2=0, y2=0;
        float offset = ValueToOffset(val);
        switch (mPresentation.orientation)
        {
            case Left:
            case Right:
                x1 = mOrigin + offset;
                x2 = x1;
                if (mPresentation.tickOrientation == LeftTop)
                {
                    y1 = mY;
                    y2 = y1 + mHeight * tickScale;
                }
                else // BottomRight
                {
                    y1 = mY + mHeight;
                    y2 = y1 - mHeight * tickScale;
                }
                break;

            case Up:
            case Down:
                y1 = mOrigin + offset;
                y2 = y1;
                if (mPresentation.tickOrientation == LeftTop)
                {
                    x1 = mX;
                    x2 = x1 + mWidth * tickScale;
                }
                else // BottomRight
                {
                    x1 = mX + mWidth;
                    x2 = x1 - mWidth * tickScale;
                }

                break;
        }
        mTicks[tick].x1 = x1;
        mTicks[tick].y1 = y1;
        mTicks[tick].x2 = x2;
        mTicks[tick].y2 = y2;

        if (drawNumber)
        {
            mTickValues[tick].value = val;
            char s[30];
            FormatValue(s, val, mPresentation.wholeDigits, mPresentation.decimalDigits);
            float width, height;
            gDisplay->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, s, width, height);
            switch (mPresentation.orientation)
            {
                case Left:
                case Right:
                    mTickValues[tick].x = x1 - (width / 2.0F);
                    if (mPresentation.tickOrientation == LeftTop)
                    {
                        mTickValues[tick].y = y1 - height;
                    }
                    else // BottomRight
                    {
                        mTickValues[tick].y = y1;
                    }
                    break;

                case Up:
                case Down:
                    mTickValues[tick].y = y1;
                    if (mPresentation.tickOrientation == LeftTop)
                    {
                        mTickValues[tick].x = x1 - (width / 2.0F);
                    }
                    else // BottomRight
                    {
                        mTickValues[tick].x = x1;
                    }
                    break;
            }
        }
        else
        {
            mTickValues[tick].x = -1.0F;
            mTickValues[tick].y = -1.0F;
            mTickValues[tick].value = 0.0;
        }

        ++tick;
        val += mPresentation.tickSpacing;
    }
}

//-------------------------------------------------------------------
float BarGauge::ValueToOffset(float val)
{
    return (val -= mMinRange) * mValueToOffset;
}


//-------------------------------------------------------------------
void BarGauge::Draw(DisplayController *display)
{
    // Draw units
    if (mPresentation.showTickValue)
    {
        unsigned tick;
        for (tick = 0; tick < mTickCount; tick++)
        {
            if (mTickValues[tick].x < 0.0F) continue;

            char s[30];
            FormatValue(s, mTickValues[tick].value, mPresentation.wholeDigits, mPresentation.decimalDigits);
            display->DrawString(s, mTickValues[tick].x, mTickValues[tick].y, mPresentation.labelStyle->foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);
        }
   }


    // draw bar
    float x=0, y=0, w=0, h=0;
    float offset = ValueToOffset(mCurrentValueClamped);
    switch (mPresentation.orientation)
    {
        case Left:
        case Right:
            x = mOrigin;
            y = mY;
            w = offset;
            h = mHeight;
            break;

        case Up:
        case Down:
            x = mX;
            y = mOrigin;
            w = mWidth;
            h = offset;
            break;
    }
    display->RectFilled(x, y, w, h, mVisualStyle.foregroundColor);

    // draw ticks
    display->Line(mTicks, mTickCount, mVisualStyle.borderSize, mVisualStyle.borderColor);

    // Draw border
    display->Rect(mX, mY, mWidth, mHeight, mVisualStyle.borderSize, mVisualStyle.borderColor);



#if 0
    if (mPresentation.showLabel)
    {
        display->DrawString(mLabel, mX + mLabelOffsetX, mY, mPresentation.labelStyle->foregroundColor, mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace);
    }

    char s[80];
    snprintf(s, 80, mValueFormat, mCurrentValueClamped);
    unsigned textWidth, textHeight;
    display->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, s, textWidth, textHeight);
    unsigned offsetX = mValueOffsetX + (mValueSizeX - textWidth);
    //printf("OffsetX of %s %d  SizeX %d   textWidth %d\n", mName, offsetX, mValueSizeX, textWidth);

    display->DrawString(s, mX + offsetX, mY, mVisualStyle.foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);

    if (mPresentation.showUnits)
    {
        display->DrawString(mUnits, mX + mUnitsOffsetX, mY, mPresentation.unitsStyle->foregroundColor, mPresentation.unitsStyle->fontsize, mPresentation.unitsStyle->fontFace);
    }
#endif
}

