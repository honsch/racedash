#ifndef LISTBOX_H
#define LISTBOX_H

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "ui/screen.h"
#include "ui/screen_element.h"

#include <vector>
//-----------------------------------------
class Listbox : public ScreenElement
{
 public:
    Listbox(const char *name, uint16_t id, ScreenElement::VisualStyle *visualStyle, bool multiSelectEnable);
    virtual ~Listbox();

    virtual void SetPosition(float x, float y);
    virtual void SetSize(float width, float height);

    void     Clear(void);
    uint32_t AddString(const char *s, bool selected);
    uint32_t FindString(const char *s);
    void     RemoveString(const char *s);
    void     RemoveSelected(void);
    void     ClearSelection(void);

    void     Sort(void);

    uint32_t    ItemCount(void);
    bool        IsSelected(uint32_t index);
    const char *GetString(uint32_t index);

    uint32_t    NumSelected(void) const;
    uint32_t    SelectedIndex(uint32_t index) const;

    virtual ElementUIResult HandleUIEvent(UIEvent *message);
    virtual void            Draw(DisplayController *display);

    static const unsigned BAD_INDEX = 0xFFFFFFFFU;

 private:

    struct Item
    {
        Item(const char *_t, bool _s = false)
        {
            text = strdup(_t);
            isSelected = _s;
            toggleSelection = false;
        }
        ~Item()
        {
            free(text);
        }
        Item(const Item &src)
        {
            text = strdup(src.text);
            isSelected = src.isSelected;
            toggleSelection = src.toggleSelection;
        }

        Item &operator=(const Item &src)
        {
            text = strdup(src.text);
            isSelected = src.isSelected;
            toggleSelection = src.toggleSelection;
            return *this;
        }

        void Swap(Item &other)
        {
            char *tTX = text;
            bool tIS = isSelected;
            bool tTS = toggleSelection;
            text            = other.text;
            isSelected      = other.isSelected;
            toggleSelection = other.toggleSelection;
            other.text            = tTX;
            other.isSelected      = tIS;
            other.toggleSelection = tTS;
        }

        char *text;
        bool  isSelected;
        bool  toggleSelection;
    };

    std::vector<Item> mItems;
    uint32_t    mFirstDisplayItem;

    // Internal cached data for drawing
    float    mItemX;
    float    mItemWidth;
    float    mFirstItemY;
    unsigned mNumItemsVisible;
    unsigned mNumCharsVisible;
    float    mBorderExtra;

    void SelectItem(uint32_t index);
    void DeselectItem(uint32_t index);
    void UpdateWindowSettings(void);

    void SetToggleSelectionRange(unsigned first, unsigned last);
    void UpdateSelectionFromToggles(void);
    uint32_t ScreenCoordToIndex(float lx, float ly, bool clamp);

};


#endif //LISTBOX_H
