#ifndef BUTTON_H
#define BUTTON_H

#include <stdint.h>
#include "ui/screen.h"
#include "ui/screen_element.h"

class Button : public ScreenElement
{

 public:

   enum State { Off, On };
   enum Style { Momentary, Toggle };

   Button(const char *text, uint16_t id, ScreenElement::VisualStyle *visualStyle);
   virtual ~Button();

   void SetName(const char *name);
   void SetSizeToContent(float extraW, float extraH);

   void SetStyle(Button::Style style);

   void SetVisualStyle(VisualStyle *style);

   Button::State GetState(void);
   void          SetState(Button::State state, bool postMessage);

   virtual ElementUIResult HandleUIEvent(UIEvent *message);
   virtual void            Draw(DisplayController *display);

 private:

    Button::Style mStyle;
    float mTextWidth;
    float mTextHeight;
};

#endif //BUTTON_H
