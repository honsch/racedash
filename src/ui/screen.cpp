#include "ui/screen.h"

#include "screen_element.h"
#include <stddef.h>
#include <stdio.h>

//----------------------------------------------------
ScreenBase::ScreenBase(void)
{
}

//----------------------------------------------------
ScreenBase::~ScreenBase()
{
   // Should I delete all the elements?
   mElements.clear();
}

//----------------------------------------------------
void ScreenBase::Draw(DisplayController *display)
{
   unsigned a;
   for (a = 0; a < mElements.size(); a++)
   {
      mElements[a]->Draw(display);
   }
}

//----------------------------------------------------
void ScreenBase::ProcessElementUIEvent(UIEvent *e)
{
   unsigned a;
   for (a = 0; a < mElements.size(); a++)
   {
      UIEvent windowSpace;
      if (mElements[a]->Intersects(e, &windowSpace))
      {
         mElements[a]->HandleUIEvent(&windowSpace);
      }
   }
}

//----------------------------------------------------
void ScreenBase::AddElement(ScreenElement *e)
{
   if (e != NULL) mElements.push_back(e);
}

//----------------------------------------------------
void ScreenBase::RemoveElement(ScreenElement *e)
{
   if (e == NULL) return;
   auto it = mElements.begin();
   for (;it != mElements.end(); ++it)
   {
      if ((*it) == e)
      {
         mElements.erase(it);
         return;
      }
   }
}
