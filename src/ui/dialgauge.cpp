#include "dialgauge.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static const float HALF_PI = 3.141592654F / 2.0F;
DialGauge::Presentation DialGauge::sDefaultPresentation =
{
    false,  // showLabel
    false,  // showUnits
    false,  // showValue
    3,      // wholeDigits
    0,      // decimalDigits
    270.0F, // sweep in degrees
    0.5F,   // topValue  value at top of gauge, error if value isn't between min and max
    1.0F,   // tickSpacing
    0,      // numMinorTicks
    NULL,   // labelStyle
    NULL    // unitsStyle
 };


//------------------------------------------------------------------------------------------------------
DialGauge::DialGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation) :
    Gauge(variableName, units, id, visualStyle)
{
    SetPresentation(presentation);
}

//------------------------------------------------------------------------------------------------------
DialGauge::~DialGauge()
{
    if (mLabel != NULL) free(mLabel);
}

//------------------------------------------------------------------------------------------------------
void DialGauge::SetVisualStyle(VisualStyle *style)
{
    if (style == NULL) mVisualStyle = sDefaultVisualStyle;
    else               mVisualStyle = *style;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void DialGauge::SetPresentation(DialGauge::Presentation *presentation)
{
    if (presentation == NULL) memcpy(&mPresentation, &sDefaultPresentation, sizeof(mPresentation));
    else                      memcpy(&mPresentation,  presentation,         sizeof(mPresentation));

    if (mPresentation.labelStyle == NULL) mPresentation.labelStyle = &mVisualStyle;
    if (mPresentation.unitsStyle == NULL) mPresentation.unitsStyle = &mVisualStyle;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void DialGauge::RecalcSizes(void)
{
    // Make a new format string
    mValueFormat[0] = '%';
    mValueFormat[1] = 0;

    if (mPresentation.wholeDigits != 0)
    {
        sprintf(mValueFormat + 1, "%d", mPresentation.wholeDigits);
    }

    unsigned len = strlen(mValueFormat);
    sprintf(mValueFormat + len, ".%df", mPresentation.decimalDigits);

    float range = mMaxRange - mMinRange;
    float sweepRads = DegreeToRadian(mPresentation.sweep);
    mValueToRads = sweepRads / range;

    // The top of the gauge is PI/2 radians, find where the min value should be
    mMinRangeRad = HALF_PI + ((mPresentation.topValue - mMinRange) * mValueToRads);

    mSizeX  = mWidth / 2;
    mSizeY  = mHeight / 2;
    mCenterX = mX + mSizeX;
    mCenterY = mY + mSizeY;
}

//-------------------------------------------------------------------
void DialGauge::ValueToSinCos(float val, float &s, float &c)
{
    val -= mMinRange;
    float rads = mMinRangeRad - (val * mValueToRads); // we're going clockwise for larger values, that's negative rad direction

    s = -sinf(rads);
    c = cosf(rads);
}


//-------------------------------------------------------------------
void DialGauge::Draw(DisplayController *display)
{
    // Draw units
    if (mPresentation.showUnits)
    {

    }


    // draw ticks
    if (mPresentation.tickSpacing > 0.0)
    {
        unsigned tickCycle = 1 + mPresentation.numMinorTicks;
        unsigned tick = 0;
        float val = mMinRange;
        while(val <= mMaxRange)
        {
            bool drawNumber = false;
            float tickScale = 0.95F;
            if ((tick % tickCycle) == 0)
            {
                tickScale = 0.90F;
                drawNumber = true;
            }

            float s, c;
            ValueToSinCos(val, s, c);
            float x1 = mCenterX + (mSizeX * c);
            float y1 = mCenterY + (mSizeY * s);
            float x2 = mCenterX + (mSizeX * c * tickScale);
            float y2 = mCenterY + (mSizeY * s * tickScale);
            display->Line(x1, y1, x2, y2, 1, mVisualStyle.foregroundColor);

            if (drawNumber)
            {
                char ts[40];
                sprintf(ts, "%.1f", val);
                float textHeight, textWidth;
                display->GetStringExtents(mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace, ts, textWidth, textHeight);
                if (c > 0.0F) x2 -= textWidth;
                if (s > 0.0F) y2 -= textHeight;
                display->DrawString(ts, x2, y2, mPresentation.labelStyle->foregroundColor, mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace);
            }
            ++tick;
            val += mPresentation.tickSpacing;
        }
    }

    display->Ellipse(mCenterX, mCenterY, mSizeX, mSizeY, 1, mVisualStyle.foregroundColor);

    float s, c;
    ValueToSinCos(mCurrentValueClamped, s, c);
    float x2 = mCenterX + (mSizeX * c);
    float y2 = mCenterY + (mSizeY * s);
    display->Line(mCenterX, mCenterY, x2, y2, 1, mVisualStyle.foregroundColor);



#if 0
    if (mPresentation.showLabel)
    {
        display->DrawString(mLabel, mX + mLabelOffsetX, mY, mPresentation.labelStyle->foregroundColor, mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace);
    }

    char s[80];
    snprintf(s, 80, mValueFormat, mCurrentValueClamped);
    unsigned textWidth, textHeight;
    display->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, s, textWidth, textHeight);
    unsigned offsetX = mValueOffsetX + (mValueSizeX - textWidth);
    //printf("OffsetX of %s %d  SizeX %d   textWidth %d\n", mName, offsetX, mValueSizeX, textWidth);

    display->DrawString(s, mX + offsetX, mY, mVisualStyle.foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);

    if (mPresentation.showUnits)
    {
        display->DrawString(mUnits, mX + mUnitsOffsetX, mY, mPresentation.unitsStyle->foregroundColor, mPresentation.unitsStyle->fontsize, mPresentation.unitsStyle->fontFace);
    }
#endif
}

