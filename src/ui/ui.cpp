#include "ui/ui.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "touchscreen.h"
#include "ui/screen.h"
#include "display.h"


#define DRAG_MIN            0.03125F // In % of screen
#define DOUBLE_CLICK_TIME   0.3      // In seconds

UI::TSEvent UI::mEvents[4];
Touchscreen *UI::mTouchscreen;
Mouse *UI::mMouse;
bool UI::mShowTouch;

UI::HoldState UI::mHoldState;

std::vector<UIEvent> UI::mMessageQueue;
std::vector<ScreenBase *> UI::mScreenStack;

//---------------------------------------------------------------------------
void PrintUIEvent(const UIEvent *e)
{
    printf("UI Evevt at %.3f\n", e->when);
    switch(e->message)
    {
        case NoEvent:
            printf("\tNo Event\n");
            break;

        case Click:
            printf("\tClick         %7.4f x %7.4f\n", e->click.x, e->click.y);
            break;

        case DoubleClick:
            printf("\tDoubleClick   %7.4fx%7.4f\n", e->click.x, e->click.y);
            break;

        case Dragging:
            printf("\tDragging      %7.4fx%7.4f - %7.4fx%7.4f\n", e->drag.start_x, e->drag.start_y, e->drag.end_x, e->drag.end_y);
            break;

        case DragDone:
            printf("\tDragDone      %7.4fx%7.4f - %7.4fx%7.4f\n", e->drag.start_x, e->drag.start_y, e->drag.end_x, e->drag.end_y);
            break;

        case Holding:
            printf("\tHolding       %7.4fx%7.4f\n", e->click.x, e->click.y);
            break;

        case HoldDone:
            printf("\tHoldDone      %7.4fx%7.4f\n", e->click.x, e->click.y);
            break;

        case Key:
            printf("\tKey            '%c' (%d)\n", e->kb.key, (int) e->kb.key);
            break;

        case ButtonClicked:
            printf("\tButtonClicked  ID:%4d  State:%d\n", (int)e->bn.id, (int)e->bn.newState);
            break;

        case ButtonDoubleClicked:
            printf("\tButtonDoubleClicked ID:%4d  State:%d\n", (int)e->bn.id, (int)e->bn.newState);
            break;

        case ButtonHolding:
            printf("\tButtonHoldind  ID:%4d  State:%d\n", (int)e->bn.id, (int)e->bn.newState);
            break;

        case ButtonHoldDone:
            printf("\tButtonHoldDone ID:%4d  State:%d\n", (int)e->bn.id, (int)e->bn.newState);
            break;

        case ListboxChanged:
            printf("\tListboxChanged ID:%4d  Index:%4d  State:%d\n", (int)e->lb.id, (int)e->lb.itemIndex, (int)e->lb.newState);
            break;

        case GettextResult:
            printf("\tGettextResult  ID:%4d  '%s'\n", (int)e->gt.id, e->gt.text);
            break;

        case SliderChanged:
            printf("\tSliderChanged  ID:%4d  Val:%f\n", (int)e->sl.id, e->sl.newValue);
            break;

        case GaugeClicked:
            printf("\tGauge  ID:%4d\n", (int)e->gt.id);
            break;
    }
}

//---------------------------------------------------------------------------
void UI::Init(Touchscreen *ts, Mouse *mouse)
{
    mTouchscreen = ts;
    mTouchscreen->RegisterCallback(&UI::HandleInputEvent, NULL);

    mMouse = mouse;
    mMouse->RegisterCallback(&UI::HandleInputEvent, NULL);

    mMessageQueue.reserve(32);
}

//---------------------------------------------------------------------------
void UI::Shutdown(void)
{
    mTouchscreen->RegisterCallback(NULL, NULL);
    mMessageQueue.clear();
}

//---------------------------------------------------------------------------
void UI::QueueMessage(UIEvent *message)
{
    if (message == NULL) return;

    //PrintUIEvent(message);

    mMessageQueue.push_back(*message);
}

//---------------------------------------------------------
int UI::SetScreen(ScreenBase *screen, bool clearStack)
{
    if (screen == NULL) return -1;

    if (clearStack != 0)
    {
        mScreenStack.clear();
        mScreenStack.push_back(screen);
    }
    else
    {
        mScreenStack.back() = screen;
   }

    return 0;
}

//---------------------------------------------------------
int UI::PushScreen(ScreenBase *screen)
{
    if (screen == NULL) return -1;
    mScreenStack.push_back(screen);

    return 0;
}

//---------------------------------------------------------
int UI::PopScreen(void)
{
    if (mScreenStack.size() < 2)
    {
        // I should put something here for a 'default' top level screen
        return -1;
    }

    delete mScreenStack.back();
    mScreenStack.pop_back();
    return 0;
}


//---------------------------------------------------------------------------
int UI::Update(DisplayController *display)
{
    mTouchscreen->Update();
    mMouse->Update();

    int err;
    ScreenBase::ScreenResult res = ScreenBase::ScreenOk;
    do
    {
        err = -1;
        ScreenBase *screen = mScreenStack.back();
        if (screen != NULL)
        {
            err = 0;
            res = screen->Update();
            switch (res)
            {
                case ScreenBase::ScreenOk:
                    screen->ScreenBase::Draw(display);
                    screen->Draw(display);
                    break;

                case ScreenBase::ScreenPop:
                    PopScreen();
                    break;
            }
        }
    } while (res == ScreenBase::ScreenPop);

    if (mMouse->Ready())
    {
        InputEvent ie;
        mMouse->LastReport(ie);
        //display->Circle(ie.screenX, ie.screenY, 0.002, 2, WHITE);
        display->DrawString("8", ie.screenX, ie.screenY, WHITE, 48, Font_Cursors);
    }

    if (mShowTouch)
    {
        InputEvent ie;
        mTouchscreen->LastReport(ie);
        display->Circle(ie.screenX, ie.screenY, 0.002, 2, WHITE);
        display->Circle(ie.screenX, ie.screenY, 0.020, 2, WHITE);
    }

    return err;
}

//---------------------------------------------------------------------------
void UI::HandleInputEvent(void *context, InputEvent *event)
{
    static float prevX = 0;
    static float prevY = 0;
    static bool  prevZ = false;

    UIEvent message;

    // press state changed
    if (prevZ != event->pressed)
    {
        // Make room for new event
        mEvents[3] = mEvents[2];
        mEvents[2] = mEvents[1];
        mEvents[1] = mEvents[0];

        mEvents[0].time = event->time;
        // If press then coords are valid
        if (event->pressed)
        {
            mEvents[0].event = Press;
            mEvents[0].x = event->screenX;
            mEvents[0].y = event->screenY;
        }
        else
        {
            mEvents[0].event = Release;
            mEvents[0].x = event->screenX;
            mEvents[0].y = event->screenY;
        }
        ParseEvents();
    }
    // Check for Dragging
    else if ((event->pressed) &&
             (mEvents[0].event == Press) &&
             ((event->time - mEvents[0].time) > DOUBLE_CLICK_TIME))
    {
        float xd = fabs(event->screenX - mEvents[0].x);
        float yd = fabs(event->screenY - mEvents[0].y);

        if ((mHoldState == HS_Dragging) || (xd > DRAG_MIN) || (yd > DRAG_MIN))
        {
            if (mHoldState == HS_Holding)
            {
                message.message      = HoldDone;
                message.when         = mEvents[0].time;
                message.click.x      = mEvents[0].x;
                message.click.y      = mEvents[0].y;
                QueueMessage(&message);
            }

            mHoldState = HS_Dragging;

            message.message      = Dragging;
            message.when         = mEvents[0].time;
            message.drag.start_x = mEvents[0].x;
            message.drag.start_y = mEvents[0].y;
            message.drag.end_x   = prevX;
            message.drag.end_y   = prevY;

            QueueMessage(&message);
        }
        else if (mHoldState == HS_None)
        {
            mHoldState = HS_Holding;
            message.message      = Holding;
            message.when         = mEvents[0].time;
            message.click.x      = mEvents[0].x;
            message.click.y      = mEvents[0].y;
            QueueMessage(&message);
        }
    }

    prevX = event->screenX;
    prevY = event->screenY;
    prevZ = event->pressed;

    ScreenBase *screen = mScreenStack.back();
    if (screen != NULL)
    {
        unsigned a;
        for (a = 0; a < mMessageQueue.size(); a++)
        {
            screen->ProcessElementUIEvent(&mMessageQueue[a]);
            ScreenBase::ScreenResult res = screen->ProcessUIEvent(&mMessageQueue[a]);
            if (res == ScreenBase::ScreenPop)
            {
                PopScreen();
                screen = mScreenStack.back();
            }
        }
    }

    mMessageQueue.clear();
    mMessageQueue.reserve(32);
}

//---------------------------------------------------------------------------
void UI::ParseEvents(void)
{
    UIEvent message;
    if (mEvents[0].event == Release)
    {
        // Detect double click
        if ((mEvents[3].event == Press) &&
            ((mEvents[1].time - mEvents[3].time) <= DOUBLE_CLICK_TIME))
        {
            message.message = DoubleClick;
            message.when    = mEvents[3].time;
            message.click.x = mEvents[3].x;
            message.click.y = mEvents[3].y;
            QueueMessage(&message);

            // reset the time on the most recent Press event so that
            // we actually need two clicks for another double click
            mEvents[1].time = 0;
        }
        // Detect drag
        else if (mHoldState == HS_Dragging)
        {
            message.message      = DragDone;
            message.when         = mEvents[1].time;
            message.drag.start_x = mEvents[1].x;
            message.drag.start_y = mEvents[1].y;
            message.drag.end_x   = mEvents[0].x;
            message.drag.end_y   = mEvents[0].y;

            QueueMessage(&message);
        }
        else if (mHoldState == HS_Holding)
        {
            message.message      = HoldDone;
            message.when         = mEvents[0].time;
            message.click.x      = mEvents[0].x;
            message.click.y      = mEvents[0].y;
            QueueMessage(&message);
        }
        // If it's not a DoubleClick or a drag, it must be a click
        else
        {
            message.message = Click;
            message.when    = mEvents[1].time;
            message.click.x = mEvents[1].x;
            message.click.y = mEvents[1].y;
            QueueMessage(&message);
        }
        mHoldState = HS_None;
    }
}

