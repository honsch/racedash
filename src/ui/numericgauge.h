#ifndef NUMERICGAUGE_H
#define NUMERICGAUGE_H

#include "gauge.h"

//------------------------------------------------------
// Base class for all of the gauges

class NumericGauge : public Gauge
{
  public:

    struct Presentation
    {
        enum LablePos { LP_None, LP_Left, LP_Right, LP_Top, LP_Bottom } labelPos;
        enum Justify { LJ_LeftTop, LJ_Center, LJ_RightBottom } labelJustify;
        bool                        showUnits;
        float                       nameAlignLength;
        unsigned                    wholeDigits;
        unsigned                    decimalDigits;
        ScreenElement::VisualStyle *labelStyle;
        ScreenElement::VisualStyle *unitsStyle;
    };

    NumericGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation);
   ~NumericGauge();

    void SetVisualStyle(VisualStyle *style);

    void SetPresentation(NumericGauge::Presentation *presentation);

    virtual void Draw(DisplayController *display);

  private:
    void RecalcSizes(void);
    Presentation  mPresentation;

    char       mValueFormat[10];
    float      mLabelPosX;
    float      mLabelPosY;
    float      mValuePosX;
    float      mValuePosY;
    float      mUnitsPosX;
    float      mUnitsPosY;
    float      mValueWidth;

    static Presentation sDefaultPresentation;
};

#endif //NUMERICGAUGE_H