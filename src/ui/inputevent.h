#ifndef INPUTEVENT_H
#define INPUTEVENT_H

struct InputEvent
{
    double   time;
    bool     pressed;
    float    screenX;
    float    screenY;
    unsigned rawX;
    unsigned rawY;
};

typedef void (*InputEventCallback)(void *context, InputEvent *event);

#endif // INPUTEVENT_H