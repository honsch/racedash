#ifndef GAUGE_H
#define GAUGE_H
#include "screen_element.h"
#include "sensors/sensormanager.h"

#define GAUGE_FLAG_HOLD (SE_FLAG_USER + 0)

class Gauge : public ScreenElement
{
  public:
    Gauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle);
   ~Gauge();

    virtual void SetSize(float width, float height);
    virtual void SetRange(double minimum, double maximum);
    virtual void SetVisualStyle(VisualStyle *style);
    virtual void SetLabel(const char *label);

    virtual ElementUIResult HandleUIEvent(UIEvent *message);
    virtual void            Draw(DisplayController *display) = 0;

    virtual double GetClampedValue(void) { return mCurrentValueClamped; }

    enum Orientation { Horizontal, Vertical };

    //----------------------------------------------
    // All the gauges need to do this, might as well make it a function
    void UpdateValue(void);

 protected:

    virtual void RecalcSizes(void) { ; }

    void FormatValue(char *out, double value, unsigned wholeDigits, unsigned decimalDigits);

    // returns the maximum number of digits (plus sign!) to display the whole number portion of the clamp range
    unsigned ClampDigits(void);

    //----------------------------------------------
    inline void ClearValueLimits(void)
    {
        mMaxValue = mMinRange;
        mMinValue = mMaxRange;
    }

    //----------------------------------------------
    inline void UpdateValueLimits(double newValue)
    {
        if      (newValue > mMaxValue) mMaxValue = newValue;
        else if (newValue < mMinValue) mMinValue = newValue;
    }

    //----------------------------------------------
    inline double ClampValue(double newValue)
    {
        if      (newValue > mMaxValue) return mMaxRange;
        else if (newValue < mMinValue) return mMinRange;
        return newValue;
    }

    uint32_t mNameHash;

    char *mUnits;
    char *mLabel;

    double mCurrentValueRaw;
    double mCurrentValueClamped;

    double mMinRange;
    double mMaxRange;

    double mMaxValue;
    double mMinValue;

   static VisualStyle sDefaultVisualStyle;
};


#endif // GAUGE_H