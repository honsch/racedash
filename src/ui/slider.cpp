#include "slider.h"
#include "util/util.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define SL_THUMB_COLOUR mVisualStyle.foregroundColor


#define SL_FLAG_DRAGGING    (SE_FLAG_USER + 0)

//-------------------------------------------------------------------------
Slider::Slider(const char *name, uint16_t id, ScreenElement::VisualStyle *visualStyle, SliderType type) :
    ScreenElement(name, id, visualStyle),
    mType(type)
{
    if (mType == Vertical)
    {
        mWidth  = 0.0125F;
        mHeight = 0.25F;
    }
    else
    {
        mWidth  = 0.25F;
        mHeight = 0.025F;
    }

    mValue       = 0.0F;
    mTopLeft     = 1.0F;
    mBottomRight = 0.0F;
    mStepSize    = 0.1F;

    UpdateWindowSettings();
}

//-------------------------------------------------------------------------
Slider::~Slider()
{
}

//-------------------------------------------------------------------------
void Slider::SetPosition(float x, float y)
{
    ScreenElement::SetPosition(x, y);
    UpdateWindowSettings();
}

//-------------------------------------------------------------------------
void Slider::SetSize(float width, float height)
{
    ScreenElement::SetSize(width, height);
    UpdateWindowSettings();
}

//-------------------------------------------------------------------------
void Slider::SetRange(float topLeft, float bottomRight)
{
    mTopLeft     = topLeft;
    mBottomRight = bottomRight;
    UpdateWindowSettings();
}

//-------------------------------------------------------------------------
void Slider::GetRange(float &topLeft, float &bottomRight)
{
    topLeft     = mTopLeft;
    bottomRight = mBottomRight;
}

//-------------------------------------------------------------------------
void Slider::SetStepSize(float stepSize)
{
    mStepSize = fabs(stepSize);
    UpdateWindowSettings();
}

//-------------------------------------------------------------------------
void Slider::SetValue(float value)
{
    if (mTopLeft < mBottomRight)
    {
        if      (value < mTopLeft)     value = mTopLeft;
        else if (value > mBottomRight) value = mBottomRight;
    }
    else
    {
        if      (value > mTopLeft)     value = mTopLeft;
        else if (value < mBottomRight) value = mBottomRight;
    }

    if (value != mValue)
    {
        mValue = value;
        UpdateWindowSettings();

        UIEvent event;
        event.message     = SliderChanged;
        event.when        = SystemTimeUTC();
        event.sl.id       = mID;
        event.sl.newValue = mValue;
        UI::QueueMessage(&event);
    }
}

//-------------------------------------------------------------------------
float Slider::GetValue(void)
{
    return mValue;
}

//-------------------------------------------------------------------------
void Slider::UpdateWindowSettings(void)
{
    float textWidth, textHeight;
    gDisplay->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);

    float ratio = (mValue - mTopLeft) / (mBottomRight - mTopLeft);

    if (mType == Vertical)
    {
        mThumbSize = mWidth;
        mThumbPos = mY + (ratio * (mHeight - mThumbSize));
    }
    else
    {
        mThumbSize = mHeight;
        mThumbPos = mX + (ratio * (mWidth - mThumbSize));
    }
}


//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
ScreenElement::ElementUIResult Slider::HandleUIEvent(UIEvent *message)
{
    if (message == NULL) return Yep;

    if (mType == Vertical)
    {
        if ((message->message == Click) || (message->message == DoubleClick))
        {
            float step = mStepSize;
            if (mTopLeft < mBottomRight) step = -step;

            float thumbCenter = (mThumbPos - mY) + (mThumbSize / 2.0F);
            if (message->click.y < thumbCenter) SetValue(mValue + step);
            else                                SetValue(mValue - step);
        }

        else if ((message->message == Dragging) || (message->message == DragDone))
        {
            float top = mY + (mThumbSize / 2.0F);
            float bottom = top + mHeight - mThumbSize;

            float ratio = (message->drag.end_y) / (bottom - top);
            float value = mTopLeft + ratio * (mBottomRight - mTopLeft);
            SetValue(value);
            return Yep;
        }
    }
    else // mType == SL_Horizontal
    {
        if ((message->message == Click) || (message->message == DoubleClick))
        {
            float step = mStepSize;
            if (mTopLeft < mBottomRight) step = -step;

            float thumbCenter = (mThumbPos - mX) + (mThumbSize / 2.0F);
            if (message->click.x < thumbCenter) SetValue(mValue + mStepSize);
            else                                SetValue(mValue - mStepSize);
        }

        else if ((message->message == Dragging) || (message->message == DragDone))
        {
            float left = mX + (mThumbSize / 2.0F);
            float right = left + mWidth - mThumbSize;

            float ratio = message->drag.end_x / (right - left);
            float value = mTopLeft + ratio * (mBottomRight - mTopLeft);
            SetValue(value);
            return Yep;
        }
    }

    return Yep;
}

//-------------------------------------------------------------------------
void Slider::Draw(DisplayController *display)
{
    float textWidth, textHeight;
    display->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);

    DrawBorder(display, ScreenElement::FillBackgroundColor);

    if (mType == Vertical)
    {
        display->RectRoundedFilled(mX, mThumbPos, mWidth, mThumbSize, mVisualStyle.cornerRadius, mVisualStyle.borderColor);
        float y = mThumbPos + mThumbSize / 4.0F;
        unsigned a;
        for (a = 0; a < 3; a++)
        {
            display->Line(mX + mWidth / 4.0F, y, mWidth / 2.0F, y, 1.0F, mVisualStyle.backgroundColor);
            y += mThumbSize / 4.0F;
        }
    }
    else
    {
        display->RectRoundedFilled(mThumbPos, mY, mThumbSize, mHeight, mVisualStyle.cornerRadius, mVisualStyle.borderColor);

        float x = mThumbPos + mThumbSize / 4.0F;
        unsigned a;
        for (a = 0; a < 3; a++)
        {
            display->Line(x, mY + mHeight / 4.0F, x, mHeight / 2.0F, 1.0F, mVisualStyle.backgroundColor);
            x += mThumbSize / 4.0F;
        }
    }
}



