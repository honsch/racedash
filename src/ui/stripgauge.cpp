#include "stripgauge.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static const float HALF_PI = 3.141592654F / 2.0F;
StripGauge::Presentation StripGauge::sDefaultPresentation =
{
    false,  // showLabel
    false,  // showUnits
    false,  // showValue
    3,      // wholeDigits
    0,      // decimalDigits
    0,      // tickSpacing
    NULL,   // labelStyle
    NULL,   // unitsStyle
    { { 0, 0, 1, 0, 0, 0, NULL, 0, 0, 0 }, { 0, 1, 1, 1, 0, 0, NULL, 0, 0, 1 } } // outline
 };


//------------------------------------------------------------------------------------------------------
StripGauge::StripGauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle, Presentation *presentation) :
    Gauge(variableName, units, id, visualStyle)
{
    SetPresentation(presentation);
}

//------------------------------------------------------------------------------------------------------
StripGauge::~StripGauge()
{
    if (mLabel != NULL) free(mLabel);
}

//------------------------------------------------------------------------------------------------------
void StripGauge::SetRange(double minimum, double maximum)
{
    Gauge::SetRange(minimum, maximum);
    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void StripGauge::SetVisualStyle(VisualStyle *style)
{
    if (style == NULL) mVisualStyle = sDefaultVisualStyle;
    else               mVisualStyle = *style;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void StripGauge::SetPresentation(StripGauge::Presentation *presentation)
{
    if (presentation == NULL) mPresentation = sDefaultPresentation;
    else                      mPresentation = *presentation;

    if (mPresentation.labelStyle == NULL) mPresentation.labelStyle = &mVisualStyle;
    if (mPresentation.unitsStyle == NULL) mPresentation.unitsStyle = &mVisualStyle;

    RecalcSizes();
}

//------------------------------------------------------------------------------------------------------
void StripGauge::RecalcSizes(void)
{
    // Make a new format string
    mValueFormat[0] = '%';
    mValueFormat[1] = 0;

    if (mPresentation.wholeDigits != 0)
    {
        sprintf(mValueFormat + 1, "%d", mPresentation.wholeDigits);
    }

    unsigned len = strlen(mValueFormat);
    sprintf(mValueFormat + len, ".%df", mPresentation.decimalDigits);

    float range = mMaxRange - mMinRange;

    unsigned a;
    for (a = 0; a < mPresentation.outline.size(); a++)
    {
        mPresentation.outline[a].scaledValue = mMinRange + (mPresentation.outline[a].value * range);
        if (mPresentation.outline[a].scaledValue > mMaxRange) mPresentation.outline[a].scaledValue = mMaxRange;
    }
}


//-------------------------------------------------------------------
void StripGauge::Draw(DisplayController *display)
{
    // Draw units
    if (mPresentation.showUnits)
    {

    }

    unsigned a;
    DisplayController::VertexData verts[4];
    // draw gauge
    for (a = 0; a < mPresentation.outline.size()-1; a++)
    {
        float diff = mPresentation.outline[a+1].scaledValue - mPresentation.outline[a].scaledValue;
        float ratio = (mCurrentValueClamped - mPresentation.outline[a].scaledValue) / diff;
        if (ratio < 0.0F) break;
        if (ratio > 1.0F) ratio = 1.0F;

        verts[0].x = mX + (mWidth  * mPresentation.outline[a].x1);
        verts[0].y = mY + (mHeight * mPresentation.outline[a].y1);

        verts[1].x = mX + (mWidth  * Interp(mPresentation.outline[a].x1, mPresentation.outline[a+1].x1, ratio));
        verts[1].y = mY + (mHeight * Interp(mPresentation.outline[a].y1, mPresentation.outline[a+1].y1, ratio));

        verts[2].x = mX + (mWidth  * Interp(mPresentation.outline[a].x2, mPresentation.outline[a+1].x2, ratio));
        verts[2].y = mY + (mHeight * Interp(mPresentation.outline[a].y2, mPresentation.outline[a+1].y2, ratio));

        verts[3].x = mX + (mWidth  * mPresentation.outline[a].x2);
        verts[3].y = mY + (mHeight * mPresentation.outline[a].y2);

        display->Polygon(verts, 4, 1.0F, mPresentation.outline[a].color, mPresentation.outline[a].color);
    }

    // draw ticks
    float    outlineThickness = mVisualStyle.borderSize;
    uint32_t outlineColor     = mVisualStyle.borderColor;
    for (a = 0; a < mPresentation.outline.size(); a += mPresentation.tickSpacing+1)
    {
        float x1 = mX + (mWidth  * mPresentation.outline[a].x1);
        float y1 = mY + (mHeight * mPresentation.outline[a].y1);
        float x2 = mX + (mWidth  * mPresentation.outline[a].x2);
        float y2 = mY + (mHeight * mPresentation.outline[a].y2);
        display->Line(x1, y1, x2, y2, outlineThickness, outlineColor);
    }

    // Draw outline
    for (a = 0; a < mPresentation.outline.size()-1; a++)
    {
        float x1 = mX + (mWidth  * mPresentation.outline[a].x1);
        float y1 = mY + (mHeight * mPresentation.outline[a].y1);
        float x2 = mX + (mWidth  * mPresentation.outline[a+1].x1);
        float y2 = mY + (mHeight * mPresentation.outline[a+1].y1);
        display->Line(x1, y1, x2, y2, outlineThickness, outlineColor);

        x1 = mX + (mWidth  * mPresentation.outline[a].x2);
        y1 = mY + (mHeight * mPresentation.outline[a].y2);
        x2 = mX + (mWidth  * mPresentation.outline[a+1].x2);
        y2 = mY + (mHeight * mPresentation.outline[a+1].y2);
        display->Line(x1, y1, x2, y2, outlineThickness, outlineColor);
    }

    // Draw Labels
    for (a = 0; a < mPresentation.outline.size()-1; a++)
    {
        if (mPresentation.outline[a].label == NULL) continue;
        float x = mX + (mWidth  * (mPresentation.outline[a].x1 + mPresentation.outline[a].labelX));
        float y = mY + (mHeight * (mPresentation.outline[a].y1 + mPresentation.outline[a].labelY));
        display->DrawString(mPresentation.outline[a].label, x, y, mPresentation.labelStyle->foregroundColor, mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace);
    }


#if 0
    if (mPresentation.showLabel)
    {
        display->DrawString(mLabel, mX + mLabelOffsetX, mY, mPresentation.labelStyle->foregroundColor, mPresentation.labelStyle->fontsize, mPresentation.labelStyle->fontFace);
    }

    char s[80];
    snprintf(s, 80, mValueFormat, mCurrentValueClamped);
    unsigned textWidth, textHeight;
    display->GetStringExtents(mVisualStyle.fontsize, mVisualStyle.fontFace, s, textWidth, textHeight);
    unsigned offsetX = mValueOffsetX + (mValueSizeX - textWidth);
    //printf("OffsetX of %s %d  SizeX %d   textWidth %d\n", mName, offsetX, mValueSizeX, textWidth);

    display->DrawString(s, mX + offsetX, mY, mVisualStyle.foregroundColor, mVisualStyle.fontsize, mVisualStyle.fontFace);

    if (mPresentation.showUnits)
    {
        display->DrawString(mUnits, mX + mUnitsOffsetX, mY, mPresentation.unitsStyle->foregroundColor, mPresentation.unitsStyle->fontsize, mPresentation.unitsStyle->fontFace);
    }
#endif
}

