#include "listbox.h"
#include "util/util.h"

#include <stdlib.h>

#define LB_THUMB_COLOUR mVisualStyle.foregroundColor

#define LB_FLAG_MULTISELECT  (SE_FLAG_USER + 0)

//------------------------------------------------------------------------------------
Listbox::Listbox(const char *name, uint16_t id, ScreenElement::VisualStyle *visualStyle, bool multiSelectEnable) :
    ScreenElement(name, id, visualStyle)
{
    mWidth  = 0.078125F;
    mHeight = 0.125F;

    mFirstDisplayItem = 0;

    // Internal cached data for drawing
    mItemX = 0;
    mItemWidth = 0;
    mFirstItemY = 0;
    mNumItemsVisible = 0;
    mNumCharsVisible = 0;
    mBorderExtra = 0.003F;

    ClearFlag(LB_FLAG_MULTISELECT);
    if (multiSelectEnable) SetFlag(LB_FLAG_MULTISELECT);

    UpdateWindowSettings();
}

//------------------------------------------------------------------------------------
Listbox::~Listbox()
{
    mItems.clear();
}

//------------------------------------------------------------------------------------
void Listbox::SetPosition(float x, float y)
{
    ScreenElement::SetPosition(x, y);
    UpdateWindowSettings();
}

//------------------------------------------------------------------------------------
void Listbox::SetSize(float width, float height)
{
    ScreenElement::SetSize(width, height);
    UpdateWindowSettings();
}

//------------------------------------------------------------------------------------
void Listbox::Clear(void)
{
    ClearSelection();
    mItems.clear();
}

//------------------------------------------------------------------------------------
uint32_t Listbox::AddString(const char *s, bool selected)
{
    if (s  == NULL) return BAD_INDEX;

    uint32_t index = mItems.size();
    mItems.emplace_back(s, selected);

    if (selected) SelectItem(index);
    else          DeselectItem(index);
    return index;
}

//------------------------------------------------------------------------------------
uint32_t Listbox::FindString(const char *s)
{
    uint32_t a;
    for(a = 0; a < mItems.size(); a++)
    {
        if (0 == strcmp(s, mItems[a].text)) return a;
    }
    return BAD_INDEX;
}

//------------------------------------------------------------------------------------
void Listbox::RemoveString(const char *s)
{
    uint32_t index = FindString(s);
    if (index == BAD_INDEX) return;

    mItems.erase(mItems.begin() + index);
}

//------------------------------------------------------------------------------------
void Listbox::RemoveSelected(void)
{
    for (auto i = mItems.begin(); i != mItems.end();)
    {
        if ((*i).isSelected) mItems.erase(i);
        else               ++i;
    }

    ClearSelection();
    mFirstDisplayItem = 0; // make sure we're not displaying off the bottom of the list
}

//------------------------------------------------------------------------------------
void Listbox::ClearSelection(void)
{
    UIEvent e;
    e.message      = ListboxChanged;
    e.when         = SystemTimeUTC();
    e.lb.id        = mID;

    unsigned a;
    for (a = 0; a < mItems.size(); a++)
    {
        if (mItems[a].isSelected)
        {
            e.lb.itemIndex = a;
            e.lb.newState  = 0;
            mItems[a].isSelected = false;
            UI::QueueMessage(&e);
        }
    }
}

//------------------------------------------------------------------------------------
// Stupid bubble sort
void Listbox::Sort(void)
{
    if (mItems.size() < 2) return;

    bool sorted;
    do
    {
        sorted = true;
        unsigned a;
        for (a = 0; a < mItems.size() - 1; a++)
        {
            if (strcmp(mItems[a].text, mItems[a + 1].text) > 0)
            {
                mItems[a].Swap(mItems[a + 1]);
                sorted = false;
            }
        }
    } while (!sorted);
}


//------------------------------------------------------------------------------------
uint32_t Listbox::ItemCount(void)
{
    return mItems.size();
}

//------------------------------------------------------------------------------------
bool Listbox::IsSelected(uint32_t index)
{
    if (index >= mItems.size()) return false;
    return mItems[index].isSelected ^ mItems[index].toggleSelection;
}

//------------------------------------------------------------------------------------
const char *Listbox::GetString(uint32_t index)
{
    if (index >= mItems.size()) return "";
    return mItems[index].text;
}

//------------------------------------------------------------------------------------
uint32_t Listbox::NumSelected(void) const
{
    uint32_t count = 0;
    unsigned a;
    for (a = 0; a < mItems.size(); a++)
    {
        if (mItems[a].isSelected) ++count;
    }
    return count;
}

//------------------------------------------------------------------------------------
uint32_t Listbox::SelectedIndex(uint32_t index) const
{
    uint32_t count = 0;
    unsigned a;
    for (a = 0; a < mItems.size(); a++)
    {
        if (mItems[a].isSelected)
        {
            if (count == index) return a;
            ++count;
        }
    }
    return BAD_INDEX;
}

//------------------------------------------------------------------------------------
void Listbox::SelectItem(uint32_t index)
{
    if (index >= mItems.size()) return;

    if (!TestFlag(LB_FLAG_MULTISELECT)) ClearSelection();

    mItems[index].isSelected = true;

    UIEvent e;
    e.message      = ListboxChanged;
    e.when         = SystemTimeUTC();
    e.lb.id        = mID;
    e.lb.itemIndex = index;
    e.lb.newState  = 1;
    UI::QueueMessage(&e);
}

//------------------------------------------------------------------------------------
void Listbox::DeselectItem(uint32_t index)
{
    if (index >= mItems.size()) return;

    mItems[index].isSelected = false;

    UIEvent e;
    e.message      = ListboxChanged;
    e.when         = SystemTimeUTC();
    e.lb.id        = mID;
    e.lb.itemIndex = index;
    e.lb.newState  = 0;
    UI::QueueMessage(&e);
}

//------------------------------------------------------------------------------------
void Listbox::UpdateWindowSettings(void)
{
    float textWidth, textHeight;
    gDisplay->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);

    mNumItemsVisible = 0;
    mNumCharsVisible = 0;
    mItemX           = mX;
    mFirstItemY      = mY;
    mItemWidth       = mWidth - textWidth;
    // Can't draw anything if no text will fit inside the borders
    if (mWidth  < (textWidth  * 5.0F)) return;
    if (mHeight < (textHeight * 4.0F)) return;

    float borderSizeY = mVisualStyle.borderSize / gDisplay->DisplayHeight();

    float overheadY = (borderSizeY * 6.0F) + (textHeight * 2.0F);

    mNumItemsVisible = floor((mHeight - overheadY) / textHeight);
    mNumCharsVisible = mItemWidth / textWidth;

    mItemX = mX + ((mWidth  - (mNumCharsVisible * textWidth))  / 2.0F);
    mFirstItemY = mY + borderSizeY + ((mHeight - (mNumItemsVisible * textHeight)) / 2.0F);
}

//------------------------------------------------------------------------------------
void Listbox::SetToggleSelectionRange(unsigned first, unsigned last)
{
    if (first >= mItems.size()) return;

    unsigned a;
    for (a=0; a < mItems.size(); a++)
    {
        mItems[a].toggleSelection = ((a >= first) && (a < last));
    }
}

//------------------------------------------------------------------------------------
void Listbox::UpdateSelectionFromToggles(void)
{
    UIEvent e;
    e.message = ListboxChanged;
    e.when    = SystemTimeUTC();
    e.lb.id   = mID;

    unsigned a;
    for (a=0; a < mItems.size(); a++)
    {
        if (mItems[a].toggleSelection)
        {
            mItems[a].isSelected = !mItems[a].isSelected;
            mItems[a].toggleSelection = false;
            e.lb.itemIndex = a;
            e.lb.newState  = mItems[a].isSelected;
            UI::QueueMessage(&e);
        }
    }
}


//-------------------------------------------------------------------
// Input coords are in local window space
uint32_t Listbox::ScreenCoordToIndex(float lx, float ly, bool clamp)
{
    float textWidth, textHeight;
    gDisplay->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);

    float windowHeight = mHeight - (textHeight * 2.0F);

    // Move local coord to relative to the first item
    ly -= (mFirstItemY - mY);

    if (clamp != 0)
    {
        //if (lx < 0) lx = 0;
        //if (lx > mWidth) lx = mWidth;
        if (ly < 0) ly = 0;
        if (ly > windowHeight) ly = windowHeight;
    }

    if (ly < 0)            return BAD_INDEX;
    if (ly > windowHeight) return BAD_INDEX;

    uint32_t index = mFirstDisplayItem + (ly / textHeight);
    if (index > mItems.size()) index = mItems.size();
    return index;
}


//------------------------------------------------------------------------------------
//------  UI Handler  ----------------------------------------------------------------
//------------------------------------------------------------------------------------
ScreenElement::ElementUIResult Listbox::HandleUIEvent(UIEvent *message)
{
    if (message == NULL) return Yep;

    // Check for Page Up/Down
    if ((message->message == Click) || (message->message == DoubleClick))
    {
        float textWidth, textHeight;
        gDisplay->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);

        if ((message->click.y < mFirstItemY) && (message->message == DoubleClick))
        {
            if (TestFlag(LB_FLAG_MULTISELECT))
            {
                SetToggleSelectionRange(0, mItems.size());
                UpdateSelectionFromToggles();
                UpdateWindowSettings();
            }
        }

        if (message->click.y >= (mHeight - textHeight))
        {
            // Pg down
            if (message->click.x < (mWidth / 2.0F))
            {
                mFirstDisplayItem += mNumItemsVisible;
                if ((mFirstDisplayItem + mNumItemsVisible) > mItems.size())
                {
                    mFirstDisplayItem = mItems.size() - mNumItemsVisible;
                }
                UpdateWindowSettings();
                return Yep;
            }
            // Pg Up
            else
            {
                mFirstDisplayItem -= mNumItemsVisible;
                if (mFirstDisplayItem < 0)
                {
                    mFirstDisplayItem = 0;
                }
                UpdateWindowSettings();
                return Yep;
            }
        }
    }

    // The rest of the UI
    uint16_t index, index2;
    switch (message->message)
    {
        case Click:
            index = ScreenCoordToIndex(message->click.x, message->click.y, false);
            if (index != BAD_INDEX)
            {
                if (IsSelected(index)) DeselectItem(index);
                else                   SelectItem(index);
            }
            break;

        case Dragging:
            if (TestFlag(LB_FLAG_MULTISELECT))
            {
                index  = ScreenCoordToIndex(message->drag.start_x, message->drag.start_y, false);
                index2 = ScreenCoordToIndex(message->drag.end_x,   message->drag.end_y, true);
                if ((index != BAD_INDEX) && (index2 != BAD_INDEX))
                {
                    if (index2 < index)
                    {
                        uint16_t temp = index;
                        index = index2;
                        index2 = temp;
                    }
                    SetToggleSelectionRange(index, index2);
                }
            }
            break;

        case DragDone:
            if (TestFlag(LB_FLAG_MULTISELECT))
            {
                UpdateSelectionFromToggles();
            }
            break;

        default:
            break;
    }

    return Yep;
}

//------------------------------------------------------------------------------------
void Listbox::Draw(DisplayController *display)
{
    // Draw the border
    DrawBorder(display, FillBackgroundColor);

    float borderSizeX = mVisualStyle.borderSize / display->DisplayWidth();
    float borderSizeY = mVisualStyle.borderSize / display->DisplayHeight();

    float textWidth, textHeight;
    display->GetFontSize(mVisualStyle.fontsize, textWidth, textHeight, mVisualStyle.fontFace);
    float x, y; // temps for lines/rects

    // Draw the page up/down and Title
    {
        if (mName != NULL)
        {
            unsigned len = strlen(mName);
            float x = mX + (mWidth / 2.0F) - ((textWidth * len) / 2.0F);
            display->DrawString(mName, x, mY + borderSizeY, mVisualStyle.borderColor, mVisualStyle.fontsize);
        }

        y = mY + textHeight + borderSizeY;
        display->Line(mX, y, mX + mWidth, y, 1, mVisualStyle.borderColor); // Title

        y = mY + mHeight - (textHeight + (borderSizeY * 2.0F));
        display->Line(mX, y, mX + mWidth, y, 1, mVisualStyle.borderColor); // Up/Down

        float quarter = mWidth / 4.0F;
        x = mX + quarter - ((textWidth * 5.0F) / 2.0F);
        display->DrawString("Pg Dn", x, mY + mHeight - (textHeight + borderSizeY), mVisualStyle.borderColor, mVisualStyle.fontsize);
        x = mX + (3.0F * quarter) - ((textWidth * 5.0F) / 2.0F);
        display->DrawString("Pg Up", x, mY + mHeight - (textHeight + borderSizeY), mVisualStyle.borderColor, mVisualStyle.fontsize);

        float half = mWidth / 2.0F;
        x = mX + half;
        display->Rect(x, (mY + mHeight) - (textHeight + borderSizeY), borderSizeX, textHeight, 1, mVisualStyle.borderColor); // Up
    }

    // Draw a 'scroll bar' highlight on the right size
    if (mItems.size() > mNumItemsVisible)
    {
        float windowHeight  = mHeight - (textHeight * 2.0F) - (borderSizeY * 4.0F);
        float screenPerItem = windowHeight / (float) mItems.size();
        float scrollTop     = mY + textHeight + (borderSizeY * 2.0F);
        float thumbTop      = scrollTop + (screenPerItem * (float)mFirstDisplayItem);
        float thumbSize     = screenPerItem * (float) mNumItemsVisible;
        if (thumbSize < borderSizeY) thumbSize = borderSizeY;

        x = mX + mWidth - borderSizeX;
        // Separating line for scroll
        //display->Line(x, scrollTop, x, scrollTop + windowHeight, mVisualStyle.borderSize, mVisualStyle.borderColor);
        // Thumb for scroll
        display->RectFilled(x, thumbTop, borderSizeX, thumbSize, LB_THUMB_COLOUR);

        //display->Rect(x, thumbTop + thumbSize, mVisualStyle.borderSize, (mY + mHeight) - (thumbTop + thumbSize), mVisualStyle.borderColor);
    }
    else
    {
        x = mX + mWidth - borderSizeX;
        display->Rect(x, mY, borderSizeX, mHeight, mVisualStyle.borderSize, mVisualStyle.borderColor);
    }

    //stringf(gTempLine, "(%d  %d)  %d  %d   ", (int) lb->itemX, (int) lb->firstItemY, (int) lb->numItemsVisible, (int) lb->numCharsVisible);
    //LCD_draw_string(gTempLine, mX, mY - 16, YELLOW, BLACK, Small);

    // Can't draw anything if no text will fit inside the borders
    if (mNumItemsVisible == 0) return;

    uint16_t index = mFirstDisplayItem;
    uint16_t lastVisible = index + mNumItemsVisible;
    if (lastVisible > mItems.size()) lastVisible = mItems.size();

    float itemX = mItemX;
    float itemY = mFirstItemY;

    char tempLine[512];
    while (index < lastVisible)
    {
        strncpy(tempLine, mItems[index].text, mNumCharsVisible);
        tempLine[mNumCharsVisible] = 0;

        if (IsSelected(index))
        {
            display->RectFilled(itemX, itemY, mItemWidth, textHeight, mVisualStyle.selectColor);
        }
        display->DrawString(tempLine, itemX, itemY, mVisualStyle.foregroundColor, mVisualStyle.fontsize);

        ++index;
        itemY += textHeight;
    }
}




//-------------------------------------------------------------------
static void strcpy_pad(char *dest, char *src, unsigned padlen, char pad)
{
    if (dest == NULL) return;
    if (src == NULL)
    {
        memset(dest, pad, padlen);
        return;
    }

    char *end = dest + padlen;

    while (dest < end)
    {
        if (*src == 0) break;
        *dest++ = *src++;
    }

    while (dest < end)
    {
        *dest++ = pad;
    }

    *dest = 0;
}













