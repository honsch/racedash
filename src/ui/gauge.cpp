#include "gauge.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "rpn/rpn.h"

ScreenElement::VisualStyle Gauge::sDefaultVisualStyle = {   YELLOW,         // FG Color
                                                            BLACK,          // BG Color
                                                            GREY,           // Border Color
                                                            PURPLE,         // Select Color
                                                            Font_Large,     // PixelFont style
                                                            2,              // Border Size
                                                            0,              // Corner Radius
                                                            Font_Mono       // Default font
                                                        };

//------------------------------------------------------------------------
Gauge::Gauge(const char *variableName, const char *units, uint16_t id, ScreenElement::VisualStyle *visualStyle) :
    ScreenElement(variableName, id, visualStyle),
    mLabel(NULL)
{
    SetLabel(NULL);
    mUnits    = strdup(units);
    mNameHash = RPN::VariableNameHash(mName);
}

//------------------------------------------------------------------------
Gauge::~Gauge()
{
    free(mUnits);
}

//------------------------------------------------------------------------
void Gauge::UpdateValue(void)
{
    SensorManager *sm = SensorManager::Get();
    if (sm != NULL)
    {
        sm->GetNewestStateValue(mName, mCurrentValueRaw);
        mCurrentValueClamped = ClampValue(mCurrentValueRaw);
        UpdateValueLimits(mCurrentValueClamped);
    }
}

//------------------------------------------------------------------------
void Gauge::SetSize(float width, float height)
{
    ScreenElement::SetSize(width, height);
    RecalcSizes();
}

//------------------------------------------------------------------------
void Gauge::SetRange(double minimum, double maximum)
{
    mMinRange = minimum;
    mMaxRange = maximum;

    // Reset 'seen limits' when range is reset
    mMinValue = maximum;
    mMaxValue = minimum;

    RecalcSizes();
}

//------------------------------------------------------------------------
void Gauge::FormatValue(char *out, double value, unsigned wholeDigits, unsigned decimalDigits)
{
    char fmt[10];
    sprintf(fmt, "%%%d.%df", wholeDigits, decimalDigits);
    sprintf(out, fmt, value);
}

//------------------------------------------------------------------------
unsigned Gauge::ClampDigits(void)
{
    double max = mMaxValue;
    if (fabs(mMinValue) > max) max = fabs(mMinValue);
    if (max < 1.0) max = 1.0;
    unsigned digits = 1 + (unsigned) log10(max);
    if (mMinValue < 0.0) ++digits;
    return digits;
}

//------------------------------------------------------------------------
void Gauge::SetVisualStyle(VisualStyle *style)
{
    if (style == NULL) mVisualStyle = sDefaultVisualStyle;
    else               mVisualStyle = *style;
    RecalcSizes();
}

//------------------------------------------------------------------------
void Gauge::SetLabel(const char *label)
{
    if (mLabel != NULL) free(mLabel);

    if (label == NULL) mLabel = strdup(mName);
    else               mLabel = strdup(label);

    RecalcSizes();
}

//------------------------------------------------------------------------
ScreenElement::ElementUIResult Gauge::HandleUIEvent(UIEvent *message)
{
    if (message == NULL) return Yep;

    if ((message->message == Click) || (message->message == DoubleClick))
    {
        if (TestFlag(GAUGE_FLAG_HOLD)) ClearFlag(GAUGE_FLAG_HOLD);
        else                           SetFlag(GAUGE_FLAG_HOLD);
        UIEvent e;
        e.when    = message->when;
        e.bn.id   = mID;
        e.message = GaugeClicked;
        UI::QueueMessage(&e);
    }
    else if (message->message == Holding)
    {
        ClearValueLimits();
    }

    return Yep;
}
