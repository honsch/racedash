
#ifndef UI_H
#define UI_H

#include <stdint.h>
#include <vector>

#include "touchscreen.h"
#include "mouse.h"

#include "ui/inputevent.h"

// Messages
enum UIMessage
{
    NoEvent = 0,
    Click,
    DoubleClick,
    Dragging,
    DragDone,
    Holding,
    HoldDone,
    Key,
    ButtonClicked,
    ButtonDoubleClicked,
    ButtonHolding,
    ButtonHoldDone,
    ListboxChanged,
    GettextResult,
    SliderChanged,
    GaugeClicked
};


//-----------------------------------------
struct ClickData
{
    float x;
    float y;
};

//-----------------------------------------
struct DragData
{
    float start_x;
    float start_y;
    float end_x;
    float end_y;
};

struct KeyboardData
{
    char key;
};

struct ButtonData
{
    uint16_t id;
    uint16_t newState;
};

struct ListboxData
{
    uint16_t id;
    uint16_t itemIndex;
    uint16_t newState;
};

struct SliderData
{
    uint16_t id;
    float    newValue;
};

struct GettextData
{
    uint16_t id;
    char    *text; // The receiver is responsible for freeing this memory
};

struct GaugeData
{
    uint16_t id;
};

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
struct UIEvent
{
    double      when;
    UIMessage   message;

    union
    {
        ClickData       click;
        DragData        drag;
        KeyboardData    kb;
        ButtonData      bn;
        ListboxData     lb;
        GettextData     gt;
        SliderData      sl;
        GaugeData       ga;
    };
};

// For debugging
void PrintUIEvent(const UIEvent *e);

class ScreenBase;
class DisplayController;

//---------------------------------------------------------------------------
class UI
{
  public:
    static void Init(Touchscreen *ts, Mouse *mouse);
    static void Shutdown(void);

    // Called by the main loop
    static int Update(DisplayController *display);

    // Called by whatever to send a message to the parent screen
    static void QueueMessage(UIEvent *message);


    static int SetScreen(ScreenBase *screen, bool clearStack);
    static int PushScreen(ScreenBase *screen);
    static int PopScreen(void);

    static Touchscreen *GetTouchscreen(void) { return mTouchscreen; }
    static Mouse       *GetMouse(void)       { return mMouse; }

    static void ShowTouch(bool show) { mShowTouch = show; }

  private:

    enum Event { Press, Release };
    struct TSEvent
    {
        double   time;
        Event    event;
        float    x;
        float    y;
    };

    static TSEvent mEvents[4];
    static Touchscreen *mTouchscreen;
    static Mouse       *mMouse;
    static bool         mShowTouch;

    enum HoldState { HS_None, HS_Holding, HS_Dragging };
    static HoldState mHoldState;

    static std::vector<UIEvent> mMessageQueue;
    static std::vector<ScreenBase *> mScreenStack;

    static void HandleInputEvent(void *context, InputEvent *event);
    static void ParseEvents(void);
};

#endif //UI_H

