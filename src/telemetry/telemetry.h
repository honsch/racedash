#ifndef TELEMETRY_H
#define TELEMETRY_H

#include <stdint.h>

#define TELEMETRY_TYPE_RT_DATA   0
#define TELEMETRY_TYPE_PIT       1
#define TELEMETRY_TYPE_TROUBLE   2 // Sent from car to indicate car is borked, header only
#define TELEMETRY_TYPE_LAP_DATA  3

//----------------------------------------------
struct TelemetryHeader
{
    uint8_t  size;
    uint8_t  type;
};

//----------------------------------------------
struct TelemetryRTData
{
    TelemetryHeader header;

    uint8_t  kmh;
    uint8_t  waterTemp;      // deg C

    uint32_t rpm        : 11; // out = rpm*10
    uint32_t pitAck     :  2;
    uint32_t fuelRemain : 11; // out = gallons * 100, max 20.47 gallons
    uint32_t battery    :  8; // out = battery / 10

    float    latitude;
    float    longitude;

    uint16_t stintTime;      // in seconds
    uint8_t  oilPressure;    // psi
    uint8_t  afr;            // out = afr / 10
};

//----------------------------------------------
struct TelemetryPitRequest
{
    TelemetryHeader header;
    uint8_t         pitLaps; // 0 is pit now, 2 pit in 2 laps
};

//----------------------------------------------
struct TelemetryLapData
{
    TelemetryHeader header;

    uint8_t  lapCompleted;      // Which lap just finished
    uint8_t  lapsFuelRemaining; // Estimated laps left before zero fuel
    uint8_t  _pad1;
    uint8_t  _pad2;
    float    lapDuration;
};

#endif // TELEMETRY_H
