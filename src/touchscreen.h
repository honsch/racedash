#ifndef TOUCHSCREEN_H
#define TOUCHSCREEN_H

#include <stdint.h>

#include "ui/inputevent.h"

#define TOUCHSCREEN_CONFIG_PATH "racedash.touchscreen"

#define TOUCHSCREEN_CONFIG_OFFSET_X "offset_x"
#define TOUCHSCREEN_CONFIG_OFFSET_Y "offset_y"
#define TOUCHSCREEN_CONFIG_SCALE_X  "scale_x"
#define TOUCHSCREEN_CONFIG_SCALE_Y  "scale_y"

class Touchscreen
{
 public:
   Touchscreen(void);
  ~Touchscreen();

   int  Init(const char *device);

   void RegisterCallback(InputEventCallback callback, void *context);
   void SetCalibration(float offX, float offY, float scaleX, float scaleY);
   void GetCalibration(float &offX, float &offY, float &scaleX, float &scaleY) const;

   int  Update(void);

   unsigned LastReport(InputEvent &dest) const { dest = mTransformedEvent; return mTotalEvents; }

 private:

   int InitInternal(void);

   char *mDeviceName;
   int mDevFD;
   InputEventCallback mCallback;
   void              *mCallbackContext;

   float mCalOffsetX;
   float mCalOffsetY;
   float mCalScaleX;
   float mCalScaleY;

   InputEvent mPreviousEvent;
   InputEvent mTransformedEvent;

   unsigned mTotalEvents;
};

#endif // TOUCHSCREEN_H