#ifndef GPIO_H
#define GPIO_H

#include <gpiod.h>
#include <vector>
#include <stdint.h>


#if ODROID == 2

#define LINE_LED1   "J2 Header Pin35"
#define LINE_LED2   "J2 Header Pin36"
#define LINE_LED3   "J2 Header Pin33"
#define LINE_LED4   "J2 Header Pin11"

#define LINE_LORA_DIO0  "J2 Header Pin18"

#define LINE_DIGITAL_IN1 "J2 Header Pin19"
#define LINE_DIGITAL_IN2 "J2 Header Pin16"
#define LINE_DIGITAL_IN3 "J2 Header Pin15"
#define LINE_DIGITAL_IN4 "J2 Header Pin13"

#define LINE_POWER_SENSE   "J2 Header Pin12"
#define LINE_POWER_CONTROL "J2 Header Pin7"

#elif ODROID == 4

#define LINE_LED1   "PIN_35"
#define LINE_LED2   "PIN_36"
#define LINE_LED3   "PIN_33"
#define LINE_LED4   "PIN_11"

#define LINE_LORA_DIO0  "PIN_18"

#define LINE_DIGITAL_IN1 "PIN_19"
#define LINE_DIGITAL_IN2 "PIN_16"
#define LINE_DIGITAL_IN3 "PIN_15"
#define LINE_DIGITAL_IN4 "PIN_13"

#define LINE_POWER_SENSE   "PIN_12"
#define LINE_POWER_CONTROL "PIN_7"

#else
#error Define ODROID to be either 2 or 4 for C2 or C4
#endif


//------------------------------------------------------
bool GpioInit(void);
void GpioShutdown(void);

struct gpiod_line *GpioFindLine(const char *name);
void GpioFreeLine(struct gpiod_line *line);

void GpioSetLineValue(struct gpiod_line *line, int value);
int  GpioGetLineValue(struct gpiod_line *line);

void GpioSetLineInput(struct gpiod_line *line, const char *consumer);
void GpioSetLineOutput(struct gpiod_line *line, int defaultValue, const char *consumer);



#endif //GPIO_H