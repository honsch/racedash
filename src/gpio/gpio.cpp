
#include "gpio.h"
#include "util/util.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>


//------------------------------------------------------
//------------------------------------------------------
static struct gpiod_chip *sGpioChip = NULL;

bool GpioInit(void)
{
    if (sGpioChip != NULL) return true;

#if ODROID == 2
    sGpioChip = gpiod_chip_open_by_number(1);
#else     
    sGpioChip = gpiod_chip_open_by_number(0);
#endif
    if (sGpioChip == NULL) return false;

    return true;
}

//------------------------------------------------------
void GpioShutdown(void)
{
    if (sGpioChip != NULL)
    {
        gpiod_chip_close(sGpioChip);
    }
}

//------------------------------------------------------
struct gpiod_line *GpioFindLine(const char *name)
{
    if (sGpioChip == NULL) return NULL;

    struct gpiod_line *line = gpiod_chip_find_line(sGpioChip, name);
    return line;
}

//------------------------------------------------------
void GpioFreeLine(struct gpiod_line *line)
{
    gpiod_line_release(line);
}

//------------------------------------------------------
void GpioSetLineValue(struct gpiod_line *line, int value)
{
    gpiod_line_set_value(line, value);
}

//------------------------------------------------------
int GpioGetLineValue(struct gpiod_line *line)
{
    return gpiod_line_get_value(line);
}

//------------------------------------------------------
void GpioSetLineInput(struct gpiod_line *line, const char *consumer)
{
    gpiod_line_request_input_flags(line, consumer, GPIOD_LINE_REQUEST_FLAG_BIAS_PULL_DOWN);
}

//------------------------------------------------------
void GpioSetLineOutput(struct gpiod_line *line, int defaultValue, const char *consumer)
{
    gpiod_line_request_output(line, consumer, defaultValue);
}
