#ifndef RPNOP_H
#define RPNOP_H

#include <stack>

#include"rpn.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#ifndef LOG_H
#include "log/log.h"
#endif

typedef std::stack<double> RPNStack;

//--------------------------------------------------
class RPNOp
{
  public:
    virtual ~RPNOp() { ; }
    virtual bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const = 0;
    virtual void Dump(void) const = 0;
  private:
};


//--------------------------------------------------
class RPNOp_Constant : public RPNOp
{
  public:
    RPNOp_Constant(double c) : mConstant(c) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        stack.push(mConstant);
        return true;
    }

    void Dump(void) const { log_info(" %f", mConstant); }
  private:
    double mConstant;
};

//--------------------------------------------------
class RPNOp_Variable : public RPNOp
{
  public:
    RPNOp_Variable(const char *name)
    {
        mNameHash = RPN::VariableNameHash(name);
        mName     = strdup(name);
    }
    ~RPNOp_Variable()
    {
        free(mName);
    }

    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        // is the variable found?
        auto it = vt.find(mNameHash);
        if (it == vt.end())
        {
            log_info("RPN Variable %s not found, using zero.", mName);
            stack.push(0.0);
            return true;
        }
        stack.push(it->second);
        return true;
    }

    void Dump(void) const { log_info(" %s", mName); }

  private:
    uint32_t mNameHash;
    char    *mName;
};


//--------------------------------------------------
class RPNOp_Operator : public RPNOp
{
  public:
    virtual int Precedence(void) const = 0;
};

//--------------------------------------------------
class RPNOp_Add : public RPNOp_Operator
{
  public:
    RPNOp_Add(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        if (stack.size() < 2) return false;
        double a = stack.top(); stack.pop();
        double b = stack.top(); stack.pop();
        stack.push(a + b);
        return true;
    }

    int Precedence(void) const { return 2; }
    void Dump(void) const { printf(" +"); }
  private:
};

//--------------------------------------------------
class RPNOp_Sub : public RPNOp_Operator
{
  public:
    RPNOp_Sub(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        if (stack.size() < 2) return false;
        double a = stack.top(); stack.pop();
        double b = stack.top(); stack.pop();
        stack.push(b - a);
        return true;
    }

    int Precedence(void) const { return 2; }
    void Dump(void) const { printf(" -"); }
  private:
};

//--------------------------------------------------
class RPNOp_Mul : public RPNOp_Operator
{
  public:
    RPNOp_Mul(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        if (stack.size() < 2) return false;
        double a = stack.top(); stack.pop();
        double b = stack.top(); stack.pop();
        stack.push(a * b);
        return true;
    }

    int Precedence(void) const { return 3; }
    void Dump(void) const { printf(" *"); }

  private:
};

//--------------------------------------------------
class RPNOp_Div : public RPNOp_Operator
{
  public:
    RPNOp_Div(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        if (stack.size() < 2) return false;
        double a = stack.top(); stack.pop();
        double b = stack.top(); stack.pop();
        stack.push(b / a);
        return true;
    }

    int Precedence(void) const { return 3; }
    void Dump(void) const { printf(" /"); }

  private:
};


//--------------------------------------------------
class RPNOp_Pow : public RPNOp_Operator
{
  public:
    RPNOp_Pow(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        if (stack.size() < 2) return false;
        double a = stack.top(); stack.pop();
        double b = stack.top(); stack.pop();
        stack.push(pow(b, a));
        return true;
    }

    int Precedence(void) const { return 4; }
    void Dump(void) const { printf(" ^"); }
  private:
};

//--------------------------------------------------
class RPNOp_Mod : public RPNOp_Operator
{
  public:
    RPNOp_Mod(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        if (stack.size() < 2) return false;
        double a = stack.top(); stack.pop();
        double b = stack.top(); stack.pop();
        stack.push(fmod(b, a));
        return true;
    }

    int Precedence(void) const { return 3; }
    void Dump(void) const { printf(" ^"); }
  private:
};

// The groups are just placeholders during parse
// They are NEVER in an evaluated RPNExpression
//--------------------------------------------------
class RPNOp_BeginGroup : public RPNOp_Operator
{
  public:
    RPNOp_BeginGroup(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        return false;
    }

    int Precedence(void) const { return -1; }
    void Dump(void) const { printf(" ("); }
  private:
};


//--------------------------------------------------
class RPNOp_EndGroup : public RPNOp_Operator
{
  public:
    RPNOp_EndGroup(void) { ; }
    bool Evaluate(RPNStack &stack, const RPN::VariableTable &vt) const
    {
        return false;
    }

    int Precedence(void) const { return -2; }
    void Dump(void) const { printf(" )"); }

  private:
};


#endif // RPNOP_H