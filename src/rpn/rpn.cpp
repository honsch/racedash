#include "rpn.h"

#include "rpnop.h"

#include <ctype.h>

#include <stack>

#include "log/log.h"

enum OpType { Value, Operator, GroupStart, GroupEnd };

//------------------------------------
static RPNOp *ParseOp(const char *&src, OpType &type)
{
    // Handle single char ops
    switch(*src)
    {
        case '+':
            ++src;
            type = Operator;
            return new RPNOp_Add();

        case '-':
            ++src;
            type = Operator;
            return new RPNOp_Sub();

        case '*':
            ++src;
            type = Operator;
            return new RPNOp_Mul();

        case '/':
            ++src;
            type = Operator;
            return new RPNOp_Div();

        case '%':
            ++src;
            type = Operator;
            return new RPNOp_Mod();

        case '^':
            ++src;
            type = Operator;
            return new RPNOp_Pow();


        case '(':
            ++src;
            type = GroupStart;
            return new RPNOp_BeginGroup();

        case ')':
            ++src;
            type = GroupEnd;
            return new RPNOp_EndGroup();
    }

    // Handle numbers
    if (isdigit(*src) || (*src == '.'))
    {
        char *next;
        double val = strtod(src , &next);
        src = next;
        type = Value;
        return new RPNOp_Constant(val);
    }

    // Handle variable names
    if (isalpha(*src) || (*src == '_'))
    {
        char temp[65];
        int pos = 0;
        while ((pos < 64) && (isalnum(*src) || ('_' == *src)))
        {
            temp[pos] = *src;
            ++src;
            ++pos;
        }
        temp[pos] = 0;
        type = Value;
        return new RPNOp_Variable(temp);
    }

    // Some sort of punctuation we don't like so skip it
    log_info("ParseOp: Ignoring unknown character '%c' (%d)", *src, (int) *src);
    ++src;
    return NULL;
}

//-------------------------------------------------------------------------------------
RPN::Expression *RPN::Parse(const char *expression, RPN::ParseError &errOut)
{
    if (expression == NULL) return NULL;

    RPNOp_Add initialPrevOp;

    RPNOp *op = &initialPrevOp;

    RPN::Expression *rpn = new RPN::Expression;

    std::stack<RPNOp *> opStack;

    const char *pos = expression;
    while (*pos != 0)
    {
        bool prevWasOp = false;
        {
            RPNOp_Operator *opOp  = dynamic_cast<RPNOp_Operator *>(op);
            if ((opOp != NULL) && (opOp->Precedence() > 0)) prevWasOp = true;
        }

        // get the next op if valid
        OpType type;
        op = ParseOp(pos, type);
        if (op == NULL) continue;

        // This is always the same so don't wait for an error to set it
        errOut.pos   = (pos - expression) - 1;

        switch (type)
        {
            case Value:
                rpn->push_back(op);
                break;

            case GroupStart:
                opStack.push(op);
                break;

            case GroupEnd:
            {
                bool foundOpen = false;

                while (!opStack.empty())
                {
                    RPNOp_Operator *opTop = dynamic_cast<RPNOp_Operator *>(opStack.top());
                    opStack.pop();
                    if (opTop->Precedence() == -1)
                    {
                        foundOpen = true;
                        break;
                    }
                    else
                    {
                        rpn->push_back(opTop);
                    }
                }
                if (!foundOpen)
                {
                    errOut.error = "Parenthesis mismatch";
                    goto error;
                }

                break;
            }

            case Operator:
            {
                RPNOp_Operator *opOp  = dynamic_cast<RPNOp_Operator *>(op);
                if (prevWasOp)
                {
                    // Only unary sub supported!
                    if ((NULL != dynamic_cast<RPNOp_Sub*>(op)) ||
                        (NULL != dynamic_cast<RPNOp_Add*>(op)))
                    {
                        rpn->push_back(new RPNOp_Constant(0.0));
                    }
                    else
                    {
                        errOut.error = "Only unary + and - supported";
                        goto error;
                    }
                }
                else
                {
                    while (!opStack.empty())
                    {
                        RPNOp_Operator *opTop = dynamic_cast<RPNOp_Operator *>(opStack.top());
                        if (opOp->Precedence() > opTop->Precedence()) break;

                        rpn->push_back(opTop);
                        opStack.pop();
                    }
                }
                opStack.push(opOp);
            }
        }
    }

    while (!opStack.empty())
    {
        RPNOp_Operator *opTop = dynamic_cast<RPNOp_Operator *>(opStack.top());
        if (opTop->Precedence() < 0)
        {
            errOut.error = "Mismatched Parenthesis";
            goto error;
        }
        rpn->push_back(opStack.top());
        opStack.pop();
    }

    return rpn;

error:
    while (!rpn->empty())
    {
        op = rpn->back();
        rpn->pop_back();
        delete op;
    }
    delete rpn;

    while (!opStack.empty())
    {
        op = opStack.top();
        opStack.pop();
        delete op;
    }

    return NULL;
}


//-------------------------------------------------------------------------------------
double RPN::Evaluate(const RPN::Expression &rpn, const RPN::VariableTable &variables)
{
    RPNStack stack;
    unsigned a;
    for (a = 0; a < rpn.size(); a++)
    {
        bool ok = rpn[a]->Evaluate(stack, variables);
        if (!ok)
        {
            log_warn("Error evaluating rpn expression element %d", a);
            return 0.0;
        }
    }

    if (stack.size() != 1)
    {
        log_warn("Error evaluating RPN expression, there were %d values left on the stack.  Should be one.", (int) stack.size());
        return 0.0;
    }

    return stack.top();
}


//-------------------------------------------------------------
void RPN::SetVariable(RPN::VariableTable &vt, const char *name, double value)
{
    uint32_t hash = VariableNameHash(name);
    vt[hash] = value;
}

//-------------------------------------------------------------
void RPN::SetVariable(RPN::VariableTable &vt, uint32_t nameHash, double value)
{
    vt[nameHash] = value;
}


//-------------------------------------------------------------
bool RPN::GetVariable(const RPN::VariableTable &vt, const char *name, double &value)
{
    uint32_t hash = VariableNameHash(name);
    // is the variable found?
    auto it = vt.find(hash);
    if (it == vt.end())
    {
        value = 0.0;
        return false;
    }
    value = it->second;
    return true;
}

//-------------------------------------------------------------
bool RPN::GetVariable(const RPN::VariableTable &vt, uint32_t nameHash, double &value)
{
    // is the variable found?
    auto it = vt.find(nameHash);
    if (it == vt.end())
    {
        value = 0.0;
        return false;
    }
    value = it->second;
    return true;
}


//-------------------------------------------------------------
void RPN::Dump(const RPN::Expression &rpn)
{
    log_info("RPN::Expression has %d elements", (int) rpn.size());

    unsigned a;
    for (a = 0; a < rpn.size(); a++)
    {
        rpn[a]->Dump();
    }
}


//-------------------------------------------------------------------------------------
// Simple PJW hash
uint32_t RPN::VariableNameHash(const char *s)
{
   if (s == NULL) return 0;

   uint32_t h = 0, high = 0;
   while (*s != 0)
   {
      h = (h << 4) + *s++;
      high = h & 0xF0000000;
      if (high != 0) h ^= high >> 24;
      h &= ~high;
   }
   return h;
}
