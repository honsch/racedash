#ifndef RPN_H
#define RPN_H

#include <vector>
#include <map>

#include <stdint.h>

class RPNOp;

class RPN
{
  public:
    typedef std::vector<RPNOp *>        Expression;
    typedef std::map<uint32_t, double>  VariableTable;

    struct ParseError
    {
        const char *error;
        unsigned    pos;
    };

    static RPN::Expression *Parse(const char *expression, RPN::ParseError &errOut);

    static void SetVariable(RPN::VariableTable &vt, const char *name, double value);
    static bool GetVariable(const RPN::VariableTable &vt, const char *name, double &value);

    static void SetVariable(RPN::VariableTable &vt, uint32_t nameHash, double value);
    static bool GetVariable(const RPN::VariableTable &vt, uint32_t nameHash, double &value);

    static double Evaluate(const RPN::Expression &rpn, const RPN::VariableTable &variables);

    static void Dump(const RPN::Expression &rpn);

    static uint32_t VariableNameHash(const char *name);
};


#endif // RPN_H