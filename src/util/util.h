#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>
#include <math.h>

#include <vector>
#include <algorithm>


void Quit(void);
bool WantQuit(void);

void SetSystemUTC(double utc);
void SaveSystemClockFreq(void);

double SystemTimeUTC(void); // returns UTC with jumps and slewing, also clock used by IIO for timestamps
double SystemTime(void);    // returns local time with jumps and slewing
double MonotonicTime(void); // returns constant incrementing time, not UTC

inline double Fraction(double v) { return v - floor(v); }
inline bool Compare(double a, double b, double epsilon = 0.000000001) { return fabs(a - b) < fabs(a * epsilon); }
const char *TimeString(void);
const char *DateString(void);
void TimeToString(char *out, double time, unsigned decimalDigits, bool includeHours);
class Listbox;

double ADCValueToOhms(double in, double driveOhms);


enum FL_Type { FL_All, FL_FileOnly, FL_DirOnly };
void FillListboxFromDir(Listbox *lb, const char *path, FL_Type type, bool clear);

// the caller is responsible for freeing the data returned!
void *ReadFile(const char *fname, uint32_t &filesize);

bool WriteFileRaw(const char *fname, const void *data, size_t size);

// returns distance in meters
double HaversineDistance(double lat1, double lng1, double lat2, double lng2);

// Forces 8N1
bool SetSerialParams(int fd, uint32_t baudrate);

//-----------------------------------------------------------------------
inline double DegreeToRadian(double angle)
{
    return M_PI * angle / 180.0;
}

//-----------------------------------------------------------------------
inline double RadianToDegree(double angle)
{
    return 180.0 * angle / M_PI;
}

//-----------------------------------------------------------------------
inline double Interp(double a, double b, double t)
{
    return a + (t * (b - a));
}

//-----------------------------------------------------------------------
inline float Interp(float a, float b, float t)
{
    return a + (t * (b - a));
}

//---------------------------------------------------
class LookupTable
{
  public:
    LookupTable(void) { mTable.clear(); }
    void AddEntry(double in, double out)
    {
        TableEntry e;
        e.in = in;
        e.out = out;
        mTable.push_back(e);
        std::sort(mTable.begin(), mTable.end());
    }

    double Lookup(double in)
    {
        if (mTable.empty()) return 0.0;
        if (in <= mTable.front().in) return mTable.front().out;
        if (in >= mTable.back().in)  return mTable.back().out;
        unsigned index = 0;
        for (index = 0; index < (mTable.size() - 1); index++)
        {
            // First one that is bigger than the in value will be the top bracket
            if (mTable[index + 1].in > in)
            {
                double diff = mTable[index + 1].in - mTable[index].in;
                double off  = in - mTable[index].in;
                double t = off / diff;
                return Interp(mTable[index].out, mTable[index + 1].out, t);
            }
        }
        // WTF, should be handled elsewhere
        return mTable.back().out;
    }

  private:

    struct TableEntry
    {
        double in;
        double out;
        bool operator<(const TableEntry &b) const { return in < b.in; }
    };

    std::vector<TableEntry> mTable;
};


//-----------------------------------------------------------------------
// Gets a number for testing printfs with %N.Mf specifier
double GetDigitWidthValue(unsigned wholeDigits, unsigned decimalDigits);

//-----------------------------------------------------------------------
inline float Center(float big, float small)
{
    return (big - small) / 2.0F;
}

#endif // UTIL_H