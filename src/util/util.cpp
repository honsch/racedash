#include "util/util.h"

#include <time.h>
#include <sys/time.h>
#include <sys/timex.h>
#include <stdio.h>
#include <dirent.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <termios.h>


#include "ui/listbox.h"
#include "config/config.h"
#include "sensors/sensormanager.h"

#include "log/log.h"

#define CLOCK_FREQ_CONFIG_PATH "racedash.system.clock"
#define CLOCK_FREQ "frequency"

static bool sQuit = false;
static double sLastUTCSetTime = 0.0;

void dump_termios(struct termios *t);

//-------------------------------------------------------------
void Quit(void)
{
    sQuit = true;
}

//-------------------------------------------------------------
bool WantQuit(void)
{
  return sQuit;
}


//-------------------------------------------------------------
double SystemTimeUTC(void)
{
   struct timespec now;

   clock_gettime(CLOCK_REALTIME, &now);


   double dnow = ((double) now.tv_sec) + (((double) now.tv_nsec) / 1000000000);
   return dnow;
}

//-------------------------------------------------------------
double SystemTime(void)
{
   static const double tzOffset = -7*60*60;
   return SystemTimeUTC() + tzOffset;
}

//-------------------------------------------------------------
double MonotonicTime(void)
{
   struct timespec now;

   clock_gettime(CLOCK_MONOTONIC, &now);

   double dnow = ((double) now.tv_sec) + (((double) now.tv_nsec) / 1000000000);
   return dnow;
}

//-------------------------------------------------------------
double sMonotonicReference = 0.0;
double sMonotonicOffset = 0.0;
bool   sMonotonicInit   = false;
bool   sUTCSet = false;
void SetSystemUTC(double utc)
{
    if (fabs(Fraction(utc)) > 0.01) return;

    double sysTime = SystemTimeUTC();
    double delta   = fabs(utc - sysTime);

    // If it's out, force it
    if (!sUTCSet && (delta > 0.05))
    {
        //time_t now = (time_t) utc;
        //stime(&now);
        struct timespec now;
        now.tv_sec = (time_t) utc;
        now.tv_nsec = (time_t) ((Fraction(utc) + 0.0) * 1000000000.0);
        clock_settime(CLOCK_REALTIME, &now);
        log_warn("UTC Time forced to %f from %f", utc, sysTime);
        sLastUTCSetTime = utc;
        sUTCSet = true;
        SensorManager::Get()->SystemTimeAdjusted();
        log_warn("Timestring: %s  %s", TimeString(), DateString());
    }

    double monoTime = MonotonicTime();
    if (!sMonotonicInit && sUTCSet)
    {
        sMonotonicReference = monoTime;
        sMonotonicOffset    = utc - monoTime;
        sMonotonicInit      = true;

        // Turn on PPS sync
        struct timex tx;
        tx.modes = 0;
        adjtimex(&tx);

        tx.modes = ADJ_STATUS | ADJ_NANO | ADJ_ESTERROR | ADJ_MAXERROR;
        tx.status |= STA_PPSFREQ | STA_PPSTIME;
        tx.status &= ~(STA_UNSYNC | STA_PPSJITTER | STA_PPSWANDER | STA_PPSERROR);
        tx.esterror = 0;
        tx.maxerror = 0;

        double freq = 0;
        bool ok = ConfigGet(CLOCK_FREQ_CONFIG_PATH, CLOCK_FREQ, freq);
        if (!ok)
        {
            // close until I store it in the config file, I've seen from 417xxx-421xxx
            tx.freq = 4223541;
        }
        else
        {
            tx.freq = freq;
            log_info("Loaded clock frequency: %ld", tx.freq);
        }
        adjtimex(&tx);
        log_debug("adjtimex Status is 0x%X", tx.status);
        SensorManager::Get()->SystemTimeAdjusted();
    }

#if 1
    else if ((utc - sLastUTCSetTime) >= 30.0)
    {
        struct timex tx;
        tx.modes = 0;
        tx.status = 0;
        adjtimex(&tx);

        double monoDuration = monoTime - sMonotonicReference;
        log_debug("UTC: %f   SystemTime: %f   Runtime: %f  delta: %f  ", utc, sysTime, monoDuration, delta);

        double ppsfreq = ((double)tx.ppsfreq) / 65536.0;
        log_debug("Jitter: %ld, est error: %ld:  max error: %ld  freq: %ld   PPS Freq: %f", tx.jitter, tx.esterror, tx.maxerror, tx.freq, ppsfreq);
        log_debug("Clock Bits: ");
        if ((tx.status & STA_PLL) != 0)       log_debug("  STA_PLL, ");
        if ((tx.status & STA_UNSYNC) != 0)    log_debug("  STA_UNSYNC, ");
        if ((tx.status & STA_FREQHOLD) != 0)  log_debug("  STA_FREQHOLD, ");
        if ((tx.status & STA_PPSSIGNAL) != 0) log_debug("  STA_PPSSIGNAL, ");
        if ((tx.status & STA_PPSJITTER) != 0) log_debug("  STA_PPSJITTER, ");
        if ((tx.status & STA_PPSWANDER) != 0) log_debug("  STA_PPSWANDER, ");
        if ((tx.status & STA_PPSERROR) != 0)  log_debug("  STA_PPSERROR, ");
        if ((tx.status & STA_MODE) != 0)      log_debug("  FLL Mode\n");
        else                                  log_debug("  PLL Mode\n");

        sLastUTCSetTime = utc;

        // Clear maxerror
        tx.modes = ADJ_MAXERROR;
        tx.esterror = 0;
        tx.maxerror = 0;
        adjtimex(&tx);
    }
#endif
}

//--------------------------------------------------------------------
void SaveSystemClockFreq(void)
{
    struct timex tx;
    tx.modes = 0;
    tx.status = 0;
    int ret = adjtimex(&tx);

    double ppsfreq = ((double)tx.ppsfreq) / 65536.0;
    log_info("Clock ret %d, est error: %ld:  freq: %ld   PPS Freq: %f\n", ret, tx.esterror, tx.freq, ppsfreq);
    log_info("Clock Bits:\n    ");
    if ((tx.status & STA_PLL) != 0)       log_debug("STA_PLL, ");
    if ((tx.status & STA_UNSYNC) != 0)    log_debug("STA_UNSYNC, ");
    if ((tx.status & STA_FREQHOLD) != 0)  log_debug("STA_FREQHOLD, ");
    if ((tx.status & STA_PPSSIGNAL) != 0) log_debug("STA_PPSSIGNAL, ");
    if ((tx.status & STA_PPSJITTER) != 0) log_debug("STA_PPSJITTER, ");
    if ((tx.status & STA_PPSWANDER) != 0) log_debug("STA_PPSWANDER, ");
    if ((tx.status & STA_PPSERROR) != 0)  log_debug("STA_PPSERROR, ");
    if ((tx.status & STA_MODE) != 0)      log_debug("FLL Mode\n");
    else                                  log_debug("PLL Mode\n");

    ConfigSet(CLOCK_FREQ_CONFIG_PATH, CLOCK_FREQ, tx.freq);
}

//--------------------------------------------------------------------
static char sTimeString[32];
const char *TimeString(void)
{
  time_t now = (time_t) SystemTime(); // Use local time
  struct tm now_tm;

  localtime_r(&now, &now_tm);

  sprintf(sTimeString, "%d:%02d:%02d", now_tm.tm_hour, now_tm.tm_min, now_tm.tm_sec);

  return sTimeString;
}

//--------------------------------------------------------------------
static char sDateString[32];
const char *DateString(void)
{
  time_t now = (time_t) SystemTime(); // Use local time
  struct tm now_tm;

  localtime_r(&now, &now_tm);

  sprintf(sDateString, "%4d-%02d-%0d", now_tm.tm_year + 1900, now_tm.tm_mon+1, now_tm.tm_mday);

  return sDateString;
}

//-----------------------------------------------------------------------
static double sDecimalDigitScale[4] = {0.0, 10.0, 100.0, 1000.0 };

void TimeToString(char *out, double time, unsigned decimalDigits, bool hours)
{
    uint64_t utime = (uint64_t) time;
    if (decimalDigits > 3) decimalDigits = 3;
    unsigned h = utime / 3600;
    unsigned m = (utime / 60) % 60;
    unsigned s = utime % 60;
    unsigned f = (Fraction(time) * sDecimalDigitScale[decimalDigits]);

    char frac[8];
    frac[0] = 0;
    if (decimalDigits > 0)
    {
        char fmt[8];
        sprintf(fmt, ".%%0%dd", decimalDigits);
        sprintf(frac, fmt, f);
        //printf("Frac %s   Fmt: %s   f:%d \n", frac, fmt, f);
    }
    if (hours) sprintf(out, "%2d:%02d:%02d%s", h, m, s, frac);
    else       sprintf(out, "%02d:%02d%s", m, s, frac);
}

//-----------------------------------------------------------------------
void FillListboxFromDir(Listbox *lb, const char *path, FL_Type type, bool clear)
{
    if (lb   == NULL) return;
    if (path == NULL) return;

    // Scan the files and add them to the lisbox
    if (clear ) lb->Clear();

    struct dirent *dir;
    DIR *d = opendir(path);
    if (d == NULL) return;

    dir = readdir(d);
    while (dir != NULL)
    {
        // skip hidden files and directories
        if (dir->d_name[0] != '.')
        {
            if       (type == FL_All)                                  lb->AddString(dir->d_name, false);
            else if ((type == FL_FileOnly) && (dir->d_type == DT_REG)) lb->AddString(dir->d_name, false);
            else if ((type == FL_DirOnly)  && (dir->d_type == DT_DIR)) lb->AddString(dir->d_name, false);
        }
        dir = readdir(d);
    }
    closedir(d);
    lb->Sort();
}

//-----------------------------------------------------------------------
void *ReadFile(const char *fname, uint32_t &filesize)
{
    filesize = 0;
    FILE *inf = fopen(fname, "rb");
    if (inf == NULL) return NULL;

    fseek(inf, 0, SEEK_END);
    filesize = ftell(inf);
    fseek(inf, 0, SEEK_SET);

    void *mem = malloc(filesize + 1);
    memset(mem, 0, filesize + 1);
    ssize_t amount = fread(mem, 1, filesize, inf);
    fclose(inf);
    if (amount <= 0)
    {
        free(mem);
        filesize = 0;
        return NULL;
    }
    filesize = (uint32_t)amount;
    return mem;
}

//-----------------------------------------------------------------------
bool WriteFileRaw(const char *fname, const void *data, size_t size)
{
    int fd = open(fname, O_WRONLY);
    if (fd < 0) return false;
    ssize_t written = write(fd, data, size);
    close(fd);
    return  (written == (ssize_t)size);
}


//-----------------------------------------------------------------------
double HaversineDistance(double lat1, double lon1, double lat2, double lon2)
{
    const static double EarthRadius_m = 6372800.0;

    double latRad1 = DegreeToRadian(lat1);
    double lonRad1 = DegreeToRadian(lon1);
    double latRad2 = DegreeToRadian(lat2);
    double lonRad2 = DegreeToRadian(lon2);

    double diffLa = latRad2 - latRad1;
    double diffLo = lonRad2 - lonRad1;

    double computation = asin(sqrt(sin(diffLa / 2.0) * sin(diffLa / 2.0) + cos(latRad1) * cos(latRad2) * sin(diffLo / 2.0) * sin(diffLo / 2.0)));
    return 2.0 * EarthRadius_m * computation;
}

//-----------------------------------------------------------------------
double HaversineDistanceX(double lat1, double lon1, double lat2, double lon2)
{
    double dx, dy, dz;
    lat1 -= lat2;

    double latRad1 = DegreeToRadian(lat1);
    double lonRad1 = DegreeToRadian(lon1);
    double lonRad2 = DegreeToRadian(lon2);

    dz = sin(lonRad1) - sin(lonRad2);
    dx = cos(latRad1) * cos(lonRad1) - cos(lonRad2);
    dy = sin(latRad1) * cos(lonRad1);
    return asin(sqrt(dx * dx + dy * dy + dz * dz) / 2.0) * 2.0 * 6372800.0;
}


//--------------------------------------------------------------
// Forces 8N1
bool SetSerialParams(int fd, uint32_t baudrate)
{
    fcntl(fd, F_SETFL, 0);

    // Get the current options for the port...
    struct termios options;
    tcgetattr(fd, &options);
    cfmakeraw(&options);

    speed_t baud = B0;
    switch (baudrate)
    {
        case    300: baud =    B300; break;
        case   1200: baud =   B1200; break;
        case   2400: baud =   B2400; break;
        case   4800: baud =   B4800; break;
        case   9600: baud =   B9600; break;
        case  19200: baud =  B19200; break;
        case  38400: baud =  B38400; break;
        case  57600: baud =  B57600; break;
        case 115200: baud = B115200; break;
        case 230400: baud = B230400; break;
        default: return false;     // Unsupported baudrate
    }

    cfsetispeed(&options, baud);
    cfsetospeed(&options, baud);

    // Enable the receiver and set local mode...
    // Turn off character processing, set to 8N1
    options.c_cflag &= ~(CSIZE | PARENB | CRTSCTS);
    options.c_cflag |= (CLOCAL | CREAD | CS8 | CREAD | CLOCAL);

    // turn off output processing
    options.c_oflag = 0;

    // Turn off input processing
    options.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    // Turn off flow control
    options.c_iflag &= ~(IXON | IXOFF | IXANY | INLCR | ICRNL);

    // One byte is enough to read, no inter-character timer
    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 0;

    // Set the new options for the port...
    tcsetattr(fd, TCSANOW, &options);
    return true;
}


//-----------------------------------------------------------------------
// Gets a number for testing printfs with %N.Mf specifier
double GetDigitWidthValue(unsigned wholeDigits, unsigned decimalDigits)
{
    double w = 9;
    double d = 0.0;
    while (wholeDigits > 1)
    {
        w *= 10.0;
        --wholeDigits;
    }
    if (decimalDigits > 0)
    {
        d = 0.5;
        while (decimalDigits > 1)
        {
            d *= 0.1;
            --decimalDigits;
        }
    }
    return w + d;
}


//--------------------------------------------------------------
void dump_termios(struct termios *t)
{
    log_debug("termios.c_iflag: %08x", t->c_iflag);
    log_debug("termios.c_oflag: %08x", t->c_oflag);
    log_debug("termios.c_cflag: %08x", t->c_cflag);
    log_debug("termios.c_lflag: %08x", t->c_lflag);
    unsigned a;
    for (a = 0; a <  NCCS; a++)
    {
        log_debug("termios.c_cc[%d] = %02X", a, t->c_cc[a]);
    }
}


//--------------------------------------------------------------------
// Returns the value of the sensor in ohms (to ground) given the sensor is driven by drive_ohms to VREF
double ADCValueToOhms(double in, double driveOhms)
{
    if (in < 0.00025F) return 0.0F; // short to ground
    if (in >= 1.0F)    return 1000000.0F; // open, return 10M ohms

    return driveOhms / ((1.0F / in) - 1.0F);
}