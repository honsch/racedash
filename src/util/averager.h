#ifndef AVERAGE_H
#define AVERAGE_H

#include <stdio.h>

//---------------------------------------------
class Averager
{
  public:
    Averager(unsigned historyLen) :
        mHistoryLen(historyLen),
        mHistorySum(0.0)
    {
        mHistory = new double[mHistoryLen];
        Clear();
    }

   ~Averager()
    {
        delete[] mHistory;
        mHistory = NULL;
        mHistoryLen = 0;
    }

    void Clear(void)
    {
        mNextIndex  = 0;
        mHistorySum = 0.0;
        memset(mHistory, 0, sizeof(double) * mHistoryLen);
    }

    double AddSample(double value)
    {
        mHistorySum         -= mHistory[mNextIndex];
        mHistory[mNextIndex] = value;
        mHistorySum         += value;
        mNextIndex           = (mNextIndex + 1) % mHistoryLen;

        //printf("Val: %f  Sum: %f\n", value, mHistorySum);
        return mHistorySum / (double) mHistoryLen;
    }

    double Average(void) const
    {
        return mHistorySum / (double) mHistoryLen;
    }

  private:

    unsigned mHistoryLen;
    unsigned mNextIndex;
    double   mHistorySum;
    double  *mHistory;
};


#endif