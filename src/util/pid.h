#ifndef PID_H
#define PID_H

class PIDController
{
  public:
    PIDController(double pGain, double iGain, double dGain, double iClamp) :
        mPGain(pGain),
        mIGain(iGain),
        mDGain(dGain),
        mIClamp(iClamp),
        mPrevError(0.0),
        mErrorSum(0.0)
    {
    }

    void Reset(void)
    {
        mPrevError = 0.0;
        mErrorSum  = 0.0;
    }

    double Update(double newError)
    {
        mErrorSum += newError;
        if      (mErrorSum >  mIClamp) mErrorSum =  mIClamp;
        else if (mErrorSum < -mIClamp) mErrorSum = -mIClamp;

        double errorDelta = newError - mPrevError;
        mPrevError = newError;
        return (newError * mPGain) * (mErrorSum * mIGain) - (errorDelta * mDGain);
    }

  private:

    double mPGain;
    double mIGain;
    double mDGain;
    double mIClamp;

    double mPrevError;
    double mErrorSum;
};


#endif //PID_H