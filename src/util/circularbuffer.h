#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H

#include <mutex>
#include <string.h>

template <class T, size_t N>
class CircularBuffer
{
  public:

    //------------------------------------------------------
    CircularBuffer(void) :
        mBuffer(),
        mHead(0),
        mCount(0)
    {
    }

    //------------------------------------------------------
    size_t PushCount(void) const
    {
        return mCount;
    }

    //------------------------------------------------------
    void Push(double timestamp, const T &item)
    {
        mLock.lock();
        if (mBuffer[mHead].timestamp != timestamp)
        {
            mHead = (mHead + 1) % N;
            mBuffer[mHead].timestamp = timestamp;
            mBuffer[mHead].value     = item;
            ++mCount;
        }
        else
        {
            printf("Dropped dupe.\n");
        }
        mLock.unlock();
    }

    //------------------------------------------------------
    // 0 is newest, 1 is previous, 2 is sample before that and so on
    T &operator[](size_t t)
    {
        mLock.lock();
        // Clamp to length of buffer but don't wrap all the way around
        if (t >= N) t = N - 1;

        size_t index = mHead - t;
        if (t > mHead) index += N;
        T &ret = mBuffer[index].value;
        mLock.unlock();
        return ret;
    }

    //------------------------------------------------------
    // Get the newest and previous sample atomically
    void LastTwo(size_t &pushCount, T &newest, T &previous) const
    {
        mLock.lock();
        pushCount = mCount;
        newest    = mBuffer[mHead].value;
        previous  = (mHead == 0) ? mBuffer[N - 1].value : mBuffer[mHead - 1].value;
        mLock.unlock();
    }

    //------------------------------------------------------
    // Get the newest sample
    void NewestSample(size_t &pushCount, T &newest) const
    {
        mLock.lock();
        pushCount = mCount;
        newest    = mBuffer[mHead].value;
        mLock.unlock();
    }

    //------------------------------------------------------
    // Get the samples around a give time
    // return zero on success,
    //        -1 if the buffer doesn't have a pair old enough,
    //        +1 if no pair new enough
    int FindTimestampBrackets(double timestamp, T &early, T &late) const
    {
        mLock.lock();
        if ((mCount < 2) || (mBuffer[mHead].timestamp <= timestamp))
        {
            mLock.unlock();
            return 1;
        }

        unsigned lateIndex = mHead;
        unsigned tests;

        //printf("\n");
        for (tests = 0; (tests < (N-1)) && (tests < (mCount-1)); tests++)
        {
            unsigned earlyIndex = (lateIndex == 0) ? (N - 1) : (lateIndex - 1);

            //printf("Testing %2d %.3f vs %.3f %.3f", lateIndex, timestamp, mBuffer[earlyIndex].timestamp, mBuffer[lateIndex].timestamp);
            if ((mBuffer[earlyIndex].timestamp <= timestamp) && (timestamp < mBuffer[lateIndex].timestamp))
            {
                early = mBuffer[earlyIndex].value;
                late  = mBuffer[lateIndex].value;
                mLock.unlock();
                //printf("Tests: %d of %d\n", tests, N);
                return 0;
            }
            else
            {
                //printf("  FAIL\n");
            }
            lateIndex = earlyIndex;
        }

        mLock.unlock();
        return -1;
    }

    //------------------------------------------------------
    double NewestSampleTime(void) const
    {
        double val = 0.0;
        mLock.lock();
        if (mCount > 0) val = mBuffer[mHead].timestamp;
        mLock.unlock();
        return val;
    }

    //------------------------------------------------------
    double OldestSampleTime(void) const
    {
        double val = 0.0;
        mLock.lock();
        val = mBuffer[(mHead+1) % N].timestamp;
        mLock.unlock();
        return val;
    }

  private:
    struct Element
    {
        double timestamp;
        T      value;
    };

    mutable std::mutex mLock;
    Element    mBuffer[N];
    size_t     mHead;
    size_t     mCount;
};



#endif //CIRCULAR_BUFFER_H
