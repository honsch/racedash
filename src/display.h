/******************************************************************************

      DisplayController Driver

*******************************************************************************/
#ifndef DISPLAY_H
#define DISPLAY_H

#include <stdint.h>
#include <linux/fb.h>
#include <time.h>
#include <vector>

#define GL_GLEXT_PROTOTYPES 1
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <xf86drm.h>
#include <xf86drmMode.h>

#include "nanovg.h"

#undef DisplayWidth
#undef DisplayHeight


//                    AARRGGBB
#define CLEAR       0x00000000
#define BLACK       0xFF000000
#define WHITE       0xFFFFFFFF
#define RED         0xFFFF0000
#define GREEN       0xFF00FF00
#define BLUE        0xFF0000FF
#define MEDIUM_BLUE 0xFF000080
#define DARK_BLUE   0xFF000040
#define YELLOW      0xFFFFFF00
#define CYAN        0xFF00FFFF
#define MEDIUM_CYAN 0xFF008080
#define DARK_CYAN   0xFF004040
#define MAGENTA     0xFFFF00FF
#define PURPLE      0xFF800080
#define BROWN       0xFF808048
#define MEDIUM_GREY 0xFF444444
#define DARK_GREY   0xFF222222
#define GREY        0xFF808080
#define LIGHT_GREY  0xFFC0C0C0

// You cay specify any size you want, these are just for convenience
#define Font_Tiny     8.0F
#define Font_Small   16.0F
#define Font_Regular 36.0F
#define Font_Large   48.0F
#define Font_Huge    96.0F

enum FontFace
{
   Font_Mono,
   Font_Bookman,
   Font_LiberationSans,
   Font_PlayerSymbols,
   Font_Cursors,
   _Font_Max,
};

typedef void *ImageHandle;

class DisplayController
{
  public:

   enum DisplayRotation { None, CCW_90, CW_90, CW_180 };

   // These dimensions are PRE ROTATION
   DisplayController(const char *device, unsigned desiredWidth, unsigned desiredHeight, DisplayRotation rotation = None);
  ~DisplayController();

   bool InitOK(void) const { return mInitOK; }

   void BeginFrame(void);
   int Swap(void);

   void Clear(uint32_t color);

   void DrawString(const char *s, float xpos, float ypos, uint32_t fgcolor, float fontsize, int fontFace = Font_Mono);
   void DrawStringRect(const char *s, float xpos, float ypos, float width, float height, uint32_t fgcolor, float fontsize, int fontFace = Font_Mono);

   struct LineData
   {
      float x1;
      float y1;
      float x2;
      float y2;
   };

   struct VertexData
   {
      float x;
      float y;
   };

   void Polygon(VertexData *verts, unsigned count, float thickness, uint32_t outlineColor, uint32_t fillcolor);

   void Line(LineData *lines, unsigned count, float thickness, uint32_t color);

   void Line(float x1, float y1, float x2, float y2, float thickness, uint32_t color);
   void Rect(float x1, float y1, float width, float height, float thickness, uint32_t color);
   void RectRounded(float x1, float y1, float width, float height, float radius, float thickness, uint32_t color);
   void Circle(float cx, float cy, float r, float thickness, uint32_t color);
   void Ellipse(float cx, float cy, float rx, float ry, float thickness, uint32_t color);

   void RectFilled(float x1, float y1, float width, float height, uint32_t color);
   void RectRoundedFilled(float x1, float y1, float width, float height, float radius, uint32_t color);
   void CircleFilled(float cx, float cy, float r, uint32_t color);
   void EllipseFilled(float cx, float cy, float rx, float ry, uint32_t color);

   // Positive dir is CCW, negative is CW
   void Arc(float cx, float cy, float r, float radStart, float radEnd, int dir, float thickness, uint32_t color);


   void GetFontSize(float fontsize, float &width, float &height, int fontFace = Font_Mono);
   void GetStringExtents(float fontsize, int fontFace, const char *string, float &width, float &height);

   ImageHandle LoadImage(const char *fname);

   void UnloadImage(ImageHandle handle);
   void DrawImage(ImageHandle handle, float x, float y, float alpha);

   double LastFrameTime(void) const { return mLastFrameTime; }

   inline float DisplayWidth(void)  const { return mDisplayWidth; }
   inline float DisplayHeight(void) const { return mDisplayHeight; }
   inline float DisplayAspectRatio(void) const { return mDisplayAspectRatio; }

   DisplayRotation GetDisplayRotation(void) const { return mDisplayRotation; }
   void SetDisplayRotation(DisplayRotation rot);

   // This only rotates by the DisplayRotation enum, not the transform stack.
   void RotateCoord(float &x, float &y) const;



   // Transform stack
   void PopTransform(void);
   void PushScale(float sx, float sy);
   void PushTranslate(float tx, float ty);
   void PushRotation(float angleRad);

  private:

   unsigned mCurrentBuffer;

   #define NUM_DISPLAY_BUFFERS 2

   struct FrameBufferInfo
   {
      uint8_t *mBuffer;
      uint32_t mBufferID;
   };

   FrameBufferInfo mFrameBuffers[NUM_DISPLAY_BUFFERS];

   int      mDrmFD;
   uint32_t mCrtcID;
   uint32_t mChosenEncoderID;
   drmModeConnector mChosenConnector;
   drmModeModeInfo  mChosenMode;

   struct gbm_device  *mGbm;
   struct gbm_surface *mGbmSurface;
   int32_t             mGbmFormat;

   struct FramebufferData
   {
      uint32_t       fbID;
   };

   #define MAX_FRAMEBUFFERS 8
   FramebufferData mFramebuffers[MAX_FRAMEBUFFERS];
   unsigned        mFramebufferNext;
   FramebufferData *FramebufferFromBO(struct gbm_bo *bo);
   static void FramebufferDestroyCallback(struct gbm_bo *bo, void *data);

   struct gbm_bo *mDisplayedBO;

   double mLastSwapTime;
   double mLastFrameTime;

   bool mInitOK;

   bool InitGL(int displayFD);

   EGLDisplay mEGLDisplay;
   EGLSurface mEGLSurface;
   EGLContext mEGLContext;

   NVGcontext *mVG;

   float mDisplayWidth;
   float mDisplayHeight;
   float mDisplayAspectRatio;

   DisplayRotation mDisplayRotation;
   float           mDisplayRotationAngle;
   float           mDisplayTranslateX;
   float           mDisplayTranslateY;

   float mRealDisplayWidth;
   float mRealDisplayHeight;
   float mRealDisplayAspectRatio;


   int mFonts[_Font_Max];

   inline uint16_t MakeColor(uint32_t color) const
   {
      return color; //((color & 0xF8) >> 3) | ((color & 0xFC00) >> 5) | ((color & 0xF80000) >> 8);
   }

   inline float RedF(uint32_t color)
   {
      return ((color & 0x00FF0000) >> 16) / 255.0F;
   }

   inline float GreenF(uint32_t color)
   {
      return ((color & 0x0000FF00) >> 8) / 255.0F;
   }

   inline float BlueF(uint32_t color)
   {
      return (color & 0x000000FF) / 255.0F;
   }

   inline float AlphaF(uint32_t color)
   {
      return ((color & 0xFF000000) >> 24) / 255.0F;
   }

   inline void PushCurrentTransform(void)
   {
       TransformState t;
       nvgCurrentTransform(mVG, t.t);
       mTransformStack.push_back(t);
   }

   void DumpEGLConfig(void *conf);

   struct Image
   {
      int      mHandle;
      int      mWidth;
      int      mHeight;
   };

   struct TransformState
   {
      float t[6];
   };
   std::vector<TransformState> mTransformStack;

};

extern DisplayController *gDisplay;

#endif // DISPLAY_H
