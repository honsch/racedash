//
//  Biquad.cpp
//
//  Created by Nigel Redmon on 11/24/12
//  EarLevel Engineering: earlevel.com
//  Copyright 2012 Nigel Redmon
//
//  For a complete explanation of the Biquad code:
//  http://www.earlevel.com/main/2012/11/26/biquad-c-source-code/
//------------------------------------------------
//
//  License:
//
//  This source code is provided as is, without warranty.
//  You may copy and distribute verbatim copies of this document.
//  You may modify and use this source code to create binary code
//  for your own purposes, free or commercial.
//

#include <math.h>
#include "biquad.h"


//------------------------------------------------
Biquad::Biquad(Biquad::Type type, double Fc, double Q, double peakGainDB) :
    mType(type),
    mFc(Fc),
    mQ(Q),
    mPeakGain(peakGainDB)
{
    CalcBiquad();
    z1 = z2 = 0.0;
}

//------------------------------------------------
Biquad::~Biquad()
{
}

//------------------------------------------------
void Biquad::CalcBiquad(void)
{
    double norm;
    double K = tan(M_PI * mFc);
    switch (mType)
    {
        case Lowpass:
            norm = 1.0 / (1.0 + K / mQ + K * K);
            a0 = K * K * norm;
            a1 = 2.0 * a0;
            a2 = a0;
            b1 = 2.0 * (K * K - 1.0) * norm;
            b2 = (1.0 - K / mQ + K * K) * norm;
            break;
            
        case Highpass:
            norm = 1.0 / (1.0 + K / mQ + K * K);
            a0 = 1.0 * norm;
            a1 = -2.0 * a0;
            a2 = a0;
            b1 = 2.0 * (K * K - 1.0) * norm;
            b2 = (1.0 - K / mQ + K * K) * norm;
            break;
            
        case Bandpass:
            norm = 1.0 / (1.0 + K / mQ + K * K);
            a0 = K / mQ * norm;
            a1 = 0.0;
            a2 = -a0;
            b1 = 2.0 * (K * K - 1.0) * norm;
            b2 = (1.0 - K / mQ + K * K) * norm;
            break;
            
        case Notch:
            norm = 1.0 / (1.0 + K / mQ + K * K);
            a0 = (1.0 + K * K) * norm;
            a1 = 2.0 * (K * K - 1.0) * norm;
            a2 = a0;
            b1 = a1;
            b2 = (1.0 - K / mQ + K * K) * norm;
            break;
            
    }
}
