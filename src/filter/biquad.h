//
//  Biquad.h
//
//  Created by Nigel Redmon on 11/24/12
//  EarLevel Engineering: earlevel.com
//  Copyright 2012 Nigel Redmon
//
//  For a complete explanation of the Biquad code:
//  http://www.earlevel.com/main/2012/11/25/biquad-c-source-code/
//
//  License:
//
//  This source code is provided as is, without warranty.
//  You may copy and distribute verbatim copies of this document.
//  You may modify and use this source code to create binary code
//  for your own purposes, free or commercial.
//

#ifndef BIQUAD_H
#define BIQUAD_H


//------------------------------------------
class Biquad
{
  public:
    enum Type
    {
        Lowpass = 0,
        Highpass,
        Bandpass,
        Notch
    };

    Biquad(Type type, double Fc, double Q, double peakGainDB);
   ~Biquad();
    double Process(double in);
    
  protected:
    void CalcBiquad(void);

    double a0, a1, a2, b1, b2;
    double z1, z2;
    Type   mType;
    double mFc;
    double mQ;
    double mPeakGain;
};

//------------------------------------------
inline double Biquad::Process(double in)
{
    double out = in * a0 + z1;
    z1 = in * a1 + z2 - b1 * out;
    z2 = in * a2 - b2 * out;
    return out;
}

#endif // BIQUAD_H
