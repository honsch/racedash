#include "butterworth.h"
#include <cmath>

#include <stdio.h>
//------------------------------------------------
ButterworthLP::ButterworthLP(int order, double fc)
{
    int sections = order / 2;
    double poleSpacing = M_PI / (double) order;
    double pole = poleSpacing / 2.0;
    int a;
    for (a = 0; a < sections; a++)
    {
        double q = 1.0 / (2.0 * cos(pole));
        printf("Butterworth section %d Q: %f\n", a, q);
        mBiquads.emplace_back(Biquad(Biquad::Lowpass, fc, q, 0.0));
        pole += poleSpacing;
    }
}

//------------------------------------------------
ButterworthLP::~ButterworthLP()
{

}