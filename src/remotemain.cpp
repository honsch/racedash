#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <signal.h>

#include "display.h"
#include "touchscreen.h"
#include "util/util.h"
#include "ui/ui.h"

#include "config/config.h"
#include "sensors/sensormanager.h"

#include "screens/gettext.h"
#include "screens/musicplayer.h"
#include "screens/dashboardgauges.h"

#include "datalogger/datalogger.h"

#include "gpio/gpio.h"

#include "log/log.h"
static FILE *sLogFile = NULL;

//----------------------------------------------------
static void sigintHandler(int s)
{
   Quit();
}

//----------------------------------------------------
static void InitLog(const char *logfile)
{
   // Turn off echoing all logs to the screen
   log_clear_callbacks();

   //log_set_quiet(true);
   // Only echo warning and higher to the screen
   log_set_level(LOG_WARN);

   sLogFile = fopen(logfile, "a+");
   if (sLogFile == NULL) sLogFile = stdout;
   fprintf(sLogFile, "\n\n\n==================    Starting a new log   ==================\n");

   // Uncomment the line for the desired level (and greater) of logging
   //log_add_fp(sLogFile, LOG_TRACE);
   //log_add_fp(sLogFile, LOG_DEBUG);
   log_add_fp(sLogFile, LOG_INFO);
   //log_add_fp(sLogFile, LOG_WARN);
   //log_add_fp(sLogFile, LOG_ERROR);
   //log_add_fp(sLogFile, LOG_FATAL);
}

//----------------------------------------------------
static void CloseLog(void)
{
   if ((sLogFile != NULL) && (sLogFile != stdout))
   {
      fprintf(sLogFile, "==================    Log end   ==================\n");
      fflush(sLogFile);
      fclose (sLogFile);
      sLogFile = NULL;
   }
}

//--------------------------------------------------------------------------
// Oh the hacks....
void TX(LoraRadio *radio)
{
    static unsigned count = 0;
    ++count;

    if ((count % 250) > 245)
    {
        TelemetryPitRequest pr;
        pr.header.type = TELEMETRY_TYPE_PIT;
        pr.header.size = sizeof(pr);
        pr.pitLaps = 2;
        radio->Send((uint8_t *)&pr, pr.header.size);
        printf("Sent pit\n");
    }
    else if ((count % 150) < 3)
    {
        TelemetryLapData ld;
        ld.header.type = TELEMETRY_TYPE_LAP_DATA;
        ld.header.size = sizeof(ld);
        ld.lapCompleted = count / 150;
        ld.lapDuration = count;
        ld.lapsFuelRemaining = 10 - ld.lapCompleted;

        radio->Send((uint8_t *)&ld, ld.header.size);
        printf("Sent lap\n");
    }
    else
    {
        TelemetryRTData rt;
        rt.header.type = TELEMETRY_TYPE_RT_DATA;
        rt.header.size = sizeof(rt);

        double now = SystemTimeUTC();

        rt.kmh         = (1.0 + sin(now)) * 100.0;
        rt.waterTemp   = 100 + (1.0 + sin(now * 1.3)) * 6.0;
        rt.rpm         = (1.0 + sin(now * 2.3)) * 350.0;
        rt.fuelRemain    = (1.0 + sin(now * 0.2)) * (7.5 * 256.0);
        rt.battery     = 143;
        rt.oilPressure = 50;
        rt.afr         = 125;

        rt.longitude =  -122.8969645 + (sin(now / 3.0) * 0.0007);
        rt.latitude  =    49.2272275 + (cos(now / 3.0) * 0.0007);

        rt.stintTime = count;

        radio->Send((uint8_t *)&rt, rt.header.size);
    }
}


//----------------------------------------------------
int main(int argc, char **argv)
{
   InitLog("remotedash.log");

   log_info("Starting Remotedash!");

   struct sigaction sa;
   sa.sa_handler = &sigintHandler;
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sigaction(SIGINT, &sa, NULL);
   //sigaction(SIGUSR0, &sa, NULL);

   GpioInit();

   LoraRadio *radio = new LoraRadio;
   radio->SetRXTimeout(100);

   printf("Main loop\n");
   while(!WantQuit())
   {
      TX(radio);
      usleep(100000);
   }

   GpioShutdown();

   delete radio;
   printf("Shutdown\n");

   CloseLog();
   return 0;
}

