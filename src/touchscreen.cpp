#include "touchscreen.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <linux/input.h>
#include "config/config.h"

#include "log/log.h"
#include "display.h"

//----------------------------------------------------------------------------------
static double EventTimeToSystemTime(const struct input_event *e)
{
   double st = ((double) e->time.tv_sec) + (((double)e->time.tv_usec) / 1000000.0);
   // I have no idea if event times are based on CLOCK_REALTIME
   return st;
}


//----------------------------------------------------------------------------------
Touchscreen::Touchscreen(void)
{
   mDeviceName = NULL;
   mDevFD = -1;
   mCallback = NULL;
   mCallbackContext = NULL;

   // rough numbers to get it close if there's no calibration
   mCalOffsetX =  0.003778F;
   mCalOffsetY = -0.009542F;
   mCalScaleX  =  0.000242F;
   mCalScaleY  =  0.000245F;

   mPreviousEvent = { 0, 0, 0, 0, 0, 0 };

   mTotalEvents = 0;

   double tmp;
   if (ConfigGet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_OFFSET_X, tmp)) mCalOffsetX = tmp;
   if (ConfigGet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_OFFSET_Y, tmp)) mCalOffsetY = tmp;
   if (ConfigGet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_SCALE_X,  tmp)) mCalScaleX  = tmp;
   if (ConfigGet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_SCALE_Y,  tmp)) mCalScaleY  = tmp;

   log_info("Touchscreen Calibration: Scale: %f, %f   Offset %f, %f", mCalScaleX, mCalScaleY, mCalOffsetX, mCalOffsetY);
}

//----------------------------------------------------------------------------------
Touchscreen::~Touchscreen()
{
   if (mDeviceName != NULL) free(mDeviceName);
   if (mDevFD > 0) close(mDevFD);
   mDeviceName = NULL;
   mDevFD = -1;
}

//----------------------------------------------------------------------------------
int Touchscreen::Init(const char *device)
{
   if (device == NULL) return -1;
   if (mDeviceName != NULL) free(mDeviceName);
   mDeviceName = strdup(device);

   return InitInternal();
}

//----------------------------------------------------------------------------------
int Touchscreen::InitInternal(void)
{
   if (mDeviceName == NULL) return -1;

   if (mDevFD > 0) close(mDevFD);
   mDevFD = open(mDeviceName, O_RDONLY | O_NONBLOCK);
   if (mDevFD < 0)
   {
      //log_warn("Touchscreen device open failure!  %d", mDevFD);
      mDevFD = -1;
      return errno;
   }
   log_info("Touchscreen device open success.");
   return 0;
}

//----------------------------------------------------------------------------------
void Touchscreen::RegisterCallback(InputEventCallback callback, void *context)
{
   mCallback        = callback;
   mCallbackContext = context;
}

//----------------------------------------------------------------------------------
void Touchscreen::SetCalibration(float offX, float offY, float scaleX, float scaleY)
{
   mCalOffsetX = offX;
   mCalOffsetY = offY;
   mCalScaleX  = scaleX;
   mCalScaleY  = scaleY;
}

//----------------------------------------------------------------------------------
void Touchscreen::GetCalibration(float &offX, float &offY, float &scaleX, float &scaleY) const
{
   offX   = mCalOffsetX;
   offY   = mCalOffsetY;
   scaleX = mCalScaleX;
   scaleY = mCalScaleY;
}

//----------------------------------------------------------------------------------
int Touchscreen::Update(void)
{
   if (mDevFD < 0)
   {
      if (InitInternal() < 0) return -1;
   }

   int count = 0;
   do
   {
      struct input_event e;
      ssize_t bytes = read(mDevFD, &e, sizeof(e));

      // file error of some sort, close device and reopen next time
      if (bytes < 0)
      {
         if (errno != EAGAIN)
         {
            close(mDevFD);
            mDevFD = -1;
         }
         return (int) bytes;
      }

      if (bytes == 0) return count;         // no more data
      if (bytes != sizeof(e)) return count; // data error of some sort

      switch (e.type)
      {
         case EV_SYN:
            mPreviousEvent.time = EventTimeToSystemTime(&e);
            ++mTotalEvents;
            mTransformedEvent = mPreviousEvent;
            gDisplay->RotateCoord(mTransformedEvent.screenX, mTransformedEvent.screenY);
            if (mCallback != NULL) mCallback(mCallbackContext, &mTransformedEvent);
            ++count;
            break;

         case EV_KEY:
            if (e.code == BTN_TOUCH) mPreviousEvent.pressed = e.value;
            break;


         case EV_ABS:
            if (e.code == ABS_X)
            {
               mPreviousEvent.rawX    = (unsigned) e.value;
               mPreviousEvent.screenX = (((float) e.value) * mCalScaleX) + mCalOffsetX;
            }

            else if (e.code == ABS_Y)
            {
               mPreviousEvent.rawY    = (unsigned) e.value;
               mPreviousEvent.screenY = (((float) e.value) * mCalScaleY) + mCalOffsetY;
            }
            break;

         default:
            break;
      }
   } while (1);

   return count;
}
