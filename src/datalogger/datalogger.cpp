#include "datalogger.h"

#include "util/util.h"

#include <vector>
#include <map>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "log/log.h"

#define LOG_HEADER_MAGIC     0x52444C42 // 'RLDB'
#define LOG_TIME_INDEX_MAGIC 0x4C475449 // 'LGTI'

#define LOG_VERSION      0x000002

DataLogger *DataLogger::sDataLogger = NULL;

//----------------------------------------------------------
bool DataLogger::LogTimeIndexBuilder::Write(int fd)
{
    if (bufferUsed == 0) return true;

    LogTimeIndex lti;
    lti.magic        = LOG_TIME_INDEX_MAGIC;
    lti.numVariables = numVariables;
    lti.time         = time;

    ssize_t written;
    written  = write(fd, &lti, sizeof(lti));
    written += write(fd, buffer, bufferUsed);
    if (written != (ssize_t) (sizeof(lti) + bufferUsed))
    {
        log_error("Bad write in DataLogger::Write()\n");
        return false;
    }
    return true;
}

//----------------------------------------------------------
bool DataLogger::LogTimeIndexBuilder::AddData(DataLoggerID id, void *data, unsigned size)
{
    if ((size + sizeof(id) + bufferUsed) > sBufferSize)
    {
        log_error("Bad add in DataLogger::Add()\n");
        return false;
    }
    ++numVariables;

    memcpy(&buffer[bufferUsed], &id, sizeof(id));
    bufferUsed += sizeof(id);
    memcpy(&buffer[bufferUsed], data, size);
    bufferUsed += size;
    return true;
}

//----------------------------------------------------------
//----------------------------------------------------------
//----------------------------------------------------------
DataLogger::DataLogger(const char *logPath) :
    mFD(-1),
    mLogPath(NULL),
    mHeaderWritten(false)
{
    mLogPath = strdup(logPath);
    log_info("DataLogger writing logs to '%s'\n", mLogPath);
}

//----------------------------------------------------------
DataLogger::~DataLogger()
{
    CloseInternal();

    while (!mLogVariables.empty())
    {
        auto it = mLogVariables.begin();
        free((*it).second.name);
        free((*it).second.units);
        mLogVariables.erase(it);
    }

    free(mLogPath);
    mLogPath = NULL;
}


//----------------------------------------------------------
DataLoggerID DataLogger::AddVariableInternal(const char *variable, const char *units, LogVariableType type, double scale)
{
    mLock.lock();

    DataLoggerID id = RPN::VariableNameHash(variable);
    if (mLogVariables.count(id) != 0)
    {
        mLock.unlock();
        return id;
    }

    LogVariable lv;
    lv.name  = strdup(variable);
    lv.units = strdup(units);
    lv.id    = id;
    lv.scale = scale;
    lv.type  = type;
    mLogVariables[id] = lv;

    mLock.unlock();

    log_debug("Adding log variable : %s in %s  ID: 0x%08X\n", lv.name, lv.units, lv.id);
    return lv.id;
}

//----------------------------------------------------------
bool DataLogger::UpdateVariableInternal(DataLoggerID id, double time, double value)
{
    // Don't store any data until the log has been opened
    // This will be when GPS starts updating the clock
    if (mFD < 0) return false;

    mLock.lock();
    auto it = mLogVariables.find(id);
    if (it == mLogVariables.end())
    {
        mLock.unlock();
        return false;
    }

    LogTimeIndexBuilder &builder = FindBuilder(time);

    const LogVariable &var = (*it).second;

    value *= var.scale;

    switch (var.type)
    {
        case Double:
            builder.AddData(id, &value, sizeof(double));
            break;

        case Float:
        {
            float v = value;
            builder.AddData(id, &v, sizeof(float));
            break;
        }

        case U32:
        {
            uint32_t v = (uint32_t)value;
            builder.AddData(id, &v, sizeof(uint32_t));
            break;
        }

        case S32:
        {
            int32_t v = (int32_t)value;
            builder.AddData(id, &v, sizeof(int32_t));
            break;
        }

        case U16:
        {
            uint16_t v = (uint16_t)value;
            builder.AddData(id, &v, sizeof(uint16_t));
            break;
        }

        case S16:
        {
            int16_t v = (int16_t)value;
            builder.AddData(id, &v, sizeof(int16_t));
            break;
        }

        case U8:
        {
            uint8_t v = (uint8_t)value;
            builder.AddData(id, &v, sizeof(uint8_t));
            break;
        }

        case S8:
        {
            int8_t v = (int8_t)value;
            builder.AddData(id, &v, sizeof(int8_t));
            break;
        }
    }

    mLock.unlock();
    return true;
}


//----------------------------------------------------------
bool DataLogger::WriteLogHeader(void)
{
    if (mHeaderWritten) return true;
    mHeaderWritten = true;

    // Write out log header
    uint32_t logHeaderSize = sizeof(LogHeader) + (sizeof(LogHeaderVariable) * mLogVariables.size());
    LogHeader *header = (LogHeader *) malloc(logHeaderSize);

    header->magic        = LOG_HEADER_MAGIC;
    header->version      = LOG_VERSION;
    header->numVariables = mLogVariables.size();
    header->_pad         = 0;

    unsigned index = 0;
    std::map<DataLoggerID, LogVariable>::iterator it;
    for (it = mLogVariables.begin(); it != mLogVariables.end(); it++)
    {
        const LogVariable &lv = (*it).second;
        strncpy(header->_variables[index].name,  lv.name, 64);
        strncpy(header->_variables[index].units, lv.units, 64);
        header->_variables[index].id = lv.id;
        header->_variables[index].scale = lv.scale;
        header->_variables[index].type  = lv.type;
        ++index;
    }
    ssize_t written = write(mFD, header, logHeaderSize);
    if (written != (ssize_t)logHeaderSize)
    {
        log_error("Log error %s, could not write %d header bytes opening log.\n", strerror(errno), (int) logHeaderSize);
    }

    return true;
}

//----------------------------------------------------------
DataLogger::LogTimeIndexBuilder &DataLogger::FindBuilder(double time)
{
    uint64_t qt = QuantizeTime(time);

    if (mTimes.size() > sMaxActiveBuilders)
    {
        WriteLogHeader();

        mTimes.front().Write(mFD);
        mTimes.erase(mTimes.begin());
    }

    std::vector<LogTimeIndexBuilder>::iterator it;

    // do I need a new block?
    if (mTimes.empty() || (qt > mTimes.back().time))
    {
        mTimes.emplace_back(qt);
        return mTimes.back();
    }

    // Look for a block in time order
    for (it = mTimes.begin(); it != mTimes.end(); ++it)
    {
        uint64_t t = (*it).time;
        if (t == qt) return *it;
        if (t > qt)
        {
            it = mTimes.emplace(it, qt);
            return *it;
        }
    }
    log_error("ERROR in FindBuilder(), reached the end!\n");
    return mTimes.back();
}

//----------------------------------------------------------
void DataLogger::OpenInternal(void)
{
    if (mFD > 0) return;
    // Create filename and open file
    char fname[512];
    sprintf(fname, "%s/log-%s-%s.rdl", mLogPath, DateString(), TimeString());
    mFD = open(fname, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

    log_info("Creating log file '%s' returned %d\n", fname, mFD);
}

//----------------------------------------------------------
void DataLogger::CloseInternal(void)
{
    if (mFD < 0) return;

    WriteLogHeader();

    while (!mTimes.empty())
    {
        mTimes.front().Write(mFD);
        mTimes.erase(mTimes.begin());
    }

    close(mFD);
    mFD = -1;
    mHeaderWritten = false;

    sync();
}

//----------------------------------------------------------
bool DataLogger::Init(const char *path)
{
    if (sDataLogger != NULL) return false;
    sDataLogger = new DataLogger(path);
    return true;
}

//----------------------------------------------------------
bool DataLogger::Begin(void)
{
    if (sDataLogger == NULL) return false;
    sDataLogger->OpenInternal();
    return true;
}

//----------------------------------------------------------
bool DataLogger::End(void)
{
    if (sDataLogger == NULL) return false;
    delete sDataLogger;
    sDataLogger = NULL;
    return true;
}

//----------------------------------------------------------
DataLoggerID DataLogger::AddVariable(const char *variable, const char *units, LogVariableType type, double scale)
{
    if (sDataLogger == NULL) return 0;
    return sDataLogger->AddVariableInternal(variable, units, type, scale);
}

//----------------------------------------------------------
bool DataLogger::UpdateVariable(DataLoggerID id, double time, double value)
{
    if (sDataLogger == NULL) return 0;
    return sDataLogger->UpdateVariableInternal(id, time, value);
}

