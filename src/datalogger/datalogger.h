#ifndef DATALOGGER_H
#define DATALOGGER_H

#include <stdint.h>
#include <string.h>

#include "rpn/rpn.h"

#include <mutex>

typedef uint32_t DataLoggerID;
enum LogVariableType { Double, Float, U32, S32, U16, S16, U8, S8 };

//----------------------------------------------------
struct LogHeaderVariable
{
    char         name[64];
    char         units[64];
    double       scale;
    uint32_t     type;
    DataLoggerID id;
};

//----------------------------------------------------
struct LogHeader
{
    uint32_t magic;
    uint32_t version;
    uint32_t numVariables;
    uint32_t _pad;
    LogHeaderVariable _variables[0];
};

//----------------------------------------------------
struct LogTimeIndex
{
    LogTimeIndex(uint64_t t = 0) : numVariables(0), time(t) {}
    uint32_t magic;
    uint32_t numVariables;
    uint64_t time;
    uint8_t  data[0];
};

//----------------------------------------------------
class DataLogger
{
  public:

    static bool Init(const char *path);
    static bool Begin(void);
    static bool End(void);

    static DataLoggerID AddVariable(const char *var, const char *units, LogVariableType type, double scale = 1.0);
    static bool         UpdateVariable(DataLoggerID id, double time, double value);

  private:

    DataLogger(const char *path);
   ~DataLogger();

    DataLoggerID AddVariableInternal(const char *var, const char *units, LogVariableType type, double scale);
    bool UpdateVariableInternal(DataLoggerID id, double time, double value);

    void OpenInternal(void);
    bool WriteLogHeader(void);
    void CloseInternal(void);

    uint64_t QuantizeTime (double time) const { return (uint64_t) (time * 10000.0); }

    struct LogVariable
    {
        char           *name;
        char           *units;
        double          scale;
        LogVariableType type;
        DataLoggerID    id;
    };

    LogVariable &FindVariable(DataLoggerID id);

    static const unsigned sMaxActiveBuilders = 50;

    struct LogTimeIndexBuilder : public LogTimeIndex
    {
        LogTimeIndexBuilder(uint64_t t) : LogTimeIndex(t), bufferUsed(0) {;}
        bool AddData(DataLoggerID id, void *data, unsigned size);
        bool Write(int fd);
        unsigned bufferUsed;
        static const unsigned sBufferSize = 512;
        uint8_t buffer[sBufferSize];

    };

    LogTimeIndexBuilder &FindBuilder(double time);

    int   mFD;
    char *mLogPath;
    bool  mHeaderWritten;

    std::mutex mLock;

    std::map<DataLoggerID, LogVariable> mLogVariables;
    std::vector<LogTimeIndexBuilder> mTimes;

    static DataLogger *sDataLogger;
};


#endif // DATALOGGER_H