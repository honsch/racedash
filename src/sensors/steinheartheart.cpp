/*
 * NTC thermistor library
 * Copyright (C) 2007 - SoftQuadrat GmbH, Germany
 * Contact: thermistor (at) softquadrat.de
 * Web site: thermistor.sourceforge.net
   GPL 2.1 or greater
*/

#include "steinheartheart.h"

#include <stdio.h>
#include "log/log.h"

//#define SH_DEBUG

const float SteinheartHeartCalc::AbsoluteZero = -273.15F;

//------------------------------------------------------------------
SteinheartHeartCalc::SteinheartHeartCalc(const std::vector<NTCData> &specification) :
    mErg{0.0F, 0.0F, 0.0F, 0.0F},
    mBasis{ {1.0F, 0.0F, 0.0F, 0.0F}, {0.0F, 1.0F, 0.0F, 0.0F}, {0.0F, 0.0F, 1.0F, 0.0F}, {0.0F, 0.0F, 0.0F, 1.0F} }
{
    unsigned a;
    for (a = 0 ; a < specification.size(); a++)
    {
        mSpec.push_back(specification[a]);
    }

    Orthonormal(mBasis);
    Approximate(mErg);

#ifdef SH_DEBUG
    TestResult();
#endif
}


//------------------------------------------------------------------
SteinheartHeartCalc::~SteinheartHeartCalc()
{

}

//------------------------------------------------------------------
// Evaluates p(x) for a Polynomial p.
// Calculates the value of Polynomial p at x accordings to
// Horners schema.
float SteinheartHeartCalc::Value(Polynomial p, float x)
{
	float retval = 0.0F;

    unsigned i;
	for (i = M; i > 0; i--)
    {
        retval = (retval * x) + p[i - 1];
    }
	return retval;
}

//------------------------------------------------------------------
// Evaluates [p,q] for two polynoms p and q.
// Calculates the scalar product of two polynoms p and q.
// This is defined as sum
// <center>[p, q] := Sum p(x<sub>i</sub>) * q(x<sub>i</sub>) &uuml;ber i = 0, .., N - 1</center>
float SteinheartHeartCalc::ScalarPolynomial(Polynomial p, Polynomial q)
{
	float retval = 0.0F;

    unsigned i;
	for (i = 0; i < mSpec.size(); i++)
    {
        float x = mSpec[i].x;
        retval += (Value(p, x) * Value(q, x));
    }

	return retval;
}

//------------------------------------------------------------------
// Evaluates p *= fact for a Polynomial p and a factor fact.
// Multiplies p with a factor fact.
void SteinheartHeartCalc::Mult(Polynomial p, float fact)
{
    unsigned i;
	for (i = 0; i < M; i++)
    {
        p[i] *= fact;
    }
}

//------------------------------------------------------------------
// Evaluates p += fact*q for two polynoms p and q and a factor fact.
// Adds a multiple of a Polynomial q to Polynomial p.
void SteinheartHeartCalc::Linear(Polynomial p, Polynomial q, float fact)
{
    unsigned i;
	for (i = 0; i < M; i++)
    {
        p[i] += (q[i] * fact);
    }
}

//------------------------------------------------------------------
// Converts a base to an orthonormal base.
// @param p base in form of an array of polynoms.
void SteinheartHeartCalc::Orthonormal(Polynomial p[])
{
	float fact, norm;

#ifdef SH_DEBUG
    log_debug("function orthonormal");
    log_debug("====================");
#endif

    unsigned i, j;
	for (i = 0; i < M; i++)
    {
        for (j = 0; j < i; j++)
        {
			fact = ScalarPolynomial(p[i], p[j]);
			Linear(p[i], p[j], -fact);
		}

		norm = ScalarPolynomial(p[i], p[i]);
      	Mult(p[i], 1.0F / sqrtf(norm));

#ifdef SH_DEBUG
        log_debug("Polynom %d: ", i);
        for (j = 0; j < M; j++)
        {
            log_debug("%f ", p[i][j]);
        }
        log_debug("");
#endif
	}
#ifdef SH_DEBUG
    log_debug("Testing orthonormal base");
    for (i = 0; i < M; i++)
    {
        for (j = 0; j <= i; j++)
        {
            log_debug("%.15f ", ScalarPolynomial(mBasis[i], mBasis[j]));
        }
        log_debug("");
    }
    log_debug("");
#endif
}

//------------------------------------------------------------------
// Evaluates [p, p<sub>f</sub>] for given Polynomial p and solving Polynomial p<sub>f</sub>.
float SteinheartHeartCalc::Scalar(Polynomial p)
{
	float retval = 0.0F;

    unsigned i;
	for (i = 0; i < mSpec.size(); i++)
    {
        retval += (mSpec[i].y * Value(p, mSpec[i].x));
	}
    return retval;
}

//------------------------------------------------------------------
// Evaluate approximation Polynomial u<sub>f</sub>.
void SteinheartHeartCalc::Approximate(Polynomial erg)
{
	float fact;

    unsigned i;
	for (i = 0; i < M; i++)
    {
        erg[i] = 0.0F;
    }
	for (i = 0; i < M; i++)
    {
		fact = Scalar(mBasis[i]);
		Linear(erg, mBasis[i], fact);
	}
#ifdef SH_DEBUG
	log_debug("Steinhart-Hart coefficients");
	for (i = 0; i < M; i++)
    {
        log_debug("a[%d] = %.15f", i, erg[i]);
    }
#endif
}

//------------------------------------------------------------------
// Tests the approximation Polynomial with all t-r pairs.
// Prints out all calculated values and the maximal error.
// The function will do nothing, if debug mode is off
void SteinheartHeartCalc::TestResult(void)
{
	log_debug("SteinheartHeartCalc Test Result");
	log_debug("===================");
	float maxerr = 0.0F;

    unsigned i, index = 0;
	for (i = 0; i < mSpec.size(); i++)
    {
		float valCalc = Temp(mSpec[i].resistance);
		float valRef  = mSpec[i].temperature;
		float err = fabsf(valRef - valCalc);
        log_debug("Ref: %.4f   Calc: %.4f  R: %f", valRef, valCalc, mSpec[i].resistance);
		if (err > maxerr)
		{
            index = i;
			maxerr = err;
		}
	}
	log_debug("");
	log_debug("Maximal error=%7.5f at temperature=%5.1f", maxerr, mSpec[index].temperature);
	log_debug("");
}

//------------------------------------------------------------------
float SteinheartHeartCalc::Temp(float resistance)
{
    return (1.0F / Value(mErg, log(resistance))) + AbsoluteZero;
}

