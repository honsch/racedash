#include "sensormanager.h"
#include "iiodevice.h"
#include "util/util.h"
#include "config/config.h"
#include "datalogger/datalogger.h"

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/pps.h>
#include <pthread.h>
#include <sched.h>

#include <utility>
#include "log/log.h"

//#include "kalman/UnscentedKalmanFilter.hpp"

// Define this if you want fake data
//#define HACK_DATA

static const double sFuelTankSize = 13.5; // gallons

std::thread SensorManager::sSensorThread;
bool SensorManager::sWantQuit = false;

#define VARIABLES_ROOT_PATH "racedash.sensors.variables"
#define DATA_ROOT_PATH      "racedash.sensors.data"

#define NUM_LEDS    16

#define LED_RPM_START   5000
#define LED_RPM_END     6250
#define LED_RPM_BLINK   6500

#define FUEL_LEVEL_SENDER_DRIVE_OHMS    80.0

static const uint32_t sRPMColors[8] =
{
    CYAN,
    CYAN,
    CYAN,
    GREEN,
    GREEN,
    GREEN,
    MAGENTA,
    MAGENTA
};

//--------------------------------------------------------------------------
SensorManager::SensorManager(bool isRemote) :
    mIsRemote(isRemote),
    mGPSSamples(0),
    mMaxDeviceSamples(0),
    mMpuDeviceSamples(0),
    mECUDeviceSamples(0),
    mTXPitRequest(false),
    mTXPitRequestSent(0),
    mTXLapData(0),
    mLoraRXCount(0),
    mTestedTrackLocation(false),
    mWarningActiveCount(0),
    mFuelLevelFilter(Biquad::Lowpass, 0.05, 0.25, 0.0)
{
    mGPS = NULL;
    mSpeeduino = NULL;
    mRadio = NULL;

    LoadData();
    InitWarnings();
    LoadVariables();

    if (!mIsRemote)
    {
        mFuelLevelLogID = DataLogger::AddVariable("fuel level", "gal", Float);
    }


#if 1 // In the car
    LEDString::Init ledInit[3] =
    {
        { 16, LEDString::RGB } ,
        {  0, LEDString::RGB }
    };
#else // On the bench
    LEDString::Init ledInit[3] =
    {
        { 8, LEDString::RGBW} ,
        { 7, LEDString::RGB },
        { 0, LEDString::RGB }
    };
#endif
    mLEDString = new LEDString("hw:0,0", ledInit);

    //                   Ohms  l to Gallons
    mFuelLevel.AddEntry( 39.0,  14.0); // actually 40.2, but thats more than 13 gal
    mFuelLevel.AddEntry( 40.2,  13.0);
    mFuelLevel.AddEntry( 42.6,  12.1);
    mFuelLevel.AddEntry( 49.1,  11.1);
    mFuelLevel.AddEntry( 58.1,  10.0);
    mFuelLevel.AddEntry( 67.0,   9.0);
    mFuelLevel.AddEntry( 74.1,   8.0);
    mFuelLevel.AddEntry( 80.3,   7.5);
    mFuelLevel.AddEntry( 95.7,   6.0);
    mFuelLevel.AddEntry(113.5,   5.0);
    mFuelLevel.AddEntry(133.0,   4.0);
    mFuelLevel.AddEntry(153.0,   3.0);
    mFuelLevel.AddEntry(197.7,   2.0);
    mFuelLevel.AddEntry(215.7,   1.5);
    mFuelLevel.AddEntry(279.0,   0.0);
}

//--------------------------------------------------------------------------
SensorManager::~SensorManager()
{
    if (!sWantQuit)
    {
        sWantQuit = true;
        sSensorThread.join();
    }

    GpioSetLineValue(mErrorLED, 0);
    GpioFreeLine(mErrorLED);

    RemoveAllVariables();

    if (mLEDString != NULL)
    {
        delete mLEDString;
    }
    if (mRadio != NULL) delete mRadio;
    if (mGPS != NULL)   delete mGPS;
}

//--------------------------------------------------------------------------
void SensorManager::LoadData(void)
{
    if (mIsRemote) return;
}

//--------------------------------------------------------------------------
void SensorManager::SaveData(void)
{
    if (mIsRemote) return;
}

//--------------------------------------------------------------------------
void *SensorManager::SensorThread(void)
{
    // local setup
    //pthread_t this_thread = pthread_self();
    //struct sched_param params;
    //params.sched_priority = sched_get_priority_max(SCHED_FIFO);
    //pthread_setschedparam(this_thread, SCHED_FIFO, &params);

    if (!mIsRemote)
    {
        // Hookup PPS first!
#if ODROID == 2        
        bool ok = BindPPS("/dev/pps1");
#else
        bool ok = BindPPS("/dev/pps0");
#endif        
        if (!ok)
        {
            log_error(" PPS BIND FAILED");
        }

#if ODROID == 2
        mGPS = new MtkGPS("/dev/ttyAML1", mNavBuffer);
#else
        mGPS = new MtkGPS("/dev/ttyAML6", mNavBuffer);
#endif

#ifdef SPEEDUINO_CAN
        mSpeeduino = new SpeeduinoCANInterface("/dev/ttyUSB0", 115200);
#else
        mSpeeduino = new SpeeduinoInterface("/dev/ttyUSB0", 115200, false);
#endif
        mIIOManager.Enumerate();
    }
    else
    {
        mGPS = NULL;
        mSpeeduino = NULL;
    }

    mRadio = new LoraRadio;

    // Setup my device pointers
    mMaxDevice = dynamic_cast<IIOMax11616 *>(mIIOManager.FindDeviceByType("max11616"));
    mMpuDevice = dynamic_cast<IIOMpu9250  *>(mIIOManager.FindDeviceByType("mpu9250"));

    //NOTE:  The MPU9250 samples the fastest, it is the psudo-clock
    //        that all other samples are interpolated to
    //       The GPS is the actual clock but it only updates at 5Hz

    if (mMpuDevice == NULL)
    {
        log_error("ERROR: CANNOT FIND MPU-9250!");
    }

    if (mMaxDevice == NULL)
    {
        log_error("ERROR: CANNOT FIND MAX11616!");
    }

    if (mMpuDevice != NULL)
    {
        // Create a sysfs trigger that we can use to chain from the mpu9250
        // to get all other data sampled reasonably coincident

        IIOSysfsTrigger::Create(1);
        mIIOManager.Enumerate();
        IIOSysfsTrigger *t = dynamic_cast<IIOSysfsTrigger *>(mIIOManager.FindTriggerByName("sysfstrig1"));
        if (t == NULL) log_error("Find Sysfs Trigger returned NULL!");

        // Set up the master
        mMpuDevice->SetChainedTrigger(t);

        // Setup the slave
        mMaxDevice->SetTrigger(t);
    }
    else if (mMaxDevice != NULL) // If the MPU is missing its trigger will be missing as well, make a new one
    {
        IIOHRTimerTrigger::Create("fallback_trigger");
        mIIOManager.Enumerate();
        IIOHRTimerTrigger *t = dynamic_cast<IIOHRTimerTrigger *>(mIIOManager.FindTriggerByName("fallback_trigger"));
        if (t == NULL) log_error("Find HRTimer Trigger returned NULL!");
        t->SetFrequency(50);
        mMaxDevice->SetTrigger(t);
    }

    mIIOManager.EnableBufferAll();
    mIIOManager.ActivateBufferAll();

    //mIIOManager.CalibrateAll();

    const char *closestTrackName = NULL;
    double nextRadioTX = SystemTimeUTC() + 0.1;
    mStartupTime = SystemTimeUTC();
    unsigned currentLap = 0xFFFFFFFF;
    while (!sWantQuit)
    {

        mLEDString->UpdateDriver();

        bool updated = mIIOManager.UpdatePoll();
        if (updated)
        {
            UpdateNewestState();
            UpdateCoherentState();
            UpdateWarnings();
        }
        if (SystemTimeUTC() > nextRadioTX)
        {
            nextRadioTX += 0.2;
            // Check to see what we should send
            if (mTXLapData > 0)
            {
                if (RadioTXLapData())
                {
                    --mTXLapData;
                }
            }
            else
            {
                RadioTXRTData();
            }
        }

        RadioRX();

        if ((mGPS != NULL) && !mTestedTrackLocation)
        {
            size_t sc;
            NMEANavData nd;
            mGPS->NewestSample(sc, nd);
            if (sc > 5) // Let the GPS settle a few samples
            {
                TestTrackLocation(nd.latitude, nd.longitude);
            }

            if (currentLap != gTrackManager->CurrentLap().lap)
            {
                currentLap = gTrackManager->CurrentLap().lap;
                mTXLapData = 5;
            }
        }
    }

    mIIOManager.ActivateBufferAll(false);
    mIIOManager.EnableBufferAll(false);
    if (!mIsRemote) DataLogger::End();

    return NULL;
}

//--------------------------------------------------------------------------
void *SensorManager::SensorThreadRemote(void)
{
    mRadio = new LoraRadio;
    mRadio->SetRXTimeout(512);

    mStartupTime = SystemTimeUTC();
    while (!sWantQuit)
    {

#ifdef HACK_DATA
        TelemetryRTData rt;
        rt.header.size = sizeof(rt);
        rt.kmh         = 100;
        rt.waterTemp   = 105;    // deg C
        rt.rpm         = 500;    // out = rpm*10
        rt.pitAck      = 0;
        rt.fuelRemain  = 1100;    // gal = fuelRemain / 100
        rt.battery     = 143;    // out = battery / 10
        // home
        //rt.latitude    = 49.227;
        //rt.longitude   = -122.896;
        // Ridge
        //rt.latitude    = 47.2543;
        //rt.longitude   = -123.1961;
        // ORP
        //rt.latitude    = 45.3633;
        //rt.longitude   = -120.7437;
        // Area 27
        rt.latitude    = 49.16399;
        rt.longitude   = -119.5223;

        rt.stintTime   = 3600;   // in seconds
        rt.oilPressure = 50;     // psi
        rt.afr         = 147;    // out = afr / 10
        RXRTData(&rt);
#endif
        while (RadioRX()) ;
        if (mTXPitRequest)
        {
            if (RadioTXPitRequest(mPitRequestLaps))
            {
                ++mTXPitRequestSent;
            }
        }
        usleep(100000);
    }

    return NULL;
}


//--------------------------------------------------------------------------
//If the system time has changed, reset the startup time
void SensorManager::SystemTimeAdjusted(void)
{
    mStartupTime = SystemTimeUTC();
}

//--------------------------------------------------------------------------
bool SensorManager::RadioTXRTData(void)
{
    TelemetryRTData rt;
    rt.header.type = TELEMETRY_TYPE_RT_DATA;
    rt.header.size = sizeof(rt);

    double v;
    GetNewestStateValue("speed", v);
    rt.kmh = v;

    GetNewestStateValue("ECU CLT", v);
    rt.waterTemp = v;

    GetNewestStateValue("ECU RPM", v);
    rt.rpm = v / 10.0;

    GetNewestStateValue("fuel level", v);
    rt.fuelRemain = v * 100.0;

    RPN::GetVariable(mRemoteState, "PIT ack", v);
    rt.pitAck = v;

    GetNewestStateValue("ECU VBat", v);
    rt.battery = v * 10.0;

    GetNewestStateValue("ECU OilPressure", v);
    rt.oilPressure = v;

    GetNewestStateValue("ECU O2", v);
    rt.afr = v * 10.0;

    GetNewestStateValue("latitude", v);
    rt.latitude = v;

    GetNewestStateValue("longitude", v);
    rt.longitude = v;

    GetNewestStateValue("runtime", v);
    rt.stintTime = v;

    return mRadio->Send((uint8_t *)&rt, rt.header.size);
}


//--------------------------------------------------------------------------
bool SensorManager::RadioRX(void)
{
    RadioRXPacket p;
    bool ok = mRadio->Receive(p);
    if (!ok) return false;
    ++mLoraRXCount;

    TelemetryHeader *h = (TelemetryHeader *)p.data;
    switch (h->type)
    {
        case TELEMETRY_TYPE_RT_DATA:
            RXRTData((TelemetryRTData *) p.data);
            UpdateWarnings();
            break;

        case TELEMETRY_TYPE_PIT:
            RXPit((TelemetryPitRequest *) p.data);
            break;

        case TELEMETRY_TYPE_TROUBLE:
            RPN::SetVariable(mRemoteState, "trouble",  1.0);
            break;

        case TELEMETRY_TYPE_LAP_DATA:
            RXLapData((TelemetryLapData *) p.data);
            break;

        default:
            log_debug("Got unknown Telemetry type: %d", h->type);
            break;
    }

    double v;
    v = p.rssi;
    RPN::SetVariable(mRemoteState, "LORA rssi", v);
    v = p.snr;
    RPN::SetVariable(mRemoteState, "LORA snr", v);
    v = mLoraRXCount;
    RPN::SetVariable(mRemoteState, "LORA rxCount", v);

    delete[] p.data;
    return true;
}

//--------------------------------------------------------------------------
void SensorManager::RXRTData(const TelemetryRTData *rt)
{
    if (rt->header.size != sizeof(TelemetryRTData))
    {
        log_warn("RX Telemey RT Data is wrong size: %d", rt->header.size);
        return;
    }

    double v;

    v = rt->kmh;
    RPN::SetVariable(mRemoteState, "speed", v);

    v = rt->waterTemp;
    RPN::SetVariable(mRemoteState, "ECU CLT", v);

    v = rt->rpm * 10.0;
    RPN::SetVariable(mRemoteState, "ECU RPM", v);

    v = rt->pitAck;
    RPN::SetVariable(mRemoteState, "PIT ack", v);
    // Stop transmitting Pit request packed when we get conformation
    if (mTXPitRequest)
    {
        PitAcknowledge ack = (PitAcknowledge) rt->pitAck;
        if (ack != PitAck_NoRequest)
        {
            mTXPitRequest = false;
        }
    }

    v = rt->fuelRemain / 100.0;
    RPN::SetVariable(mRemoteState, "fuel level", v);

    v = rt->battery / 10.0;
    RPN::SetVariable(mRemoteState, "ECU VBat", v);

    v = rt->oilPressure;
    RPN::SetVariable(mRemoteState, "ECU OilPressure", v);

    v = rt->afr / 10.0;
    RPN::SetVariable(mRemoteState, "ECU O2", v);

    v = rt->latitude;
    RPN::SetVariable(mRemoteState, "latitude", v);

    v = rt->longitude;
    RPN::SetVariable(mRemoteState, "longitude", v);

    v = rt->stintTime;
    RPN::SetVariable(mRemoteState, "runtime", v);

    if (!mTestedTrackLocation)
    {
        TestTrackLocation(rt->latitude, rt->longitude);
    }
}

//--------------------------------------------------------------------------
// Called for the remote UI
void SensorManager::PitRequest(unsigned laps)
{
    mPitRequestLaps   = laps;
    mTXPitRequest     = true;
    mTXPitRequestSent = 0;
}

//--------------------------------------------------------------------------
// Called from the car's UI
void SensorManager::PitAcknowledged(PitAcknowledge ack)
{
    double v = 0.0;
    RPN::SetVariable(mRemoteState, "PIT request", v);

    v = (double) ack;
    RPN::SetVariable(mRemoteState, "PIT ack", v);
}

//--------------------------------------------------------------------------
void SensorManager::ClearPitAcknowledge(void)
{
    double v = PitAck_NoRequest;
    RPN::SetVariable(mRemoteState, "PIT ack", v);
}

//--------------------------------------------------------------------------
bool SensorManager::RadioTXPitRequest(unsigned laps)
{
    TelemetryPitRequest pr;
    pr.header.type = TELEMETRY_TYPE_PIT;
    pr.header.size = sizeof(pr);
    pr.pitLaps     = laps;

    return mRadio->Send((uint8_t *)&pr, pr.header.size);
}

//--------------------------------------------------------------------------
void SensorManager::RXPit(const TelemetryPitRequest * pr)
{
    if (pr->header.size != sizeof(TelemetryPitRequest))
    {
        log_warn("RX Telemey Pit Request is wrong size: %d", pr->header.size);
        return;
    }

    double v = 1;
    RPN::SetVariable(mRemoteState, "PIT request", v);

    v = pr->pitLaps;
    RPN::SetVariable(mRemoteState, "PIT laps", v);
}

//--------------------------------------------------------------------------
void SensorManager::RXLapData(const TelemetryLapData *ld)
{
    if (ld->header.size != sizeof(TelemetryLapData))
    {
        log_warn("RX Telemey Lap Data is wrong size: %d", ld->header.size);
        return;
    }

    double v;

    v = ld->lapCompleted;
    RPN::SetVariable(mRemoteState, "speed",  v);

    v = ld->lapCompleted;
    RPN::SetVariable(mRemoteState, "LAP lastLap", v);

    v = ld->lapDuration;
    RPN::SetVariable(mRemoteState, "LAP lastDuration", v);

    v = ld->lapsFuelRemaining;
    RPN::SetVariable(mRemoteState, "LAP fuelLapsRemaining", v);
}

//--------------------------------------------------------------------------
bool SensorManager::RadioTXLapData(void)
{
    TelemetryLapData ld;
    ld.header.type = TELEMETRY_TYPE_LAP_DATA;
    ld.header.size = sizeof(ld);

    double v;

    GetNewestStateValue("LAP lastLap",  v);
    ld.lapCompleted = v;

    GetNewestStateValue("LAP lastDuration",  v);
    ld.lapDuration = v;

    GetNewestStateValue("LAP fuelLapsRemaining", v);
    ld.lapsFuelRemaining = v;
    if (v < 0.0) v = 0.0;

    ld._pad1 = mTXLapData;
    ld._pad2 = 0;

    return mRadio->Send((uint8_t *)&ld, ld.header.size);
}

//--------------------------------------------------------------------------
bool SensorManager::UpdateCoherentState(void)
{
    double newest = 0.0;
    if      (mMpuDevice != NULL) newest = mMpuDevice->NewestSampleTime();
    else if (mMaxDevice != NULL) newest = mMaxDevice->NewestSampleTime();
    else if (mGPS != NULL)       newest = mGPS->NewestSampleTime();
    else return false; // no sensors, nothing to do!

    // if all the samples are in the past it's crap data
    if (newest < 1600000000.0) return false;

    DataLogger::Begin();

    if (mMasterSampleTimes.empty() || (mMasterSampleTimes.back() < newest))
    {
        log_debug("Pushing %.3f after %.3f  %d", 
            newest, 
            mMasterSampleTimes.empty() ? 0.0 : mMasterSampleTimes.back(), 
            (int)mMasterSampleTimes.size());

        mMasterSampleTimes.push(newest);
    }

    RPN::VariableTable vt;

    while (1)
    {
        if (mMasterSampleTimes.empty()) break;
        double oldest = mMasterSampleTimes.front();

        NMEANavData         navData   = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        IIOMax11616::Sample maxSample;
        IIOMpu9250::Sample  mpuSample;

        // If the source is missing, use an empty sample
        int gpsStatus = (mGPS != NULL)       ? mGPS->SampleAt(oldest, navData)         : 0;
        int maxStatus = (mMaxDevice != NULL) ? mMaxDevice->SampleAt(oldest, maxSample) : 0;
        int mpuStatus = (mMpuDevice != NULL) ? mMpuDevice->SampleAt(oldest, mpuSample) : 0;
        // is this sample too old for the devices?
        if ((gpsStatus < 0) || (maxStatus < 0) || (mpuStatus < 0))
        {
            // If so, drop it and try the next one
            mMasterSampleTimes.pop();

            log_debug("Dropped sample at %f because it's too old  GPS: %d  max: %d  mpu: %d  size: %d", oldest, gpsStatus, maxStatus, mpuStatus, (int)mMasterSampleTimes.size());
            continue;
        }

        // Are we asking for data that hasn't been sampled yet?
        if ((gpsStatus > 0) || (mpuStatus > 0) || (maxStatus > 0))
        {
            // If so, we're done and need to wait for the next update
            break;
        }

        // Clear the good sample time from the list
        mMasterSampleTimes.pop();
        log_trace("Updating sample at %f", oldest);

        // We've got valid samples!

        // Update heading info here

        // Setup system and input variables
        UpdateVT_TIME(vt, oldest, SystemTimeUTC(), SystemTimeUTC() - mStartupTime);

        UpdateVT_GPS(vt, navData);
        UpdateVT_MPU(vt, mpuSample);
        UpdateVT_ECU(vt, mSpeeduino);
        UpdateVT_MAX(vt, maxSample);

         // Run all the expressions in order
        unsigned a;
        for (a = 0; a < mVariables.size(); a++)
        {
            double result = RPN::Evaluate(*mVariables[a].expression, vt);
            RPN::SetVariable(vt, mVariables[a].nameHash, result);
        }
        mCurrentStateCoherent = std::move(vt);
    }

    return true;
}

//--------------------------------------------------------------------------
bool SensorManager::UpdateNewestState(void)
{
    bool updated = false;
    double time = 0;

    if (mECUDeviceSamples != mSpeeduino->ValidSampleCount())
    {
        updated = true;
        mECUDeviceSamples = mSpeeduino->ValidSampleCount();
        UpdateVT_ECU(mCurrentStateNewest, mSpeeduino);
        if (time < mSpeeduino->NewestSampleTime()) time = mSpeeduino->NewestSampleTime();
    }

    if (mGPS != NULL)
    {
        size_t count;
        NMEANavData navData   = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        mGPS->NewestSample(count, navData);
        if (count > mGPSSamples)
        {
            updated = true;
            mGPSSamples = count;
            UpdateVT_GPS(mCurrentStateNewest, navData);
            TrackManager::AuxData aux;
            GetNewestStateValue("ECU GallonsUsed", aux.fuelUsed);
            bool added = gTrackManager->UpdateLap(navData, aux);
            if (time < navData.utc) time = navData.utc;
            UpdateVT_LAP(mCurrentStateNewest, gTrackManager, added);
        }
    }

    if (mMaxDevice != NULL)
    {
        size_t count;
        IIOMax11616::Sample maxSample;
        mMaxDevice->NewestSample(count, maxSample);
        if (count > mMaxDeviceSamples)
        {
            updated = true;
            mMaxDeviceSamples = count;
            UpdateVT_MAX(mCurrentStateNewest, maxSample);
            if (time < maxSample.Timestamp()) time = maxSample.Timestamp();
        }
    }

    if (mMpuDevice != NULL)
    {
        size_t count;
        IIOMpu9250::Sample  mpuSample;
        mMpuDevice->NewestSample(count, mpuSample);
        if (count > mMpuDeviceSamples)
        {
            updated = true;
            mMpuDeviceSamples = count;
            UpdateVT_MPU(mCurrentStateNewest, mpuSample);
            if (time < mpuSample.Timestamp()) time = mpuSample.Timestamp();
        }
    }


    if (updated)
    {
        UpdateVT_TIME(mCurrentStateNewest, time, SystemTimeUTC(), SystemTimeUTC() - mStartupTime);

         // Run all the expressions in order
        unsigned a;
        for (a = 0; a < mVariables.size(); a++)
        {
            double result = RPN::Evaluate(*mVariables[a].expression, mCurrentStateNewest);
            RPN::SetVariable(mCurrentStateNewest, mVariables[a].nameHash, result);
        }
    }
    return updated;
}

static const char *sWarningNames[] =
{
    "OilPressure",
    "CoolantTemperature",
    "BatteryVoltage",
    "Warn_AFR",
    "Boost",
    "FuelUsed",
    "StintTime"
};

//--------------------------------------------------------------------------
void SensorManager::InitWarnings(void)
{
    // LED 4 on board, GPIO X.19, or J2.11
    mErrorLED = GpioFindLine(LINE_LED4);
    GpioSetLineOutput(mErrorLED, 0, "ErrorLED");
    GpioSetLineValue(mErrorLED, 0); // Active High

    Warning w;
    w.count = 0;
    w.expireTime = 0;

    mWarnings.resize(Warn_Last);
    w.item = Warn_OilPressure;
    mWarnings[Warn_OilPressure] = w;

    w.item = Warn_CoolantTemperature;
    mWarnings[Warn_CoolantTemperature] = w;

    w.item = Warn_BatteryVoltage;
    mWarnings[Warn_BatteryVoltage] = w;

    w.item = Warn_AFR;
    mWarnings[Warn_AFR] = w;

    w.item = Warn_Boost;
    mWarnings[Warn_Boost] = w;

    w.item = Warn_FuelRemaining;
    mWarnings[Warn_FuelRemaining] = w;

    w.item = Warn_StintTime;
    mWarnings[Warn_StintTime] = w;
}

//--------------------------------------------------------------------------
void SensorManager::ActivateWarning(enum WarningItem item, double now, double duration)
{
    double end = now + duration;
    if (mWarnings[item].expireTime < now) ++mWarnings[item].count;
    mWarnings[item].expireTime = end;
}


//--------------------------------------------------------------------------
bool SensorManager::UpdateWarnings(void)
{
    double vbat, rpm, clt, oilPressure, boost, afr, stint, fuelRemain;
    GetNewestStateValue("ECU RPM", rpm);

    // If we're not running turn off the LED and stop checking
    if (rpm < 500)
    {
        unsigned a;
        for (a = 0; a < 16; a++) mLEDString->SetLED(a, 0, 0, 0, 0);
        //GpioSetLineValue(mErrorLED, 0);
        return false;
    }

    GetNewestStateValue("ECU VBat", vbat);
    GetNewestStateValue("ECU CLT", clt);
    GetNewestStateValue("ECU AFR", afr);
    GetNewestStateValue("ECU MAP", boost);
    GetNewestStateValue("ECU OilPressure", oilPressure);
    GetNewestStateValue("ECU GallonsRemaining", fuelRemain);
    GetNewestStateValue("runtime", stint);

    // Convert to psig
    boost = (boost - 100.0) / 14.5;

    double now = SystemTimeUTC();
    double warnDuration = 3.0;

    if ((vbat < 13.0) || (vbat > 14.5)) ActivateWarning(Warn_BatteryVoltage, now, warnDuration);
    if (clt >= 115.0)                   ActivateWarning(Warn_CoolantTemperature, now, warnDuration);
    //if (fuelRemain <= 1.0)              ActivateWarning(Warn_FuelRemaining, now, warnDuration);
    if (boost > 10.0)                   ActivateWarning(Warn_Boost, now, warnDuration);
    if ((boost > 0.0) && (afr > 13.0))  ActivateWarning(Warn_AFR, now, warnDuration);

#if 1
    if (rpm >= 1500.0)
    {
        double psiPerK = 1000.0 * oilPressure / rpm;
        if (psiPerK < 5.0) ActivateWarning(Warn_OilPressure, now, warnDuration);
    }
    else
    {
        if (oilPressure < 6.0) ActivateWarning(Warn_OilPressure, now, warnDuration);
    }

    if (stint > (116.0 * 60.0))
    {
        ActivateWarning(Warn_StintTime, now, warnDuration);
    }
#endif

    unsigned a;
    mWarningActiveCount = 0;
    for (a = 0; a < mWarnings.size(); a++)
    {
        if (mWarnings[a].expireTime > now)
        {
            ++mWarningActiveCount;
        }
    }

    // If we're using single LED
    //if (mWarningActiveCount > 0) GpioSetLineValue(mErrorLED, 1); // Active high
    //else                         GpioSetLineValue(mErrorLED, 0);

    //mLEDString->SetDimmer(0x20);

    if (mWarningActiveCount > 0)
    {
        uint8_t red = 255;
        if (Fraction(SystemTimeUTC() * 2.0) < 0.125) red = 0;
        unsigned a;
        for (a = 0; a < NUM_LEDS; a++) mLEDString->SetLED(a, red, 0, 0, 0);
    }
    else
    {
        if (rpm >= LED_RPM_START)
        {
            unsigned index;
            unsigned step = (LED_RPM_END - LED_RPM_START) / (NUM_LEDS / 2);
            for (index = 0; index < (NUM_LEDS / 2) ; index++)
            {
                uint32_t color = sRPMColors[index];
                if (rpm >= LED_RPM_BLINK)
                {
                    if (Fraction(SystemTimeUTC() * 5.0) < 0.5) color = ~color;
                }
                double ledrpm = LED_RPM_START + (index * step);
                if (rpm < ledrpm) color = BLACK;
                mLEDString->SetLED(index, color);
                mLEDString->SetLED(NUM_LEDS - (index + 1), color);
            }
        }
        else
        {
            for (a = 0; a < NUM_LEDS; a++) mLEDString->SetLED(a, BLACK);
        }
    }

    return mWarningActiveCount > 0;
}

//--------------------------------------------------------------------------
void SensorManager::ResetStintTime(void)
{
    mStartupTime = SystemTimeUTC(); // - (15 + (109 * 60));
}

//--------------------------------------------------------------------------
void SensorManager::UpdateVT_TIME(RPN::VariableTable &vt, double updateTime, double systemTime, double runtime)
{
    RPN::SetVariable(vt, "time",        updateTime);
    RPN::SetVariable(vt, "utc",         systemTime);
    RPN::SetVariable(vt, "runtime",     runtime);
}

//--------------------------------------------------------------------------
void SensorManager::UpdateVT_GPS(RPN::VariableTable &vt, NMEANavData &navData)
{
    RPN::SetVariable(vt, "latitude",  navData.latitude);
    RPN::SetVariable(vt, "longitude", navData.longitude);
    RPN::SetVariable(vt, "altitude",  navData.altitude);
    RPN::SetVariable(vt, "speed",     navData.velocity * 3.6);

#ifdef HACK_DATA
    RPN::SetVariable(vt, "speed", 100.0 * (sin(SystemTimeUTC() / 7.0) + 1.0));
    RPN::SetVariable(vt, "latitude",  49.227);
    RPN::SetVariable(vt, "longitude", -122.896);
#endif
}

//--------------------------------------------------------------------------
void SensorManager::UpdateVT_MAX(RPN::VariableTable &vt, IIOMax11616::Sample &sample)
{
    // Square it so it has a closer to log brightness
    unsigned dimmer = (256.0 * (sample[11] * sample[11]));
    if (dimmer < 2) dimmer = 2;
    mLEDString->SetDimmer(dimmer);

    // Don't need to update the fuel at 50Hz, let's filter based on 5Hz
    if ((mMaxDeviceSamples % 10) == 0)
    {
        double ohms = ADCValueToOhms(sample[0], FUEL_LEVEL_SENDER_DRIVE_OHMS);
        double fuelLevel = mFuelLevel.Lookup(ohms);
        fuelLevel = mFuelLevelFilter.Process(fuelLevel);

        //printf("Fuel level %s Ohms %f  Level: %f\n", TimeString(), ohms, fuelLevel);

        RPN::SetVariable(vt, "fuel level", fuelLevel);
        DataLogger::UpdateVariable(mFuelLevelLogID, SystemTimeUTC(), fuelLevel);
    }
}

//--------------------------------------------------------------------------
void SensorManager::UpdateVT_MPU(RPN::VariableTable &vt, IIOMpu9250::Sample &sample)
{
    RPN::SetVariable(vt, "acc_x", sample[0]);
    RPN::SetVariable(vt, "acc_y", sample[1]);
    RPN::SetVariable(vt, "acc_z", sample[2]);

    RPN::SetVariable(vt, "ang_x", sample[3]);
    RPN::SetVariable(vt, "ang_y", sample[4]);
    RPN::SetVariable(vt, "ang_z", sample[5]);

    RPN::SetVariable(vt, "mag_x", sample[6]);
    RPN::SetVariable(vt, "mag_y", sample[7]);
    RPN::SetVariable(vt, "mag_z", sample[8]);
}


//--------------------------------------------------------------------------
#ifdef SPEEDUINO_CAN
void SensorManager::UpdateVT_ECU(RPN::VariableTable &vt, SpeeduinoCANInterface *ecu)
{
    // Speeduino Data
    RPN::SetVariable(vt, "ECU RPM",              ecu->RPM());
    RPN::SetVariable(vt, "ECU MAP",              ecu->MAP());
    RPN::SetVariable(vt, "ECU IAT",              ecu->IAT());
    RPN::SetVariable(vt, "ECU CLT",              ecu->CLT());
    RPN::SetVariable(vt, "ECU TPS",              ecu->TPS());
    RPN::SetVariable(vt, "ECU O2",               ecu->O2());
    RPN::SetVariable(vt, "ECU OilPressure",      ecu->OilPressure());
    RPN::SetVariable(vt, "ECU VBat",             ecu->VBat());
    RPN::SetVariable(vt, "ECU Boost",            ((ecu->MAP() / 100.0) - 1.0) * 14.5);

#ifdef HACK_DATA
    // HACKS!!!!
    RPN::SetVariable(vt, "ECU RPM", 3600.0 * (sin(SystemTimeUTC() / 2.0) + 1.0));
    RPN::SetVariable(vt, "ECU Boost", 15.0 * (sin(SystemTimeUTC() / 2.0)));
    RPN::SetVariable(vt, "ECU O2", 14.7);
    RPN::SetVariable(vt, "ECU OilPressure", 25.0);
    RPN::SetVariable(vt, "ECU CLT", 100.0 + 10 * (sin(SystemTimeUTC() / 8.0) + 1.0));
    RPN::SetVariable(vt, "ECU VBat", 14.0);
#endif
}
#else
void SensorManager::UpdateVT_ECU(RPN::VariableTable &vt, SpeeduinoInterface *ecu)
{
    // Speeduino Data
    RPN::SetVariable(vt, "ECU RPM",              ecu->RPM());
    RPN::SetVariable(vt, "ECU MAP",              ecu->MAP());
    RPN::SetVariable(vt, "ECU IAT",              ecu->IAT());
    RPN::SetVariable(vt, "ECU CLT",              ecu->CLT());
    RPN::SetVariable(vt, "ECU TPS",              ecu->TPS());
    RPN::SetVariable(vt, "ECU O2",               ecu->O2());
    RPN::SetVariable(vt, "ECU OilPressure",      ecu->OilPressure());
    RPN::SetVariable(vt, "ECU VBat",             ecu->VBat());
    RPN::SetVariable(vt, "ECU SyncLossCounter",  ecu->SyncLossCounter());
    RPN::SetVariable(vt, "ECU Boost",            ((ecu->MAP() / 100.0) - 1.0) * 14.5);

#ifdef HACK_DATA
    // HACKS!!!!
    RPN::SetVariable(vt, "ECU RPM", 3600.0 * (sin(SystemTimeUTC() / 2.0) + 1.0));
    RPN::SetVariable(vt, "ECU Boost", 15.0 * (sin(SystemTimeUTC() / 2.0)));
    RPN::SetVariable(vt, "ECU O2", 14.7 + 2.0 * sin(SystemTimeUTC() / 2.0));
    RPN::SetVariable(vt, "ECU OilPressure", 75.0);
    RPN::SetVariable(vt, "ECU CLT", 100.0 + 10 * (sin(SystemTimeUTC() / 8.0) + 1.0));
    RPN::SetVariable(vt, "ECU VBat", 14.0);
#endif
}
#endif // SPEEDUINO_CAN

//--------------------------------------------------------------------------
void SensorManager::UpdateVT_LAP(RPN::VariableTable &vt, TrackManager *tm, bool lapChanged)
{
    const LapData &current = tm->CurrentLap();

    RPN::SetVariable(vt, "LAP currentLap",      current.lap);
    RPN::SetVariable(vt, "LAP currentDuration", current.duration);
    RPN::SetVariable(vt, "LAP currentDistance", current.distance);

    static unsigned lastLapUpdated = 0;
    static unsigned bestLapUpdated = 0;

    if (tm->LapsComplete() > 0)
    {
        RPN::SetVariable(vt, "LAP currentDelta", tm->LapDelta());
    }

    if (lapChanged)
    {
        const LapData &best = tm->BestLap();
        const LapData &last = tm->LastLap();

        if (bestLapUpdated != best.lap)
        {
            RPN::SetVariable(vt, "LAP bestLap",      best.lap);
            RPN::SetVariable(vt, "LAP bestDuration", best.duration);
            RPN::SetVariable(vt, "LAP bestDistance", best.distance);
            RPN::SetVariable(vt, "LAP bestFuelUsed", best.fuelUsed);
            bestLapUpdated = best.lap;
        }
        if (lastLapUpdated != last.lap)
        {
            RPN::SetVariable(vt, "LAP lastLap",      last.lap);
            RPN::SetVariable(vt, "LAP lastDuration", last.duration);
            RPN::SetVariable(vt, "LAP lastDistance", last.distance);
            RPN::SetVariable(vt, "LAP lastFuelUsed", last.fuelUsed);
            lastLapUpdated = last.lap;
        }

        // Average last three laps fuel burn for display with emphasis on last lap
        unsigned lap = last.lap;
        double averageUse = (tm->GetLap(lap - 2).fuelUsed + tm->GetLap(lap - 1).fuelUsed + (last.fuelUsed * 2.0)) / 4.0;
        RPN::SetVariable(vt, "LAP averageFuelUsed", averageUse);

        double fuelRemain;
        RPN::GetVariable(vt, "ECU GallonsRemaining", fuelRemain);

        double fuelLapsRemaining = fuelRemain / averageUse;
        RPN::SetVariable(vt, "LAP fuelLapsRemaining", fuelLapsRemaining);
    }
}

//--------------------------------------------------------------------------
void SensorManager::RemoveAllVariables(void)
{
    while(!mVariables.empty())
    {
        Variable v = mVariables.back();
        mVariables.pop_back();
        free(v.name);
        free(v.equation);
        delete v.expression;
    }
}

//--------------------------------------------------------------------------
bool SensorManager::RemoveVariable(const char *name)
{
    uint32_t hash = RPN::VariableNameHash(name);

    // Next, see if it's an existing variable so it can be updated
    auto it = mVariables.begin();
    for(;it != mVariables.end(); ++it)
    {
        if (it->nameHash == hash)
        {
            Variable v = *it;
            mVariables.erase(it);
            free(v.name);
            free(v.equation);
            delete v.expression;
            return true;
        }
    }
    return false;
}


//--------------------------------------------------------------------------
bool SensorManager::LoadVariables(void)
{
    RemoveAllVariables();

    const char *name;
    const char *equation;
    unsigned index = 0;
    while (ConfigGet(VARIABLES_ROOT_PATH, index, name, equation))
    {
        log_info("Loading variable %s = %s", name, equation);
        SetVariable(name, equation);
        ++index;
    }
    return true;
}

//--------------------------------------------------------------------------
bool SensorManager::SetVariable(const char *name, const char *eq)
{
    // First make sure it parses
    RPN::ParseError err;
    RPN::Expression *ex = RPN::Parse(eq, err);
    if (ex == NULL)
    {
        log_warn("Error parsing Variable %s: %s at %d", name, err.error, err.pos);
        return false;
    }

    uint32_t hash = RPN::VariableNameHash(name);

    // Next, see if it's an existing variable so it can be updated
    unsigned a;
    for(a = 0; a < mVariables.size(); a++)
    {
        if (mVariables[a].nameHash == hash)
        {
            char *oldEq            = mVariables[a].equation;
            RPN::Expression *oldEx = mVariables[a].expression;
            mVariables[a].equation   = strdup(eq);
            mVariables[a].expression = ex;
            free(oldEq);
            delete oldEx;
            return true;
        }
    }

    // Last, make a new one if it doesn't exist
    Variable v;
    v.name       = strdup(name);
    v.nameHash   = hash;
    v.equation   = strdup(eq);
    v.expression = ex;

    mVariables.push_back(v);
    return true;
}

//--------------------------------------------------------------------------
// Saves them to the global config
bool SensorManager::SaveVariables(void)
{
    unsigned a;
    for(a = 0; a < mVariables.size(); a++)
    {
        ConfigSet(VARIABLES_ROOT_PATH, mVariables[a].name, mVariables[a].equation);
    }
    return true;
}

//--------------------------------------------------------------------------
bool SensorManager::BindPPS(const char *device)
{
    int f = open(device, O_RDWR);
    if (f < 0)
    {
        log_error("Can't open PPS device %s", device);
        return false;
    }

    int ret;

    struct pps_fdata fdata;
    ret = ioctl(f, PPS_FETCH, &fdata);
    log_info("PPS Fetch returned %d", ret);
    log_info("info.assert_seq:   %08X", fdata.info.assert_sequence);
    log_info("info.current_mode: %08X", fdata.info.current_mode);

    struct pps_bind_args __bind_args;

    __bind_args.tsformat = PPS_TSFMT_TSPEC;
    __bind_args.edge     = PPS_CAPTUREASSERT;
    __bind_args.consumer = PPS_KC_HARDPPS;

    ret = ioctl(f, PPS_KC_BIND, &__bind_args);
    close(f);
    log_info("PPS Bind returned %d", ret);

    return ret == 0;
}


//--------------------------------------------------------------------------
void SensorManager::TestTrackLocation(double lat, double lon)
{
    if (mTestedTrackLocation) return;
    mTestedTrackLocation = true;

    log_info("Testing GPS locations vs. current track:");
    // If the currently selected track is over 5Km away, select the current track
    std::vector<const Track *> tracks;
    gTrackManager->ClosestTracks(lat, lon, tracks);
    if ((gTrackManager->SelectedTrack() == NULL) ||
        (gTrackManager->SelectedTrack()->Distance() > 5000.0))
    {
        log_info("  GPS says we were too far away.");
        gTrackManager->SelectTrack(tracks[0]->Name());
    }
    else
    {
        log_info("  GPS says we were close, keeping selected track.");
    }
}


//--------------------------------------------------------------------------
//--------------------------------------------------------------------------
void *SensorManager::ThreadInit(void *context)
{
    SensorManager *sm = (SensorManager *) context;
    if (sm == NULL) return NULL;

    void *retval = NULL;

    if (sm->mIsRemote) retval = sm->SensorThreadRemote();
    else               retval = sm->SensorThread();

    delete sm;
    return retval;
}

//--------------------------------------------------------------------------
SensorManager *SensorManager::sSensorManager = NULL;
bool SensorManager::Init(bool isRemote)
{
    sSensorManager = new SensorManager(isRemote);
    if (sSensorManager == NULL) return false;

    sSensorThread = std::thread(&SensorManager::ThreadInit, sSensorManager);
    pthread_setname_np(sSensorThread.native_handle(), "SensorManager");
    return true;
}

//--------------------------------------------------------------------------
bool SensorManager::Quit(void)
{
    sWantQuit = true;
    sSensorThread.join();
    return true;
}

