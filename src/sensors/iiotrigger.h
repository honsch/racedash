#ifndef IIOTRIGGER_H
#define IIOTRIGGER_H

//--------------------------------------------------------------------
class IIOTrigger
{
  public:

    IIOTrigger(const char *id, const char *name);
    virtual ~IIOTrigger();

    virtual const char *Type(void) const { return "Generic"; }
    const char *ID(void)   const { return mID; }
    const char *Name(void) const { return mName; }

  private:
    char *mID;
    char *mName;
};

//--------------------------------------------------------------------
class IIOSysfsTrigger : public IIOTrigger
{
  public:

    IIOSysfsTrigger(const char *id, const char *name);
    virtual ~IIOSysfsTrigger();

    const char *Type(void) const { return "Sysfs"; }

    bool Trigger(void);

    static bool Create(unsigned id);
    static bool Destroy(unsigned id);

  private:
    int   mFD;
};

//--------------------------------------------------------------------
class IIOHRTimerTrigger : public IIOTrigger
{
  public:

    IIOHRTimerTrigger(const char *id, const char *name);
    virtual ~IIOHRTimerTrigger();

    const char *Type(void) const { return "HrTimer"; }

    bool SetFrequency(unsigned freq);

    static bool Create(const char *name);
    static bool Destroy(const char *name);

    static bool IsTrigger(const char *id);

  private:
    unsigned   mFrequency;

};

#endif // IIOTRIGGER_H
