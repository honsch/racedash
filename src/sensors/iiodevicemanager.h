#ifndef IIO_DEVICEMANAGER_H
#define IIO_DEVICEMANAGER_H

#include <vector>
#include <string.h>

#include <poll.h>

class IIODevice;
class IIOTrigger;

class IIODeviceManager
{
  public:
    IIODeviceManager(void);
   ~IIODeviceManager();

    int Enumerate(void);

    IIODevice  *FindDeviceByType(const char *type, unsigned which = 0);
    IIODevice  *FindDeviceByID(const char *id);
    IIOTrigger *FindTriggerByName(const char *name);

    bool EnableBufferAll(bool enable = true);
    bool ActivateBufferAll(bool enable = true);
    bool UpdateAll(void);

    bool CreateSysfsTrigger(const char *name);

    bool UpdatePoll(void);

    bool CalibrateAll(void);

  private:
    bool ResetAllDeviceState(void);
    int EnumerateDevices(void);
    int EnumerateTriggers(void);

    IIODevice  *CreateDevice(const char *id, const char *name);
    IIOTrigger *CreateTrigger(const char *id, const char *name);

    std::vector<IIODevice *> mDevices;
    std::vector<IIOTrigger *> mTriggers;
    struct pollfd mPollFDs[64]; // max 64 devices
};


#endif // IIO_DEVICEMANAGER_H