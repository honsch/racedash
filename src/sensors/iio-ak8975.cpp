#include "iio-ak8975.h"
#include "iiodevicemanager.h"

#include <stdio.h>

#include "log/log.h"

//---------------------------------------------------------
IIOAk8975::IIOAk8975(const char *id, IIODeviceManager *owner) :
    IIODevice(id)
{
    // Setup the axis
    mMag[X].id    = AddChannel("in_magn_x");
    mMag[X].bias  = 0.0;
    mMag[X].scale = ReadChannel("in_magn_x_scale");

    mMag[Y].id    = AddChannel("in_magn_y");
    mMag[Y].bias  = 0.0;
    mMag[Y].scale = ReadChannel("in_magn_y_scale");

    mMag[Z].id    = AddChannel("in_magn_z");
    mMag[Z].bias  = 0.0;
    mMag[Z].scale = ReadChannel("in_magn_z_scale");

    mTimestampID = AddChannel("in_timestamp", true);

    IIOTrigger *t = owner->FindTriggerByName("mpu9250-dev2");
    if (t == NULL) log_warn("IIOAk8975 init error: Can't find device trigger!");
    SetBufferSize(256);
    SetTrigger(t);

    mMagID[0] = DataLogger::AddVariable("IMU Mag X", "", S16, 32767.0);
    mMagID[1] = DataLogger::AddVariable("IMU Mag Y", "", S16, 32767.0);
    mMagID[2] = DataLogger::AddVariable("IMU Mag Z", "", S16, 32767.0);
}

//---------------------------------------------------------
IIOAk8975::~IIOAk8975()
{

}

//---------------------------------------------------------
void IIOAk8975::ProcessUpdate(void)
{
    Sample s;
    double raw;

    ChannelCurrentValue(mTimestampID, raw);
    s.Timestamp() = RawTimeToTimestamp(raw); //raw / 1000000000.0; // convert to seconds UTC
    //log_debug("AK8975    sample at %f\n", s.Timestamp());

    unsigned a;
    for (a = 0;a < s.Size(); a++)
    {
        double raw;
        ChannelCurrentValue(mMag[a].id, raw);
        s[a] = (raw + mMag[a].bias) * mMag[a].scale;
        DataLogger::UpdateVariable(mMagID[a], s.Timestamp(), s[a]);
    }

    mBuffer.Push(s.Timestamp(), s);

    //log_debug("AK  %f %f %f %f\n", s.Timestamp(), s[0], s[1], s[2]);
}

//---------------------------------------------------------
int IIOAk8975::SampleAt(double time, Sample &out) const
{
    Sample early, late;
    int result = mBuffer.FindTimestampBrackets(time, early, late);
    if (result != 0) return result;
    early.Interpolate(out, time, late);
    return 0;
}
