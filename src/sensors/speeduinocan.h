#ifndef SPEEDUINOCAN_H
#define SPEEDUINOCAN_H

#include "datalogger/datalogger.h"

#include <stdint.h>
#include <thread>


// All data is little endian
struct SpeeduinoCANBinaryData
{
    uint8_t     seconds;            //   0
    uint8_t     status1;            //   1
    uint8_t     engineStatus;       //   2
    uint8_t     dwell;              //   3
    uint16_t    MAP;                //   4-5
    uint8_t     IAT;                //   6  *C+40
    uint8_t     CLT;                //   7  *C+40
    uint8_t     batteryCorrection;  //   8
    uint8_t     batteryVoltage;     //   9  10*Volts
    uint8_t     O2;                 //  10
    uint8_t     egoCorrection;      //  11  %
    uint8_t     IATCorrection;      //  12  %
    uint8_t     WUECorrection;      //  13  %
    uint16_t    RPM;                //  14-15
    uint8_t     accelEnrichment;    //  16  %
    uint8_t     gammaEnrichment;    //  17  %
    uint8_t     VE;                 //  18  %
    uint8_t     AFRTarget;          //  19
    uint16_t    pulsewidth1;        //  20-21 in 100us increments
    uint8_t     TPSDot;             //  22
    uint8_t     advance;            //  23
    uint8_t     TPS;                //  24
    uint16_t    loopsPerSecond;     //  25-26
    uint16_t    freeRAM;            //  27-28
    uint8_t     boostTarget;        //  29  boost/2
    uint8_t     boostSolenoidDuty;  //  30  %
    uint8_t     sparkBitfield;      //  31
    int16_t     rpmDot;             //  32-33
    uint8_t     ethanolPercent;     //  34
    uint8_t     flexCorrection;     //  35  (% above or below 100)
    uint8_t     flexIgnitionCorrection; // 36  degrees advanced
    uint8_t     idleLoad;           //  37
    uint8_t     testOutputs;        //  38
    uint8_t     O2_2;               //  39
    uint8_t     barometer;          //  40
    uint16_t    canIn[16];          //  41-72
    uint8_t     TPSADC;             //  73
    uint8_t     nextError;          //  74
    uint8_t     launchCorrection;   //  75
    uint16_t    pulsewidth2;        //  76-77 in 100us increments
    uint16_t    pulsewidth3;        //  78-79 in 100us increments
    uint16_t    pulsewidth4;        //  80-81 in 100us increments
    uint8_t     status3;            //  82    resentLockOn(0), nitrousOn(1), fuel2Active(2), vssRefresh(3), halfSync(4), nSquirts(6:7)
    uint8_t     engineProtectStatus;//  83    RPM(0), MAP(1), OIL(2), AFR(3), Unused(4:7)
    uint16_t    fuelLoad;           //  84-85
    uint16_t    ignitionLoad;       //  86-87
    uint16_t    injectionAngle;     //  88-89
    uint8_t     idleDuty;           //  90
    uint8_t     CLIdleTarget;       //  91
    uint8_t     mapDot;             //  92
    int8_t      vvt1Angle;          //  93
    int8_t      vvt1AngleTarget;    //  94
    uint8_t     vvt1Duty;           //  95
    uint16_t    flexBoostCorrection;//  96-97
    uint8_t     baroCorrection;     //  98
    uint8_t     ASEValue;           //  99 %
    uint16_t    VSS;                // 100-101
    int8_t      gear;               // 102
    uint8_t     fuelPressure;       // 103
    uint8_t     oilPressure;        // 104
    uint8_t     wmiPW;              // 105
    uint8_t     status4;            // 106    wmiEmptyBit(0), vvt1Error(1), vvt2Error(2), fanStatus(3), UnusedBits(4:7)
    int8_t      vvt2Angle;          // 107
    int8_t      vvt2AngleTarget;    // 108
    uint8_t     vvt2Duty;           // 109
    uint8_t     outputStatus;       // 110
    uint8_t     fuelTemp;           // 111 *C+40
    uint8_t     fuelTempCorrection; // 112 %
    uint8_t     VE1;                // 113
    uint8_t     VE2;                // 114
    uint8_t     advance1;           // 115
    uint8_t     advance2;           // 116
    uint8_t     nitrousStatus;      // 117
    uint8_t     SDStatus;           // 118 SD card status
    uint16_t    EMAP;               // 119-120    !!!!! BAD ALIGNMENT !!!!!
    uint8_t     fanDuty;            // 121
};

//------------------------------------------------------------
class SpeeduinoCANInterface
{
  public:
    SpeeduinoCANInterface(const char *device, uint32_t baudrate);
   ~SpeeduinoCANInterface();

    bool InitOK(void) const { return mDevice > 0; }

    double NewestSampleTime(void) const { return mSpeeduinoStatusTime; }

    unsigned ValidSampleCount(void) const { return mUpdateCount; }
    //unsigned SyncLossCounter(void) const  { return (unsigned) mSpeeduinoStatus.syncLossCounter; }

    double Seconds(void) const          { return (double) mSpeeduinoStatus.seconds; }
    double MAP(void) const              { return (double) mSpeeduinoStatus.MAP; }
    double RPM(void) const              { return (double) mSpeeduinoStatus.RPM; }
    double IAT(void) const              { return -40.0 + (double) mSpeeduinoStatus.IAT; }
    double CLT(void) const              { return -40.0 + (double) mSpeeduinoStatus.CLT; }
    double TPS(void) const              { return (double) mSpeeduinoStatus.TPS; }
    double TPSDot(void) const           { return (double) mSpeeduinoStatus.TPSDot; }
    double Advance(void) const          { return (double) mSpeeduinoStatus.advance; }
    double O2(void) const               { return 0.1 * (double) mSpeeduinoStatus.O2; }
    double AFRTarget(void) const        { return 0.1 * (double) mSpeeduinoStatus.AFRTarget; }
    double OilPressure(void) const      { return (double) mSpeeduinoStatus.oilPressure; }

    double VBat(void) const             { return 0.1 * (double) mSpeeduinoStatus.batteryVoltage; }

    double EGOCorrection(void) const    { return (double) mSpeeduinoStatus.egoCorrection; }
    double IATCorrection(void) const    { return (double) mSpeeduinoStatus.IATCorrection; }
    double WUECorrection(void) const    { return (double) mSpeeduinoStatus.WUECorrection; }

    double Pulsewidth(void) const      { return 0.1 * (double) mSpeeduinoStatus.pulsewidth1; }

  protected:
    char  *mSerialDeviceName;
    int    mDevice;
    double mSecsPerByte;

    bool mWantQuit;
    std::thread mUpdateThread;

    static void *ThreadInit(void *);
    void *UpdateThread(void);

    bool ReadCurrentStatus(void);
    bool ProcessInput(void);
    void CopyBadData(SpeeduinoCANBinaryData &out, uint8_t *raw);
    enum State { Idle, WaitStart, ReadingData } mState;

    static const unsigned sDataSize = 122;
    static const unsigned sResponseHeaderSize = 2;
    unsigned mReadProgress;
    uint8_t  mDataBuffer[sDataSize];
    uint8_t  mResponseHeader[sResponseHeaderSize];
    uint8_t  mExpectedResponseHeader[sResponseHeaderSize];

    double                 mSpeeduinoStatusTime;
    SpeeduinoCANBinaryData mSpeeduinoStatus;
    unsigned               mUpdateCount;

    double                 mFailTime;

    bool OpenSerial(const char *device);
    void CloseSerial(void);
    void InitSerial(void);

    void ResetReadState(void);

    void InitLog(void);
    void UpdateLog(double time);

    DataLoggerID mSyncLossCounterID;
    DataLoggerID mMAPID;
    DataLoggerID mMAPDotID;
    DataLoggerID mRPMID;
    DataLoggerID mIATID;
    DataLoggerID mCLTID;
    DataLoggerID mTPSID;
    DataLoggerID mTPSDotID;
    DataLoggerID mAdvanceID;
    DataLoggerID mO2ID;
    DataLoggerID mAFRTargetID;
    DataLoggerID mOilPressureID;
    DataLoggerID mVBatID;

    DataLoggerID mEGOCorrectionID;
    DataLoggerID mIATCorrectionID;
    DataLoggerID mWUECorrectionID;

    DataLoggerID mPulsewidthID;
};







#endif // SPEEDUINOCAN_H
