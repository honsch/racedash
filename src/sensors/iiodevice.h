#ifndef IIODEVICE_H
#define IIODEVICE_H

#include <vector>
#include <mutex>

#include <stdint.h>
#include <string.h>
#include <poll.h>
#include "iiopaths.h"
#include "util/util.h"

class IIOTrigger;
class IIOSysfsTrigger;
class IIODeviceManager;

//----------------------------------------------
template <size_t N>
class IIODeviceSample
{
  public:
    size_t Size(void) const { return N ; }
    double &operator[](size_t index) { return values[index]; }
    double &Timestamp(void) { return timestamp; }
    bool    Interpolate(IIODeviceSample<N> &out, double time, const IIODeviceSample<N> &late) const
    {
        if (time < timestamp)
        {
            printf("time < timestamp fail  %.3f  %.3f\n", time, timestamp);
            return false;
        }
        if (time > late.timestamp)
        {
            printf("time > late.timestamp fail  %.3f  %.3f\n", time, late.timestamp);
            return false;
        }

        double timeDiff = late.timestamp - timestamp;
        if (timeDiff < 0.0)
        {
            printf ("Interp %.3f  %.3f - %.3f failed\n", time, timestamp, late.timestamp);
            return false;
        }
        double t = (time - timestamp) / timeDiff;
        out.timestamp = time;
        unsigned a;
        for (a = 0; a < N; a++)
        {
            out.values[a] = Interp(values[a], late.values[a], t);
        }
        return true;
    }

    IIODeviceSample(void)
    {
        timestamp = 0.0;
        memset(values, 0, sizeof(double) * N);
    }

  private:
    double timestamp;
    double values[N];
};

//----------------------------------------------
class IIODevice
{
  public:
    IIODevice(const char *id);
    virtual ~IIODevice();

    const char *ID(void) const { return mID; }
    virtual const char *Type(void) const = 0;

    bool BufferEnableAll(bool enable = true);

    bool Update(bool triggered);
    virtual void ProcessUpdate(void) = 0;

    // These are for the entire device
    bool SetBufferSize(uint32_t size);
    bool SetTrigger(IIOTrigger *trigger);
    bool SetChainedTrigger(IIOSysfsTrigger *trigger);

    virtual bool ActivateBuffer(struct pollfd *fds, bool enable = true);

    bool CurrentTrigger(void);

    virtual bool Calibrate(void) { return true; }

  protected:
    inline void Lock(void)   { mLock.lock();   }
    inline void Unlock(void) { mLock.unlock(); }

    // Add the channel to the device
    uint32_t AddChannel(const char *name, bool bufferedOnly = false);

    // Enable the channel for buffer based capturing
    bool BufferEnableChannel(uint32_t id, bool enable = true);

    // Single shot read.  Does not work if buffered capture is running!
    // This is very slow.  Don't use it often.
    double ReadChannel(uint32_t id);
    double ReadChannel(const char *name);

    bool ChannelCurrentValue(uint32_t id, double &value);

    bool EnableTrigger(bool enable = true);
    bool ClearSysfsTrigger(void);
    bool ClearSystemBuffer(void);

    double RawTimeToTimestamp(double raw);

  private:
  protected:
    struct IIOChannel
    {
        IIOChannel(uint32_t cid, const char *id, const char *name, bool _bufferedOnly = false);
       ~IIOChannel();
        bool UpdateBufferSpecs(uint16_t &curOffset);
        bool EnableBuffer(bool enable, const char *id);

        bool Update(uint8_t *buffer, uint32_t size);

        bool operator<(const IIOChannel &rhs) const { return index < rhs.index; }
        char       *baseName;
        uint32_t    id;
        bool        isValid;
        bool        bufferedOnly;
        bool        scanEnabled;
        bool        isBigEndian;
        bool        isSigned;
        int8_t      index;
        uint8_t     size;   // in BYTES
        uint8_t     shift;  // out >>= bits
        uint16_t    offset; // In BYTES
        uint64_t    mask;   // mask for valid bits
        double      value;  // Set in Update()
    };

    char            *mID;
    uint8_t         *mRawBuffer;
    IIOTrigger      *mTrigger;
    IIOSysfsTrigger *mChainedTrigger;
    bool             mBufferActivated;
    uint16_t         mBufferSampleSize;
    int              mBufferFD;
    int              mChainedTriggerFD;

    std::mutex mLock;

    IIOChannel *FindChannel(uint32_t id);
    uint32_t mNextChannelID;
    std::vector<IIOChannel *> mChannels;
};

#endif //IIODEVICE_H