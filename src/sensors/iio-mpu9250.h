#ifndef IIO_MPU9250_H
#define IIO_MPU9250_H

#include "iiodevice.h"
#include "util/circularbuffer.h"
#include "datalogger/datalogger.h"

#include "util/averager.h"
class IIOMpu9250 : public IIODevice
{
  public:
    IIOMpu9250(const char *id, IIODeviceManager *owner);
    virtual ~IIOMpu9250();
    const char *Type(void) const { return "mpu9250"; }

    typedef IIODeviceSample<9> Sample;
    void ProcessUpdate(void);

    bool Calibrate(void);

    int SampleAt(double time, Sample &out) const;

    double NewestSampleTime(void) const { return mBuffer.NewestSampleTime(); }
    void NewestSample(size_t &count, Sample &out) { mBuffer.NewestSample(count, out); }

  private:

    bool SetSampleRate(unsigned rate);
    bool SetAccelerationScale(double scale);
    bool SetAngleScale(double scale);

    enum { AccX, AccY, AccZ, AngX, AngY, AngZ, MagX, MagY, MagZ, __AxisCount };

    struct Axis
    {
        int    id;
        double scale;
        double bias;
    };

    int  mTimestampID;
    Axis mAxis[__AxisCount];
    DataLoggerID mAxisIDs[__AxisCount];

    CircularBuffer<Sample, 50> mBuffer;

    static const unsigned CAL_RUN_LEN = 250;
    bool      mCalRunning;
    unsigned  mCalSampleCount;
    Averager *mCalAverages[6];

};



#endif // IIO_MPU9250_H