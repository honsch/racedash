#include "iiodevicemanager.h"
#include "util/util.h"

#include <dirent.h>
#include <stdio.h>
#include <stdint.h>
#include <poll.h>

#include "iio-max11616.h"
#include "iio-mpu9250.h"

#include "iiotrigger.h"

#include "log/log.h"

//------------------------------------------------------------
IIODeviceManager::IIODeviceManager(void)
{
    mDevices.clear();
    mTriggers.clear();

    ResetAllDeviceState();

    EnumerateTriggers();

    // This will delete all non-generic triggers
    // We want to make sure that any old trigger left around gets cleaned up
    while (!mTriggers.empty())
    {
        delete mTriggers.back();
        mTriggers.pop_back();
    }
}

//------------------------------------------------------------
IIODeviceManager::~IIODeviceManager()
{
    while (!mDevices.empty())
    {
        delete mDevices.back();
        mDevices.pop_back();
    }

    while (!mTriggers.empty())
    {
        delete mTriggers.back();
        mTriggers.pop_back();
    }
}

//------------------------------------------------------------
IIODevice *IIODeviceManager::CreateDevice(const char *id, const char *name)
{
    if      (0 == strcmp(name, "max11616")) return new IIOMax11616(id, this);
    else if (0 == strcmp(name, "mpu9250"))  return new IIOMpu9250(id, this);

    return NULL;
}

//------------------------------------------------------------
IIOTrigger *IIODeviceManager::CreateTrigger(const char *id, const char *name)
{
#if ODROID == 2    
    if      (0 == strcmp(name, "mpu9250-dev1"))  return new IIOTrigger(id, name);
#else
    if      (0 == strcmp(name, "mpu9250-dev2"))  return new IIOTrigger(id, name);
#endif    
    else if (0 == strncmp(name, "sysfstrig", 9)) return new IIOSysfsTrigger(id, name);
    else if (IIOHRTimerTrigger::IsTrigger(id))   return new IIOHRTimerTrigger(id, name);

    return NULL;
}

//------------------------------------------------------------
int IIODeviceManager::Enumerate(void)
{
    int count = 0;
    // Enumerate triggers first so a device can find its custom trigger on creation
    count += EnumerateTriggers();
    count += EnumerateDevices();
    return count;
}

//------------------------------------------------------------
int IIODeviceManager::EnumerateDevices(void)
{
    int index = 0;
    while (1)
    {
        char id[64];
        snprintf(id, 64, "iio:device%d", index);

        char fname[512];
        snprintf(fname, 512, "%s/%s/name", IIO_SYSFS_PATH, id);
        uint32_t size;
        char *name = (char *)ReadFile(fname, size);
        if (name == NULL) break;
        char *end = strchr(name, '\n');
        if (end != NULL) *end = 0;

        // Only add unique devices
        if (FindDeviceByID(id) == NULL)
        {
            IIODevice *d = CreateDevice(id, name);
            if (d != NULL) mDevices.push_back(d);
            else           log_warn("No matching IIODevice for %s/%s", id, name);
        }
        free(name);
        ++index;
    }

    log_info("IIODeviceManager found %d devices", (int) mDevices.size());
    return (int) mDevices.size();
}

//------------------------------------------------------------
int IIODeviceManager::EnumerateTriggers(void)
{
    struct dirent *dir;
    DIR *d = opendir(IIO_SYSFS_PATH);
    if (d == NULL) return -1;

    for (dir = readdir(d); dir != NULL; dir = readdir(d))
    {
        // skip hidden items
        if (dir->d_name[0] == '.') continue;
        if (0 != strncmp(dir->d_name, "trigger", 7)) continue;
        // if it's not a dir I don't know what it is
        //if (dir->d_type != DT_DIR) continue;

        char fname[512];
        snprintf(fname, 512, "%s/%s/name", IIO_SYSFS_PATH, dir->d_name);
        uint32_t size;
        char *name = (char *)ReadFile(fname, size);
        if (name == NULL) continue;
        char *end = strchr(name, '\n');
        if (end != NULL) *end = 0;

        // Only add unique devices
        if (FindTriggerByName(name) == NULL)
        {
            IIOTrigger *t = CreateTrigger(dir->d_name, name);
            if (t != NULL) mTriggers.push_back(t);
        }
        free(name);
    }
    closedir(d);

    log_info("IIODeviceManager found %d triggers", (int) mTriggers.size());
    return (int) mTriggers.size();
}

//------------------------------------------------------------
IIODevice *IIODeviceManager::FindDeviceByType(const char *type, unsigned which)
{
    unsigned a;
    unsigned index = 0;
    for (a = 0;a < mDevices.size(); a++)
    {
        if (0 == strcmp(mDevices[a]->Type(), type))
        {
            if (index == which) return mDevices[a];
            ++index;
        }
    }
    return NULL;
}

//------------------------------------------------------------
IIODevice *IIODeviceManager::FindDeviceByID(const char *id)
{
    unsigned a;
    for (a = 0;a < mDevices.size(); a++)
    {
        if (0 == strcmp(mDevices[a]->ID(), id))
        {
            return mDevices[a];
        }
    }
    return NULL;
}

//------------------------------------------------------------
IIOTrigger *IIODeviceManager::FindTriggerByName(const char *name)
{
    unsigned a;
    for (a = 0;a < mTriggers.size(); a++)
    {
        if (0 == strcmp(mTriggers[a]->Name(), name))
        {
            return mTriggers[a];
        }
    }
    return NULL;
}

//------------------------------------------------------------
bool IIODeviceManager::EnableBufferAll(bool enable)
{
    bool ok = true;
    unsigned a;
    for (a = 0;a < mDevices.size(); a++)
    {
        ok &= mDevices[a]->BufferEnableAll(enable);
    }

    return ok;
}

//------------------------------------------------------------
bool IIODeviceManager::ActivateBufferAll(bool enable)
{
    bool ok = true;
    unsigned a;
    for (a = 0;a < mDevices.size(); a++)
    {
        ok &= mDevices[a]->ActivateBuffer(&mPollFDs[a], enable);
    }

    return ok;
}

//------------------------------------------------------------
bool IIODeviceManager::UpdateAll(void)
{
    bool ok = true;
    unsigned a;
    for (a = 0;a < mDevices.size(); a++)
    {
        ok &= mDevices[a]->Update(false);
    }

    return ok;
}
//------------------------------------------------------------
bool IIODeviceManager::CalibrateAll(void)
{
    bool ok = true;
    unsigned a;
    for (a = 0;a < mDevices.size(); a++)
    {
        ok &= mDevices[a]->Calibrate();
    }

    return ok;
}

//------------------------------------------------------------
bool IIODeviceManager::UpdatePoll(void)
{
    int num = poll(mPollFDs, mDevices.size(), 250);
    if (num > 0)
    {
        bool ok = true;
        unsigned a;
        for (a = 0;a < mDevices.size(); a++)
        {
            if ((mPollFDs[a].revents & POLLIN) != 0)
            {
                ok &= mDevices[a]->Update(true);
            }
        }
        return ok;
    }
    return false;
}

//------------------------------------------------------------
bool IIODeviceManager::ResetAllDeviceState(void)
{
    int index = 0;
    int found = 0;
    // If there's more than 256 devices I'd really like to see the car!
    while (index < 256)
    {
        char id[64];
        snprintf(id, 64, "iio:device%d", index);

        char fname[512];

        // Turn off buffering (if it exists)
        snprintf(fname, 512, "%s/%s/buffer/enable", IIO_SYSFS_PATH, id);
        WriteFileRaw(fname, "0", 1);

        // Same with triggering
        snprintf(fname, 512, "%s/%s/trigger/current_trigger", IIO_SYSFS_PATH, id);
        bool ok = WriteFileRaw(fname, "NULL", 4);
        if (ok) ++found;
        //log_debug("Clearing trigger on %s : %d", fname, (int) ok);
        ++index;
    }
    log_info("Cleared %d triggers", found);
    return (found > 0);
}
