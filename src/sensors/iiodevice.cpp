#include "iiodevice.h"
#include "iiotrigger.h"
#include "util/util.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

#include "log/log.h"

#include <algorithm>


//------------------------------------------------------------
IIODevice::IIODevice(const char *id) :
    mRawBuffer(NULL),
    mTrigger(NULL),
    mChainedTrigger(NULL),
    mBufferActivated(true),
    mBufferSampleSize(0),
    mBufferFD(-1),
    mNextChannelID(1)
{
    mID = strdup(id);
    ActivateBuffer(NULL, false);
    ClearSysfsTrigger();
}

//------------------------------------------------------------
IIODevice::~IIODevice()
{
    ActivateBuffer(NULL, false);
    SetTrigger(NULL);
    free(mID);

    while (!mChannels.empty())
    {
        IIOChannel *c = mChannels[mChannels.size() - 1];
        mChannels.pop_back();
        delete c;
    }
}

//------------------------------------------------------------
uint32_t IIODevice::AddChannel(const char *name, bool bufferedOnly)
{
    uint32_t cid = mNextChannelID;
    ++mNextChannelID;

    IIOChannel *c = new IIOChannel(cid, ID(), name, bufferedOnly);
    if (c->isValid)
    {
        mChannels.push_back(c);
        //std::sort(mChannels.begin(), mChannels.end());
        c->EnableBuffer(false, ID());
    }
    else
    {
        cid = 0;
        delete c;
    }
    return cid;
}

//------------------------------------------------------------
IIODevice::IIOChannel *IIODevice::FindChannel(uint32_t id)
{
    unsigned a;
    for (a = 0; a < mChannels.size(); a++)
    {
        if (mChannels[a]->id == id) return mChannels[a];
    }
    return NULL;
}

//------------------------------------------------------------
bool IIODevice::BufferEnableChannel(uint32_t id, bool enable)
{
    if (mBufferActivated) return false;

    IIOChannel *c = FindChannel(id);
    if (c == NULL) return false;
    if (c->size == 0) return false;

    bool ok = c->EnableBuffer(enable, ID());
    return ok;
}

//------------------------------------------------------------
bool IIODevice::BufferEnableAll(bool enable)
{
    if (mBufferActivated) return false;

    bool ok = true;
    unsigned a;
    for (a = 0; a < mChannels.size(); a++)
    {
        IIOChannel *c = mChannels[a];
        if (c == NULL)    continue;
        if (c->size == 0) continue;

        ok &= c->EnableBuffer(enable, ID());
    }

    return ok;
}

//------------------------------------------------------------
double IIODevice::ReadChannel(uint32_t id)
{
    if (mBufferActivated) return 0.0;

    IIOChannel *c = FindChannel(id);
    if (c == NULL) return false;

    char rawName[64];
    strcpy(rawName, c->baseName);
    strcat(rawName,"_raw");
    return ReadChannel(rawName);
}

//------------------------------------------------------------
double IIODevice::ReadChannel(const char *name)
{
    char pathName[512];
    snprintf(pathName, 512, "%s/%s/%s", IIO_SYSFS_PATH, ID(), name);

    int fd = open(pathName, O_RDONLY);
    if (fd < 0) return 0.0;

    char line[80];
    ssize_t got = read(fd, line, 80);
    close(fd);
    if (got == 0) return 0;
    line[got] = 0;

    return atof(line);
}

//------------------------------------------------------------
bool IIODevice::ChannelCurrentValue(uint32_t id, double &out)
{
    IIOChannel *c = FindChannel(id);
    if (c == NULL) return false;
    if (c->size == 0) return false;
    out = c->value;
    return true;
}


//------------------------------------------------------------
bool IIODevice::SetBufferSize(uint32_t size)
{
    char bufferSizeName[512];
    snprintf(bufferSizeName, 512, "%s/%s/buffer/length", IIO_SYSFS_PATH, ID());

    char sizeString[10];
    ssize_t len = snprintf(sizeString, 10, "%u", size);
    bool ok = WriteFileRaw(bufferSizeName, sizeString, len);
    return ok;
}

//------------------------------------------------------------
bool IIODevice::SetTrigger(IIOTrigger *trigger)
{
    if (mBufferActivated) return false;
    mTrigger = trigger;
    return true;
}

//------------------------------------------------------------
bool IIODevice::SetChainedTrigger(IIOSysfsTrigger *trigger)
{
    mChainedTrigger = trigger;
    return true;
}

//------------------------------------------------------------
bool IIODevice::EnableTrigger(bool enable)
{
    if (mBufferActivated) return false;

    const char *val;
    if ((mTrigger == NULL) || !enable) val = "NULL";
    else                               val = mTrigger->Name();

    char triggerName[512];
    snprintf(triggerName, 512, "%s/%s/trigger/current_trigger", IIO_SYSFS_PATH, ID());

    bool ok = WriteFileRaw(triggerName, val, strlen(val));
    if (!ok) log_warn("SetTrigger %s to %s failed", triggerName, val);

    return ok;
}

//------------------------------------------------------------
bool IIODevice::ClearSysfsTrigger(void)
{
    char triggerName[512];
    snprintf(triggerName, 512, "%s/%s/trigger/current_trigger", IIO_SYSFS_PATH, ID());

    bool ok = WriteFileRaw(triggerName, "NULL", 4);
    CurrentTrigger();

    return ok;
}

//------------------------------------------------------------
bool IIODevice::CurrentTrigger(void)
{
    char triggerName[512];
    snprintf(triggerName, 512, "%s/%s/trigger/current_trigger", IIO_SYSFS_PATH, ID());

    uint32_t size;
    char *trig = (char *)ReadFile(triggerName, size);
    if (trig == NULL)
    {
        log_info("%s has no trigger", triggerName);
    }
    else
    {
        log_info("%s -> %s", triggerName, trig);
        free(trig);
    }

    return true;
}

//------------------------------------------------------------
bool IIODevice::ActivateBuffer(struct pollfd *fds, bool enable)
{
    if ((mTrigger == NULL) && enable) return false;
    if (enable == mBufferActivated)   return true;

    delete[] mRawBuffer;

    if (enable)
    {
        uint16_t offset = 0;
        unsigned a;
        for (a = 0; a < mChannels.size(); a++)
        {
            mChannels[a]->UpdateBufferSpecs(offset);
        }
        mBufferSampleSize = offset;
        mRawBuffer = new uint8_t[mBufferSampleSize];
        log_debug("%s buffer is %d bytes per sample", ID(), (int) mBufferSampleSize);
    }
    else if (mBufferFD > 0)
    {
        close(mBufferFD);
        mBufferFD = -1;
    }

    ClearSystemBuffer();

    EnableTrigger(enable);

    CurrentTrigger();
    char enableName[512];
    snprintf(enableName, 512, "%s/%s/buffer/enable", IIO_SYSFS_PATH, ID());

    uint8_t val = enable ? '1' : '0';
    bool ok = WriteFileRaw(enableName, &val, 1);
    if (ok)
    {
        mBufferActivated = enable;
    }
    else
    {
        log_warn("Failed to set enable to %c for %s %s", val, ID(), enableName);
    }
    if (mBufferActivated)
    {
        char devName[32];
        snprintf(devName, 32, "/dev/%s", ID());
        mBufferFD = open(devName, O_RDONLY | O_NONBLOCK);
        if (mBufferFD < 0)
        {
            log_warn("Failed to open buffer for %s: %s", ID(), strerror(errno));
        }
        if (fds != NULL)
        {
            fds->fd     = mBufferFD;
            fds->events = POLLIN;
        }
    }

    return ok;
}

//------------------------------------------------------------
bool IIODevice::ClearSystemBuffer(void)
{
    if (mBufferActivated) return false;

    char devName[32];
    snprintf(devName, 32, "/dev/%s", ID());
    int fd = open(devName, O_RDONLY | O_NONBLOCK);
    if (fd < 0)
    {
        log_warn("ClearSystemBufer() Failed to open buffer for %s: %s", ID(), strerror(errno));
        return false;
    }

    char buffer[1024];
    while (1)
    {
        ssize_t amount = read(fd, buffer, mBufferSampleSize);
        if (amount != mBufferSampleSize) break;
    }

    close(fd);
    return true;
}

//------------------------------------------------------------
bool IIODevice::Update(bool triggered)
{
    if (triggered && (mChainedTrigger != NULL))
    {
        mChainedTrigger->Trigger();
        log_trace("Trigger chained trigger %s", mChainedTrigger->Name());
    }
    if (mBufferFD < 0) return false;

    bool ok = true;

    int sampleCount = 0;
    while (1)
    {
        ssize_t amount = read(mBufferFD, mRawBuffer, mBufferSampleSize);
        if (amount != (ssize_t) mBufferSampleSize)
        {
            if (sampleCount == 0) log_debug("%s:%s Amount read does not match %d should be %d", ID(), Type(), (int)amount, (int) mBufferSampleSize);
            break;
            //return false;
        }
        log_trace("%s read %d bytes from buffer", ID(), (int) amount);
        unsigned a;
#if 0
        printf("IIODevice::Update %s: ", ID());
        for (a = 0; a < amount; a++) printf(" 0x%02X", mRawBuffer[a]);
        printf("\n");
#endif
        for (a = 0; a < mChannels.size(); a++)
        {
            ok &= mChannels[a]->Update(mRawBuffer, mBufferSampleSize);
        }
        ProcessUpdate();
        ++sampleCount;
    }

    if (sampleCount == 0)
    {
        log_warn("Error %s did not read a sample from buffer", ID());
        return false;
    }
    else if (sampleCount > 10)
    {
        log_info("%s read %d samples on Update()", ID(), sampleCount);
    }
    return ok;
}

//------------------------------------------------------------
double IIODevice::RawTimeToTimestamp(double raw)
{
    uint64_t ms = (uint64_t) (raw / 1000000.0);
    return ((double)ms) / 1000.0;
}


//------------------------------------------------------------
//------------------------------------------------------------
IIODevice::IIOChannel::IIOChannel(uint32_t _cid, const char *_id, const char *name, bool _bufferedOnly) :
    baseName(NULL),
    id(_cid),
    isValid(false),
    bufferedOnly(_bufferedOnly),
    scanEnabled(false),
    isBigEndian(false),
    isSigned(false),
    index(-1),
    size(0),
    offset(0)
{
    char fn[512];
    if (bufferedOnly) snprintf(fn, 512, "%s/%s/scan_elements/%s_en", IIO_SYSFS_PATH, _id, name);
    else              snprintf(fn, 512, "%s/%s/%s_raw", IIO_SYSFS_PATH, _id, name);
    FILE *inf = fopen(fn, "rb");
    if (inf == NULL) return;
    baseName = strdup(name);
    isValid = true;
    fclose(inf);

    snprintf(fn, 512, "%s/%s/scan_elements/%s_index", IIO_SYSFS_PATH, _id, baseName);
    uint32_t filesize;
    char *indexFile = (char *)ReadFile(fn, filesize);
    if (indexFile == NULL) return;
    index = atoi(indexFile);
    free(indexFile);

    snprintf(fn, 512, "%s/%s/scan_elements/%s_type", IIO_SYSFS_PATH, _id, baseName);
    char *typeFile = (char *)ReadFile(fn, filesize);
    if (typeFile == NULL) return;

    char typeString[32];
    char *type = typeString;
    strncpy(type, typeFile, 31);
    free (typeFile);

    if (filesize > 31) return;

    // Parse the type
    // Typical values are 'be:s12/16>>0' or 'le:u16/16>>0' or 'le:s64/64>>0' for timestamps

    // Big endian or little endian?
    bool be = (NULL != strstr(type, "be"));

    // Signed or unsigned?
    while ((*type != 0) && (*type != ':')) ++type;
    ++type;
    if (*type == 0) return;
    bool si = (*type == 's');

    // How many valid bits?
    ++type;
    if (*type == 0) return;
    int vb = atoi(type);
    if (vb == 0) return;

    // How many storage bits
    while ((*type != 0) && (*type != '/')) ++type;
    ++type;
    if (*type == 0) return;
    int sb = atoi(type);
    if (sb == 0) return;
    if (sb < vb) return;
    if ((sb % 8) != 0) return;

    // What shift count?
    while ((*type != 0) && (*type != '>')) ++type;
    if (*type == 0) return;
    type += 2;
    if (*type == 0) return;
    int sc = atoi(type);
    // don't allow thrown away bits, it's an error.
    if ((sb - sc) < vb) return;

    // Don't update the defaults unless all parts are there
    isBigEndian = be;
    isSigned    = si;
    size        = sb / 8;
    shift       = sc;
    mask        = (1 << vb) - 1;
    if (vb == 64) mask = -1;

    log_debug("%s is %s %d with a shift of %d", fn, isSigned ? "Signed" : "Unsigned", ((int)size) * 8, (int) shift);
}

//------------------------------------------------------------
IIODevice::IIOChannel::~IIOChannel()
{
    if (baseName != NULL) free(baseName);
}

//------------------------------------------------------------
bool IIODevice::IIOChannel::UpdateBufferSpecs(uint16_t &curOffset)
{
    if (!isValid) return false;
    if (!scanEnabled || (size == 0))
    {
        log_info("Scan disabled for %s", baseName);
        offset =  0;
        return false;
    }

    // Align to size
    uint16_t pad = curOffset % size;
    if (pad != 0) pad = size - pad;

    // calc my offset
    offset = pad + curOffset;
    log_debug("Channel %s is at offset %d", baseName, (int) offset);
    // update where the next one starts
    curOffset = offset + size;
    return true;
}

//------------------------------------------------------------
bool IIODevice::IIOChannel::EnableBuffer(bool enable, const char *id)
{
    if (size == 0) return false;

    char enableName[512];
    snprintf(enableName, 512, "%s/%s/scan_elements/%s_en", IIO_SYSFS_PATH, id, baseName);
    uint8_t val = enable ? '1' : '0';
    bool ok = WriteFileRaw(enableName, &val, 1);
    if (ok) scanEnabled = enable;

    return ok;
}

//------------------------------------------------------------
bool IIODevice::IIOChannel::Update(uint8_t *buffer, uint32_t bufferSize)
{
    if (size == 0) return true;
    if ((offset + size) > bufferSize) return false;
    unsigned a;

    union
    {
        int64_t val64;
        uint8_t val8[8];
    } sv;

    union
    {
        uint64_t val64;
        uint8_t  val8[8];
    } uv;

    sv.val64 = 0;
    uv.val64 = 0;

    if (isSigned)
    {
        if (isBigEndian)
        {
            for (a = 0; a < size; a++) sv.val8[7 - a] = buffer[offset + a];
        }
        else
        {
            for (a = 0; a < size; a++) sv.val8[(8 - size) + a] = buffer[offset + a];
        }
        sv.val64 >>= (shift + ((8 - size) * 8));
        value = (double) (sv.val64 & mask);
    }
    else
    {
        if (isBigEndian)
        {
            for (a = 0; a < size; a++) uv.val8[(size - 1) - a] = buffer[offset + a];
        }
        else
        {
            for (a = 0; a < size; a++) uv.val8[a] = buffer[offset + a];
        }
        uv.val64 >>= shift;
        value = (double) (uv.val64 & mask);
    }
    return true;
}
