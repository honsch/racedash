//=====================================================================================================
// MadgwickAHRS.c
//=====================================================================================================
//
// Implementation of Madgwick's IMU and AHRS algorithms.
// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
//
// Date            Author          Notes
// 29/09/2011    SOH Madgwick    Initial release
// 02/10/2011    SOH Madgwick    Optimised for reduced CPU load
// 19/02/2012    SOH Madgwick    Magnetometer measurement is normalised
// xx/xx/2018    EH              cleaned up/cpp
//=====================================================================================================

//---------------------------------------------------------------------------------------------------
// Header files

#include "madgwick-ahrs.h"
#include <math.h>

//---------------------------------------------------------------------------------------------------
MadgwickAHRS::MadgwickAHRS(double sampleFrequency, double mBeta) :
	mSampleFrequency(sampleFrequency),
	mBeta(mBeta)
{
	mSensorFrame.x = 1.0;
	mSensorFrame.y = 0.0;
	mSensorFrame.z = 0.0;
	mSensorFrame.w = 0.0;    // quaternion of sensor frame relative to auxiliary frame
}

//---------------------------------------------------------------------------------------------------
MadgwickAHRS::~MadgwickAHRS()
{

}


//---------------------------------------------------------------------------------------------------
// AHRS algorithm update
// If you have magnetometer data use this
void MadgwickAHRS::Update(double gx, double gy, double gz, double ax, double ay, double az, double mx, double my, double mz)
{
    double recipNorm;
    Quaternion s;
    Quaternion qDot;

    double hx, hy;
    double _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;

    // Rate of change of quaternion from gyroscope
    qDot.x = 0.5 * (-mSensorFrame.y * gx - mSensorFrame.z * gy - mSensorFrame.w * gz);
    qDot.y = 0.5 * ( mSensorFrame.x * gx + mSensorFrame.z * gz - mSensorFrame.w * gy);
    qDot.z = 0.5 * ( mSensorFrame.x * gy - mSensorFrame.y * gz + mSensorFrame.w * gx);
    qDot.w = 0.5 * ( mSensorFrame.x * gz + mSensorFrame.y * gy - mSensorFrame.z * gx);

    // Normalize accelerometer measurement
    recipNorm = 1.0 / sqrt(ax * ax + ay * ay + az * az);
    ax *= recipNorm;
    ay *= recipNorm;
    az *= recipNorm;

    // Normalize magnetometer measurement
    recipNorm = 1.0 / sqrt(mx * mx + my * my + mz * mz);
    mx *= recipNorm;
    my *= recipNorm;
    mz *= recipNorm;

    // Auxiliary variables to avoid repeated arithmetic
    _2q0mx = 2.0 * mSensorFrame.x * mx;
    _2q0my = 2.0 * mSensorFrame.x * my;
    _2q0mz = 2.0 * mSensorFrame.x * mz;
    _2q1mx = 2.0 * mSensorFrame.y * mx;
    _2q0 = 2.0 * mSensorFrame.x;
    _2q1 = 2.0 * mSensorFrame.y;
    _2q2 = 2.0 * mSensorFrame.z;
    _2q3 = 2.0 * mSensorFrame.w;
    _2q0q2 = 2.0 * mSensorFrame.x * mSensorFrame.z;
    _2q2q3 = 2.0 * mSensorFrame.z * mSensorFrame.w;
    q0q0 = mSensorFrame.x * mSensorFrame.x;
    q0q1 = mSensorFrame.x * mSensorFrame.y;
    q0q2 = mSensorFrame.x * mSensorFrame.z;
    q0q3 = mSensorFrame.x * mSensorFrame.w;
    q1q1 = mSensorFrame.y * mSensorFrame.y;
    q1q2 = mSensorFrame.y * mSensorFrame.z;
    q1q3 = mSensorFrame.y * mSensorFrame.w;
    q2q2 = mSensorFrame.z * mSensorFrame.z;
    q2q3 = mSensorFrame.z * mSensorFrame.w;
    q3q3 = mSensorFrame.w * mSensorFrame.w;

    // Reference direction of Earth's magnetic field
    hx = mx * q0q0 - _2q0my * mSensorFrame.w + _2q0mz * mSensorFrame.z + mx * q1q1 + _2q1 * my * mSensorFrame.z + _2q1 * mz * mSensorFrame.w - mx * q2q2 - mx * q3q3;
    hy = _2q0mx * mSensorFrame.w + my * q0q0 - _2q0mz * mSensorFrame.y + _2q1mx * mSensorFrame.z - my * q1q1 + my * q2q2 + _2q2 * mz * mSensorFrame.w - my * q3q3;
    _2bx = sqrt(hx * hx + hy * hy);
    _2bz = -_2q0mx * mSensorFrame.z + _2q0my * mSensorFrame.y + mz * q0q0 + _2q1mx * mSensorFrame.w - mz * q1q1 + _2q2 * my * mSensorFrame.w - mz * q2q2 + mz * q3q3;
    _4bx = 2.0 * _2bx;
    _4bz = 2.0 * _2bz;

    // Gradient decent algorithm corrective step
    s.x = -_2q2 * (2.0 * q1q3 - _2q0q2 - ax) + _2q1 * (2.0 * q0q1 + _2q2q3 - ay) -   _2bz * mSensorFrame.z * (_2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * mSensorFrame.w + _2bz * mSensorFrame.y) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * mSensorFrame.z * (_2bx * (q0q2 + q1q3) + _2bz * (0.5 - q1q1 - q2q2) - mz);
    s.y =  _2q3 * (2.0 * q1q3 - _2q0q2 - ax) + _2q0 * (2.0 * q0q1 + _2q2q3 - ay) -    4.0 * mSensorFrame.y * (1 - 2.0 * q1q1 - 2.0 * q2q2 - az) + _2bz * mSensorFrame.w * (_2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * mSensorFrame.z + _2bz * mSensorFrame.x) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * mSensorFrame.w - _4bz * mSensorFrame.y) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s.z = -_2q0 * (2.0 * q1q3 - _2q0q2 - ax) + _2q3 * (2.0 * q0q1 + _2q2q3 - ay) -    4.0 * mSensorFrame.z * (1 - 2.0 * q1q1 - 2.0 * q2q2 - az) + (-_4bx * mSensorFrame.z - _2bz * mSensorFrame.x) * (_2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * mSensorFrame.y + _2bz * mSensorFrame.w) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * mSensorFrame.x - _4bz * mSensorFrame.z) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - mz);
    s.w =  _2q1 * (2.0 * q1q3 - _2q0q2 - ax) + _2q2 * (2.0 * q0q1 + _2q2q3 - ay) + (-_4bx * mSensorFrame.w + _2bz * mSensorFrame.y) * (_2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * mSensorFrame.x + _2bz * mSensorFrame.z) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * mSensorFrame.y * (_2bx * (q0q2 + q1q3) + _2bz * (0.5 - q1q1 - q2q2) - mz);
    recipNorm = 1.0 / sqrt(s.x * s.x + s.y * s.y + s.z * s.z + s.w * s.w); // normalise step magnitude
    s.x *= recipNorm;
    s.y *= recipNorm;
    s.z *= recipNorm;
    s.w *= recipNorm;

    // Apply feedback step
    qDot.x -= mBeta * s.x;
    qDot.y -= mBeta * s.y;
    qDot.z -= mBeta * s.z;
    qDot.w -= mBeta * s.w;

    // Integrate rate of change of quaternion to yield quaternion
    mSensorFrame.x += qDot.x * (1.0 / mSampleFrequency);
    mSensorFrame.y += qDot.y * (1.0 / mSampleFrequency);
    mSensorFrame.z += qDot.z * (1.0 / mSampleFrequency);
    mSensorFrame.w += qDot.w * (1.0 / mSampleFrequency);

    // Normalize quaternion
    recipNorm = 1.0 / sqrt(mSensorFrame.x * mSensorFrame.x + mSensorFrame.y * mSensorFrame.y + mSensorFrame.z * mSensorFrame.z + mSensorFrame.w * mSensorFrame.w);
    mSensorFrame.x *= recipNorm;
    mSensorFrame.y *= recipNorm;
    mSensorFrame.z *= recipNorm;
    mSensorFrame.w *= recipNorm;
}

//---------------------------------------------------------------------------------------------------
// IMU algorithm update
// If you have no magnetometer data use this
void MadgwickAHRS::UpdateIMU(double gx, double gy, double gz, double ax, double ay, double az)
{
    double recipNorm;
    Quaternion s;
    Quaternion qDot;
    double _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

    // Rate of change of quaternion from gyroscope
    qDot.x = 0.5 * (-mSensorFrame.y * gx - mSensorFrame.z * gy - mSensorFrame.w * gz);
    qDot.y = 0.5 * ( mSensorFrame.x * gx + mSensorFrame.z * gz - mSensorFrame.w * gy);
    qDot.z = 0.5 * ( mSensorFrame.x * gy - mSensorFrame.y * gz + mSensorFrame.w * gx);
    qDot.w = 0.5 * ( mSensorFrame.x * gz + mSensorFrame.y * gy - mSensorFrame.z * gx);

    // Normalize accelerometer measurement
    recipNorm = 1.0 / sqrt(ax * ax + ay * ay + az * az);
    ax *= recipNorm;
    ay *= recipNorm;
    az *= recipNorm;

    // Auxiliary variables to avoid repeated arithmetic
    _2q0 = 2.0 * mSensorFrame.x;
    _2q1 = 2.0 * mSensorFrame.y;
    _2q2 = 2.0 * mSensorFrame.z;
    _2q3 = 2.0 * mSensorFrame.w;
    _4q0 = 4.0 * mSensorFrame.x;
    _4q1 = 4.0 * mSensorFrame.y;
    _4q2 = 4.0 * mSensorFrame.z;
    _8q1 = 8.0 * mSensorFrame.y;
    _8q2 = 8.0 * mSensorFrame.z;
    q0q0 = mSensorFrame.x * mSensorFrame.x;
    q1q1 = mSensorFrame.y * mSensorFrame.y;
    q2q2 = mSensorFrame.z * mSensorFrame.z;
    q3q3 = mSensorFrame.w * mSensorFrame.w;

    // Gradient decent algorithm corrective step
    s.x = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
    s.y = _4q1 * q3q3 - _2q3 * ax + 4.0 * q0q0 * mSensorFrame.y - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
    s.z = 4.0 * q0q0 * mSensorFrame.z + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
    s.w = 4.0 * q1q1 * mSensorFrame.w - _2q1 * ax + 4.0 * q2q2 * mSensorFrame.w - _2q2 * ay;
    recipNorm = 1.0 / sqrt(s.x * s.x + s.y * s.y + s.z * s.z + s.w * s.w); // normalise step magnitude
    s.x *= recipNorm;
    s.y *= recipNorm;
    s.z *= recipNorm;
    s.w *= recipNorm;

    // Apply feedback step
    qDot.x -= mBeta * s.x;
    qDot.y -= mBeta * s.y;
    qDot.z -= mBeta * s.z;
    qDot.w -= mBeta * s.w;

    // Integrate rate of change of quaternion to yield quaternion
    mSensorFrame.x += qDot.x * (1.0 / mSampleFrequency);
    mSensorFrame.y += qDot.y * (1.0 / mSampleFrequency);
    mSensorFrame.z += qDot.z * (1.0 / mSampleFrequency);
    mSensorFrame.w += qDot.w * (1.0 / mSampleFrequency);

    // Normalize quaternion
    recipNorm = 1.0 / sqrt(mSensorFrame.x * mSensorFrame.x + mSensorFrame.y * mSensorFrame.y + mSensorFrame.z * mSensorFrame.z + mSensorFrame.w * mSensorFrame.w);
    mSensorFrame.x *= recipNorm;
    mSensorFrame.y *= recipNorm;
    mSensorFrame.z *= recipNorm;
    mSensorFrame.w *= recipNorm;
}

