#ifndef SENSORSTATE_H
#define SENSORSTATE_H

#include "gnss/mtkgps.h"

struct SensorState
{
    double timestamp;

    // GPS info
    NMEANavData navData;


    // IMU, ignore compass for now
    double acc[3]; // X Y Z, in M/S^2
    double ang[3]; // X Y Z, in RAD/S

};

#endif // SENSORSTATE_H