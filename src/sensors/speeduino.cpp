#include "speeduino.h"

#include "util/util.h"
#include "sensors/sensormanager.h"

#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#include "log/log.h"

static bool needToggle = false;

//-----------------------------------------------------------------------------
SpeeduinoInterface::SpeeduinoInterface(const char *device, uint32_t baudrate, bool requireCommandEcho) :
    mRequireCommandEcho(requireCommandEcho)
{
    mSpeeduinoStatusTime = 0.0;
    memset(&mSpeeduinoStatus, 0, sizeof(mSpeeduinoStatus));
    mUpdateCount = 0;
    mFailTime = -1;

    mSerialDeviceName = strdup(device);

    mSecsPerByte = 10.0 / (double)baudrate;

    mState = Idle;
    InitLog();

    mWantQuit = false;
    mUpdateThread = std::thread(&SpeeduinoInterface::ThreadInit, this);
    pthread_setname_np(mUpdateThread.native_handle(), "SpeeduinoUpdate");
}

//-----------------------------------------------------------------------------
SpeeduinoInterface::~SpeeduinoInterface()
{
    mWantQuit = true;
    mUpdateThread.join();
    CloseSerial();
}

//-----------------------------------------------------------------------------
bool SpeeduinoInterface::OpenSerial(const char *device)
{
    mDevice = open(device, O_RDWR | O_NDELAY | O_NOCTTY);
    if (mDevice < 0)
    {
        log_error("Speeduino open failed with err %d,  %d %s\n", mDevice, errno, strerror(errno));
        return false;
    }

    // Get the current options for the port...
    struct termios options;
    tcgetattr(mDevice, &options);
    cfmakeraw(&options);
    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);

    options.c_iflag = ICRNL | BRKINT | IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0x00008A30; // some of these bits aren't defined in the header?
    options.c_cflag = 0x000018B2; // some of these bits aren't defined in the header?

    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 2; // setting this to zero breaks the poll!

    // Set the new options for the port...
    tcsetattr(mDevice, TCSAFLUSH, &options);
    usleep(5000);

    ReadCurrentStatusNew();

    return true;
}

//-----------------------------------------------------------------------------
void SpeeduinoInterface::CloseSerial(void)
{
    close(mDevice);
    mDevice = -1;
}

//-----------------------------------------------------------------------------
// Does the magic weirdness to get an ACM to work properly
void SpeeduinoInterface::InitSerial(void)
{
    log_info("Trying Speedy device at %s\n", mSerialDeviceName);

    bool ok = OpenSerial(mSerialDeviceName);
    // If the device disappears, toggle between ACM0 and ACM1
    // Sometimes when connection is reestablished moves
    if (!ok)
    {
        int len = strlen(mSerialDeviceName);
        if (mSerialDeviceName[len - 1] == '0') mSerialDeviceName[len - 1] = '1';
        else                                   mSerialDeviceName[len - 1] = '0';
        usleep(100000);
        return;
    }

    ReadCurrentStatusNew();

    usleep(1000000);
    CloseSerial();
    usleep(1000000);
    OpenSerial(mSerialDeviceName);
}

//-----------------------------------------------------------------------------
void *SpeeduinoInterface::ThreadInit(void *context)
{
    SpeeduinoInterface *speedy = (SpeeduinoInterface *) context;
    if (speedy == NULL) return NULL;
    return speedy->UpdateThread();
}

//-----------------------------------------------------------------------------
void *SpeeduinoInterface::UpdateThread(void)
{
    while(!WantQuit() && !mWantQuit)
    {
        if (!InitOK())
        {
            InitSerial();
        }

        bool ok = ReadCurrentStatusNew();
        if (ok) ProcessInput();
        usleep(89000);
    }
    log_info("Speedy Thread exiting %d %d\n", (int) WantQuit(), (int)mWantQuit);
    return NULL;
}

//-----------------------------------------------------------------------------
void SpeeduinoInterface::ResetReadState(void)
{
    mState = Idle;
    mStartByte = 0;
    mReadProgress = 0;
    memset(mDataBuffer, 0, sDataSize);
}

//-----------------------------------------------------------------------------
bool SpeeduinoInterface::ReadCurrentStatusNew(void)
{
    if (!InitOK()) return false;
    //if (mState != Idle) return false;
    //mRequireCommandEcho = false;

    mStartByte = 'r';
    uint8_t cmd[8];
    cmd[0] = 'r';       // New read
    cmd[1] = 0;         // canID
    cmd[2] = 0x30;      // cmd
    cmd[3] = 0;         // offset low
    cmd[4] = 0;         // offset high
    cmd[5] = sDataSize; // len low
    cmd[6] = 0;         // len high

    ssize_t amount = write(mDevice, cmd, 7);
    if (amount != 7)
    {
        log_error("Speedy write failed\n");
        ResetReadState();
        CloseSerial();
        return false;
    }

    if (mState == Idle)
    {
        if (mRequireCommandEcho) mState = WaitStart;
        else                     mState = ReadingData;
        mReadProgress = 0;
    }
    return true;
}

//-----------------------------------------------------------------------------
bool SpeeduinoInterface::ReadCurrentStatus(void)
{
    if (!InitOK()) return false;
    //if (mState != Idle) return false;

    mStartByte = 'A';
    ssize_t amount = write(mDevice, &mStartByte, 1);
    if (amount != 1)
    {
        log_error("Speedy write failed\n");
        ResetReadState();
        CloseSerial();
        return false;
    }

    if (mState == Idle)
    {
        if (mRequireCommandEcho) mState = WaitStart;
        else                     mState = ReadingData;
        mReadProgress = 0;
    }
    return true;
}

//-----------------------------------------------------------------------------
bool SpeeduinoInterface::ProcessInput(void)
{
    uint8_t readBuffer[256];

    while (1)
    {
        if (mFailTime > 0.0)
        {
            double failedTime = SystemTimeUTC() - mFailTime;
            log_info("Failed time os %f\n", failedTime);
            if (failedTime > 2.0)
            {
                ResetReadState();
                CloseSerial();
                mFailTime = -1.0;
            }
           log_error("Speedy failed for too long\n");
           return false;
        }

        struct pollfd fds;
        fds.fd = mDevice;
        fds.events = POLLIN;

        int res = poll(&fds, 1, 40);
        // for us a timeout is as good as an error
        if (res <= 0)
        {
            if (errno != EAGAIN)
            {
                log_error("Poll on %d got error %d %s\n", mDevice, errno, strerror(errno));
                ResetReadState();
                CloseSerial();
                usleep(1000000);
                needToggle = true;
                return false;
            }
            if (mFailTime < 0.0)
            {
                mFailTime = SystemTimeUTC();
                log_error("Poll Setting mFailTime\n");
            }

            log_error("Speedy poll failed %d %d\n", res, errno);
            return false;
        }

        ssize_t readPos = 0;
        ssize_t amount = read(mDevice, readBuffer, 256);
        if (amount < 0)
        {
            log_debug("Read got %s\n", strerror(errno));
            if (errno != EAGAIN)
            {
                ResetReadState();
                CloseSerial();
            }
            log_error("Speedy read failed\n");
            return false;
        }

        if ((amount == 0) && (mFailTime < 0.0))
        {
            mFailTime = SystemTimeUTC();
            log_debug("Speedy read set failtime\n");
            return false;
        }

        // IF we got here we got some data
        mFailTime = -1.0;

        log_debug("Speedy read %d\n", (int)amount);
        while (readPos < amount)
        {
            switch (mState)
            {
                case Idle: // ERROR!
                    log_error("Speedy state Idle error\n");
                    return false;

                case WaitStart:
                {
                    uint8_t val = readBuffer[readPos];
                    ++readPos;
                    if (val == mStartByte)
                    {
                        mState = ReadingData;
                        log_debug("Speedy got start at %d\n", (int)readPos - 1);
                    }
                    break;
                }

                case ReadingData:
                {
                    if (mReadProgress == 0) mSpeeduinoStatusTime = SystemTimeUTC(); // Use UTC to sync with other sensors

                    // use as much data as I can, but don't go over sDataSize
                    ssize_t dataAvailable = amount - readPos;
                    ssize_t dataNeeded = sDataSize - mReadProgress;
                    if (dataAvailable > dataNeeded) dataAvailable = dataNeeded;

                    memcpy(&mDataBuffer[mReadProgress], &readBuffer[readPos], dataAvailable);
                    readPos += dataAvailable;
                    mReadProgress += dataAvailable;
                    log_debug("Speedy update got %d bytes, progress %d of %d\n", (int)dataAvailable, (int)mReadProgress, (int)sDataSize);
                    if (mReadProgress >= sDataSize)
                    {
                        CopyBadData(mSpeeduinoStatus, mDataBuffer);
                        ++mUpdateCount;
                        UpdateLog(mSpeeduinoStatusTime);
                        log_info("Speedy update %d succeeded at %f\n", mUpdateCount, mSpeeduinoStatusTime);

                        // Get ready for the next update
                        if (mRequireCommandEcho) mState = WaitStart;
                        else                     mState = ReadingData;
                        mReadProgress = 0;
                        return true;
                    }
                }
            }
        }
    }
    log_error("Speedy Update failed\n");
    return false;
}

//-----------------------------------------------------------------------------
void SpeeduinoInterface::InitLog(void)
{
    mSyncLossCounterID = DataLogger::AddVariable("ECU Sync Loss Counter", "",   U8);

    mSecondsID     = DataLogger::AddVariable("ECU Seconds",     "s",     U8);
    mMAPID         = DataLogger::AddVariable("ECU MAP",         "KPa",   U16);
    mMAPDotID      = DataLogger::AddVariable("ECU MAP Dot",     "KPa/s", S16);
    mRPMID         = DataLogger::AddVariable("ECU RPM",         "rpm",   U16);
    mIATID         = DataLogger::AddVariable("ECU IAT",         "C",     S16);
    mCLTID         = DataLogger::AddVariable("ECU CLT",         "C",     S16);
    mTPSID         = DataLogger::AddVariable("ECU TPS",         "%",     U8);
    mTPSDotID      = DataLogger::AddVariable("ECU TPS Dot",     "%/s",   S16);
    mAdvanceID     = DataLogger::AddVariable("ECU Advance",     "deg",   S8);
    mO2ID          = DataLogger::AddVariable("ECU O2",          "afr",   U8, 10.0);
    mAFRTargetID   = DataLogger::AddVariable("ECU AFR Target",  "afr",   U8, 10.0);
    mOilPressureID = DataLogger::AddVariable("ECU OilPressure", "psi",   U8);
    mVBatID        = DataLogger::AddVariable("ECU VBat",        "v",     U8, 10.0);

    mEGOCorrectionID = DataLogger::AddVariable("ECU EGO Correction",  "%",   S16);
    mIATCorrectionID = DataLogger::AddVariable("ECU IAT Correction",  "%",   S16);
    mWUECorrectionID = DataLogger::AddVariable("ECU WUE Correction",  "%",   S16);

    mPulsewidth0ID = DataLogger::AddVariable("ECU Inj 1 Pulsewidth",  "ms", S16, 10.0);
    mPulsewidth1ID = DataLogger::AddVariable("ECU Inj 2 Pulsewidth",  "ms", S16, 10.0);
    mPulsewidth2ID = DataLogger::AddVariable("ECU Inj 3 Pulsewidth",  "ms", S16, 10.0);
    mPulsewidth3ID = DataLogger::AddVariable("ECU Inj 4 Pulsewidth",  "ms", S16, 10.0);

}

//-----------------------------------------------------------------------------
void SpeeduinoInterface::UpdateLog(double time)
{
    //if (mUpdateCount % 20 == 0) printf("Speedy updated %d\n", mUpdateCount);
    DataLogger::UpdateVariable(mSyncLossCounterID, time, SyncLossCounter());

    DataLogger::UpdateVariable(mSecondsID,     time, Seconds());
    DataLogger::UpdateVariable(mMAPID,         time, MAP());
    DataLogger::UpdateVariable(mMAPDotID,      time, MAPDot());
    DataLogger::UpdateVariable(mRPMID,         time, RPM());
    DataLogger::UpdateVariable(mIATID,         time, IAT());
    DataLogger::UpdateVariable(mCLTID,         time, CLT());
    DataLogger::UpdateVariable(mTPSID,         time, TPS());
    DataLogger::UpdateVariable(mTPSDotID,      time, TPSDot());
    DataLogger::UpdateVariable(mAdvanceID,     time, Advance());
    DataLogger::UpdateVariable(mO2ID,          time, O2());
    DataLogger::UpdateVariable(mAFRTargetID,   time, AFRTarget());
    DataLogger::UpdateVariable(mOilPressureID, time, OilPressure());
    DataLogger::UpdateVariable(mVBatID,        time, VBat());

    DataLogger::UpdateVariable(mEGOCorrectionID, time, EGOCorrection());
    DataLogger::UpdateVariable(mIATCorrectionID, time, IATCorrection());
    DataLogger::UpdateVariable(mWUECorrectionID, time, WUECorrection());

    DataLogger::UpdateVariable(mPulsewidth0ID, time, Pulsewidth0());
    DataLogger::UpdateVariable(mPulsewidth1ID, time, Pulsewidth1());
    DataLogger::UpdateVariable(mPulsewidth2ID, time, Pulsewidth2());
    DataLogger::UpdateVariable(mPulsewidth3ID, time, Pulsewidth3());

    //if (mUpdateCount % 20 == 0) printf("Speedy updated %d %d\n", (int) TPS(), (int) mDataBuffer[24]);

}

//--------------------------------------------------------------------------
static uint16_t RawTo16(uint8_t *raw, unsigned lowByte)
{
    uint16_t low  = raw[lowByte];
    uint16_t high = raw[lowByte + 1];
    return low | (high << 8);
}

//--------------------------------------------------------------------------
void SpeeduinoInterface::CopyBadData(SpeeduinoBinaryData &out, uint8_t *raw)
{
    memcpy(&out.canIn, &raw[41], 32);

    out.seconds           = raw[0];     //   0
    out.status1           = raw[1];     //   1
    out.engineStatus      = raw[2];     //   2
    out.syncLossCounter   = raw[3];     //   3
    out.MAP               = RawTo16(raw, 4); //   4-5
    out.IAT               = raw[6];     //   6  *C
    out.CLT               = raw[7];     //   7  *C
    out.batteryCorrection = raw[8];     //   8
    out.batteryVoltage    = raw[9];     //   9  10*Volts
    out.O2                = raw[10];    //  10
    out.egoCorrection     = raw[11];    //  11  %
    out.IATCorrection     = raw[12];    //  12  %
    out.WUECorrection     = raw[13];    //  13  %
    out.RPM               = RawTo16(raw, 14); //  14-15
    out.accelEnrichment   = raw[16];    //  16  AE/2
    out.gammaEnrichment   = RawTo16(raw, 17); //  17-18 %
    out.VE1               = raw[19];    //  19  %
    out.VE2               = raw[20];    //  20  %
    out.AFRTarget         = raw[21];    //  21
    out.TPSDot            = raw[22];    //  22
    out.advance           = raw[23];    //  23
    out.TPS               = raw[24];    //  24
    out.loopsPerSecond    = RawTo16(raw, 25);  //  25-26
    out.freeRAM           = RawTo16(raw, 27);  //  27-28
    out.boostTarget       = raw[29];    //  29  boost/2
    out.boostSolenoidDuty = raw[30];    //  30  %
    out.sparkBitfield     = raw[31];    //  31
    out.rpmDot            = RawTo16(raw, 32);   //  32-33
    out.ethanolPercent    = raw[34];    //  34
    out.flexCorrection    = raw[35];    //  35  (% above or below 100)
    out.flexIgnitionCorrection = raw[36]; // 36  degrees advanced
    out.idleLoad          = raw[37];    //  37
    out.testOutputs       = raw[38];    //  38
    out.O2_2              = raw[39];    //  39
    out.barometer         = raw[40];    //  40
    out.TPSADC            = raw[73];    //  73
    out.nextError         = raw[74];    //  74
    out.Pulsewidth[0]     = RawTo16(raw, 75);  //  75-82 in 100us increments
    out.Pulsewidth[1]     = RawTo16(raw, 77);  //  75-82 in 100us increments
    out.Pulsewidth[2]     = RawTo16(raw, 79);  //  75-82 in 100us increments
    out.Pulsewidth[3]     = RawTo16(raw, 81);  //  75-82 in 100us increments
    out.status3           = raw[83];     //  83
    out.engineProtectStatus = raw[84];   //  84
    out.fuelLoad          = RawTo16(raw, 85); //  85-86
    out.ignitionLoad      = RawTo16(raw, 87); //  87-88
    out.dwell             = RawTo16(raw, 89); //  89-90
    out.CLTIdleTarget     = raw[91];     //  91
    out.MAPDot            = raw[92];     //  92
    out.VVTAngle          = RawTo16(raw, 93); //  93-94
    out.VVTTargetAngle    = raw[95];     //  95
    out.VVTDuty           = raw[96];     //  96
    out.flexBoostCorrection = RawTo16(raw, 97);  //  97-98
    out.barometerCorrection = raw[99];   //  99
    out.VE                = raw[100];    // 100 Current VE
    out.ASEValue          = raw[101];    // 101
    out.VSS               = RawTo16(raw, 102);   // 102-103
    out.currentGear       = raw[104];    // 104
    out.fuelPressure      = raw[105];    // 105
    out.oilPressure       = raw[106];    // 106
    out.WMIDuty           = raw[107];    // 107 %
    out.status4           = raw[108];
    out.VVTAngle2         = RawTo16(raw, 109);
    out.VVTTargetAngle2   = raw[111];
    out.VVTDuty2          = raw[112];
    out.outpusStatus      = raw[113];
    out.fuelTemp          = raw[114];
    out.fuelTempCorrection= raw[115];
    out.advance1          = raw[116];
    out.advance2          = raw[117];
    out.SDStatus          = raw[118];
    out.EMAP              = RawTo16(raw, 119);
    out.fanDuty           = raw[121];
}

