#include "iio-mpu9250.h"
#include "iiodevicemanager.h"
#include "util/util.h"

#include <stdio.h>
#include "config/config.h"

#include "log/log.h"

//---------------------------------------------------------
IIOMpu9250::IIOMpu9250(const char *id, IIODeviceManager *owner) :
    IIODevice(id),
    mCalRunning(false)
{
    // Setup the HW to the resolution I want
    SetBufferSize(256);
    SetSampleRate(50);
    SetAccelerationScale(0.001196);
    SetAngleScale(0.000266181);

    // Setup the axis
    // The onboard calibbias values are garbage.
    // If you set them the driver stops working properly.  Woo.

    mAxis[AccX].id    = AddChannel("in_accel_x");
    mAxis[AccX].scale = ReadChannel("in_accel_scale");
    mAxisIDs[AccX]    = DataLogger::AddVariable("IMU Accel X", "g", Float);

    mAxis[AccY].id    = AddChannel("in_accel_y");
    mAxis[AccY].scale = ReadChannel("in_accel_scale");
    mAxisIDs[AccY]    = DataLogger::AddVariable("IMU Accel Y", "g", Float);

    mAxis[AccZ].id    = AddChannel("in_accel_z");
    mAxis[AccZ].scale = ReadChannel("in_accel_scale");
    mAxisIDs[AccZ]    = DataLogger::AddVariable("IMU Accel Z", "g", Float);

    mAxis[AngX].id    = AddChannel("in_anglvel_x");
    mAxis[AngX].scale = ReadChannel("in_anglvel_scale");
    mAxisIDs[AngX]    = DataLogger::AddVariable("IMU Gyro X", "deg/s", Float);

    mAxis[AngY].id    = AddChannel("in_anglvel_y");
    mAxis[AngY].scale = ReadChannel("in_anglvel_scale");
    mAxisIDs[AngY]    = DataLogger::AddVariable("IMU Gyro Y", "deg/s", Float);

    mAxis[AngZ].id    = AddChannel("in_anglvel_z");
    mAxis[AngZ].scale = ReadChannel("in_anglvel_scale");
    mAxisIDs[AngZ]    = DataLogger::AddVariable("IMU Gyro Z", "deg/s", Float);

    mAxis[MagX].id    = AddChannel("in_magn_x");
    mAxis[MagX].scale = ReadChannel("in_magn_x_scale");
    mAxisIDs[MagX]    = DataLogger::AddVariable("IMU Mag X", "", S16, 32767.0);
    mAxis[MagX].bias  = 0.0;

    mAxis[MagY].id    = AddChannel("in_magn_y");
    mAxis[MagY].scale = ReadChannel("in_magn_y_scale");
    mAxisIDs[MagY]    = DataLogger::AddVariable("IMU Mag Y", "", S16, 32767.0);
    mAxis[MagY].bias  = 0.0;

    mAxis[MagZ].id    = AddChannel("in_magn_z");
    mAxis[MagZ].scale = ReadChannel("in_magn_z_scale");
    mAxisIDs[MagZ]    = DataLogger::AddVariable("IMU Mag Z", "", S16, 32767.0);
    mAxis[MagZ].bias  = 0.0;

    mTimestampID = AddChannel("in_timestamp", true);

    printf("X Axis accl scale: %f\n", mAxis[AccX].scale);

    // reset calibration to zero
    unsigned a;
    for (a = 0; a < 6; a++)
    {
        mAxis[a].bias = 0.0;
        mCalAverages[a] = NULL;
    }

    // read the bias values from the config file
    bool ok = true;
    ok &= ConfigGet("racedash.calibration.mpu9250", "accelerometer_bias_x", mAxis[AccX].bias);
    ok &= ConfigGet("racedash.calibration.mpu9250", "accelerometer_bias_y", mAxis[AccY].bias);
    ok &= ConfigGet("racedash.calibration.mpu9250", "accelerometer_bias_z", mAxis[AccZ].bias);
    ok &= ConfigGet("racedash.calibration.mpu9250", "gyro_bias_x",          mAxis[AngX].bias);
    ok &= ConfigGet("racedash.calibration.mpu9250", "gyro_bias_y",          mAxis[AngY].bias);
    ok &= ConfigGet("racedash.calibration.mpu9250", "gyro_biaz_z",          mAxis[AngZ].bias);
    if (!ok)
    {
        log_warn("MPU-9250: No calibration found, auto-calibrating.");
        Calibrate();
    }

#if ODROID == 2
     IIOTrigger *t = owner->FindTriggerByName("mpu9250-dev1");
#else
     IIOTrigger *t = owner->FindTriggerByName("mpu9250-dev2");
#endif     
    if (t == NULL) log_warn("IIOMpu9250 init error: Can't find device trigger!");
    SetTrigger(t);
}

//---------------------------------------------------------
IIOMpu9250::~IIOMpu9250()
{

}

//---------------------------------------------------------
void IIOMpu9250::ProcessUpdate(void)
{
    Sample s;
    double raw;

    ChannelCurrentValue(mTimestampID, raw);
    s.Timestamp() = RawTimeToTimestamp(raw); //raw / 1000000000.0; // convert to seconds UTC
    //log_debug("MPU9250   sample at %f\n", s.Timestamp());

    unsigned a;
    for (a = 0;a < s.Size(); a++)
    {
        double raw;
        ChannelCurrentValue(mAxis[a].id, raw);
        s[a] = (raw + mAxis[a].bias) * mAxis[a].scale;
        if (mCalRunning)
        {
            if (a < 6) mCalAverages[a]->AddSample(raw);
        }
        else
        {   // Don't log while calibrating, the values aren't good
            DataLogger::UpdateVariable(mAxisIDs[a], s.Timestamp(), s[a]);
        }
    }

    mBuffer.Push(s.Timestamp(), s);

    if (mCalRunning)
    {
        ++mCalSampleCount;
        if (mCalSampleCount > CAL_RUN_LEN)
        {
            mCalRunning = false;
            for (a = 0;a < 6; a++)
            {
                mAxis[a].bias = -mCalAverages[a]->Average();
                delete mCalAverages[a];
                mCalAverages[a] = NULL;
            }

            // Magnetometer has no auto-removable bias
            mAxis[MagX].bias = 0.0;
            mAxis[MagY].bias = 0.0;
            mAxis[MagZ].bias = 0.0;

            // Make sure gravity isn't canceled out!
            if (mAxis[AccZ].bias < 0.0) mAxis[AccZ].bias += (9.80 / mAxis[AccZ].scale);
            else                        mAxis[AccZ].bias -= (9.80 / mAxis[AccZ].scale);

            mCalSampleCount = 0;

            ConfigSet("racedash.calibration.mpu9250", "accelerometer_bias_x", mAxis[AccX].bias);
            ConfigSet("racedash.calibration.mpu9250", "accelerometer_bias_y", mAxis[AccY].bias);
            ConfigSet("racedash.calibration.mpu9250", "accelerometer_bias_z", mAxis[AccZ].bias);
            ConfigSet("racedash.calibration.mpu9250", "gyro_bias_x",          mAxis[AngX].bias);
            ConfigSet("racedash.calibration.mpu9250", "gyro_bias_y",          mAxis[AngY].bias);
            ConfigSet("racedash.calibration.mpu9250", "gyro_biaz_z",          mAxis[AngZ].bias);
            ConfigSave();
        }
    }

    //log_debug("MPU %f %f %f %f %f %f %f", s.Timestamp(), s[0], s[1], s[2], s[3], s[4], s[5]);
}

//---------------------------------------------------------
bool IIOMpu9250::SetSampleRate(unsigned rate)
{
    char fname[512];
    snprintf(fname, 512, "%s/%s/sample_rate", IIO_SYSFS_PATH, ID());

    char sr[32];
    snprintf(sr, 32, "%u\n", rate);
    return WriteFileRaw(fname, sr, strlen(sr));
}

//---------------------------------------------------------
bool IIOMpu9250::SetAccelerationScale(double scale)
{
    char fname[512];
    snprintf(fname, 512, "%s/%s/in_accel_scale", IIO_SYSFS_PATH, ID());

    char sr[32];
    snprintf(sr, 32, "%.9f\n", scale);
    return WriteFileRaw(fname, sr, strlen(sr));
}

//---------------------------------------------------------
bool IIOMpu9250::SetAngleScale(double scale)
{
    char fname[512];
    snprintf(fname, 512, "%s/%s/in_anglvel_scale", IIO_SYSFS_PATH, ID());

    char sr[32];
    snprintf(sr, 32, "%.9f\n", scale);
    return WriteFileRaw(fname, sr, strlen(sr));
}

//---------------------------------------------------------
bool IIOMpu9250::Calibrate(void)
{
    if (mCalRunning) return false;

    unsigned a;
    for (a = 0; a < 6; a++) mCalAverages[a] = new Averager(CAL_RUN_LEN);
    mCalSampleCount = 0;
    mCalRunning = true;
    return true;
}

//---------------------------------------------------------
int IIOMpu9250::SampleAt(double time, Sample &out) const
{
    Sample early, late;
    int result = mBuffer.FindTimestampBrackets(time, early, late);
    if (result != 0) return result;
    early.Interpolate(out, time, late);
    return 0;
}
