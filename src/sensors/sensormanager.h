#ifndef SENSORMANAGER_H
#define SENSORMANAGER_H

#include <thread>
#include <vector>
#include <queue>

#include "datalogger/datalogger.h"
#include "filter/butterworth.h"
#include "gnss/mtkgps.h"
#include "sensors/iiodevicemanager.h"
#include "sensors/iiotrigger.h"
#include "sensors/sensorstate.h"
#include "sensors/speeduinocan.h"
#include "sensors/speeduino.h"
#include "util/circularbuffer.h"
#include "rpn/rpn.h"
#include "radio/radio_1276_lora.h"
#include "telemetry/telemetry.h"
#include "gpio/gpio.h"
#include "track/track.h"
#include "util/util.h"

#include "iio-max11616.h"
#include "iio-mpu9250.h"

#include "led/ledstring.h"

enum WarningItem
{
    Warn_OilPressure,
    Warn_CoolantTemperature,
    Warn_BatteryVoltage,
    Warn_AFR,
    Warn_Boost,
    Warn_FuelRemaining,
    Warn_StintTime,

    Warn_Last
};

enum PitAcknowledge
{
    PitAck_NoRequest,
    PitAck_Pending,
    PitAck_OK,
    PitAck_Timeout
};

// Define this if you're using the CAN style interface
//#define SPEEDUINO_CAN

class SensorManager
{
  public:
    static bool Init(bool isRemote);
    static bool Quit(void);

    void LoadData(void);
    void SaveData(void);

    static SensorManager *Get(void) { return sSensorManager; }

    bool SetVariable(const char *name, const char *eq);
    bool RemoveVariable(const char *name);

    bool SaveVariables(void);

    bool WarningActive(enum WarningItem item) const { return SystemTimeUTC() < mWarnings[item].expireTime; }

    // Gets the newest data that aligns with all other data in time
    // can be delayed by a significant number of milliseconds,  usually updates in batches
    bool GetCoherentStateValue(const char *name, double &value) const
    {
        bool ok = RPN::GetVariable(mCurrentStateCoherent, name, value);
        //if (!ok) printf("Failed lookup of '%s'\n", name);
        return ok;
    }

    // Gets the newest data regardless if it is in phase with other measurements
    // updates at once, minimal delay
    // Used by dashboard gauges, swapped with remote data if this is a remote dash
    bool GetNewestStateValue(const char *name, double &value) const
    {
        bool ok = false;
        if (mIsRemote) ok = RPN::GetVariable(mRemoteState, name, value);
        else           ok = RPN::GetVariable(mCurrentStateNewest, name, value);
        //if (!ok) printf("Failed lookup of '%s'\n", name);
        return ok;
    }

    // Gets the newest data sent over LoRa
    bool GetRemoteStateValue(const char *name, double &value) const
    {
        bool ok = RPN::GetVariable(mRemoteState, name, value);
        //if (!ok) printf("Failed lookup of '%s'\n", name);
        return ok;
    }

    void SystemTimeAdjusted(void);

    void PitAcknowledged(PitAcknowledge ack);
    void ClearPitAcknowledge(void);
    void PitRequest(unsigned laps);

    void ResetStintTime(void);

  private:

    SensorManager(bool isRemote);
   ~SensorManager();

    static bool sWantQuit;
    static std::thread sSensorThread;

    static void *ThreadInit(void *);
    void *SensorThread(void);
    void *SensorThreadRemote(void);
    static SensorManager *sSensorManager;

    bool mIsRemote;
    MtkGPS *mGPS;
    LoraRadio *mRadio;
    IIODeviceManager mIIOManager;

    CircularBuffer<SensorState, 200> mSensorBuffer;
    // Just another circular buffer
    NMEAInterface::NavBuffer         mNavBuffer;

    struct Variable
    {
        char            *name;
        uint32_t         nameHash;
        char            *equation;
        RPN::Expression *expression;
    };

    std::vector<Variable> mVariables;
    RPN::VariableTable mCurrentStateCoherent;
    RPN::VariableTable mCurrentStateNewest;
    RPN::VariableTable mRemoteState;

    IIOMax11616 *mMaxDevice;
    IIOMpu9250  *mMpuDevice;

    size_t mGPSSamples;
    size_t mAkDeviceSamples;
    size_t mMaxDeviceSamples;
    size_t mMpuDeviceSamples;
    size_t mECUDeviceSamples;

#ifdef SPEEDUINO_CAN
    SpeeduinoCANInterface *mSpeeduino;
#else
    SpeeduinoInterface    *mSpeeduino;
#endif

    volatile double mStartupTime;

    PitAcknowledge mLatestPitAck;

    std::queue<double> mMasterSampleTimes;

    bool UpdateCoherentState(void);
    bool UpdateNewestState(void);

    void UpdateVT_TIME(RPN::VariableTable &vt, double updateTtime, double systemTime, double runtime);
    void UpdateVT_GPS(RPN::VariableTable &vt, NMEANavData &navData);
    void UpdateVT_MAX(RPN::VariableTable &vt, IIOMax11616::Sample &sample);
    void UpdateVT_MPU(RPN::VariableTable &vt, IIOMpu9250::Sample &sample);
    void UpdateVT_LAP(RPN::VariableTable &vt, TrackManager *tm, bool lapChanged);

#ifdef SPEEDUINO_CAN
    void UpdateVT_ECU(RPN::VariableTable &vt, SpeeduinoCANInterface *ecu);
#else
    void UpdateVT_ECU(RPN::VariableTable &vt, SpeeduinoInterface *ecu);
#endif

    void InitWarnings(void);
    void ActivateWarning(enum WarningItem item, double now, double duration);
    bool UpdateWarnings(void);

    void RemoveAllVariables(void);
    bool LoadVariables(void);

    bool BindPPS(const char *device);

    bool RadioTXRTData(void);
    bool RadioTXLapData(void);
    bool RadioTXPitRequest(unsigned laps);

    bool RadioRX(void);
    void RXRTData(const TelemetryRTData *rt);
    void RXPit(const TelemetryPitRequest *pr);
    void RXLapData(const TelemetryLapData *rt);

    bool     mTXPitRequest;
    unsigned mPitRequestLaps;
    unsigned mTXPitRequestSent;
    unsigned mTXPitAck;
    unsigned mTXLapData;
    unsigned mLoraRXCount;

    bool mTestedTrackLocation;

    void TestTrackLocation(double lat, double lon);

    struct Warning
    {
        enum WarningItem item;
        unsigned         count;
        double           expireTime;
    };
    std::vector<Warning> mWarnings;
    struct gpiod_line *mErrorLED;
    unsigned mWarningActiveCount;


    LookupTable  mFuelLevel;
    Biquad       mFuelLevelFilter;
    DataLoggerID mFuelLevelLogID;

    LEDString *mLEDString;
};





#endif //SENSORMANAGER_H