#ifndef IIOPATHS_H
#define IIOPATHS_H

#define IIO_SYSFS_PATH                  "/sys/bus/iio/devices"
#define IIO_HRTIMER_TRIGGER_PATH        "/sys/kernel/config/iio/triggers/hrtimer"

#define IIO_SYSFS_TRIGGER_ADD_PATH      "/sys/bus/iio/devices/iio_sysfs_trigger/add_trigger"
#define IIO_SYSFS_TRIGGER_REMOVE_PATH   "/sys/bus/iio/devices/iio_sysfs_trigger/remove_trigger"

#endif //IIOPATHS_H