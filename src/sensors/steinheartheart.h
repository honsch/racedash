#ifndef STEINHEARTHEART_H
#define STEINHEARTHEART_H

#include <vector>
#include <math.h>

struct NTCData
{
    float temperature; // in degrees Celsius
    float resistance;  // what the NTC resistor measures at this temp
};


class SteinheartHeartCalc
{
  public:
    SteinheartHeartCalc(const std::vector<NTCData> &specification);
   ~SteinheartHeartCalc(void);

   float Temp(float resistance);

  private:

    static const unsigned M = 4;
    static const float AbsoluteZero;

    typedef float Polynomial[M];

    static float XFromR(float r) { return logf(r); }
    static float YFromT(float t) { return 1.0F / (t - AbsoluteZero); }

    float Value(Polynomial p, float x);
    float ScalarPolynomial(Polynomial p, Polynomial q);
    void  Mult(Polynomial p, float fact);
    void  Linear(Polynomial p, Polynomial q, float fact);
    void  Orthonormal(Polynomial p[]);
    float Scalar(Polynomial p);
    void  Approximate(Polynomial erg);
    void  TestResult(void);

    struct NTCDataInternal
    {
        NTCDataInternal(const NTCData &in) :
            temperature(in.temperature),
            resistance(in.resistance)
        {
            x = XFromR(in.resistance);
            y = YFromT(in.temperature);
        }
        float temperature;
        float resistance;
        float x;
        float y;
    };

    std::vector<NTCDataInternal> mSpec;
    Polynomial mErg;
    Polynomial mBasis[4];
};

#endif // STEINHEARTHEART_H
