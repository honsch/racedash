#ifndef IIO_AK8975_H
#define IIO_AK8975_H

#include "iiodevice.h"
#include "util/circularbuffer.h"
#include "datalogger/datalogger.h"

class IIOAk8975 : public IIODevice
{
  public:
    IIOAk8975(const char *id, IIODeviceManager *owner);
    virtual ~IIOAk8975();
    const char *Type(void) const { return "ak8975"; }

    typedef IIODeviceSample<3> Sample;
    void ProcessUpdate(void);

    int SampleAt(double time, Sample &out) const;
    double NewestSampleTime(void) const { return mBuffer.NewestSampleTime(); }
    void NewestSample(size_t &count, Sample &out) { mBuffer.NewestSample(count, out); }
  private:

    enum { X, Y, Z };

    struct Axis
    {
        int    id;
        double scale;
        double bias;
    };

    Axis mMag[3];
    int  mTimestampID;
    CircularBuffer<Sample, 40> mBuffer;

    DataLoggerID mMagID[3];
};



#endif // IIO_AK8975_H