//=====================================================================================================
// MadgwickAHRS.h
//=====================================================================================================
//
// Implementation of Madgwick's IMU and AHRS algorithms.
// See: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
// Modified by EH 2018
// Date			Author          Notes
// 29/09/2011	SOH Madgwick    Initial release
// 02/10/2011	SOH Madgwick	Optimised for reduced CPU load
//
//=====================================================================================================
#ifndef MADGWICKAHRS_H
#define MADGWICKAHRS_H

//---------------------------------------------------------------------------------------------------
struct Quaternion
{
    double x;
    double y;
    double z;
    double w;
};

//---------------------------------------------------------------------------------------------------
class MadgwickAHRS
{
  public:
    MadgwickAHRS(double sampleFrequency, double beta = 0.1);
   ~MadgwickAHRS();

    // Call this if you have magnetometer data
    void Update(double gx, double gy, double gz, double ax, double ay, double az, double mx, double my, double mz);

    // Call this if you do not have magnetometer data
    void UpdateIMU(double gx, double gy, double gz, double ax, double ay, double az);

    const Quaternion &SensorFrame(void) const { return mSensorFrame; }

  private:

    Quaternion mSensorFrame;
    double     mSampleFrequency;
    double     mBeta;
};

#endif // MADGWICKAHRS_H
