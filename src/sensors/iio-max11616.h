#ifndef IIO_MAX11616_H
#define IIO_MAX11616_H

#include "iiodevice.h"
#include "util/circularbuffer.h"
#include "datalogger/datalogger.h"

class IIOMax11616 : public IIODevice
{
  public:
    IIOMax11616(const char *id, IIODeviceManager *owner);
    virtual ~IIOMax11616();
    const char *Type(void) const { return "max11616"; }

    typedef IIODeviceSample<12> Sample;
    void ProcessUpdate(void);

    int SampleAt(double time, Sample &out) const;
    double NewestSampleTime(void) const { return mBuffer.NewestSampleTime(); }
    void NewestSample(size_t &count, Sample &out) { mBuffer.NewestSample(count, out); }

  private:
    int mChannelIDs[12];
    int mTimestampID;

    CircularBuffer<Sample, 30> mBuffer;

    DataLoggerID mLogIDs[12];
};



#endif // IIO_MAX11616_H
