#include "iio-max11616.h"
#include "iiodevicemanager.h"

#include <stdio.h>

#include "log/log.h"

//---------------------------------------------------------
IIOMax11616::IIOMax11616(const char *id, IIODeviceManager *owner) :
    IIODevice(id)
{
    unsigned a;
    for (a = 0; a < 12; a++)
    {
        char bn[64];
        snprintf(bn, 64, "in_voltage%d", a);
        mChannelIDs[a] = AddChannel(&bn[0]);

        snprintf(bn, 64, "ADC Input %02d", a);
        mLogIDs[a] = DataLogger::AddVariable(&bn[0], "%", Float);
    }

    mTimestampID = AddChannel("in_timestamp", true);
    SetBufferSize(256);
}

//---------------------------------------------------------
IIOMax11616::~IIOMax11616()
{

}

//---------------------------------------------------------
void IIOMax11616::ProcessUpdate(void)
{
    Sample s;
    double raw;

    ChannelCurrentValue(mTimestampID, raw);
    s.Timestamp() = RawTimeToTimestamp(raw); //raw / 1000000000.0; // convert to seconds UTC
    //log_debug("Max11616  sample at %f", s.Timestamp());

    unsigned a;
    for (a = 0;a < s.Size(); a++)
    {
        double raw;
        ChannelCurrentValue(mChannelIDs[a], raw);
        s[a] = raw / 4096.0; // Convert to 0.0 - 1.0
        DataLogger::UpdateVariable(mLogIDs[a], s.Timestamp(), s[a]*100.0);
    }

    mBuffer.Push(s.Timestamp(), s);

#if 0
    printf("MAX %f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n",
           s.Timestamp(), s[0], s[1], s[2], s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11]);
#endif
}

//---------------------------------------------------------
int IIOMax11616::SampleAt(double time, Sample &out) const
{
    Sample early, late;
    int result = mBuffer.FindTimestampBrackets(time, early, late);
    if (result != 0) return result;
    early.Interpolate(out, time, late);
    return 0;
}
