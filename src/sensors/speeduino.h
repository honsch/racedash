#ifndef SPEEDUINO_H
#define SPEEDUINO_H

#include "datalogger/datalogger.h"

#include <stdint.h>
#include <thread>

/******
    "===Command Help===\n\n"
    "All commands are single character and are concatenated with their parameters \n"
    "without spaces."
    "Syntax:  <command>+<parameter1>+<parameter2>+<parameterN>\n\n"
    "===List of Commands===\n\n"
    "A - Displays 108 bytes of currentStatus values in binary (live data)\n"
    "B - Burn current map and configPage values to eeprom\n"
    "C - Test COM port.  Used by Tunerstudio to see whether an ECU is on a given serial \n"
    "    port. Returns a binary number.\n"
    "N - Print new line.\n"
    "P - Set current page.  Syntax:  P+<pageNumber>\n"
    "R - Same as A command\n"
    "S - Display signature number\n"
    "Q - Same as S command\n"
    "V - Display map or configPage values in binary\n"
    "W - Set one byte in map or configPage.  Expects binary parameters. \n"
    "    Syntax:  W+<offset>+<newbyte>\n"
    "t - Set calibration values.  Expects binary parameters.  Table index is either 0, \n"
    "    1, or 2.  Syntax:  t+<tble_idx>+<newValue1>+<newValue2>+<newValueN>\n"
    "Z - Display calibration values\n"
    "T - Displays 256 tooth log entries in binary\n"
    "r - Displays 256 tooth log entries\n"
    "U - Prepare for firmware update. The next byte received will cause the Arduino to reset.\n"
    "? - Displays this help page"
******/

// All data is little endian
struct SpeeduinoBinaryData
{
    uint8_t     seconds;            //   0
    uint8_t     status1;            //   1
    uint8_t     engineStatus;       //   2
    uint8_t     syncLossCounter;    //   3
    uint16_t    MAP;                //   4-5
    uint8_t     IAT;                //   6  *C+40
    uint8_t     CLT;                //   7  *C+40
    uint8_t     batteryCorrection;  //   8
    uint8_t     batteryVoltage;     //   9  10*Volts
    uint8_t     O2;                 //  10
    uint8_t     egoCorrection;      //  11  %
    uint8_t     IATCorrection;      //  12  %
    uint8_t     WUECorrection;      //  13  %
    uint16_t    RPM;                //  14-15
    uint8_t     accelEnrichment;    //  16  AE/2
    uint16_t    gammaEnrichment;    //  17-18 %
    uint8_t     VE1;                //  19  %
    uint8_t     VE2;                //  20  %
    uint8_t     AFRTarget;          //  21
    uint8_t     TPSDot;             //  22
    uint8_t     advance;            //  23
    uint8_t     TPS;                //  24
    uint16_t    loopsPerSecond;     //  25-26
    uint16_t    freeRAM;            //  27-28
    uint8_t     boostTarget;        //  29  boost/2
    uint8_t     boostSolenoidDuty;  //  30  %
    uint8_t     sparkBitfield;      //  31
    int16_t     rpmDot;             //  32-33
    uint8_t     ethanolPercent;     //  34
    uint8_t     flexCorrection;     //  35  (% above or below 100)
    uint8_t     flexIgnitionCorrection; // 36  degrees advanced
    uint8_t     idleLoad;           //  37
    uint8_t     testOutputs;        //  38
    uint8_t     O2_2;               //  39
    uint8_t     barometer;          //  40
    uint16_t    canIn[16];          //  41-72
    uint8_t     TPSADC;             //  73
    uint8_t     nextError;          //  74
    uint16_t    Pulsewidth[4];      //  75-82 in 100us increments
    uint8_t     status3;            //  83
    uint8_t     engineProtectStatus;//  84
    uint16_t    fuelLoad;           //  85-86
    uint16_t    ignitionLoad;       //  87-88
    uint16_t    dwell;              //  89-90
    uint8_t     CLTIdleTarget;      //  91
    uint16_t    MAPDot;             //  92
    int16_t     VVTAngle;           //  93-94
    int8_t      VVTTargetAngle;     //  95
    uint8_t     VVTDuty;            //  96
    uint16_t    flexBoostCorrection;//  97-98
    uint8_t     barometerCorrection;//  99
    uint8_t     VE;                 // 100 Current VE
    uint8_t     ASEValue;           // 101
    uint16_t    VSS;                // 102-103
    int8_t      currentGear;        // 104
    uint8_t     fuelPressure;       // 105
    uint8_t     oilPressure;        // 106
    uint8_t     WMIDuty;            // 107
    uint8_t     status4;            // 108
    uint16_t    VVTAngle2;          // 109-110
    uint8_t     VVTTargetAngle2;    // 111
    uint8_t     VVTDuty2;           // 112
    uint8_t     outpusStatus;       // 113
    uint8_t     fuelTemp;           // 114;
    uint8_t     fuelTempCorrection; // 115;
    uint8_t     advance1;           // 116;
    uint8_t     advance2;           // 117
    uint8_t     SDStatus;           // 118
    uint16_t    EMAP;               // 119-120
    uint8_t     fanDuty;            // 121;
};

const double InjectorConstantL = (0.440 / 60.0); // 440 cc/min to l/sec
const double InjectorConstantG = (InjectorConstantL / 3.785411784); // l/sec to gal/sec

//------------------------------------------------------------
class SpeeduinoInterface
{
  public:
    SpeeduinoInterface(const char *device, uint32_t baudrate, bool requireCommandEcho);
   ~SpeeduinoInterface();

    bool InitOK(void) const { return mDevice > 0; }

    double NewestSampleTime(void) const { return mSpeeduinoStatusTime; }

    unsigned ValidSampleCount(void) const { return mUpdateCount; }
    unsigned SyncLossCounter(void) const  { return (unsigned) mSpeeduinoStatus.syncLossCounter; }

    double Seconds(void) const          { return (double) mSpeeduinoStatus.seconds; }
    double MAP(void) const              { return (double) mSpeeduinoStatus.MAP; }
    double MAPDot(void) const           { return (double) mSpeeduinoStatus.MAPDot; }
    double RPM(void) const              { return (double) mSpeeduinoStatus.RPM; }
    double IAT(void) const              { return -40.0 + (double) mSpeeduinoStatus.IAT; }
    double CLT(void) const              { return -40.0 + (double) mSpeeduinoStatus.CLT; }
    double TPS(void) const              { return (double) mSpeeduinoStatus.TPS; }
    double TPSDot(void) const           { return (double) mSpeeduinoStatus.TPSDot; }
    double Advance(void) const          { return (double) mSpeeduinoStatus.advance; }
    double O2(void) const               { return 0.1 * (double) mSpeeduinoStatus.O2; }
    double AFRTarget(void) const        { return 0.1 * (double) mSpeeduinoStatus.AFRTarget; }
    double OilPressure(void) const      { return (double) mSpeeduinoStatus.oilPressure; }

    double VBat(void) const             { return 0.1 * (double) mSpeeduinoStatus.batteryVoltage; }

    double EGOCorrection(void) const    { return (double) mSpeeduinoStatus.egoCorrection; }
    double IATCorrection(void) const    { return (double) mSpeeduinoStatus.IATCorrection; }
    double WUECorrection(void) const    { return (double) mSpeeduinoStatus.WUECorrection; }

    double Pulsewidth0(void) const      { return 0.1 * (double) mSpeeduinoStatus.Pulsewidth[0]; }
    double Pulsewidth1(void) const      { return 0.1 * (double) mSpeeduinoStatus.Pulsewidth[1]; }
    double Pulsewidth2(void) const      { return 0.1 * (double) mSpeeduinoStatus.Pulsewidth[2]; }
    double Pulsewidth3(void) const      { return 0.1 * (double) mSpeeduinoStatus.Pulsewidth[3]; }

  protected:
    char  *mSerialDeviceName;
    int    mDevice;
    double mSecsPerByte;
    bool   mRequireCommandEcho;

    bool mWantQuit;
    std::thread mUpdateThread;

    static void *ThreadInit(void *);
    void *UpdateThread(void);

    bool ReadCurrentStatus(void);
    bool ReadCurrentStatusNew(void);
    bool ProcessInput(void);
    void CopyBadData(SpeeduinoBinaryData &out, uint8_t *raw);
    enum State { Idle, WaitStart, ReadingData } mState;

    static const unsigned sDataSize = 108;
    uint8_t  mStartByte;
    unsigned mReadProgress;
    uint8_t  mDataBuffer[sDataSize];

    double              mSpeeduinoStatusTime;
    SpeeduinoBinaryData mSpeeduinoStatus;
    unsigned            mUpdateCount;

    double              mFailTime;

    bool OpenSerial(const char *device);
    void CloseSerial(void);
    void InitSerial(void);

    void ResetReadState(void);

    void InitLog(void);
    void UpdateLog(double time);

    DataLoggerID mSyncLossCounterID;
    DataLoggerID mSecondsID;
    DataLoggerID mMAPID;
    DataLoggerID mMAPDotID;
    DataLoggerID mRPMID;
    DataLoggerID mIATID;
    DataLoggerID mCLTID;
    DataLoggerID mTPSID;
    DataLoggerID mTPSDotID;
    DataLoggerID mAdvanceID;
    DataLoggerID mO2ID;
    DataLoggerID mAFRTargetID;
    DataLoggerID mOilPressureID;
    DataLoggerID mVBatID;

    DataLoggerID mEGOCorrectionID;
    DataLoggerID mIATCorrectionID;
    DataLoggerID mWUECorrectionID;

    DataLoggerID mPulsewidth0ID;
    DataLoggerID mPulsewidth1ID;
    DataLoggerID mPulsewidth2ID;
    DataLoggerID mPulsewidth3ID;
};







#endif // SPEEDUINO_H
