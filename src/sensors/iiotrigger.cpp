#include "iiotrigger.h"
#include "iiopaths.h"
#include "util/util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "log/log.h"
//----------------------------------------------------------------
// Generic trigger
//----------------------------------------------------------------
IIOTrigger::IIOTrigger(const char *id, const char *name)
{
    mID   = strdup(id);
    mName = strdup(name);
}

//----------------------------------------------------------------
IIOTrigger::~IIOTrigger()
{
    free(mID);
    free(mName);
}


//----------------------------------------------------------------
// Sysfs Manual Trigger
//----------------------------------------------------------------
IIOSysfsTrigger::IIOSysfsTrigger(const char *id, const char *name) :
    IIOTrigger(id, name)
{
    char trigger[512];
    snprintf(trigger, 512, "%s/%s/trigger_now", IIO_SYSFS_PATH, ID());
    mFD = open(trigger, O_WRONLY);
    if (mFD < 0) log_error("Cannot open trigger %s", trigger);
}

//----------------------------------------------------------------
IIOSysfsTrigger::~IIOSysfsTrigger()
{
    if (mFD > 0) close(mFD);

    int index = atoi(Name() + strlen("sysfstrig"));
    IIOSysfsTrigger::Destroy(index);
}

//----------------------------------------------------------------
bool IIOSysfsTrigger::Trigger(void)
{
    if (mFD > 0)
    {
        ssize_t amount = write(mFD, "1", 1);
        if (amount == 1) return true;
        else             log_warn("Error triggering %s", ID());
    }
    return false;
}

//----------------------------------------------------------------
// This creates the kernel trigger.
// It DOES NOT enumerate it
bool IIOSysfsTrigger::Create(unsigned id)
{
    char i[12];
    sprintf(i, "%d", id);
    return WriteFileRaw(IIO_SYSFS_TRIGGER_ADD_PATH, i, strlen(i));
}

//----------------------------------------------------------------
// This destroys the kernel trigger.
// It DOES NOT un-enumerate it
bool IIOSysfsTrigger::Destroy(unsigned id)
{
    char i[12];
    sprintf(i, "%d", id);
    log_info("Removing sysfs trigger %s", i);
    return WriteFileRaw(IIO_SYSFS_TRIGGER_REMOVE_PATH, i, strlen(i));
}



//----------------------------------------------------------------
// HR Timer Trigger
//----------------------------------------------------------------
IIOHRTimerTrigger::IIOHRTimerTrigger(const char *id, const char *name) :
    IIOTrigger(id, name)
{
}

//----------------------------------------------------------------
IIOHRTimerTrigger::~IIOHRTimerTrigger()
{
    IIOHRTimerTrigger::Destroy(Name());
}

//----------------------------------------------------------------
bool IIOHRTimerTrigger::SetFrequency(unsigned frequency)
{
    mFrequency = frequency;

    char f[12];
    sprintf(f, "%d", mFrequency);

    // Set it
    char sf[512];
    snprintf(sf, 512, "%s/%s/sampling_frequency", IIO_SYSFS_PATH, ID());
    WriteFileRaw(sf, f, strlen(f));
    return false;
}

//----------------------------------------------------------------
bool IIOHRTimerTrigger::Create(const char *name)
{
    char rm[512];
    snprintf(rm, 512, "%s/%s", IIO_HRTIMER_TRIGGER_PATH, name);
    // Create it
    int ret = mkdir(rm, S_IRWXU | S_IRWXG | S_IRWXO);
    return (ret == 0);
}

//----------------------------------------------------------------
bool IIOHRTimerTrigger::Destroy(const char *name)
{
    char rm[512];
    snprintf(rm, 512, "%s/%s", IIO_HRTIMER_TRIGGER_PATH, name);
    // Create it
    int ret = rmdir(rm);
    return (ret == 0);
}

//----------------------------------------------------------------
bool IIOHRTimerTrigger::IsTrigger(const char *id)
{
    char sf[512];
    snprintf(sf, 512, "%s/%s/sampling_frequency", IIO_SYSFS_PATH, id);

    uint32_t size;
    void *value = ReadFile(sf, size);
    free(value);
    return (size > 0);
}
