#include "speeduinocan.h"

#include "util/util.h"
#include "sensors/sensormanager.h"

#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#include "log/log.h"

//-----------------------------------------------------------------------------
SpeeduinoCANInterface::SpeeduinoCANInterface(const char *device, uint32_t baudrate)
{
    mSpeeduinoStatusTime = 0.0;
    memset(&mSpeeduinoStatus, 0, sizeof(mSpeeduinoStatus));
    mUpdateCount = 0;
    mFailTime = -1;

    mSerialDeviceName = strdup(device);

    mSecsPerByte = 10.0 / (double)baudrate;

    mState = Idle;
    InitLog();

    mWantQuit = false;
    mUpdateThread = std::thread(&SpeeduinoCANInterface::ThreadInit, this);
    pthread_setname_np(mUpdateThread.native_handle(), "SpeeduinoUpdate");
}

//-----------------------------------------------------------------------------
SpeeduinoCANInterface::~SpeeduinoCANInterface()
{
    mWantQuit = true;
    mUpdateThread.join();
    CloseSerial();
}

//-----------------------------------------------------------------------------
bool SpeeduinoCANInterface::OpenSerial(const char *device)
{
    mDevice = open(device, O_RDWR | O_NDELAY | O_NOCTTY);
    if (mDevice < 0)
    {
        log_error("Speeduino open failed with err %d,  %d %s", mDevice, errno, strerror(errno));
        return false;
    }

    // Get the current options for the port...
    struct termios options;
    tcgetattr(mDevice, &options);
    cfmakeraw(&options);

    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 1; // setting this to zero breaks the poll!

    cfsetispeed(&options, B115200);
    cfsetospeed(&options, B115200);

    // Set the new options for the port...
    tcsetattr(mDevice, TCSAFLUSH, &options);
    usleep(5000);

    ReadCurrentStatus();

    return true;
}

//-----------------------------------------------------------------------------
void SpeeduinoCANInterface::CloseSerial(void)
{
    close(mDevice);
    mDevice = -1;
}

//-----------------------------------------------------------------------------
// Does the magic weirdness to get an ACM to work properly
void SpeeduinoCANInterface::InitSerial(void)
{
    log_info("Trying Speedy device at %s", mSerialDeviceName);

    bool ok = OpenSerial(mSerialDeviceName);
    // If the device disappears, toggle between USB0 and USB1
    // Sometimes when connection is reestablished moves
    if (!ok)
    {
        usleep(50000);
        return;
    }

    //ReadCurrentStatus();
}

//-----------------------------------------------------------------------------
void *SpeeduinoCANInterface::ThreadInit(void *context)
{
    SpeeduinoCANInterface *speedy = (SpeeduinoCANInterface *) context;
    if (speedy == NULL) return NULL;
    return speedy->UpdateThread();
}

//-----------------------------------------------------------------------------
void *SpeeduinoCANInterface::UpdateThread(void)
{
    while(!WantQuit() && !mWantQuit)
    {
        if (!InitOK())
        {
            InitSerial();
            ResetReadState();
        }

        bool ok = ReadCurrentStatus();
        if (ok) ProcessInput();
        usleep(89000);
    }
    log_debug("Speedy Thread exiting %d %d", (int) WantQuit(), (int)mWantQuit);
    return NULL;
}

//-----------------------------------------------------------------------------
void SpeeduinoCANInterface::ResetReadState(void)
{
    mState = Idle;
    mReadProgress = 0;
    memset(mDataBuffer, 0, sDataSize);
}


//-----------------------------------------------------------------------------
bool SpeeduinoCANInterface::ReadCurrentStatus(void)
{
    if (!InitOK()) return false;
    //if (mState != Idle)
    //{
    //    log_debug("Speedy can't poll state, not idle %d", (int) mState);
    //    return false;
    //}

    log_debug("Speedy polling current status");

    uint8_t cmd[8];
    cmd[0] = 'r';       // New read
    cmd[1] = 0;         // canID
    cmd[2] = 0x30;      // cmd
    cmd[3] = 0;         // offset low
    cmd[4] = 0;         // offset high
    cmd[5] = sDataSize; // len low
    cmd[6] = 0;         // len high

    mExpectedResponseHeader[0] = 'r';
    mExpectedResponseHeader[1] = 0x30;

    ssize_t amount = write(mDevice, cmd, 7);
    if (amount != 7)
    {
        log_error("Speedy write failed");
        ResetReadState();
        CloseSerial();
        return false;
    }

    if (mState == Idle)
    {
        mState = WaitStart;
        mReadProgress = 0;

    }
    return true;
}

//-----------------------------------------------------------------------------
bool SpeeduinoCANInterface::ProcessInput(void)
{
    uint8_t readBuffer[256];

    while (1)
    {
        if (mFailTime > 0.0)
        {
            double failedTime = SystemTimeUTC() - mFailTime;
            log_debug("Speedy Failed time os %f", failedTime);
            if (failedTime > 2.0)
            {
                ResetReadState();
                CloseSerial();
                mFailTime = -1.0;
            }
           log_debug("Speedy failed for too long");
           return false;
        }

        struct pollfd fds;
        fds.fd = mDevice;
        fds.events = POLLIN;

        int res = poll(&fds, 1, 40);
        // for us a timeout is as good as an error
        if (res <= 0)
        {
            if (errno != EAGAIN)
            {
                log_error("Poll on %d got error %d %s", mDevice, errno, strerror(errno));
                ResetReadState();
                CloseSerial();
                usleep(1000000);
                return false;
            }
            if (mFailTime < 0.0)
            {
                mFailTime = SystemTimeUTC();
                log_error("Poll Setting mFailTime");
            }

            log_error("Speedy poll failed %d %d", res, errno);
            return false;
        }

        ssize_t readPos = 0;
        ssize_t amount = read(mDevice, readBuffer, 256);
        if (amount < 0)
        {
            log_debug("Read got %s", strerror(errno));
            if (errno != EAGAIN)
            {
                ResetReadState();
                CloseSerial();
            }
            log_error("Speedy read failed");
            return false;
        }

        if ((amount == 0) && (mFailTime < 0.0))
        {
            mFailTime = SystemTimeUTC();
            log_debug("Speedy read set failtime");
            return false;
        }

        // IF we got here we got some data
        mFailTime = -1.0;

        log_debug("Speedy read %d", (int)amount);
        while (readPos < amount)
        {
            switch (mState)
            {
                case Idle: // ERROR!
                    log_error("Speedy state Idle error");
                    return false;

                case WaitStart:
                {
                    // If the first byte doesn't match skip it
                    if ((mReadProgress == 0) && (readBuffer[readPos] != mExpectedResponseHeader[0]))
                    {
                        log_debug("Speedy dropped bad data at WaitStart: 0x%X", (int) readBuffer[readPos]);
                        ++readPos;
                        break;
                    }

                    mResponseHeader[mReadProgress] = readBuffer[readPos];
                    ++readPos;
                    ++mReadProgress;

                    if (mReadProgress == sResponseHeaderSize)
                    {
                        mReadProgress = 0;
                        if ( 0 == memcmp(mResponseHeader, mExpectedResponseHeader, sResponseHeaderSize))
                        {
                            mState = ReadingData;
                            log_debug("Speedy got start at %d %d %f", (int)readPos - 1, (int)amount, SystemTimeUTC());
                        }
                        else
                        {
                            log_debug("Speedy got bad header, dropping");
                        }
                    }
                    break;
                }

                case ReadingData:
                {
                    if (mReadProgress == 0) mSpeeduinoStatusTime = SystemTimeUTC(); // Use UTC to sync with other sensors

                    // use as much data as I can, but don't go over sDataSize
                    ssize_t dataAvailable = amount - readPos;
                    ssize_t dataNeeded = sDataSize - mReadProgress;
                    if (dataAvailable > dataNeeded) dataAvailable = dataNeeded;

                    memcpy(&mDataBuffer[mReadProgress], &readBuffer[readPos], dataAvailable);
                    readPos += dataAvailable;
                    mReadProgress += dataAvailable;
                    log_debug("Speedy update got %d bytes, progress %d of %d", (int)dataAvailable, (int)mReadProgress, (int)sDataSize);
                    if (mReadProgress >= sDataSize)
                    {
                        CopyBadData(mSpeeduinoStatus, mDataBuffer);
                        ++mUpdateCount;
                        UpdateLog(mSpeeduinoStatusTime);
                        log_debug("Speedy update %d succeeded at %f %f", mUpdateCount, mSpeeduinoStatusTime, SystemTimeUTC());

                        // Get ready for the next update
                        mState = WaitStart;
                        mReadProgress = 0;
                        return true;
                    }
                }
            }
        }
    }
    log_debug("Speedy Update failed");
    ResetReadState();
    return false;
}

//-----------------------------------------------------------------------------
void SpeeduinoCANInterface::InitLog(void)
{
    //mSyncLossCounterID = DataLogger::AddVariable("ECU Sync Loss Counter", "",   U8);

    mMAPID         = DataLogger::AddVariable("ECU MAP",         "KPa",   U16);
    mMAPDotID      = DataLogger::AddVariable("ECU MAP Dot",     "KPa/s", S16);
    mRPMID         = DataLogger::AddVariable("ECU RPM",         "rpm",   U16);
    mIATID         = DataLogger::AddVariable("ECU IAT",         "C",     S16);
    mCLTID         = DataLogger::AddVariable("ECU CLT",         "C",     S16);
    mTPSID         = DataLogger::AddVariable("ECU TPS",         "%",     U8);
    mTPSDotID      = DataLogger::AddVariable("ECU TPS Dot",     "%/s",   S16);
    mAdvanceID     = DataLogger::AddVariable("ECU Advance",     "deg",   S8);
    mO2ID          = DataLogger::AddVariable("ECU O2",          "afr",   U8);
    mAFRTargetID   = DataLogger::AddVariable("ECU AFR Target",  "afr",   U8, 10.0);
    mOilPressureID = DataLogger::AddVariable("ECU OilPressure", "psi",   U8);
    mVBatID        = DataLogger::AddVariable("ECU VBat",        "v",     U8, 10.0);

    mEGOCorrectionID = DataLogger::AddVariable("ECU EGO Correction",  "%",   S16);
    mIATCorrectionID = DataLogger::AddVariable("ECU IAT Correction",  "%",   S16);
    mWUECorrectionID = DataLogger::AddVariable("ECU WUE Correction",  "%",   S16);

    mPulsewidthID = DataLogger::AddVariable("ECU Inj Pulsewidth",  "ms", S16, 10.0);

}

//-----------------------------------------------------------------------------
void SpeeduinoCANInterface::UpdateLog(double time)
{

    //DataLogger::UpdateVariable(mSyncLossCounterID, time, SyncLossCounter());

    DataLogger::UpdateVariable(mMAPID,         time, MAP());
    DataLogger::UpdateVariable(mRPMID,         time, RPM());
    DataLogger::UpdateVariable(mIATID,         time, IAT());
    DataLogger::UpdateVariable(mCLTID,         time, CLT());
    DataLogger::UpdateVariable(mTPSID,         time, TPS());
    DataLogger::UpdateVariable(mTPSDotID,      time, TPSDot());
    DataLogger::UpdateVariable(mAdvanceID,     time, Advance());
    DataLogger::UpdateVariable(mO2ID,          time, O2());
    DataLogger::UpdateVariable(mAFRTargetID,   time, AFRTarget());
    DataLogger::UpdateVariable(mOilPressureID, time, OilPressure());
    DataLogger::UpdateVariable(mVBatID,        time, VBat());

    DataLogger::UpdateVariable(mEGOCorrectionID, time, EGOCorrection());
    DataLogger::UpdateVariable(mIATCorrectionID, time, IATCorrection());
    DataLogger::UpdateVariable(mWUECorrectionID, time, WUECorrection());

    DataLogger::UpdateVariable(mPulsewidthID, time, Pulsewidth());

    if (mUpdateCount % 20 == 0) log_debug("Speedy updated %d %d", (int) TPS(), (int) mDataBuffer[24]);

}

//--------------------------------------------------------------------------
static uint16_t RawTo16(uint8_t *raw, unsigned lowByte)
{
    uint16_t low  = raw[lowByte];
    uint16_t high = raw[lowByte + 1];
    return low | (high << 8);
}

//--------------------------------------------------------------------------
void SpeeduinoCANInterface::CopyBadData(SpeeduinoCANBinaryData &out, uint8_t *raw)
{
    memcpy(&out.canIn, &raw[41], 32);

    out.seconds           = raw[0];     //   0
    out.status1           = raw[1];     //   1
    out.engineStatus      = raw[2];     //   2
    out.dwell             = raw[3];     //   3
    out.MAP               = RawTo16(raw, 4); //   4-5
    out.IAT               = raw[6];     //   6  *C
    out.CLT               = raw[7];     //   7  *C
    out.batteryCorrection = raw[8];     //   8
    out.batteryVoltage    = raw[9];     //   9  10*Volts
    out.O2                = raw[10];    //  10
    out.egoCorrection     = raw[11];    //  11  %
    out.IATCorrection     = raw[12];    //  12  %
    out.WUECorrection     = raw[13];    //  13  %
    out.RPM               = RawTo16(raw, 14); //  14-15
    out.accelEnrichment   = raw[16];    //  16  AE/2
    out.gammaEnrichment   = raw[17];    //  17  %
    out.VE                = raw[18];    //  18  %
    out.AFRTarget         = raw[19];    //  19
    out.pulsewidth1       = RawTo16(raw, 20); // 20-21 in 100us increments
    out.TPSDot            = raw[22];    //  22
    out.advance           = raw[23];    //  23
    out.TPS               = raw[24];    //  24
    out.loopsPerSecond    = RawTo16(raw, 25);  //  25-26
    out.freeRAM           = RawTo16(raw, 27);  //  27-28
    out.boostTarget       = raw[29];    //  29  boost/2
    out.boostSolenoidDuty = raw[30];    //  30  %
    out.sparkBitfield     = raw[31];    //  31
    out.rpmDot            = RawTo16(raw, 32);   //  32-33
    out.ethanolPercent    = raw[34];    //  34
    out.flexCorrection    = raw[35];    //  35  (% above or below 100)
    out.flexIgnitionCorrection = raw[36]; // 36  degrees advanced
    out.idleLoad          = raw[37];    //  37
    out.testOutputs       = raw[38];    //  38
    out.O2_2              = raw[39];    //  39
    out.barometer         = raw[40];    //  40
    out.TPSADC            = raw[73];    //  73
    out.nextError         = raw[74];    //  74
    out.launchCorrection  = raw[75];    // 75
    out.pulsewidth2       = RawTo16(raw, 76);//  76-77 in 100us increments
    out.pulsewidth3       = RawTo16(raw, 78);//  78-79 in 100us increments
    out.pulsewidth4       = RawTo16(raw, 80);//  80-81 in 100us increments
    out.status3           = raw[82];    //  82    resentLockOn(0), nitrousOn(1), fuel2Active(2), vssRefresh(3), halfSync(4), nSquirts(6:7)
    out.engineProtectStatus = raw[83];  //  83    RPM(0), MAP(1), OIL(2), AFR(3), Unused(4:7)
    out.fuelLoad          = RawTo16(raw, 84);//  84-85
    out.ignitionLoad      = RawTo16(raw, 86);//  86-87
    out.injectionAngle    = RawTo16(raw, 88);//  88-89
    out.idleDuty          = raw[90];    //  90 %
    out.CLIdleTarget      = raw[91];    //  91
    out.mapDot            = raw[92];    //  92
    out.vvt1Angle         = raw[93];    //  93
    out.vvt1AngleTarget   = raw[94];    //  94
    out.vvt1Duty          = raw[95];    //  95
    out.flexBoostCorrection = RawTo16(raw, 96);//  96-97
    out.baroCorrection    = raw[98];    //  98
    out.ASEValue          = raw[99];    //  99  %
    out.VSS               = RawTo16(raw, 100);// 100-101
    out.gear              = raw[102];   // 102
    out.fuelPressure      = raw[103];   // 103
    out.oilPressure       = raw[104];   // 104
    out.wmiPW             = raw[105];   // 105
    out.status4           = raw[106];   // 106 wmiEmptyBit(0), vvt1Error(1), vvt2Error(2), fanStatus(3), UnusedBits(4:7)
    out.vvt2Angle         = raw[107];   // 107
    out.vvt2AngleTarget   = raw[108];   // 108
    out.vvt2Duty          = raw[109];   // 109
    out.outputStatus      = raw[110];   // 110
    out.fuelTemp          = raw[111];   // 111 *C+40
    out.fuelTempCorrection = raw[112];   // 112 %
    out.VE1               = raw[113];   // 113
    out.VE2               = raw[114];   // 114
    out.advance1          = raw[115];   // 115
    out.advance2          = raw[116];   // 116
    out.nitrousStatus     = raw[117];   // 117
    out.SDStatus          = raw[118];   // 118 SD card status
    out.EMAP              = RawTo16(raw, 119);// 119-120    !!!!! BAD ALIGNMENT !!!!!
    out.fanDuty           = raw[121];   // 121 %
}

