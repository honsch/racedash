#ifndef CONFIG_H
#define CONFIG_H

bool ConfigInit(void);
void ConfigSave(void);

bool ConfigGet(const char *path, const char *item, double      &val);
bool ConfigGet(const char *path, const char *item, const char *&val);

// Get by index inside a group
bool ConfigGet(const char *path, unsigned index, const char *&name, double      &val);
bool ConfigGet(const char *path, unsigned index, const char *&name, const char *&val);


bool ConfigSet(const char *path, const char *item, double      val);
bool ConfigSet(const char *path, const char *item, const char *val);

bool ConfigCreateGroup(const char *path, const char *group);

#endif //CONFIG_H