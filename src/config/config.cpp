#include "config.h"

#include <libconfig.h++>

#include <vector>
#include <mutex>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "log/log.h"

static libconfig::Config sConfig;
static std::mutex sConfigLock;

#define CONFIG_FILE "racedash.config"

//-------------------------------------------------------------
static void SplitConfigPath(std::vector<const char *> &levels, char *path)
{
    char *next = path;
    while (1)
    {
        levels.push_back(next);
        next = strchr(next, '.');
        if (next == NULL) break;
        *next = 0;
        ++next;
    }

#if 0
    printf("SplitConfigPath '%s'\n", path);
    unsigned a;
    for (a = 0; a < levels.size(); a++)
    {
        printf("    %2d: '%s'\n", a, levels[a]);
    }
#endif
}

//-------------------------------------------------------------
static void CreateGroupChild(std::vector<const char *> &levels, unsigned level, libconfig::Setting &parent)
{
    if (level >= levels.size())
    {
        //printf("Level is %d, done\n", level);
        return;
    }

    if (!parent.exists(levels[level]))
    {
        //printf("Adding group '%s'", levels[level]);
        parent.add(levels[level], libconfig::Setting::TypeGroup);
    }
    CreateGroupChild(levels, level + 1, parent.lookup(levels[level]));
}

//-------------------------------------------------------------
// Creates groups all the way down if they don't exist
static bool ConfigCreateGroup(const char *_path)
{
    if (_path == NULL) return false;
    char path[512];
    strncpy(path, _path, 512);

    std::vector<const char *> levels;
    SplitConfigPath(levels, path);

    // I need to do this recursively because the Setting API is brain-damaged!
    CreateGroupChild(levels, 0, sConfig.getRoot());

    return true;
}

//-------------------------------------------------------------
// returns false if no config file exists, new config file created
bool ConfigInit(void)
{
    bool ok = true;
    FILE *inf = fopen(CONFIG_FILE, "r");
    if (inf == NULL)
    {
        inf = fopen(CONFIG_FILE, "w");
        fprintf(inf, "# Racedash config file\n");
        fclose(inf);
        inf = fopen(CONFIG_FILE, "r");
        ok = false;
    }

    sConfigLock.lock();
    sConfig.read(inf);
    sConfigLock.unlock();
    fclose(inf);

    return ok;
}

//-------------------------------------------------------------
void ConfigSave(void)
{
    sConfigLock.lock();
    sConfig.writeFile(CONFIG_FILE);
    sConfigLock.unlock();
}

//-------------------------------------------------------------
bool ConfigGet(const char *path, const char *item, double &val)
{
    sConfigLock.lock();
    char p[512];
    strcpy(p, path);
    strcat(p, ".");
    strcat(p, item);
    bool ok = sConfig.lookupValue(p, val);
    sConfigLock.unlock();
    return ok;
}

//-------------------------------------------------------------
bool ConfigGet(const char *path, const char *item, const char *&val)
{
    sConfigLock.lock();
    char p[512];
    strcpy(p, path);
    strcat(p, ".");
    strcat(p, item);
    bool ok = sConfig.lookupValue(p, val);
    sConfigLock.unlock();
    return ok;
}

//-------------------------------------------------------------
bool ConfigGet(const char *path, unsigned index, const char *&name, double &val)
{
    sConfigLock.lock();

    if (!sConfig.exists(path))
    {
        sConfigLock.unlock();
        return false;
    }

    libconfig::Setting &group = sConfig.lookup(path);
    if (index >= (unsigned) group.getLength())
    {
        sConfigLock.unlock();
        return false;
    }

    libconfig::Setting &s = group[index];
    name = s.getName();
    val  = (double)s;

    sConfigLock.unlock();
    return true;
}

//-------------------------------------------------------------
bool ConfigGet(const char *path, unsigned index, const char *&name, const char *&val)
{
    sConfigLock.lock();

    if (!sConfig.exists(path))
    {
        sConfigLock.unlock();
        return false;
    }

    libconfig::Setting &group = sConfig.lookup(path);
    if (index >= (unsigned) group.getLength())
    {
        sConfigLock.unlock();
        return false;
    }

    libconfig::Setting &s = group[index];
    val  = (const char *)s;
    name = s.getName();

    sConfigLock.unlock();
    return true;
}

//-------------------------------------------------------------
bool ConfigSet(const char *path, const char *item, double  val)
{
    sConfigLock.lock();

    if (!sConfig.exists(path))
    {
        bool ok = ConfigCreateGroup(path);
        if (!ok)
        {
            sConfigLock.unlock();
            return false;
        }
    }

    libconfig::Setting &s = sConfig.lookup(path);
    if (!s.exists(item))
    {
        s.add(item, libconfig::Setting::TypeFloat);
    }

    s[item] = val;

    sConfigLock.unlock();

    return true;
}

//-------------------------------------------------------------
bool ConfigSet(const char *path, const char *item, const char *val)
{
    sConfigLock.lock();

    if (!sConfig.exists(path))
    {
        bool ok = ConfigCreateGroup(path);
        if (!ok)
        {
            sConfigLock.unlock();
            return false;
        }
    }

    libconfig::Setting &s = sConfig.lookup(path);
    if (!s.exists(item))
    {
        s.add(item, libconfig::Setting::TypeString);
    }

    s[item] = val;

    sConfigLock.unlock();

    return true;
}

//-------------------------------------------------------------
bool ConfigCreateGroup(const char *path, const char *group)
{
    char fullpath[512];
    strcpy(fullpath, path);
    strcat(fullpath, ".");
    strcat(fullpath, group);

    if (!sConfig.exists(path))
    {
        bool ok = ConfigCreateGroup(path);
        if (!ok) return false;
    }

    return true;
}