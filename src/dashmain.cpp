#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <signal.h>

#include "display.h"
#include "touchscreen.h"
#include "mouse.h"
#include "util/util.h"
#include "ui/ui.h"

#include "config/config.h"
#include "sensors/sensormanager.h"

#include "screens/gettext.h"
#include "screens/musicplayer.h"
#include "screens/dashboardgauges.h"
#include "screens/remotedash.h"
#include "screens/touchscreencal.h"
#include "screens/pit.h"

#include "datalogger/datalogger.h"

#include "radio/radio_1276_lora.h"
#include "gpio/gpio.h"

#include "track/track.h"

#include "log/log.h"
static void CreateDefaultConfig(void);

static FILE *sLogFile = NULL;

//----------------------------------------------------
static void sigintHandler(int s)
{
   log_info("Got quit signal");
   Quit();
}

//----------------------------------------------------
static void InitLog(const char *logfile)
{
   // Turn off echoing all logs to the screen
   log_clear_callbacks();

   //log_set_quiet(true);
   // Only echo warning and higher to the screen
   log_set_level(LOG_WARN);

   sLogFile = fopen(logfile, "a+");
   if (sLogFile == NULL) sLogFile = stdout;
   fprintf(sLogFile, "\n\n\n==================    Starting a new log   ==================\n");

   // Uncomment the line for the desired level (and greater) of logging
   //log_add_fp(sLogFile, LOG_TRACE);
   //log_add_fp(sLogFile, LOG_DEBUG);
   log_add_fp(sLogFile, LOG_INFO);
   //log_add_fp(sLogFile, LOG_WARN);
   //log_add_fp(sLogFile, LOG_ERROR);
   //log_add_fp(sLogFile, LOG_FATAL);
}

//----------------------------------------------------
static void CloseLog(void)
{
   if ((sLogFile != NULL) && (sLogFile != stdout))
   {
      fprintf(sLogFile, "==================    Log end   ==================\n");
      fflush(sLogFile);
      fclose (sLogFile);
      sLogFile = NULL;
   }
}

//----------------------------------------------------
int main(int argc, char **argv)
{
   InitLog("racedash.log");

   log_info("Starting Racedash!");
   struct sigaction sa;
   sa.sa_handler = &sigintHandler;
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = 0;
   sigaction(SIGINT, &sa, NULL);
   //sigaction(SIGUSR0, &sa, NULL);

   bool ok = GpioInit();
   if (!ok)
   {
      log_error("Can't initialize GPIO, quitting\n");
      exit(0);
   }

   struct gpiod_line *powerSense   = GpioFindLine(LINE_POWER_SENSE);
   if (powerSense == NULL)
   {
      log_error("Can't find power sense GPIO (%s), exiting", LINE_POWER_SENSE);
      exit(0);
   }
   struct gpiod_line *powerControl = GpioFindLine(LINE_POWER_CONTROL);
   if (powerSense == NULL)
   {
      log_error("Can't find power control GPIO (%s), exiting", LINE_POWER_CONTROL);
      exit(0);
   }

   GpioSetLineInput(powerSense, "Power Sense");
   GpioSetLineOutput(powerControl, 1, "Power Control");
   GpioSetLineValue(powerControl, 1);
   bool isRemote = false;
   bool recalTouchscreen = false;
   bool forcePower = false;
   int height = 1480;
   int width = 320;
   int logLevel;
   int opt;
   DisplayController::DisplayRotation defaultRotation = DisplayController::CCW_90;
   while ((opt = getopt(argc, argv, "rstph:w:l:d:")) != -1)
   {
      switch (opt)
      {
         case 'r':
            isRemote = true;
            defaultRotation = DisplayController::None;
            width  = 1920;
            height = 1080;
            break;

         case 's':
            UI::ShowTouch(true);
            break;

         case 't':
            recalTouchscreen = true;
            break;

         case 'h':
            height = atoi(optarg);
            break;

         case 'p':
            forcePower = true;
            break;

         case 'w':
            width = atoi(optarg);
            break;

         case 'd':
            switch (atoi(optarg))
            {
               case 0:
                  defaultRotation = DisplayController::None;
                  break;
               case 90: defaultRotation = DisplayController::CCW_90;
                  break;
               case 180: defaultRotation = DisplayController::CW_180;
                  break;
               case 270: defaultRotation = DisplayController::CW_90;
                  break;
               default:
                  fprintf(stderr, "Invalid display rotation (%d) must be 0, 90, 180, or 270.\n", atoi(optarg));
                  exit(-1);
            }
            break;

         case 'l':
            logLevel = atoi(optarg);
            if ((logLevel < LOG_TRACE) || (logLevel > LOG_FATAL))
            {
               fprintf(stderr, "Invalid log level specified (%d) must be between %d and %d, 0 is most verbose.\n", logLevel, LOG_TRACE, LOG_FATAL);
               exit(-1);
            }
            fprintf(stderr, "Setting log level to %d\n", logLevel);
            log_set_level(atoi(optarg));
            log_clear_callbacks();
            log_add_fp(sLogFile, logLevel);
            break;

         default: /* '?' */
            fprintf(stderr, "Usage: %s [-r(emoteDash)] [-t] [-h height] [-w width] [-d rotation] [-l loglevel(0-5)]\n", argv[0]);
            exit(-1);
      }
   }

   if (!isRemote) DataLogger::Init("logs");

   DisplayController *display = new DisplayController("/dev/dri/card1", width, height, defaultRotation);
   if (!display->InitOK())
   {
      log_error("Display Init failed.  No display will be used\n");
      delete display;
      display = NULL;
   }

   bool configOK = ConfigInit();
   if (!configOK)
   {
      CreateDefaultConfig();
      recalTouchscreen = true;
   }

   gTrackManager = new TrackManager("tracks");

   SensorManager::Init(isRemote);

   if (display != NULL)
   {
      Touchscreen *touchscreen = new Touchscreen;
      //touchscreen->Init("/dev/input/by-id/usb-eGalax_Inc._USB_TouchController-event-if00");
      touchscreen->Init("/dev/input/by-id/usb-WaveShare_WaveShare_000000000089-event-if00");

      Mouse *mouse = new Mouse;
      mouse->Init("/dev/input/by-id/usb-Logitech_USB_Receiver-if01-event-mouse");

      UI::Init(touchscreen, mouse);

      if (isRemote) RemoteDashboard::Show();
      else          DashboardGauges::Show();

      if (recalTouchscreen)
      {
         TouchscreenCal::Push();
      }
   }

   unsigned powerMissingCount = 0;
   while(!WantQuit())
   {
      MusicPlayer::UpdateAudio();

      if (display != NULL)
      {
         display->BeginFrame();

         UI::Update(display);

         display->Swap();
         display->Clear(BLACK);
      }
      else
      {
         usleep(10000);
      }

      //if (Fraction(SystemTime() / 2.0F) < 0.5F) GpioSetLineValue(powerControl, 1);
      //else GpioSetLineValue(powerControl, 0);

      // Only look for power in car
      if (!isRemote && !forcePower)
      {
         // Quit on power off!
         if (GpioGetLineValue(powerSense) == 0)
         {
            ++powerMissingCount;
            if (powerMissingCount > 30)
            {
               log_info("Power sense quit\n");
               Quit();
            }
         }
         else
         {
            powerMissingCount = 0;
         }
      }
   }

   SaveSystemClockFreq();
   SensorManager::Get()->SaveData();

   ConfigSave();

   SensorManager::Quit();
   if (display!= NULL) delete display;
   if (gTrackManager != NULL) delete gTrackManager;

   log_info("Shutdown");
   CloseLog();

   // Turn off board
   GpioSetLineValue(powerControl, 0);

   GpioShutdown();
   return 0;
}

//----------------------------------------------------
// Put a default setup into the config file
static void CreateDefaultConfig(void)
{
   ConfigSave();
}
