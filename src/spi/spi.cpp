#include "spi.h"

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>

#include "log/log.h"

//------------------------------------------------------
SpiDevice::SpiDevice(const char *name, const char *devNode)
{
    mName = strdup(name);
    mFD   = open(devNode, O_RDWR);

    if (mFD < 0)
    {
        log_error("Create SpiDevice %s failed, %s can't be opened", mName, devNode);
    }

    memset(mXfer, 0, sizeof (mXfer));
}

//------------------------------------------------------
SpiDevice::~SpiDevice()
{
    free(mName);
    if (mFD > 0) close(mFD);
}

//------------------------------------------------------
bool SpiDevice::Send(uint8_t address, uint8_t *out, int len)
{
    if (mFD < 0) return false;
    if (out == NULL) return false;

    mXfer[0].tx_buf = (unsigned long)&address;
    mXfer[0].rx_buf = 0;
    mXfer[0].len = 1;

    mXfer[1].tx_buf = (unsigned long)out;
    mXfer[1].rx_buf = 0;
    mXfer[1].len = len;

    int status = ioctl(mFD, SPI_IOC_MESSAGE(2), mXfer);
    if (status < 0)
    {
        return false;
    }

    return true;
}

//------------------------------------------------------
bool SpiDevice::Receive(uint8_t address, uint8_t *in, int len)
{
    if (mFD < 0) return false;
    if (in == NULL) return false;

    mXfer[0].tx_buf = (unsigned long)&address;
    mXfer[0].rx_buf = 0;
    mXfer[0].len = 1;

    mXfer[1].tx_buf = 0;
    mXfer[1].rx_buf = (unsigned long) in;
    mXfer[1].len = len;

    int status = ioctl(mFD, SPI_IOC_MESSAGE(2), mXfer);
    if (status < 0)
    {
        return false;
    }

    return true;
}

//------------------------------------------------------
bool SpiDevice::SetMaxHz(uint32_t hz)
{
    if (mFD < 0) return false;
    int ret = ioctl(mFD, SPI_IOC_WR_MAX_SPEED_HZ, &hz);
    if (ret < 0)
    {
        log_error("SpiDevice %s cannot set MaxHz to %d", mName, hz);
        return false;
    }
    return true;
}