#ifndef SPI_H
#define SPI_H

#include <stdint.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <linux/types.h>
#include <linux/spi/spidev.h>

//------------------------------------------------------
class SpiDevice
{
  public:

    SpiDevice(const char *name, const char *devnode);
   ~SpiDevice();

    bool Send(uint8_t address, uint8_t *out, int len);
    bool Receive(uint8_t address, uint8_t *in, int len);

    bool SetMaxHz(uint32_t hz);

  protected:

    char *mName;
    int   mFD;

    struct spi_ioc_transfer mXfer[2];
};

#endif // SPI_H