#include "track.h"
#include "sensors/sensormanager.h"
#include "datalogger/datalogger.h"
#include "config/config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dirent.h>

#include <algorithm>

#include "log/log.h"

TrackManager *gTrackManager = NULL;

static TrackSector sInvalid;

#define TRACK_ROOT_PATH      "racedash.track"

//-----------------------------------------------------
Track::Track(void)
{
    mLoadedFile = NULL;
    mName = NULL;
    mSizeNS = 0.0;
    mSizeEW = 0.0;
    mDistance = 20000000.0; // half way around the planet

    Unload();
    memset(&sInvalid, 0, sizeof(sInvalid));
}

//-----------------------------------------------------
Track::~Track()
{

}

//-----------------------------------------------------
bool Track::Load(const char *fname)
{
    if (fname == NULL) return false;
    if (strlen(fname) < 1) return false;

    Unload();

    char line[1024];
    char rawLine[1024];

    mLoadedFile = strdup(fname);

    FILE *inf = fopen(fname, "rt");
    if (inf == NULL)
    {
        log_warn("Cannot open %s for reading.\n", fname);
        return false;
    }

    const char *strings[32];
    unsigned currentLine = 0;

    enum LoadState { Unknown, Inner, Outer, Sectors, Name, StartFinish } loadState;
    loadState = Unknown;


    mMinLat =  90.0;
    mMaxLat = -90.0;
    mMinLon =  360.0;
    mMaxLon = -360.0;

    while (!feof(inf))
    {
        ++currentLine;
        char *lineRet = fgets(line, 1023, inf);
        if (lineRet == NULL) continue;
        strcpy(rawLine, line);

        unsigned numStrings = ExtractStrings(strings, 32, line);

        if (numStrings < 1) continue;

        if (strings[0][0] == '#') // Comment line, ignore
        {
            continue;
        }
        else if (0 == strcasecmp("[inner]", strings[0]))
        {
            loadState = Inner;
            continue;
        }
        else if (0 == strcasecmp("[outer]", strings[0]))
        {
            loadState = Outer;
            continue;
        }
        else if (0 == strcasecmp("[sectors]", strings[0]))
        {
            loadState = Sectors;
            continue;
        }
        else if (0 == strcasecmp("[name]", strings[0]))
        {
            loadState = Name;
            continue;
        }
        else if (0 == strcasecmp("[startfinish]", strings[0]))
        {
            loadState = StartFinish;
            continue;
        }

        TrackCoord tc;
        TrackSector ts;
        switch (loadState)
        {
            case Inner:
            case Outer:
            case StartFinish:
                if (numStrings == 3)
                {
                    tc.longitude = atof(strings[0]);
                    tc.latitude  = atof(strings[1]);
                    tc.altitude  = atof(strings[2]);

                    if      (tc.latitude < mMinLat) mMinLat = tc.latitude;
                    else if (tc.latitude > mMaxLat) mMaxLat = tc.latitude;
                    if      (tc.longitude < mMinLon) mMinLon = tc.longitude;
                    else if (tc.longitude > mMaxLon) mMaxLon = tc.longitude;
                }
                else
                {
                    if (strlen(rawLine) > 3) log_warn("Loading track %s line %d, wrong number of strings: %d, should be 3", mLoadedFile, currentLine, numStrings);
                    continue;
                }
                break;

            case Sectors:
                if (numStrings == 5)
                {
                    strncpy(ts.name, strings[1], 63);
                    ts.index     = atoi(strings[0]);
                    ts.longitude = atof(strings[2]);
                    ts.latitude  = atof(strings[3]);
                    ts.altitude  = atof(strings[4]);
                    log_info("Found sector %d  %s %f %f %f", ts.index, ts.name, ts.longitude, ts.latitude, ts.altitude);
                }
                else
                {
                    if (strlen(rawLine) > 3) log_warn("Loading track %s line %d, wrong number of strings: %d, should be 5", mLoadedFile, currentLine, numStrings);
                    continue;
                }
                break;

            case Name:
                {
                    unsigned len = strlen(rawLine);
                    unsigned a = 0;
                    while (a < len)
                    {
                        if (rawLine[a] < 32) rawLine[a] = 0;
                        ++a;
                    }

                    if (strlen(rawLine) > 0)
                    {
                        if (mName != NULL) free(mName);
                        mName = strdup(rawLine);
                    }
                }
                break;


            default:
                continue;
                break;
        }

        switch (loadState)
        {
            case Inner:
                mInner.push_back(tc);
                break;

            case Outer:
                mOuter.push_back(tc);
                break;

            case StartFinish:
                mStartFinish = tc;
                break;

            case Sectors:
                mSectors.push_back(ts);
                break;

            default:
                break;
        }
    }

    // Is any required data missing?
    if ((mName == NULL) || (mInner.size() == 0) || (mOuter.size() == 0) || (mStartFinish.altitude < -500.0))
    {
        if (mName == NULL) log_warn("Track %s (%s) is missing [name], dropped", mName, mLoadedFile);
        if (mInner.size() == 0) log_warn("Track %s (%s) is missing [inner] path, dropped", mName, mLoadedFile);
        if (mOuter.size() == 0) log_warn("Track %s (%s) is missing [outer] path, dropped", mName, mLoadedFile);
        if (mStartFinish.altitude < -500.0) log_warn("Track %s (%s) is missing [startfinish] point, dropped", mName, mLoadedFile);

        Unload();
        return false;
    }

    mSizeEW = HaversineDistance(mMinLat, mMinLon, mMinLat, mMaxLon);
    mSizeNS = HaversineDistance(mMinLat, mMinLon, mMaxLat, mMinLon);

    log_info("Loaded track %s (%s), %d inner, %d outer, %d sectors, SizeNS: %.1f, SizeEW: %.1f", mName, mLoadedFile, (int)mInner.size(), (int)mOuter.size(), (int)mSectors.size(), mSizeNS, mSizeEW);
    log_info("   Min %f %f  max %f %f", mMinLat, mMinLon, mMaxLat, mMaxLon);

    return true;
}

//-----------------------------------------------------
void Track::Unload(void)
{
    if (mLoadedFile != NULL) free(mLoadedFile);
    if (mName != NULL)       free(mName);
    mLoadedFile = NULL;
    mName = NULL;
    mInner.clear();
    mOuter.clear();
    mSectors.clear();
    mStartFinish.latitude  =  0.0;
    mStartFinish.longitude =  0.0;
    mStartFinish.altitude  = -999.0;
}

//---------------------------------------------------------
unsigned Track::ExtractStrings(const char **strings, unsigned maxStrings, char *line)
{
    unsigned a;
    for (a = 0; a < maxStrings; a++) strings[a] = NULL;

    if (line == NULL) return 0;

    unsigned len = strlen(line);
    if (len == 0) return 0;

    // strip ending CR/LF from string
    while ((len > 0) && ((line[len - 1] == '\n') || (line[len - 1] == '\r')))
    {
        --len;
        line[len] = 0;
    }

    unsigned numStrings = 0;
    unsigned pos = 0;
    while (pos < len)
    {
        strings[numStrings] = &line[pos];
        ++numStrings;

        // Advance past the text of the entry, possibly blank!
        bool inQuotes = false;
        while (pos < len)
        {
            if (line[pos] == '"') inQuotes = !inQuotes;
            if (!inQuotes && (line[pos] == ',')) break;
            ++pos;
        }

        if (pos >= len) break;

        line[pos] = 0;
        ++pos; // move past the previous string
    }

    return numStrings;
}

//-------------------------------------------------------
const TrackSector &Track::FindSector(unsigned index) const
{
    unsigned a;
    for (a = 0; a < mSectors.size(); a++)
    {
        if (mSectors[a].index == index) return mSectors[a];
    }
    return sInvalid;
}

//-------------------------------------------------------
const TrackSector &Track::Sector(unsigned index) const
{
    if (index < mSectors.size()) return mSectors[index];
    return sInvalid;
}

//-------------------------------------------------------
//Make a representation for drawing with polygon
void Track::MakeScaledPolygons(DisplayController::VertexData *&outer, unsigned &outerCount,
                               DisplayController::VertexData *&inner, unsigned &innerCount,
                               double &outMinLat, double &outMinLon,
                               double &outScaleLat, double &outScaleLon,
                               float left, float top, float w, float h) const
{
    // Alloc verts
    outer = new DisplayController::VertexData[mOuter.size()];
    inner = new DisplayController::VertexData[mInner.size()];
    outerCount = mOuter.size();
    innerCount = mInner.size();

    outMinLat = mMinLat;
    outMinLon = mMinLon;

    // Find the scale
    double diffLat = mMaxLat - mMinLat;
    double diffLon = mMaxLon - mMinLon;

    outScaleLon = mSizeEW / diffLon;
    outScaleLat = mSizeNS * (16.0 / 9.0) / diffLat;

    double hUsed = outScaleLat * diffLat;;
    double wUsed = outScaleLon * diffLon;

    if (wUsed > w)
    {
        double scale = w / wUsed;
        outScaleLat *= scale;
        outScaleLon *= scale;
        wUsed *= scale;
        hUsed *= scale;
    }

    if (hUsed > h)
    {
        double scale = h / hUsed;
        outScaleLat *= scale;
        outScaleLon *= scale;
        wUsed *= scale;
        hUsed *= scale;
    }

    double midLat = (mMaxLat + mMinLat) / 2.0;
    double midLon = (mMaxLon + mMinLon) / 2.0;

    outMinLat = ((h / hUsed) * (mMinLat - midLat)) + midLat;
    outMinLon = ((w / wUsed) * (mMinLon - midLon)) + midLon;

    log_info("Track range is (%f, %f) x (%f, %f)", mMinLat, mMinLon, mMaxLat, mMaxLon);
    log_info("Selected scale is %f %f  %f %f", outScaleLat, outScaleLon, outMinLat, outMinLon);

    // Fill them in
    unsigned a;
    for (a = 0; a < mOuter.size(); a++)
    {
        outer[a].x = left + ((mOuter[a].longitude - outMinLon) * outScaleLon);
        outer[a].y = top  + (h - ((mOuter[a].latitude - outMinLat) * outScaleLat));
    }

    for (a = 0; a < mInner.size(); a++)
    {
        inner[a].x = left + ((mInner[a].longitude - outMinLon) * outScaleLon);
        inner[a].y = top  + (h - ((mInner[a].latitude - outMinLat) * outScaleLat));
    }
}




//-------------------------------------------------------
//-------------------------------------------------------
//-------------------------------------------------------
TrackManager::TrackManager(const char *dir) :
    mSelectedTrack(NULL)
{
    mTrackPath = strdup(dir);
    int count = LoadTracks();
    log_info("Loaded %d tracks", count);
    InitLog();
    mLapUpdateCount = 0;
    mBestLap.lap = 0;
    mBestLap.duration = 1000000.0;
    mBestLap.distance = 0.0;

    mCurrentLap.lap = 0;
    mCurrentLap.startUTC = 0;
    mCurrentLap.distance = 0;
    mCurrentLap.duration = 0;

    mPredictedLapDelta = 0.0;

    mTrackOuter = NULL;
    mTrackInner = NULL;

    mTrackOuterCount = 0;
    mTrackInnerCount = 0;

    const char *track;
    ConfigGet(TRACK_ROOT_PATH, "current", track);
    SelectTrack(track);
}

//-------------------------------------------------------
TrackManager::~TrackManager()
{
    FreeTracks();
    free(mTrackPath);
}

//-------------------------------------------------------
const char *TrackManager::ClosestTrack(double lat, double lon)
{
    std::vector<const Track *> tracks;
    ClosestTracks(lat, lon, tracks);

    if (tracks.size() == 0) return "No tracks loaded";

    log_info("Closest track is %s, %6.1fKm", tracks[0]->Name(), tracks[0]->Distance()/1000.0);
    return tracks[0]->Name();
}

//-------------------------------------------------------
static bool trackCmp(const Track *&a, const Track *&b)
{
    return a->Distance() < b->Distance();
}

//-------------------------------------------------------
void TrackManager::ClosestTracks(double lat, double lon, std::vector<const Track *> &tracks)
{
    tracks.clear();
    if (mTracks.size() == 0) return;

    unsigned a;
    for (a = 0; a < mTracks.size(); a++)
    {
        double dist = HaversineDistance(lat, lon, mTracks[a]->InnerSamples()[0].latitude, mTracks[a]->InnerSamples()[0].longitude);
        mTracks[a]->SetDistance(dist);

        tracks.push_back(mTracks[a]);

        //log_debug("Distance to track %s is %f", mTracks[a]->Name(), dist);
    }

    // Sort the tracks by distance
    std::sort(tracks.begin(), tracks.end(), trackCmp);

    // Ditch all further away than 2Km from the closest
    //double dist = tracks[0]->Distance() + 2000.0;
    //while ((*tracks.rbegin())->Distance() > dist) tracks.pop_back();

#if 0
    printf("Closest Tracks to %f %f\n", lat, lon);
    for (a = 0; a < tracks.size(); a++)
    {
        printf("  %d: %6.1fKm %s\n", a, tracks[a]->Distance() / 1000.0, tracks[a]->Name());
    }
#endif
}

//-------------------------------------------------------
bool TrackManager::SelectTrack(const char *name)
{
    unsigned a;
    for(a = 0; a < mTracks.size(); a++)
    {
        if (0 == strcmp(name, mTracks[a]->Name()))
        {
            if (mSelectedTrack != mTracks[a])
            {
                mSelectedTrack = mTracks[a];
                log_debug("Selected track %s StartFinish Lat:%f  Long:%f\n", mSelectedTrack->Name(), mSelectedTrack->StartFinish().latitude, mSelectedTrack->StartFinish().longitude);
                printf("Selecting track: %s\n", mSelectedTrack->Name());

                ConfigSet(TRACK_ROOT_PATH, "current", mSelectedTrack->Name());

                if (mTrackOuter != NULL) delete[] mTrackOuter;
                if (mTrackInner != NULL) delete[] mTrackInner;
                mTrackOuter = NULL;
                mTrackInner = NULL;
                mTrackOuterCount = 0;
                mTrackInnerCount = 0;

                SelectedTrack()->MakeScaledPolygons(mTrackOuter, mTrackOuterCount, mTrackInner, mTrackInnerCount,
                                                    mTrackMinLat, mTrackMinLon, mTrackScaleLat, mTrackScaleLon, 0.0F, 0.0F, 1.0F, 1.0F);

            }
            return true;
        }
    }
    return false;
}

//-------------------------------------------------------
void TrackManager::DrawSelectedTrack(DisplayController *display, float topX, float topY, float width, float height)
{
    if (mTrackOuterCount > 0)
    {
        float scale = (height < width) ? height : width;
        display->PushTranslate(topX, topY);
        display->PushScale(scale, scale);

        display->Polygon(mTrackOuter, mTrackOuterCount, 2.0F, WHITE, CLEAR);
        display->Polygon(mTrackInner, mTrackInnerCount, 2.0F, WHITE, CLEAR);
        display->Line(mTrackInner[0].x, mTrackInner[0].y, mTrackOuter[0].x, mTrackOuter[0].y, 4.0F, YELLOW);

        display->PopTransform();
        display->PopTransform();
    }
}

//-------------------------------------------------------
void TrackManager::DrawTrackMarker(DisplayController *display, float topX, float topY, float width, float height, double lat, double lon, uint32_t color)
{
    if (mTrackOuterCount > 0)
    {
        float scale = (height < width) ? height : width;
        display->PushTranslate(topX, topY);
        display->PushScale(scale, scale);

        double carX = 0.0 + ((lon - mTrackMinLon) * mTrackScaleLon);
        double carY = 1.0 - ((lat - mTrackMinLat) * mTrackScaleLat);
        display->CircleFilled(carX, carY, 0.005F, color);

        display->PopTransform();
        display->PopTransform();
    }
}


//-------------------------------------------------------
int TrackManager::LoadTracks(void)
{
    struct dirent *dir;
    DIR *d = opendir(mTrackPath);
    if (d == NULL) return 0;

    dir = readdir(d);
    while (dir != NULL)
    {
        // skip hidden files and directories
        if (dir->d_name[0] != '.')
        {
            if (dir->d_type == DT_REG)
            {
                Track *t = new Track;
                char fullname[1024];
                strcpy(fullname, mTrackPath);
                strcat(fullname, "/");
                strcat(fullname, dir->d_name);
                bool ok = t->Load(fullname);
                if (!ok) delete t;
                else mTracks.push_back(t);
            }
        }
        dir = readdir(d);
    }
    closedir(d);
    return mTracks.size();
}

//-------------------------------------------------------
void TrackManager::FreeTracks(void)
{
    while(!mTracks.empty())
    {
        delete mTracks.back();
        mTracks.pop_back();
    }
}

//-------------------------------------------------------
bool TrackManager::UpdateLap(NMEANavData &curPos, AuxData &aux)
{

    if (mSelectedTrack == NULL)
    {
        log_debug("TrackManager::UpdateLap(): No track selected");
        return false;
    }

    ++mLapUpdateCount;
    if (mLapUpdateCount == 1)
    {
        mPrevPos = curPos;
        mCurrentLap.startUTC = curPos.utc;
        return false;
    }


    bool addedLap = false;
    const TrackCoord &startFinish = mSelectedTrack->StartFinish();
    double t = ClosestApproachT(startFinish.latitude, startFinish.longitude,
                                mPrevPos.latitude, mPrevPos.longitude,
                                curPos.latitude, curPos.longitude);

    log_debug("Lat:%.9f Long:%.9f  T:%f  ", curPos.latitude, curPos.longitude, t);

    if ((t >= 0.0) && (t < 1.0))
    {
        NMEANavData closestPos = { 0 };
        double utcClosest = Interp(mPrevPos.utc, curPos.utc, t);
        bool ok = mPrevPos.Interpolate(closestPos, utcClosest, curPos);

        double dist = HaversineDistance(startFinish.latitude, startFinish.longitude, closestPos.latitude, closestPos.longitude);
        log_debug("Interp:%s Dist:%f  Closest Lat:%.9f Long:%.9f  ", ok ? "OK" :"Bad", dist, closestPos.latitude, closestPos.longitude);

        // If the closest point on the line to startfinish is less than 15m, we've crossed it
        // Might need to check the lap distance to make sure we never double-start...
        if ((dist < 15.0) && (mCurrentLap.distance > 25.0))
        {
            log_debug("Crossed StartFinish  ");
            mCurrentLap.distance += HaversineDistance(mPrevPos.latitude, mPrevPos.longitude, closestPos.latitude, closestPos.longitude);
            // Passing start finish again
            if (mCurrentLap.lap > 0)
            {
                // Complete the current lap

                mCurrentLap.duration  = closestPos.utc - mCurrentLap.startUTC;
                mCurrentLap.fuelUsed += aux.fuelUsed;
                UpdatePath(closestPos);
                LogLap(mCurrentLap);
                UpdateBestLap(mCurrentLap);
                log_debug("Ending lap %d  Duration:%f  ", mCurrentLap.lap, mCurrentLap.duration);

                // I don't think every lap needs the path stored outside the log so clear it before saving it
                mCurrentLap.path.clear();
                mCurrentLap.path.shrink_to_fit();
                mLaps.push_back(mCurrentLap);

                // Setup the next lap
                ++mCurrentLap.lap;
                addedLap = true;
            }
            else // this is the first lap
            {
                mCurrentLap.lap = 1;
                DataLogger::UpdateVariable(mLapStartLog, closestPos.utc, mLaps.size() + 1);
            }

            log_debug("Starting lap %d  ", mCurrentLap.lap);

            mCurrentLap.startUTC = closestPos.utc;
            mCurrentLap.distance = HaversineDistance(closestPos.latitude, closestPos.longitude, curPos.latitude, curPos.longitude);
            mCurrentLap.duration = curPos.utc - mCurrentLap.startUTC;
            mCurrentLap.fuelUsed = -aux.fuelUsed;

            UpdatePath(closestPos);
        }
        else // not close enough to the startfinish
        {
            mCurrentLap.distance += HaversineDistance(mPrevPos.latitude, mPrevPos.longitude, curPos.latitude, curPos.longitude);
            mCurrentLap.duration = curPos.utc - mCurrentLap.startUTC;
        }
    }
    else // T not between 0-1
    {
        mCurrentLap.distance += HaversineDistance(mPrevPos.latitude, mPrevPos.longitude, curPos.latitude, curPos.longitude);
        mCurrentLap.duration = curPos.utc - mCurrentLap.startUTC;
    }


    UpdatePath(curPos);
    UpdatePrediction(mCurrentLap);

    log_debug("Delta: %.3f  ", mPredictedLapDelta);

    mPrevPos = curPos;
    return addedLap;
}

//---------------------------------------------------------
void TrackManager::UpdateBestLap(const LapData &current)
{
    if (mBestLap.lap == 0)
    {
        mBestLap = current;
        return;
    }

    // Was this the best lap?
    if (current.duration < mBestLap.duration)
    {
        mBestLap = current;
    }
}

//---------------------------------------------------------
void TrackManager::UpdatePath(const NMEANavData &curPos)
{
    LapPath lp;
    lp.duration = mCurrentLap.duration;;
    lp.distance = mCurrentLap.distance;
    mCurrentLap.path.emplace_back(lp);
}

//---------------------------------------------------------
void TrackManager::UpdatePrediction(const LapData &current)
{
    if (mBestLap.lap == 0) return;

    double bestTimeAtDist = -1.0;
    // Find bracketing distance & time from best lap for current lap's distance
    unsigned pos;
    for (pos = 0; pos < mBestLap.path.size() - 1; pos++)
    {
        if ((mBestLap.path[pos].distance <= current.distance) &&
            (mBestLap.path[pos+1].distance > current.distance))
        {

            double bestBracketDist = mBestLap.path[pos+1].distance - mBestLap.path[pos].distance;
            double t = (current.distance - mBestLap.path[pos].distance) / bestBracketDist;
            bestTimeAtDist = Interp(mBestLap.path[pos].duration, mBestLap.path[pos+1].duration, t);

            log_debug("CurDu:%f CurDi:%f BLP: preD:%f postD:%f T:%f BTAD:%f ", current.duration, current.distance, mBestLap.path[pos].distance, mBestLap.path[pos+1].distance, t, bestTimeAtDist);

            mPredictedLapDelta = current.duration - bestTimeAtDist;
            if (mPredictedLapDelta < -99.0) mPredictedLapDelta = -99.0;
            else if (mPredictedLapDelta > 99.0) mPredictedLapDelta = 99.0;

            return;
        }
    }

    // If we didn't find a bracketing best lap dist than we're longer by some amount
    // leave previous prediction alone, we'll cross the line soon and get a full lap time

}

//---------------------------------------------------------
void TrackManager::LogLap(const LapData &ld)
{
    DataLogger::UpdateVariable(mLapDistanceLog, ld.startUTC, ld.distance);
    DataLogger::UpdateVariable(mLapDurationLog, ld.startUTC, ld.duration);
    DataLogger::UpdateVariable(mLapStartLog,    ld.startUTC, ld.lap + 1);
}

//---------------------------------------------------------
void TrackManager::InitLog(void)
{
    mLapStartLog    = DataLogger::AddVariable("lapStartUTC", "utc", U16);
    mLapDurationLog = DataLogger::AddVariable("lapDuration", "s",  Float);
}

//---------------------------------------------------------
double TrackManager::ClosestApproachT(double latP, double lonP, double lat1, double lon1, double lat2, double lon2)
{
    // t = (P-B).(A-B) / (A-B).(A-B)

    double latD = lat2 - lat1;
    double lonD = lon2 - lon1;
    double lenD = sqrt((latD * latD) + (lonD * lonD));
    latD /= lenD;
    lonD /= lenD;

    double latPD = latP - lat1;
    double lonPD = lonP - lon1;

    double t = (latPD * latD) + (lonPD * lonD);
    return t / lenD;
}

