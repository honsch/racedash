#ifndef TRACK_H
#define TRACK_H

#include <stdint.h>
#include <limits.h>
#include <vector>

#include "util/util.h"
#include "gnss/nmeainterface.h"
#include "display.h"


//-----------------------------------------------------
struct TrackCoord
{
    double latitude;
    double longitude;
    double altitude;
};

//-----------------------------------------------------
struct TrackSector : public TrackCoord
{
    unsigned index;
    char name[64];
};

//-----------------------------------------------------
class Track
{
  public:

    Track(void);
   ~Track();

    bool Load(const char *fname);
    void Unload(void);

    const char *Name(void) const { return mName; }
    bool HasSectors(void) const { return !mSectors.empty(); }

    void   SetDistance(double dist) { mDistance = dist; }
    double Distance(void) const     { return mDistance; }

    const std::vector<TrackCoord>  &InnerSamples(void) const { return mInner; }
    const std::vector<TrackCoord>  &OuterSamples(void) const { return mOuter; }
    const std::vector<TrackSector> &Sectors(void)      const { return mSectors; }
    const TrackCoord               &StartFinish(void)  const { return mStartFinish; }

    const TrackSector &FindSector(unsigned index) const;
    const TrackSector &Sector(unsigned index) const;
    const unsigned     NumSectors(void) const { return mSectors.size(); }

    void MakeScaledPolygons(DisplayController::VertexData *&outer, unsigned &outerCount,
                            DisplayController::VertexData *&inner, unsigned &innerCount,
                            double &outMinLat, double &outMinLon,
                            double &outScaleLat, double &outScaleLon,
                            float left, float top, float w, float h) const;

  private:

    unsigned ExtractStrings(const char **strings, unsigned maxStrings, char *line);

    char *mLoadedFile;
    char *mName;

    // size of the track in meters North-South and East-West
    double mSizeNS;
    double mSizeEW;
    double mMinLon;
    double mMaxLon;
    double mMinLat;
    double mMaxLat;

    double mDistance; // for closest track stuff


    std::vector<TrackCoord>  mInner;
    std::vector<TrackCoord>  mOuter;
    std::vector<TrackSector> mSectors;
    TrackCoord mStartFinish;
};

//-----------------------------------------------------
struct LapPath
{
    double duration;
    double distance;
    // More state here for speed, slip angle and the like
};

//-----------------------------------------------------
// I'll probably add all the GPS samples to this in the future
struct LapData
{
    unsigned lap;
    double   startUTC;
    double   duration;
    double   distance;
    double   fuelUsed;  // Only valid after lap is complete
    std::vector<LapPath> path;
};

//-----------------------------------------------------
class TrackManager
{
  public:

    TrackManager(const char *dir);
   ~TrackManager();

    // Returns the name of the closest track
    const char *ClosestTrack(double lat, double lon);
    void        ClosestTracks(double lat, double lon, std::vector<const Track *> &tracks);

    bool SelectTrack(const char *name);

    const Track *SelectedTrack(void) const { return mSelectedTrack; }

    void DrawSelectedTrack(DisplayController *display, float topX, float topY, float width, float height);
    void DrawTrackMarker(DisplayController *display, float topX, float topY, float width, float height, double lat, double lon, uint32_t color);


    // Add more data here as needed
    struct AuxData
    {
        double fuelUsed;
    };


    // returns true if a lap was just completed
    bool UpdateLap(NMEANavData &curPos, AuxData &aux);

    unsigned LapsComplete(void) const { return mLaps.size(); }

    const LapData &LastLap(void) const { return mLaps.back(); }
    const LapData &BestLap(void) const { return mBestLap; }
    const LapData &CurrentLap(void) const { return mCurrentLap; }
    const LapData &GetLap(unsigned lap) const { return ((lap-1) < mLaps.size()) ? mLaps[lap-1] : mLaps[0]; }

    double LapDelta(void) const { return mPredictedLapDelta; }

  private:
    int  LoadTracks(void);
    void FreeTracks(void);

    double ClosestApproachT(double latP, double lonP, double lat1, double lon1, double lat2, double lon2);
    void   LogLap(const LapData &ld);
    void   InitLog(void);

    void   UpdateBestLap(const LapData &current);
    void   UpdatePath(const NMEANavData &curPos);
    void   UpdatePrediction(const LapData &current);

    void   Test(void);

    Track *mSelectedTrack;

    char *mTrackPath;
    std::vector<Track *> mTracks;

    unsigned mLapUpdateCount;

    std::vector<LapData> mLaps;
    NMEANavData mPrevPos;
    double mPredictedLapDelta;

    LapData mBestLap;
    LapData mCurrentLap;

    DataLoggerID mLapStartLog;
    DataLoggerID mLapDurationLog;
    DataLoggerID mLapDistanceLog;

    // For displaying the track
    DisplayController::VertexData *mTrackOuter;
    DisplayController::VertexData *mTrackInner;
    unsigned mTrackOuterCount;
    unsigned mTrackInnerCount;

    double mTrackMinLat;
    double mTrackMinLon;
    double mTrackScaleLat;
    double mTrackScaleLon;

};

extern TrackManager *gTrackManager;

#endif // TRACK_H
