#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <stdint.h>
#include <vector>

//--------------------------
struct PlaylistItem
{
    uint16_t dir;
    uint16_t flags;
    char    *filename;
};

#define _MAX_LFN    1024
#define PLAYLIST_DIR "/home/alarm/Playlists/"

//--------------------------
class Playlist
{
  public:

    enum PlaylistOrder { Sequential, RandomAlbum, RandomSong };

    enum SaveError { SE_None, SE_EmptyPlaylist, SE_NoFilename, SE_OpenFail };


    Playlist(void);
    Playlist(const char *filname, const char *startItem);
   ~Playlist();

    void Close(void);

    SaveError Save(void);
    SaveError SaveAs(const char *fname);

    // Make a copy of this playlist
    Playlist *Clone(void);

    // Start the playlist over
    void Reset(void);

    void SetOrder(PlaylistOrder order);
    PlaylistOrder GetOrder(void) { return mOrder; }

    // Move the playlist one name forward or backward
    void Prev(void);
    void Next(void);

    // returns negative if fname isn't in the playlist
    int SetCurrent(const char *fname);

    // Update the next & prev title and artist strings
    void UpdateTags(void);

    // This return NULL if the requested file is invalid
    char *CurrentFilename(void);

    const char *NextArtist(void);
    const char *NextTitle(void);
    const char *NextAlbum(void);

    unsigned CurrentIndex(void);
    unsigned ItemCount(void);

    void AddItem(const char *path, const char *fname);
    void RemoveItem(unsigned index);

    const char *GetFilename(unsigned index);

    bool HasEdits(void) { return mHasEdits; }

    bool IsEmpty(void) { return mItems.size() == 0; }

    const char *GetPlaylistFilename(void) { return mFilename; }

  private:

    std::vector<PlaylistItem> mItems;
    std::vector<char *>       mDirs;
    std::vector<uint16_t>     mItemOrder;

    unsigned      mCurIndex;

    enum NextInfo { Unknown, OK, Failed};
    NextInfo      mFilledNextInfo;

    PlaylistOrder mOrder;
    bool          mHasEdits;

    char          mNextTitle[120];
    char          mNextArtist[120];
    char          mNextAlbum[120];

    char          mCurrentFilename[_MAX_LFN + 1];
    char          mFilename[_MAX_LFN + 1];

    void MakeFilename(char *dest, unsigned itemIndex);
    void FillOrder(void);


};

//---------------------------------------------------------

#endif // PLAYLIST_H
