#include "audio.h"

#include <stdlib.h>
#include <string.h>
#include <string.h>

#include "util/util.h"

#include "log/log.h"

//-----------------------------------------------------------------------------
VorbisPlayer::VorbisPlayer(void)
{
    memset(&mCurrentArtist, 0, sizeof(mCurrentArtist));
    memset(&mCurrentTitle, 0, sizeof(mCurrentTitle));

    mFile = NULL;
    mPlayingState = Vorbis_Stopped;

    mFilenameCallback = NULL;
    mVal = 0;

    mTotalSamplesInFile = 0;
    mDecodedSamplesInFile = 0;

    mAlsaDriver = new AlsaDriver("front:CARD=Set,DEV=0", 44100, 2, true, true,
                                  &VorbisPlayer::SampleCallback, this);
    SetMasterVolumeDB(-40.0F);
}

//-----------------------------------------------------------------------------
VorbisPlayer::~VorbisPlayer()
{
    mAlsaDriver->Mute(true);
    mAlsaDriver->Stop();
    delete mAlsaDriver;

    if (mFile != NULL) fclose(mFile);
}

//-----------------------------------------------------------------------------
VorbisPlayer::PlayingState VorbisPlayer::GetPlayingState(void)
{
    return mPlayingState;
}

//-----------------------------------------------------------------------------
float VorbisPlayer::Progress(void)
{
    if (mPlayingState == Vorbis_Stopped) return 0.0F;
    return ((float)mDecodedSamplesInFile) / (float)mTotalSamplesInFile;
}

//-----------------------------------------------------------------------------
float VorbisPlayer::ProgressSeconds(void)
{
    if (mPlayingState == Vorbis_Stopped) return 0.0F;
    return (float) ov_time_tell(&mVorbisFile);
}

//-----------------------------------------------------------------------------
float VorbisPlayer::LengthSeconds(void)
{
    if (mPlayingState == Vorbis_Stopped) return 0.0F;
    return (float) ov_time_total(&mVorbisFile, -1);
}

//-----------------------------------------------------------------------------
const char *VorbisPlayer::CurrentArtist(void)
{
    if (mPlayingState == Vorbis_Stopped) return NULL;
    return mCurrentArtist;
}

//-----------------------------------------------------------------------------
const char *VorbisPlayer::CurrentTitle(void)
{
    if (mPlayingState == Vorbis_Stopped) return NULL;
    return mCurrentTitle;
}

//-----------------------------------------------------------------------------
const char *VorbisPlayer::CurrentAlbum(void)
{
    if (mPlayingState == Vorbis_Stopped) return NULL;
    return mCurrentAlbum;
}

//-----------------------------------------------------------------------------
int VorbisPlayer::DecodeTags(OggVorbis_File *vf, char *title, char *artist, char *album)
{
    memset(title,  0, TAG_MAX_LEN);
    memset(artist, 0, TAG_MAX_LEN);
    memset(album,  0, TAG_MAX_LEN);

    vorbis_comment *comment = ov_comment(vf, -1);
    if (comment == NULL) return -1;

    int a;
    for (a = 0; a < comment->comments; a++)
    {
        const char *s = comment->user_comments[a];
        int dataLen   = comment->comment_lengths[a];
        if ((s == NULL) || (dataLen == 0)) continue;

        if (0 == memcmp("ARTIST=", s, 7))
        {
            int len = strlen(artist);
            int room = TAG_MAX_LEN - len;
            int copySize = (dataLen < room) ? dataLen : (room - 1);
            memcpy(artist + len, s + 7, copySize);
        }

        if (0 == memcmp("TITLE=", s, 6))
        {
            int len = strlen(title);
            int room = TAG_MAX_LEN - len;
            int copySize = (dataLen < room) ? dataLen : (room - 1);
            memcpy(title + len, s + 6, copySize);
        }

        if (0 == memcmp("ALBUM=", s, 6))
        {
            int len = strlen(album);
            int room = TAG_MAX_LEN - len;
            int copySize = (dataLen < room) ? dataLen : (room - 1);
            memcpy(album + len, s + 6, copySize);
        }
    }

    return 0;
}

//-----------------------------------------------------------------------------
int VorbisPlayer::Value(void)
{
    return mVal;
}

//-----------------------------------------------------------------------------
void VorbisPlayer::SetFilenameCallback(NEXT_FILE_CALLBACK callback, void *context)
{
    mFilenameCallback = callback;
    mFilenameCallbackContext = context;
}

//-----------------------------------------------------------------------------
void VorbisPlayer::SetMasterVolumeDB(float dbGain)
{
    mAlsaDriver->SetVolume(dbGain);
}

//-----------------------------------------------------------------------------
float VorbisPlayer::GetMaxVolumeDB(void) const
{
    return mAlsaDriver->GetMaxVolumeDB();
}

//-----------------------------------------------------------------------------
float VorbisPlayer::GetMinVolumeDB(void) const
{
    return mAlsaDriver->GetMinVolumeDB();
}

//-----------------------------------------------------------------------------
int VorbisPlayer::StartFile(bool skipToNext, bool flush)
{
    mTotalSamplesInFile = 0;
    mDecodedSamplesInFile = 0;

    while (1)
    {
        // Close any open file
        if (mFile != NULL)
        {
            fclose(mFile);
            mFile = NULL;
        }

        if (mFilenameCallback == NULL) break;

        const char *filename = mFilenameCallback(skipToNext, mFilenameCallbackContext);
        if (filename == NULL) break;
        if (strlen(filename) < 4) break;

        // Start parsing the File
        mFile = fopen(filename, "rb");
        if (mFile == NULL) continue;

        int err = ov_open(mFile, &mVorbisFile, NULL, 0);
        if (err < 0)
        {
            ov_clear(&mVorbisFile);
            continue;
        }

        mTotalSamplesInFile = ov_pcm_total(&mVorbisFile, -1);
        DecodeTags(&mVorbisFile, mCurrentTitle, mCurrentArtist, mCurrentAlbum);

        return 0;
    }

    return -1;
}

//-----------------------------------------------------------------------------
void VorbisPlayer::Play(void)
{
    if (mPlayingState == Vorbis_Playing) return;

    // Queue up the next song to play from stopped
    if (mPlayingState == Vorbis_Stopped)
    {
        int err = StartFile(false, true);
        if (err < 0)
        {
            mPlayingState = Vorbis_Stopped;
            return;
        }

        mPlayingState = Vorbis_Playing;
        Update();
    }

    mPlayingState = Vorbis_Playing;

    mAlsaDriver->Play();
    mAlsaDriver->Update();
    mAlsaDriver->Mute(false);
}

//-----------------------------------------------------------------------------
void VorbisPlayer::Pause(void)
{
    if (mPlayingState == Vorbis_Stopped) return;
    mPlayingState = Vorbis_Paused;
    mAlsaDriver->Mute(true);
    mAlsaDriver->Pause();
}

//-----------------------------------------------------------------------------
void VorbisPlayer::Stop(void)
{
    if (mPlayingState == Vorbis_Stopped) return;
    mPlayingState = Vorbis_Stopped;
    mAlsaDriver->Mute(true);
    mAlsaDriver->Stop();
}

//-----------------------------------------------------------------------------
void VorbisPlayer::Beep(void)
{

}


//-----------------------------------------------------------------------------
int VorbisPlayer::SampleCallback(void *context, void *dest, unsigned frames, unsigned channels)
{
    if (context == NULL) return -1;
    if (dest == NULL)    return -1;
    if (channels != 2)   return -1;

    VorbisPlayer *player = (VorbisPlayer *) context;
    return player->SampleCallbackInternal(dest, frames, channels);
}

//-----------------------------------------------------------------------------
int VorbisPlayer::SampleCallbackInternal(void *dest, unsigned frames, unsigned channels)
{
    int id;
    int len = frames * channels * 2;
    char *buffer = (char *)dest;
    while (len != 0)
    {
        int bytes_read = ov_read(&mVorbisFile, buffer, len, 0, 2, 1, &id);

        //printf("Fr: %d  Len: %d  BR: %d  ID: %d\n", frames, len, bytes_read, id);

        if (bytes_read < 0) return -1;
        else if (bytes_read == 0)
        {
            int err = StartFile(true, false);
            if (err < 0)
            {
                mPlayingState = Vorbis_Stopped;
                mAlsaDriver->Mute(true);
                mAlsaDriver->Stop();
                return -1;
            }
        }
        else
        {
            len    -= bytes_read;
            buffer += bytes_read;
        }
    }

    mDecodedSamplesInFile = ov_pcm_tell(&mVorbisFile);
    return 0;
}

//-----------------------------------------------------------------------------
void VorbisPlayer::Update(void)
{
    if (mPlayingState == Vorbis_Stopped) return;
    mAlsaDriver->Update();
}



