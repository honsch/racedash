#ifndef ALSA_H
#define ALSA_H

#include <stdint.h>
#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

//--------------------------------------------------------
class AlsaDriver
{
  public:

    typedef int (*SampleGenerator)(void *context, void *dest, unsigned frames, unsigned numChannels);

    AlsaDriver(const char *device, unsigned sampleRate, unsigned channels, bool resample, bool enableVolume, SampleGenerator generator, void *context);
   ~AlsaDriver();

    int Play(void);
    int Pause(void);
    int Stop(void);

    int Mute(bool enable);
    int SetVolume(float db);
    float GetMaxVolumeDB(void) const { return mVolumeMax; }
    float GetMinVolumeDB(void) const { return mVolumeMin; }

    int Update(void);

  private:

    int SetupHW(void);
    int SetupSW(void);

    // PCM Stuff
    snd_pcm_t           *mAlsaHandle;
    snd_output_t        *mDebugOutput;
    snd_pcm_uframes_t    mRingBufferSize;
    snd_pcm_uframes_t    mPeriodSize;
    snd_pcm_hw_params_t *mHWParams;
    snd_pcm_sw_params_t *mSWParams;

    char     mDeviceName[64];
    unsigned mSampleRate;
    unsigned mChannels;
    unsigned mRingBufferDuration; // In uS
    unsigned mPeriodDuration;     // in uS
    int      mEnableAlsaResampler;
    bool     mEnableVolume;

    snd_pcm_format_t mPCMFormat;
    snd_pcm_access_t mChannelFormat;

    enum StreamState { Stopped, PlayPending, Playing, Paused, ResumePending };
    StreamState mStreamState;
    SampleGenerator mGenerator;
    void           *mGeneratorContext;

    // Mixer stuff
    snd_mixer_t          *mMixerHandle;
    snd_mixer_elem_t     *mMixerVolumeElement;
    float                 mVolume; // in dB gain
    float                 mHWVolume; // what gets sent to the HW
    float                 mSWVolume; // what is added by software
    float                 mHWVolumeMin;
    float                 mVolumeMax;
    float                 mVolumeMin;
    bool                  mMuted;
    int16_t               mSWVolumeMult;
    int16_t              *mVolumeBuffer;

};

#endif // ALSA_H