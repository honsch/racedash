#ifndef ALSALED_H
#define ALSALED_H

#include <stdint.h>
#include <alsa/asoundlib.h>
#include <alsa/mixer.h>

//--------------------------------------------------------
class AlsaLEDDriver
{
  public:

    typedef int (*SampleGenerator)(void *context, void *dest, unsigned frames, unsigned numChannels);

    AlsaLEDDriver(const char *device, unsigned sampleRate, unsigned channels, SampleGenerator generator, void *context);
   ~AlsaLEDDriver();

    int Play(void);
    int Pause(void);
    int Stop(void);

    int Update(void);

  private:

    int SetupHW(void);
    int SetupSW(void);

    // PCM Stuff
    snd_pcm_t           *mAlsaHandle;
    snd_output_t        *mDebugOutput;
    snd_pcm_uframes_t    mRingBufferSize;
    snd_pcm_uframes_t    mPeriodSize;
    snd_pcm_hw_params_t *mHWParams;
    snd_pcm_sw_params_t *mSWParams;

    char     mDeviceName[64];
    unsigned mSampleRate;
    unsigned mChannels;
    unsigned mRingBufferDuration; // In uS
    unsigned mPeriodDuration;     // in uS

    snd_pcm_format_t mPCMFormat;
    snd_pcm_access_t mChannelFormat;

    enum StreamState { Stopped, PlayPending, Playing, Paused, ResumePending };
    StreamState mStreamState;
    SampleGenerator mGenerator;
    void           *mGeneratorContext;
};

#endif // ALSALED_H