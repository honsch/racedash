#ifndef AUDIO_H
#define AUDIO_H

#include <stdint.h>
#include <stdio.h>
#include "alsa.h"

#include <vorbis/vorbisfile.h>

class VorbisPlayer
{
 public:

   VorbisPlayer(void);
  ~VorbisPlayer();

   // Valid range is gotten from mixer
   void SetMasterVolumeDB(float dbGain);

   float GetMaxVolumeDB(void) const;
   float GetMinVolumeDB(void) const;

   void Beep(void);

   void Play(void);
   void Pause(void);
   void Stop(void);

   // Returns the last decoder error
   int Value(void);

   // Gets the file from the callback
   int StartFile(bool skipToNext, bool flush);

   typedef const char *(*NEXT_FILE_CALLBACK)(bool skipToNext, void *);

   void SetFilenameCallback(NEXT_FILE_CALLBACK callback, void *context);

   // call this periodically to keep the data flowing
   void Update(void);

   // Returns 0-100 in percent of how full the file buffer is
   unsigned FileBufferState(void);

   uint32_t NumDecodedBuffers(void);

   bool     DecodeLate(void);
   uint32_t DecoderResetCount(void);

   enum PlayingState { Vorbis_Stopped, Vorbis_Playing, Vorbis_Paused };

   PlayingState GetPlayingState(void);

   float    Progress(void);        // returns 0 to 1
   float    ProgressSeconds(void); // returns seconds in to the current song
   float    LengthSeconds(void);   // returns song length
   unsigned ProcessingTime(void);  // returns the number of ticks burned by Vorbis decoding

   const char *CurrentArtist(void);
   const char *CurrentTitle(void);
   const char *CurrentAlbum(void);

   // Returns negative on failure, zero on success
   static int DecodeTags(OggVorbis_File *vf, char *title, char *artist, char *album);

 private:

   #define TAG_MAX_LEN  120
   FILE              *mFile;
   PlayingState       mPlayingState;
   char               mCurrentArtist[TAG_MAX_LEN];
   char               mCurrentTitle[TAG_MAX_LEN];
   char               mCurrentAlbum[TAG_MAX_LEN];
   NEXT_FILE_CALLBACK mFilenameCallback;
   void              *mFilenameCallbackContext;
   int                mVal;

   OggVorbis_File     mVorbisFile;

   uint64_t           mTotalSamplesInFile;
   uint64_t           mDecodedSamplesInFile;

   AlsaDriver *mAlsaDriver;
   static int SampleCallback(void *context, void *dest, unsigned frames, unsigned channels);
   int SampleCallbackInternal(void *dest, unsigned frames, unsigned channels);

};

#endif // AUDIO_H
