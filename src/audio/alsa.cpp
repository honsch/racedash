#include "alsa.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include "log/log.h"

//--------------------------------------------------------
AlsaDriver::AlsaDriver(const char *device, unsigned sampleRate, unsigned channels, bool resample, bool enableVolume, SampleGenerator generator, void *generatorContext) :
    mAlsaHandle(NULL),
    mGenerator(generator),
    mGeneratorContext(generatorContext),
    mVolumeBuffer(NULL)
{
    if (mGenerator == NULL) return;

    strcpy(mDeviceName, device);

    int err = snd_pcm_open(&mAlsaHandle, mDeviceName, SND_PCM_STREAM_PLAYBACK, 0);
    if (err < 0)
    {
        log_error("Alsa not available for %s", mDeviceName);
        mAlsaHandle = NULL;
        return;
    }
    else
    {
        log_error("Alsa opened for %s", mDeviceName);
    }

    mSampleRate          = sampleRate;
    mChannels            = channels;
    mPeriodDuration      = (4096UL * 1000000UL) / (unsigned long) sampleRate; //in uS ~4096 samples
    mRingBufferDuration  = mPeriodDuration * 8;
    mEnableAlsaResampler = resample ? 1 : 0;
    mPCMFormat           = SND_PCM_FORMAT_S16;
    mChannelFormat       = SND_PCM_ACCESS_MMAP_INTERLEAVED;
    mEnableVolume        = enableVolume;

    err = SetupHW();
    if (err < 0)
    {
        snd_pcm_close(mAlsaHandle);
        mAlsaHandle = NULL;
        return;
    }
    err = SetupSW();
    if (err < 0)
    {
        snd_pcm_close(mAlsaHandle);
        mAlsaHandle = NULL;
        return;
    }

    mStreamState = Stopped;

    snd_output_stdio_attach(&mDebugOutput, stdout, 0);

    // print out all the info
    //snd_pcm_dump(mAlsaHandle, mDebugOutput);

    // Setup volume control
    snd_mixer_open(&mMixerHandle, 0);
    snd_mixer_attach(mMixerHandle, "hw:1");
    snd_mixer_selem_register(mMixerHandle, NULL, NULL);
    snd_mixer_load(mMixerHandle);

    snd_mixer_selem_id_t *id;
    snd_mixer_selem_id_alloca(&id);
    snd_mixer_selem_id_set_index(id, 0);
    snd_mixer_selem_id_set_name(id, "Headphone");
    mMixerVolumeElement = snd_mixer_find_selem(mMixerHandle, id);

    //long min, max;
    //snd_mixer_selem_get_playback_volume_range(mMixerVolumeElement, &min, &max);
    //mVolumeMin = 0.01F * (float) min;
    //mVolumeMax = 0.01F * (float) max;

    if (mEnableVolume)
    {
        mVolumeMax    =   0.0F;
        mHWVolumeMin  = -25.0F;
        mVolumeMin    = -60.0F; // reported min, use software to attenuate further than HW min
        mVolumeBuffer = new int16_t[mPeriodSize * mChannels];

        // Now mute the output since the mixer is setup
        Mute(true);

        SetVolume(-35.0F);
        log_debug("Mixer MaxDB :%5.2fdb", mVolumeMax);
        log_debug("Mixer MinDB :%5.2fdb", mVolumeMin);
    }
}

//--------------------------------------------------------
AlsaDriver::~AlsaDriver(void)
{
    if (mVolumeBuffer != NULL)
    {
        delete[] mVolumeBuffer;
        mVolumeBuffer = NULL;
    }

    if (mAlsaHandle != NULL)
    {
        snd_pcm_close(mAlsaHandle);
        mAlsaHandle = NULL;
    }

    if (mMixerHandle != NULL)
    {
        snd_mixer_close(mMixerHandle);
        mMixerHandle = NULL;
    }
}


//--------------------------------------------------------
int AlsaDriver::Play(void)
{
    if (mAlsaHandle == NULL) return -999;
    if (mStreamState == Stopped) mStreamState = PlayPending;
    if (mStreamState == Paused)  mStreamState = ResumePending;

    return 0;
}

//--------------------------------------------------------
int AlsaDriver::Pause(void)
{
    if (mAlsaHandle == NULL) return -999;
    if (mStreamState != Paused)
    {
        int err = snd_pcm_pause(mAlsaHandle, 1);
        if (err == 0)
        {
            mStreamState = Paused;
        }
        else return err;
    }
    return 0;
}

//--------------------------------------------------------
int AlsaDriver::Stop(void)
{
    if (mAlsaHandle == NULL) return -999;
    if (mStreamState != Stopped)
    {
        int err = snd_pcm_drop(mAlsaHandle);
        if (err < 0) return err;
        mStreamState = Stopped;
    }
    return 0;
}


//--------------------------------------------------------
int AlsaDriver::Mute(bool enable)
{
    if (mEnableVolume)
    {
        mMuted = enable;
        if (mAlsaHandle == NULL) return -999;

        if (mMuted)
        {
            snd_mixer_selem_set_playback_volume_all(mMixerVolumeElement, 0);
        }
        else
        {
            long voldB = (long) (100.0F * mHWVolume);
            snd_mixer_selem_set_playback_dB_all(mMixerVolumeElement, voldB, -1);
        }
    }
    return 0;
}


//--------------------------------------------------------
int AlsaDriver::SetVolume(float db)
{
    if (mEnableVolume)
    {
        if (mAlsaHandle == NULL) return -999;
        mVolume = db;
        if (mVolume < mVolumeMin) mVolume = mVolumeMin;
        if (mVolume > mVolumeMax) mVolume = mVolumeMax;

        if (mVolume >= mHWVolumeMin)
        {
            mHWVolume = mVolume;
            mSWVolume = 1.0F;
            mSWVolumeMult = 32767;
        }
        else
        {
            mHWVolume = mHWVolumeMin;
            mSWVolume = mVolume - mHWVolume;
            mSWVolumeMult = (int) (32768.0F * powf(10.0F, mSWVolume / 20.0F));
        }

        if (mMuted) return 0;

        long voldB = (long) (100.0F * mHWVolume);
        snd_mixer_selem_set_playback_dB_all(mMixerVolumeElement, voldB, -1);
    }
    return 0;
}


//--------------------------------------------------------
int AlsaDriver::Update(void)
{
    if ((mStreamState == Stopped) || (mStreamState == Paused)) return 0;
    if (mAlsaHandle == NULL) return -999;

    if (mStreamState == PlayPending)
    {
        int err = snd_pcm_prepare(mAlsaHandle);
        if (err < 0) return err;
    }

    int err = 0;
    snd_pcm_state_t state = snd_pcm_state(mAlsaHandle);
    if ((state == SND_PCM_STATE_XRUN) || (state == SND_PCM_STATE_SUSPENDED))
    {
        log_warn("Alsa error XRUN or SUSPENDED");
        Stop();
        return -1;
    }

    // Fill up the buffer as much as possible
    //err = snd_pcm_wait(mAlsaHandle, -1);
    //if (err < 0) return err;

    snd_pcm_uframes_t avail = snd_pcm_avail_update(mAlsaHandle);
    while (avail >= mPeriodSize)
    {
        snd_pcm_uframes_t frames = mPeriodSize;
        snd_pcm_uframes_t offset = 0;
        const snd_pcm_channel_area_t *areas;

        err = snd_pcm_mmap_begin(mAlsaHandle, &areas, &offset, &frames);
        if (err < 0)
        {
            Stop();
            return err;
        }

        printf("Offset: %ld  Frames: %ld\n", frames, offset);
        unsigned a;
        for (a = 0; a < 2; a++)
        {
            printf("Area %d:   First: %d     Step: %d\n", a, areas[a].first, (int)areas[a].step);
        }


        // Make sure the buffer is 16 bit interleaved stereo
        if ((areas[0].addr != areas[1].addr) || (areas[0].first != 0) || (areas[1].first != 16))
        {
            snd_pcm_mmap_commit(mAlsaHandle, offset, frames);
            log_error("BAD MEMORY FORMAT from mmap!");
            Stop();
            return -1;
        }
        uint8_t *base = (offset * (areas[0].step / 8)) + (uint8_t*)areas[0].addr;

        if (mEnableVolume && (mSWVolumeMult < 32767))
        {
            err = mGenerator(mGeneratorContext, mVolumeBuffer, frames, mChannels);
            int16_t *src = mVolumeBuffer;
            int16_t *dest = (int16_t *) base;
            unsigned sampleCount = frames * mChannels;
            unsigned a;
            for (a = 0; a < sampleCount; a++)
            {
                int s = (int) *src;
                src++;
                s = (s * mSWVolumeMult) >> 15;
                *dest = (s & 0xFFFF);
                dest++;
            }
        }
        else
        {
            err = mGenerator(mGeneratorContext, base, frames, mChannels);
        }
        // If the generator returns error fill with silence
        if (err < 0) memset(base, 0, frames * areas[0].step / 8);

        snd_pcm_sframes_t commitSize = snd_pcm_mmap_commit(mAlsaHandle, offset, frames);
        if ((commitSize < 0) || (frames != (snd_pcm_uframes_t) commitSize))
        {
            err = -1;
            break;
        }
        avail -= frames;
    }

    if (err < 0) return err;

    if (mStreamState == PlayPending)
    {
        err = snd_pcm_start(mAlsaHandle);
        if (err < 0) return err;
        mStreamState = Playing;
    }
    else if (mStreamState == ResumePending)
    {
        err = snd_pcm_pause(mAlsaHandle, 0);
        if (err < 0) return err;
        mStreamState = Playing;
    }

    return 0;
}


//--------------------------------------------------------
int AlsaDriver::SetupHW(void)
{
    int err, dir;
    snd_pcm_hw_params_alloca(&mHWParams);

    err = snd_pcm_hw_params_any(mAlsaHandle, mHWParams);
    if (err < 0)
    {
        log_error("Broken configuration for playback: no configurations available: %s", snd_strerror(err));
        return err;
    }
    /* set hardware resampling */
    err = snd_pcm_hw_params_set_rate_resample(mAlsaHandle, mHWParams, mEnableAlsaResampler);
    err = snd_pcm_hw_params_set_access(mAlsaHandle, mHWParams, mChannelFormat);
    err = snd_pcm_hw_params_set_format(mAlsaHandle, mHWParams, mPCMFormat);
    err = snd_pcm_hw_params_set_channels(mAlsaHandle, mHWParams, mChannels);

    unsigned rrate = mSampleRate;
    dir = 0;
    err = snd_pcm_hw_params_set_rate_near(mAlsaHandle, mHWParams, &rrate, &dir);
    if (rrate != mSampleRate)
    {
        log_error("Rate doesn't match (requested %iHz, get %iHz  %d dir)", mSampleRate, rrate, dir);
        return -EINVAL;
    }

    err = snd_pcm_hw_params_set_buffer_time_near(mAlsaHandle, mHWParams, &mRingBufferDuration, &dir);
    err = snd_pcm_hw_params_get_buffer_size(mHWParams, &mRingBufferSize);

    err = snd_pcm_hw_params_set_period_time_near(mAlsaHandle, mHWParams, &mPeriodDuration, &dir);
    err = snd_pcm_hw_params_get_period_size(mHWParams, &mPeriodSize, &dir);

    // write the setup
    err = snd_pcm_hw_params(mAlsaHandle, mHWParams);
    if (err < 0)
    {
        log_error("Unable to set hw params for playback: %s", snd_strerror(err));
        return err;
    }
    return 0;
}


//--------------------------------------------------------
int AlsaDriver::SetupSW(void)
{
    int err;
    snd_pcm_sw_params_alloca(&mSWParams);

    err = snd_pcm_sw_params_current(mAlsaHandle, mSWParams);
    if (err < 0)
    {
        log_error("Unable to determine current swparams for playback: %s", snd_strerror(err));
        return err;
    }

    // Don't ever start automatically
    err = snd_pcm_sw_params_set_start_threshold(mAlsaHandle, mSWParams, 1<<30);
    err = snd_pcm_sw_params_set_stop_threshold(mAlsaHandle, mSWParams, 1<<30);

    err = snd_pcm_sw_params_set_avail_min(mAlsaHandle, mSWParams, mPeriodSize);

    // write the setup
    err = snd_pcm_sw_params(mAlsaHandle, mSWParams);
    if (err < 0)
    {
        log_error("Unable to set sw params for playback: %s", snd_strerror(err));
        return err;
    }
    return 0;
}

