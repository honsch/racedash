#include "playlist.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include "audio.h"

#include "log/log.h"

//------------------------------------------------------------------------
static void splitpath(char *path, char *name, const char *input)
{
    unsigned len = strlen(input);
    while (len != 0)
    {
        if (input[len] == '/') break;
        --len;
    }
    memcpy(path, input, len + 1);
    path[len + 1] = 0;
    strcpy(name, input + len + 1);
}

//------------------------------------------------------------------------
// returns negative if there are no more valid lines in the file
static int get_next_valid_file(FILE *file, char *dest, unsigned destLen)
{
    if (file == NULL) return -1;
    if (dest == NULL) return -1;
    if (destLen < 64) return -1;

    while (1)
    {
        if (feof(file) != 0) return -1;

        memset(dest, 0, destLen);
        char *s = fgets(dest, destLen, file);
        if (s != dest) return -1;

        // Kill leading whitespace
        while (dest[0] == ' ') memmove(dest, dest + 1, destLen - 1);

        if (dest[0] == '#') continue; // a comment line

        unsigned len = strlen(dest);
        if (len < 5) continue; // ?.mp3 is minimum len
        --len;

        // Kill trailing whitespace
        while (dest[len] < ' ') dest[len--] = 0;
        if (len < 5) continue; // ?.mp3 is minimum len

        return 0;
    }
    return -1;
}

//------------------------------------------------------------------------
Playlist::Playlist(void)
{
    memset(mFilename, 0, sizeof(mFilename));
    mOrder = Sequential;
    mCurIndex = 0;
    mHasEdits = false;
}


//------------------------------------------------------------------------
Playlist::Playlist(const char *filename, const char *startItem)
{
    memset(mFilename, 0, sizeof(mFilename));
    mOrder = Sequential;
    mHasEdits = false;
    mCurIndex = 0;

    strcpy(mFilename, filename);

    char fullname[_MAX_LFN];
    strcpy(fullname, PLAYLIST_DIR);
    strcat(fullname, mFilename);

    FILE *file = fopen(fullname, "rt");
    if (file == NULL) return;

    char line[_MAX_LFN + 1];
    char path[_MAX_LFN];
    char fname[_MAX_LFN];

    while (1)
    {
        int result = get_next_valid_file(file, line, _MAX_LFN);
        if (result < 0) break;

        splitpath(path, fname, line);
        AddItem(path, fname);
    }

    fclose(file);

    FillOrder();

    Reset();

    // Set the current index to the last playing file if possible
    SetCurrent(startItem);
    mHasEdits = false;
}

//------------------------------------------------------------------------
Playlist::~Playlist(void)
{
    Close();
}

//------------------------------------------------------------------------
Playlist *Playlist::Clone(void)
{
    Playlist *pl = new Playlist;
    pl->mOrder    = mOrder;
    pl->mCurIndex = mCurIndex;
    pl->mHasEdits = mHasEdits;

    unsigned a;
    for (a = 0; a < mDirs.size(); a++)  pl->mDirs.push_back(strdup(mDirs[a]));
    for (a = 0; a < mItems.size(); a++)
    {
        PlaylistItem i = mItems[a];
        i.filename = strdup(mItems[a].filename);
        pl->mItems.push_back(i);
    }

    memcpy(pl->mNextTitle,  mNextTitle,  sizeof(mNextTitle));
    memcpy(pl->mNextArtist, mNextArtist, sizeof(mNextArtist));

    memcpy(pl->mCurrentFilename, mCurrentFilename, sizeof(mCurrentFilename));
    memcpy(pl->mFilename, mFilename, sizeof(mFilename));

    return pl;
}

//------------------------------------------------------------------------
void Playlist::FillOrder(void)
{
    if (IsEmpty()) return;

    mItemOrder.resize(mItems.size());

    unsigned a;
    if (mOrder == RandomAlbum)
    {
        unsigned dirStart = mItems.size() - (mDirs.size() + 1);
        // Fill sequential then swap a bunch of random pairs
        for (a = 0; a < mDirs.size(); a++) mItemOrder[dirStart + a] = a;
        for (a = 0; a < mDirs.size() * 4; a++)
        {
            unsigned i1 = dirStart + random() % mDirs.size();
            unsigned i2 = dirStart + random() % mDirs.size();

            uint16_t temp = mItemOrder[i1];
            mItemOrder[i1] = mItemOrder[i2];
            mItemOrder[i2] = temp;
        }

        // Now we have a random sequence of dirs at the end of the order table,
        // Fill the order table sequentially by random directory
        unsigned idx = 0;
        for (a = 0; a < mDirs.size(); a++)
        {
            unsigned b;
            for (b = 0; b < mItems.size(); b++)
            {
                if (mItems[b].dir == mItemOrder[dirStart])
                {
                    mItemOrder[idx] = b;
                    ++idx;
                }
            }
            ++dirStart;
        }
        return;
    }

    if (mOrder == RandomSong)
    {
        // Fill sequential then swap a bunch of random pairs
        for (a = 0; a < mItems.size(); a++) mItemOrder[a] = a;
        for (a = 0; a < mItems.size() * 3; a++)
        {
            unsigned i1 = random() % mItems.size();
            unsigned i2 = random() % mItems.size();

            uint16_t temp = mItemOrder[i1];
            mItemOrder[i1] = mItemOrder[i2];
            mItemOrder[i2] = temp;
        }
        return;
    }

    // Default is Sequential
    for (a = 0; a < mItems.size(); a++) mItemOrder[a] = a;
}

//------------------------------------------------------------------------
void Playlist::MakeFilename(char *dest, unsigned itemIndex)
{
    *dest = 0;

    if (itemIndex >= mItems.size()) return;
    unsigned orderIndex = mItemOrder[itemIndex];
    if (mItems[orderIndex].dir >= mDirs.size()) return;

    strcpy(dest, mDirs[mItems[orderIndex].dir]);
    strcat(dest, mItems[orderIndex].filename);
}


//------------------------------------------------------------------------
void Playlist::AddItem(const char *path, const char *fname)
{
    unsigned a;
    for (a = 0; a < mDirs.size(); a++) if (0 == strcmp(mDirs[a], path)) break;

    if (a >= mDirs.size())
    {
        mDirs.push_back(strdup(path));
    }

    PlaylistItem i;
    i.dir      = a;
    i.flags    = 0;
    i.filename = strdup(fname);
    mItems.push_back(i);

    log_debug("Playlist added %s  %s", mDirs[i.dir], i.filename);

    mHasEdits = true;
}

//------------------------------------------------------------------------
void Playlist::RemoveItem(unsigned index)
{
    if (index >= mItems.size()) return;

    mHasEdits = true;
    PlaylistItem i = mItems[index];
    mItems.erase(mItems.begin() + index);

    free(i.filename);
}


//------------------------------------------------------------------------
const char *Playlist::GetFilename(unsigned index)
{
    if (index >= mItems.size()) return "Invalid";
    return mItems[index].filename;
}

//------------------------------------------------------------------------
// returns negative if fname isn't in the playlist
int  Playlist::SetCurrent(const char *fname)
{
    if (fname == NULL)      return -1;
    if (strlen(fname) == 0) return -1;

    unsigned a;
    for (a = 0; a < mItems.size(); a++)
    {
        char tempLine[_MAX_LFN];
        MakeFilename(tempLine, a);
        if (0 == strcmp(fname, tempLine))
        {
            mCurIndex = a;
            MakeFilename(mCurrentFilename, mCurIndex);
            mFilledNextInfo = Unknown;
            return 0;
        }
    }
    return -1;
}


//------------------------------------------------------------------------
void Playlist::Close(void)
{
    unsigned a;

    for (a = 0; a < mDirs.size(); a++)  if (mDirs[a] != NULL)           free(mDirs[a]);
    for (a = 0; a < mItems.size(); a++) if (mItems[a].filename != NULL) free(mItems[a].filename);

    mDirs.clear();
    mItems.clear();
    mItemOrder.clear();

    mCurIndex       = 0;
    mOrder          = Sequential;
    mFilledNextInfo = Unknown;
    mHasEdits       = false;

    memset(mNextTitle, 0, sizeof(mNextTitle));
    memset(mNextArtist, 0, sizeof(mNextArtist));

    memset(mCurrentFilename, 0, sizeof(mCurrentFilename));
    memset(mFilename, 0, sizeof(mFilename));
}

    enum SaveError { None, EmptyPlaylist, NoFilename };

//------------------------------------------------------------------------
Playlist::SaveError Playlist::Save(void)
{
    if (IsEmpty())
    {
        log_warn("Can't save an empty playlist");
        return SE_EmptyPlaylist;
    }

    if (strlen(mFilename) == 0) return SE_NoFilename;

    char fullname[_MAX_LFN];
    strcpy(fullname, PLAYLIST_DIR);
    strcat(fullname, mFilename);

    FILE *file = fopen(fullname, "wt");
    if (file == NULL)
    {
        log_error("Can't write to file: %s", mFilename);
        //sprintf(gTempLine, "Can't write to file: %s %d", pl->mFilename, err);
        //LCD_draw_string(gTempLine, 125, 10, RED, BLACK, Small);
        return SE_OpenFail;
    }

    fputs("# Written by playlist code\n", file);

    unsigned a;
    for (a = 0;a < mItems.size(); a++)
    {
        fputs(mDirs[mItems[a].dir], file);
        fputs(mItems[a].filename, file);
        fputs("\n", file);
    }
    fclose(file);

    mHasEdits = 0;

    return SE_None;
}

//------------------------------------------------------------------------
Playlist::SaveError Playlist::SaveAs(const char *fname)
{
    if (fname == NULL) return SE_NoFilename;

    strcpy(mFilename, fname);
    return Save();
}

//------------------------------------------------------------------------
// Start the playlist over
void Playlist::Reset(void)
{
    mCurIndex = 0;
    MakeFilename(mCurrentFilename, mCurIndex);
    mFilledNextInfo = Unknown;
}

//------------------------------------------------------------------------
void Playlist::Prev(void)
{
    if (IsEmpty()) return;

    mCurIndex = (mCurIndex + (mItems.size() - 1)) % mItems.size();
    MakeFilename(mCurrentFilename, mCurIndex);
    mFilledNextInfo = Unknown;
}

//------------------------------------------------------------------------
void Playlist::Next(void)
{
    if (IsEmpty()) return;
    mCurIndex = (mCurIndex + 1) % mItems.size();
    MakeFilename(mCurrentFilename, mCurIndex);
    mFilledNextInfo = Unknown;
}

//------------------------------------------------------------------------
void Playlist::SetOrder(PlaylistOrder order)
{
    if (IsEmpty()) return;
    mFilledNextInfo = Unknown;
    unsigned curItem = 0;
    if (mItemOrder.size() > mCurIndex) curItem = mItemOrder[mCurIndex];

    mOrder = order;
    FillOrder();

    // Keep the current song the 'current' in the playlist
    unsigned a;
    for (a = 0; a < mItems.size(); a++)
    {
        if (mItemOrder[a] == curItem)
        {
            mCurIndex = a;
            break;
        }
    }
}

//------------------------------------------------------------------------
const char *Playlist::NextArtist(void)
{
    if (mFilledNextInfo != OK) return "";
    return mNextArtist;
}

//------------------------------------------------------------------------
const char *Playlist::NextTitle(void)
{
    if (mFilledNextInfo != OK) return "";
    return mNextTitle;
}

//------------------------------------------------------------------------
const char *Playlist::NextAlbum(void)
{
    if (mFilledNextInfo != OK) return "";
    return mNextAlbum;
}

//------------------------------------------------------------------------
unsigned Playlist::CurrentIndex(void)
{
    return mCurIndex;
}

//------------------------------------------------------------------------
unsigned Playlist::ItemCount(void)
{
    return mItems.size();
}

//------------------------------------------------------------------------
void Playlist::UpdateTags(void)
{
    if (IsEmpty()) return;
    if (mFilledNextInfo == Unknown)
    {
        mNextTitle[0]  = 0;
        mNextArtist[0] = 0;
        mNextAlbum[0]  = 0;
        mFilledNextInfo = Failed;

        char fname[_MAX_LFN + 1];
        unsigned next = (mCurIndex + 1) % mItems.size();
        MakeFilename(fname, next);

        // Start parsing the File
        FILE *f = fopen(fname, "rb");
        if (f != NULL)
        {
            OggVorbis_File vf;
            int err = ov_open(f, &vf, NULL, 0);
            if (err == 0)
            {
                int err = VorbisPlayer::DecodeTags(&vf, mNextTitle, mNextArtist, mNextAlbum);
                if (err == 0) mFilledNextInfo = OK;
            }
            ov_clear(&vf);
        }
    }
}

//------------------------------------------------------------------------
char *Playlist::CurrentFilename(void)
{
    return mCurrentFilename;
}

