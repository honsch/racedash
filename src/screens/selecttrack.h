#ifndef SELECTTRACK_H
#define SELECTTRACK_H

#include "display.h"

#include "ui/listbox.h"
#include "ui/button.h"

//-------------------------------------------------------
class SelectTrackScreen : public ScreenBase
{
  public:
    SelectTrackScreen(bool isRemote, double lat, double lon);
   ~SelectTrackScreen();

   static void Push(bool isRemote, double lat, double lon);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

  private:
    Button *mSelectButton;
    Button *mCancelButton;

    Listbox *mTrackList;

    bool mIsRemote;

    void SelectTrackInternal(void);

 };

#endif // SELECTTRACK_H
