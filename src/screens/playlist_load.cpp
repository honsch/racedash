#include "playlist_load.h"


#include "display.h"

#include "util/util.h"

#include "ui/listbox.h"
#include "ui/button.h"

#include "screens/gettext.h"
#include "screens/musicplayer.h"

#include "audio/audio.h"

#include <stdint.h>
#include <string.h>
#include <unistd.h>


//#define WANT_SCAN_CONTROLS

//-----------------------------------------------------------------------------
PlaylistLoad::PlaylistLoad(MusicPlayer *owner) :
    mOwner(owner)
{
    mDeleteButton = new Button("Delete", PLL_Delete, NULL);
    mDeleteButton->SetPosition(0.46875F, 0.15F);
    mDeleteButton->SetSize(0.3F, 0.125F);
    AddElement(mDeleteButton);

    mOkButton = new Button("Ok", PLL_Ok, NULL);
    mOkButton->SetPosition(0.46875F, 0.650F);
    mOkButton->SetSize(0.3F, 0.125F);
    AddElement(mOkButton);

    mCancelButton = new Button("Cancel", PLL_Cancel, NULL);
    mCancelButton->SetPosition(0.46875F, 0.825F);
    mCancelButton->SetSize(0.3F, 0.125F);
    AddElement(mCancelButton);

    mFiles = new Listbox("Playlists", PLL_Files, NULL, false);
    mFiles->SetPosition(0.05F, 0.15F);
    mFiles->SetSize(0.4F, 0.8F);
    AddElement(mFiles);

    FillListboxFromDir(mFiles, PLAYLIST_DIR, FL_FileOnly, true);
}

//-----------------------------------------------------------------------------
PlaylistLoad::~PlaylistLoad()
{
    delete mDeleteButton;
    delete mOkButton;
    delete mCancelButton;
    delete mFiles;
}



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PlaylistLoad::Update(void)
{
    return ScreenOk;
}

//-----------------------------------------------------------------------------
void PlaylistLoad::Draw(DisplayController *display)
{
    display->DrawString("Choose Playlist", 0.05F, 0.01F, BLUE, Font_Large);
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PlaylistLoad::ProcessUIEvent(UIEvent *message)
{
    static uint32_t selectedIndex;
    char fname[_MAX_LFN];

    if (message->message == ListboxChanged)
    {
        if (message->lb.newState != 0) selectedIndex = message->lb.itemIndex;
    }

    if ((message->message == ButtonHolding) && (message->bn.id == PLL_Delete))
    {
        sprintf(fname, "%s/%s", PLAYLIST_DIR, mFiles->GetString(selectedIndex));
        unlink(fname);
        FillListboxFromDir(mFiles, PLAYLIST_DIR, FL_FileOnly, true);
        mOwner->Beep();
    }

    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case PLL_Cancel:
                mFiles->Clear();
                mOwner->Beep();
                return ScreenPop;
                break;

            case PLL_Ok:
            {
                mOwner->Beep();
                strcpy(fname, mFiles->GetString(selectedIndex));
                mFiles->Clear();
                mOwner->OpenPlaylist(fname, true);
                return ScreenPop;
                break;
            }
        }
    }

    return ScreenOk;
}

//-----------------------------------------------------------------------------
void PlaylistLoad::Push(MusicPlayer *owner)
{
    PlaylistLoad *pll = new PlaylistLoad(owner);
    UI::PushScreen(pll);
}


