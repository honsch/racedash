#include "screens/dashboardgauges.h"

//#include "sensors.h"
//#include "acc.h"

#include "util/util.h"

#include "screens/debug.h"
#include "screens/musicplayer.h"
#include "screens/selecttrack.h"

#include "screens/pit.h"

#include "gnss/nmeainterface.h"
#include "sensors/sensormanager.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

enum Control_IDs { DebugScreen,
                   MusicPlayerScreen,
                   StintTimer,
                   GaugeTach,
                   GaugeSpeed,
                   GaugeRPM,
                   GaugeWaterTemp,
                   GaugeOilTemp,
                   GaugeOilPressure,
                   GaugeBoost,
                   GaugeAFR,
                   GaugeBattery,
                   GaugeFuelLevel
                 };

static ScreenElement::VisualStyle sTachStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    MEDIUM_GREY,    // Border Color
    DARK_BLUE,      // Select Color
    64.0F,          // Font size in pixels
    Font_Mono,      // Font face
    4.0F,           // Border Size
    0               // Border Radius
};

static ScreenElement::VisualStyle sButtonStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    MEDIUM_GREY,    // Border Color
    DARK_BLUE,      // Select Color
    48.0F,          // Font size in pixels
    Font_Mono,      // Font face
    2.0F,           // Border Size
    0.008F          // Border Radius
};


static ScreenElement::VisualStyle sStintTimerButtonStyle =
{
    GREEN,          // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    64.0F,          // Font size in pixels
    Font_Mono,      // Font face
    1.0F,           // Border Size
    0.008F          // Border Radius
};


static ScreenElement::VisualStyle sUnitsStyle =
{
    CYAN,           // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    28.0F,          // Font size in pixels
    Font_Mono,      // Font face
    0,              // Border Size
    0               // Border Radius
};

static ScreenElement::VisualStyle sNumericLabelStyle =
{
    LIGHT_GREY,     // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    64.0F,          // Font size in pixels
    Font_Mono,      // Font face
    0,              // Border Size
    0               // Border Radius
};

static ScreenElement::VisualStyle sNumericStyle =
{
    GREEN,          // FG Color
    BLACK,          // BG Color
    MEDIUM_GREY,    // Border Color
    DARK_BLUE,      // Select Color
    64.0F,          // Font size in pixels
    Font_Mono,      // Font face
    2.0F,           // Border Size
    0.008F          // Border Radius
};

static ScreenElement::VisualStyle sNumericSpeedoStyle =
{
    GREEN,          // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    120.0F,         // Font size in pixels
    Font_Mono,      // Font face
    0.0F,           // Border Size
    0.008F          // Border Radius
};

static ScreenElement::VisualStyle sNumericTachStyle =
{
    GREEN,          // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    90.0F,          // Font size in pixels
    Font_Mono,      // Font face
    0.0F,           // Border Size
    0.008F          // Border Radius
};

static NumericGauge::Presentation sNumericPres =
{
    NumericGauge::Presentation::LP_Left,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    3,            // wholeDigits
    0,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericSpeedoPres =
{
    NumericGauge::Presentation::LP_None,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    3,            // wholeDigits
    0,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericTachPres =
{
    NumericGauge::Presentation::LP_None,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    3,            // wholeDigits
    0,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericPresTenths =
{
    NumericGauge::Presentation::LP_Left,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    4,            // wholeDigits
    1,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericPresHundreths =
{
    NumericGauge::Presentation::LP_Left,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    2,            // wholeDigits
    2,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

#if 0
static ScreenElement::VisualStyle sSpeedoStyle =
{
    BROWN,          // FG Color
    BLACK,          // BG Color
    LIGHT_GREY,     // Border Color
    DARK_BLUE,      // Select Color
    30.0F,          // Font size in pixels
    Font_Mono,      // Font face
    2.0F,           // Border Size
    0.008F          // Border Radius
};

static BarGauge::Presentation sSpeedoPres =
{
    false,           // showLabel
    true,            // showUnits
    false,           // showValue
    3,               // wholeDigits
    0,               // decimalDigits
    BarGauge::Right, // orientation
    5.0F,            // tickSpacing
    1,               // numMinorTicks
    0,               // numSubTicks;
    BarGauge::RightBottom, // tickOrientation

    &sSpeedoStyle,   // labelStyle
    &sUnitsStyle     // unitsStyle
};

static ScreenElement::VisualStyle sTachLabelStyle =
{
    WHITE,          // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    48.0F,          // Font size in pixels
    Font_Mono,      // Font face
    0,              // Border Size
    0               // Border Radius
};

static StripGauge::Presentation sTachPres =
{
    false, // showLabel
    false, // showUnits
    false, // showValue
    4,     // wholeDigits
    0,     // decimalDigits
    0,     // tickSpacing
    &sTachLabelStyle,  // labelStyle
    NULL   // unitsStyle
};
#endif

#define GREEN_12    0xFF002000
#define GREEN_25    0xFF004000
#define GREEN_37    0xFF006000
#define GREEN_50    0xFF008000
#define GREEN_62    0xFF00AC00
#define GREEN_75    0xFF00BF00
#define GREEN_87    0xFF00DF00
#define GREEN_100   0xFF00FF00

StripGauge::Presentation::CoordPair sTachOutline[15] =
{ //  x1      y1     x2      y2       value               color     Label lx       ly      scaled
    { 0.000F, 0.125F, 0.000F, 0.250F,    0.0F / 7000.0F,  GREEN_37, NULL, -0.025F, 0.130F, 0.0F }, //    0 rpm
    { 0.025F, 0.075F, 0.025F, 0.200F, 1000.0F / 7000.0F,  GREEN_37, "10", -0.025F, 0.130F, 0.0F }, // 1000
    { 0.050F, 0.025F, 0.050F, 0.150F, 1500.0F / 7000.0F,  GREEN_50, NULL, -0.025F, 0.130F, 0.0F }, // 1500
    { 0.100F, 0.0F,   0.100F, 0.125F, 2000.0F / 7000.0F,  GREEN_62, "20", -0.025F, 0.130F, 0.0F }, // 2000
    { 0.150F, 0.0F,   0.150F, 0.125F, 2500.0F / 7000.0F,  GREEN_75, "25", -0.025F, 0.130F, 0.0F }, // 2500
    { 0.250F, 0.0F,   0.250F, 0.125F, 3000.0F / 7000.0F,  GREEN_87, "30", -0.025F, 0.130F, 0.0F }, // 3000
    { 0.350F, 0.0F,   0.350F, 0.125F, 3500.0F / 7000.0F,  GREEN,    "35", -0.025F, 0.130F, 0.0F }, // 3500
    { 0.450F, 0.0F,   0.450F, 0.125F, 4000.0F / 7000.0F,  GREEN,    "40", -0.025F, 0.130F, 0.0F }, // 4000
    { 0.550F, 0.0F,   0.550F, 0.125F, 4500.0F / 7000.0F,  GREEN,    "45", -0.025F, 0.130F, 0.0F }, // 4500
    { 0.650F, 0.0F,   0.650F, 0.125F, 5000.0F / 7000.0F,  GREEN,    "50", -0.025F, 0.130F, 0.0F }, // 5000
    { 0.750F, 0.0F,   0.750F, 0.125F, 5500.0F / 7000.0F,  GREEN,    "55", -0.025F, 0.130F, 0.0F }, // 5500
    { 0.850F, 0.0F,   0.850F, 0.125F, 6000.0F / 7000.0F, YELLOW,    "60", -0.025F, 0.130F, 0.0F }, // 6000
    { 0.950F, 0.0F,   0.950F, 0.125F, 6500.0F / 7000.0F,    RED,    "65", -0.025F, 0.130F, 0.0F }, // 6500
    { 1.000F, 0.0F,   1.000F, 0.125F, 7000.0F / 7000.0F,    RED,    NULL, -0.025F, 0.130F, 0.0F }, // 7000
    { 1.000F, 0.0F,   1.000F, 0.125F, 7500.0F / 7000.0F,    RED,    NULL, -0.025F, 0.130F, 0.0F }  // 7500
};


#define EXTRA_X 0.01562F
#define EXTRA_Y 0.01F


//-----------------------------------------------------------------------------
DashboardGauges::DashboardGauges(void)
{
#if 0
    mMusicPlayerScreenButton = new Button("Music Player", MusicPlayerScreen, &sButtonStyle);
    mMusicPlayerScreenButton->SetSizeToContent(EXTRA_X, EXTRA_Y);
    mMusicPlayerScreenButton->SetPosition(0.75F, 0.8F);
    AddElement(mMusicPlayerScreenButton);

    mDebugScreenButton = new Button("Debug Screen", DebugScreen, &sButtonStyle);
    mDebugScreenButton->SetSizeToContent(EXTRA_X, EXTRA_Y);
    mDebugScreenButton->SetPosition(0.75F, 0.9F);
    AddElement(mDebugScreenButton);
#endif

    mStintTimerButton = new Button("0:00:00", StintTimer, &sStintTimerButtonStyle);
    mStintTimerButton->SetSizeToContent(EXTRA_X, EXTRA_Y);
    mStintTimerButton->SetPosition(0.825F, 0.0F);
    AddElement(mStintTimerButton);


    // Speedo, centered top
    mSpeed = new NumericGauge("speed", "km/h", GaugeSpeed, &sNumericSpeedoStyle, &sNumericSpeedoPres);
    mSpeed->SetPosition(0.45F, 0.00F);
    mSpeed->SetSize(0.20F, 0.30F);
    mSpeed->SetLabel("Speed");
    mSpeed->SetRange(0, 300);
    AddElement(mSpeed);

    // Right Top
    mWaterTemp = new NumericGauge("ECU CLT", "c", GaugeWaterTemp, &sNumericStyle, &sNumericPres);
    mWaterTemp->SetPosition(0.80F, 0.40F);
    mWaterTemp->SetSize(0.2F, 0.20F);
    mWaterTemp->SetLabel("WT");
    mWaterTemp->SetRange(-20, 150);
    AddElement(mWaterTemp);

    // Right middle
    mOilPressure = new NumericGauge("ECU OilPressure", "psi", GaugeOilPressure, &sNumericStyle, &sNumericPres);
    mOilPressure->SetPosition(0.80F, 0.60F);
    mOilPressure->SetSize(0.2F, 0.20F);
    mOilPressure->SetLabel("OP");
    mOilPressure->SetRange(0, 150);
    AddElement(mOilPressure);

    // Right bottom
    mFuelLevel = new NumericGauge("fuel level", "gal", GaugeFuelLevel, &sNumericStyle, &sNumericPresTenths);
    mFuelLevel->SetPosition(0.80F, 0.80F);
    mFuelLevel->SetSize(0.2F, 0.20F);
    mFuelLevel->SetLabel("FL");
    mFuelLevel->SetRange(0, 20);
    AddElement(mFuelLevel);

    // Left top
    mBattery = new NumericGauge("ECU VBat", "v", GaugeBattery, &sNumericStyle, &sNumericPresTenths);
    mBattery->SetPosition(0.02F, 0.40F);
    mBattery->SetSize(0.20F, 0.20F);
    mBattery->SetLabel("Bat");
    mBattery->SetRange(0, 16);
    AddElement(mBattery);

    // Left middle
    mBoost = new NumericGauge("ECU Boost", "psi", GaugeBoost, &sNumericStyle, &sNumericPresTenths);
    mBoost->SetPosition(0.02F, 0.60F);
    mBoost->SetSize(0.20F, 0.20F);
    mBoost->SetLabel("Map");
    mBoost->SetRange(-14.5, 30);
    AddElement(mBoost);

    // Left bottom
    mAFR = new NumericGauge("ECU O2", "", GaugeAFR, &sNumericStyle, &sNumericPresTenths);
    mAFR->SetPosition(0.02F, 0.80F);
    mAFR->SetSize(0.20F, 0.20F);
    mAFR->SetLabel("Afr");
    mAFR->SetRange(7, 22);
    AddElement(mAFR);

    // Tach
    mRPM = new NumericGauge("ECU RPM", "rpm", GaugeRPM, &sNumericTachStyle, &sNumericTachPres);
    mRPM->SetPosition(0.20F, 0.00F);
    mRPM->SetSize(0.20F, 0.15F);
    mRPM->SetLabel("RPM");
    mRPM->SetRange(0, 9999);
    AddElement(mRPM);
}

//-----------------------------------------------------------------------------
DashboardGauges::~DashboardGauges()
{
    delete mDebugScreenButton;
    delete mMusicPlayerScreenButton;
    delete mStintTimerButton;

    //delete mTach;
    delete mSpeed;
    delete mRPM;
    delete mWaterTemp;
    delete mOilPressure;
    delete mBoost;
    delete mAFR;
    delete mBattery;
    delete mFuelLevel;
}

//--------------------------------------------------------------------------
ScreenBase::ScreenResult DashboardGauges::Update(void)
{
    //NMEANavData initial, last;

    //mTach->UpdateValue();
    mRPM->UpdateValue();
    mSpeed->UpdateValue();
    mWaterTemp->UpdateValue();
    mOilPressure->UpdateValue();
    mBoost->UpdateValue();
    mAFR->UpdateValue();
    mBattery->UpdateValue();
    mFuelLevel->UpdateValue();

    SensorManager *sm = SensorManager::Get();
    if (sm->WarningActive(Warn_OilPressure)) mOilPressure->SetForegroundColor(RED);
    else                                     mOilPressure->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_CoolantTemperature)) mWaterTemp->SetForegroundColor(RED);
    else if (mWaterTemp->GetClampedValue() > 110.9) mWaterTemp->SetForegroundColor(YELLOW);
    else                                            mWaterTemp->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_AFR)) mAFR->SetForegroundColor(RED);
    else                             mAFR->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_BatteryVoltage)) mBattery->SetForegroundColor(RED);
    else                                        mBattery->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_Boost)) mBoost->SetForegroundColor(RED);
    else                               mBoost->SetForegroundColor(GREEN);


    // Stint
    char s[50], s2[49];
    double runtime;
    sm->GetNewestStateValue("runtime", runtime);
    TimeToString(s2, runtime, 0, true);
    sprintf(s, "%s", s2);

    uint32_t fg = GREEN;
    uint32_t bg = BLACK;

    if (sm->WarningActive(Warn_StintTime))
    {
        int v1 = (int) runtime;
        int v2 = (int) (runtime + 0.5);
        if (v1 != v2)
        {
            fg = RED;
            bg = BLACK;
        }
        else
        {
            fg = BLACK;
            bg = RED;
        }

    }
    else if (runtime > (110 * 60))
    {
        fg = BLACK;
        bg = YELLOW;
    }
    mStintTimerButton->SetName(s);
    mStintTimerButton->SetForegroundColor(fg);
    mStintTimerButton->SetBackgroundColor(bg);

    double val;
    // Got a pit request, show the screen
    sm->GetRemoteStateValue("PIT request", val);
    if (val > 0.9)
    {
        PitConfirmScreen::Push();
    }

    double rpm = mRPM->GetClampedValue();
    val = mOilPressure->GetClampedValue();
    if (rpm >= 1500.0)
    {
        double psiPerK = 1000.0 * val / rpm;
        if (psiPerK < 5.0)      mOilPressure->SetForegroundColor(RED);
        else if (psiPerK < 7.0) mOilPressure->SetForegroundColor(YELLOW);
        else                    mOilPressure->SetForegroundColor(GREEN);
    }
    else
    {
        if (val > 5.0) mOilPressure->SetForegroundColor(GREEN);
        else mOilPressure->SetForegroundColor(RED);
    }

    return ScreenOk;
}


//-----------------------------------------------------------------------------
void DashboardGauges::Draw(DisplayController *display)
{
    SensorManager *sm = SensorManager::Get();
    if (sm == NULL)
    {
        printf("SM IS NULL\n");
        return;
    }

    char s[128], s2[100];
    uint32_t color;

    // Current lap
    double curLapDuration, curLapDist, curLap, delta;
    sm->GetNewestStateValue("LAP currentLap", curLap);
    sm->GetNewestStateValue("LAP currentDuration", curLapDuration);
    sm->GetNewestStateValue("LAP currentDistance", curLapDist);
    sm->GetNewestStateValue("LAP currentDelta", delta);

    double lastDuration, bestDuration;
    sm->GetNewestStateValue("LAP lastDuration", lastDuration);
    sm->GetNewestStateValue("LAP bestDuration", bestDuration);

    color = GREY;
    if (delta <= -0.1) color = GREEN;
    if (delta >=  0.1) color = RED;
    sprintf(s, "%+5.1f", delta);
    display->DrawString(s, 0.4F, 0.33F, color, 96);


    TimeToString(s2, lastDuration, 2, false);
    sprintf(s, "L:%s", s2);
    color = (curLap > 0) ? YELLOW : GREY;
    display->DrawString(s, 0.4F, 0.60F, color, 72.0);

    TimeToString(s2, bestDuration, 2, false);
    sprintf(s, "B:%s", s2);
    color = (curLap > 0) ? GREEN : GREY;
    display->DrawString(s, 0.4F, 0.80F, color, 72.0);


    // Local
    sprintf(s, "%s", TimeString());
    display->DrawString(s, 0.01F, 0.00F, WHITE, 64.0);

    // Track
    if (gTrackManager->SelectedTrack() != NULL)
    {
        display->DrawString(gTrackManager->SelectedTrack()->Name(), 0.01F, 0.2F, GREY, 32.0);
    }
}


//-----------------------------------------------------------------------------
ScreenBase::ScreenResult DashboardGauges::ProcessUIEvent(UIEvent *message)
{

    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case MusicPlayerScreen:
                MusicPlayer::Show();
                break;

            case DebugScreen:
                DebugScreen::Push();
                break;
        }
    }

    if (message->message == ButtonDoubleClicked)
    {
        switch (message->bn.id)
        {
            // Reset sting timer on double click
            case StintTimer:
                SensorManager::Get()->ResetStintTime();
                break;
        }
    }

    if (message->message == DoubleClick)
    {
        // If you click on the lap info it pulls up the track select screen
        float x = message->click.x;
        float y = message->click.y;
        if ((x >= 0.30F) && (x < 0.70F) &&
            (y >= 0.30F) && (y < 1.0F))
        {
            SensorManager *sm = SensorManager::Get();

            double lat, lon;
            sm->GetNewestStateValue("latitude", lat);
            sm->GetNewestStateValue("longitude", lon);

            SelectTrackScreen::Push(false, lat, lon);
            return ScreenOk;
        }
    }


    return ScreenOk;
}

static DashboardGauges *sDashboardGauges = NULL;
//-----------------------------------------------------------------------------
void DashboardGauges::Show(void)
{
    if (sDashboardGauges == NULL) sDashboardGauges = new DashboardGauges;
    UI::SetScreen(sDashboardGauges, true);
}


