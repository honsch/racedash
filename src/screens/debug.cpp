#include "screens/debug.h"

#include "util/util.h"

#include "audio/audio.h"

#include "screens/gettext.h"

#include <stdio.h>

enum DS_Controls
{
    DS_Return,
    DS_Quit = 0xDEAD
};

//-----------------------------------------------------------------------------
DebugScreen::DebugScreen(void)
{
   mReturnButton = new Button("Return", DS_Return, NULL);
   mReturnButton->SetPosition(0.75F, 0.1F);
   mReturnButton->SetSize(0.25F, 0.0875F);
   AddElement(mReturnButton);

   mQuitButton = new Button("QUIT", DS_Quit, NULL);
   mQuitButton->SetPosition(0.01F, 0.1F);
   mQuitButton->SetSize(0.25F, 0.0875F);
   AddElement(mQuitButton);

   //mImage = gDisplay->LoadImage("images/FoxCrop.jpg");
}

//-----------------------------------------------------------------------------
DebugScreen::~DebugScreen()
{
    delete mReturnButton;
    delete mQuitButton;
}


//-----------------------------------------------------------------------------
ScreenBase::ScreenResult DebugScreen::Update(void)
{
    return ScreenOk;
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult DebugScreen::ProcessUIEvent(UIEvent *message)
{
    if (message->message == ButtonClicked)
    {
        if (message->bn.id == DS_Return) return ScreenPop;
        if (message->bn.id == DS_Quit)
        {
            Quit();
            return ScreenPop;
        }
    }

    return ScreenOk;
}


//-----------------------------------------------------------------------------
void DebugScreen::Draw(DisplayController *display)
{

    //display->DrawImage(mImage, 0, 0, 1.0F);

    char s[100];

    sprintf(s, "Time: %s", TimeString());
    display->DrawString(s, 0.45F, 0.01F, BLUE, Font_Regular);

    double now = SystemTimeUTC();
    double diff = (now - mPrevSysTime) * 1000.0;
    mPrevSysTime = now;

    sprintf(s, "TimeUTC: %6d  Frame Time: %5.2fms", (int) now, diff);
    display->DrawString(s, 0.45F, 0.05F, BLUE, Font_Regular);
}


//-----------------------------------------------------------------------------
void DebugScreen::Push(void)
{
    DebugScreen *ds = new DebugScreen;
    UI::PushScreen(ds);
}


