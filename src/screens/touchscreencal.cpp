#include "screens/touchscreencal.h"

#include "util/util.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "config/config.h"


TouchscreenCal::Pos TouchscreenCal::sCircles[5] = { {0.5F, 0.5F}, {0.1F, 0.1F}, {0.9F, 0.1F}, {0.1F, 0.9F}, {0.9F, 0.9F} };
//-----------------------------------------------------------------------------
TouchscreenCal::TouchscreenCal(void)
{
    // reset the scale & offset so I get raw values
    UI::GetTouchscreen()->GetCalibration(mOrigXOff, mOrigYOff, mOrigXScale, mOrigYScale);
    UI::GetTouchscreen()->SetCalibration(0.0F, 0.0F, 1.0F, 1.0F);
    mState = Start;
    mOriginalAngle = gDisplay->GetDisplayRotation();
    gDisplay->SetDisplayRotation(DisplayController::None);
}

//-----------------------------------------------------------------------------
TouchscreenCal::~TouchscreenCal()
{
    gDisplay->SetDisplayRotation(mOriginalAngle);
}


//-----------------------------------------------------------------------------
ScreenBase::ScreenResult TouchscreenCal::Update(void)
{
    return ScreenOk;
}

//-----------------------------------------------------------------------------
void TouchscreenCal::Draw(DisplayController *display)
{
    display->DrawString("Tap the circles", 0.3F, 0.3F, GREEN, Font_Regular);
    display->Circle(sCircles[mState].x, sCircles[mState].y, 0.015F, 1.0F, YELLOW);
}


//-----------------------------------------------------------------------------
ScreenBase::ScreenResult TouchscreenCal::ProcessUIEvent(UIEvent *message)
{
    if (message->message == Click)
    {
        mTaps[mState].x = message->click.x;
        mTaps[mState].y = message->click.y;
        printf("TS State %d is %f  %f\n", mState, mTaps[mState].x, mTaps[mState].y);

        switch (mState)
        {
            case Start:
                mState = TL;
                break;
            case  TL:
                mState = TR;
                break;
            case  TR:
                mState = BL;
                break;
            case  BL:
                mState = BR;
                break;
            case  BR:
                UpdateTSCal();
                return ScreenPop;
                break;
        }
    }
    return ScreenOk;
}

//-----------------------------------------------------------------------------
void TouchscreenCal::UpdateTSCal(void)
{
    float cDiffX = sCircles[TR].x - sCircles[TL].x;
    float cDiffY = sCircles[BL].y - sCircles[TL].y;

    float xDiffT = mTaps[TR].x - mTaps[TL].x;
    float xDiffB = mTaps[BR].x - mTaps[BL].x;
    float xDiff = (xDiffT + xDiffB) / 2.0F;

    float yDiffL = mTaps[BL].y - mTaps[TL].y;
    float yDiffR = mTaps[BR].y - mTaps[TR].y;
    float yDiff = (yDiffL + yDiffR) / 2.0F;

    float xScale = cDiffX / xDiff;
    float yScale = cDiffY / yDiff;

    float xOff = sCircles[TL].x - (((mTaps[TL].x + mTaps[BL].x) * xScale) / 2.0F);
    float yOff = sCircles[TL].y - (((mTaps[TL].y + mTaps[TR].y) * yScale) / 2.0F);


    // check the results
    bool ok = true;
    unsigned a;
    for (a = 0; a < 5; a++)
    {
        float x = (xScale * mTaps[a].x) + xOff;
        float y = (yScale * mTaps[a].y) + yOff;
        float errX = fabs(sCircles[a].x - x);
        float errY = fabs(sCircles[a].y - y);
        float rms = sqrtf(errX*errX + errY*errY);
        printf("TS Error %d: RMS:%f  XE:%f YE:%f    x:%f y:%f    RX:%f  RY:%f\n", a, rms, errX, errY, x, y, mTaps[a].x, mTaps[a].y);
        if (rms > 0.025F) ok = false;
    }
    if (ok)
    {
        UI::GetTouchscreen()->SetCalibration(xOff, yOff, xScale, yScale);
        printf("Touchscreen calibrated, Off %f %f    Scale %f %f\n", xOff, yOff, xScale, yScale);
        ConfigSet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_OFFSET_X, xOff);
        ConfigSet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_OFFSET_Y, yOff);
        ConfigSet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_SCALE_X, xScale);
        ConfigSet(TOUCHSCREEN_CONFIG_PATH, TOUCHSCREEN_CONFIG_SCALE_Y, yScale);
        ConfigSave();
    }
    else
    {
        UI::GetTouchscreen()->SetCalibration(mOrigXOff, mOrigYOff, mOrigXScale, mOrigYScale);
        printf("Touchscreen calibration failed\n");
    }
}

//-----------------------------------------------------------------------------
void TouchscreenCal::Push(void)
{
    TouchscreenCal *tsr = new TouchscreenCal();
    UI::PushScreen(tsr);
}

