#ifndef PITREQUEST_H
#define PITREQUEST_H

#include "display.h"

#include "ui/listbox.h"
#include "ui/button.h"
#include "ui/slider.h"
#include "ui/bargauge.h"
#include "ui/dialgauge.h"
#include "ui/numericgauge.h"
#include "ui/stripgauge.h"

//-------------------------------------------------------
class PitRequestScreen : public ScreenBase
{
  public:
    PitRequestScreen(void);
   ~PitRequestScreen();

   static void Push(void);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

  private:
    Button *mGoBackButton;

    Button *mPitNextButton;
    Button *mPitOneButton;
    Button *mPitTwoButton;
    Button *mPitThreeButton;

    Button *mSendButton;

    int mRequestLaps;
    bool mSendAdded;
    void DrawLiveData(DisplayController *display);

    void SendRequest(void);

 };

#endif // PITREQUEST_H
