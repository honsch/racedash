#ifndef TOUCHSCREENCAL_H
#define TOUCHSCREENCAL_H

#include "display.h"

#include "ui/listbox.h"
#include "ui/button.h"
#include "ui/slider.h"
#include "ui/bargauge.h"
#include "ui/dialgauge.h"
#include "ui/numericgauge.h"



//-------------------------------------------------------
class TouchscreenCal : public ScreenBase
{
  public:
    TouchscreenCal(void);
   ~TouchscreenCal();

   static void Push(void);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

  private:
    enum State { Start, TL, TR, BL, BR } mState;
    struct Pos
    {
        float x;
        float y;
    };
    Pos mTaps[5];
    static Pos sCircles[5];
    void UpdateTSCal(void);

    float mOrigXOff, mOrigYOff;
    float mOrigXScale, mOrigYScale;

    DisplayController::DisplayRotation mOriginalAngle;
};

#endif // TOUCHSCREENCAL_H
