#ifndef PITCONFIRM_H
#define PITCONFIRM_H

#include <stdint.h>

#include "ui/screen.h"

#include "ui/button.h"
#include "ui/listbox.h"
#include "ui/slider.h"

//-----------------------------------------------------------------------------
class PitConfirmScreen : public ScreenBase
{
  public:
    PitConfirmScreen(void);
    virtual ~PitConfirmScreen();

    virtual ScreenBase::ScreenResult Update(void);
    virtual void                     Draw(DisplayController *display);
    virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

    static void Push(void);

  private:
    double mPushTime;
    const int kMaxDisplayTime = 15;
};

#endif //PITCONFIRM_H
