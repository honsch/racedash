#include "gettext.h"

#include "ui/ui.h"
#include "display.h"

//#include "sys_globals.h"
#include <ctype.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>



//----------------------------------------------------------------------------
static const char sKeyboardLayout[2][4][12] = { { { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=' },
                                                  { 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']' },
                                                  { 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', 0  },
                                                  { 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/', '\\', 0  } },
                                                { { '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+' },
                                                  { 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}' },
                                                  { 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"',  0  },
                                                  { 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?', '|',  0  } } };


static const uint8_t sLayoutRowCount[4] = { 12, 12, 11, 11 };


// For Large font
#define CELL_WIDTH   0.046875F
#define CELL_HEIGHT  0.09F
#define CELL_GAP     0.01F  // On each side

#define ROW_TOP(x) (1.0F - (CELL_HEIGHT * (4 - (x))))
#define ROW_LEFT(x) (((0.917F - (13 * CELL_WIDTH)) / 2) + (CELL_WIDTH * (x)) / 2)


#define KB_BG   DARK_GREY

#define TITLE_Y 0.05F

#define TEXT_Y  0.125F


#define KEY_LEFT    1
#define KEY_RIGHT   2
#define KEY_BS      8
#define KEY_ENTER  13
#define KEY_ESC    27
#define KEY_SPACE  32



//----------------------------------------------------------------------------
GetTextScreen::GetTextScreen(bool push, const char *title, uint16_t id, const char *preload)
{
    mTitle = strdup(title);
    mID    = id;

    mShiftState = false;
    memset(mText, 0, sizeof(mText));
    if (preload != NULL) strncpy(mText, preload, MAX_TEXT_LEN);

    mCursorPos = strlen(mText);

    if (push) UI::PushScreen(this);
}


//----------------------------------------------------------------------------
GetTextScreen::~GetTextScreen()
{
    free(mTitle);
}

//----------------------------------------------------------------------------
void GetTextScreen::DrawKeyboard(DisplayController *display)
{
    display->RectFilled(0, ROW_TOP(0) + 0.005, 1.0F, (CELL_HEIGHT * 5.0F), KB_BG);

    char s[2];
    s[1] = 0;
    float top = ROW_TOP(0);
    float left = 0;
    unsigned a;
    for (a = 0; a < 4; a++)
    {
        left = ROW_LEFT(a);
        //LCD_H_line(left - (CELL_WIDTH / 2), top, 800, BLACK);
        display->Line(0, top, 1280, top, 1, MEDIUM_GREY);
        display->Line(left, top, left, top + CELL_HEIGHT, 1, MEDIUM_GREY);
        left += CELL_GAP;

        unsigned b;
        for (b = 0; b < sLayoutRowCount[a]; b++)
        {
            s[0] = sKeyboardLayout[mShiftState][a][b];
            display->DrawString(s, left, top + CELL_GAP, YELLOW, Font_Large);
            left += CELL_WIDTH;
            display->Line(left - CELL_GAP, top, left - CELL_GAP, top + CELL_HEIGHT, 1, MEDIUM_GREY);
        }
        top += CELL_HEIGHT;
    }

    display->DrawString("ESC",    0.01175F, ROW_TOP(0) + CELL_GAP, BLUE, Font_Large);
    display->DrawString(" <--",   0.01175F, ROW_TOP(2) + CELL_GAP, BLUE, Font_Large);
    display->DrawString(" Space", 0.01175F, ROW_TOP(3) + CELL_GAP, BLUE, Font_Large);

    display->DrawString("Backspace", 0.742F, ROW_TOP(0) + CELL_GAP, BLUE, Font_Large);
    display->DrawString("  Enter  ", 0.742F, ROW_TOP(1) + CELL_GAP, BLUE, Font_Large);
    display->DrawString("  -->",     0.742F, ROW_TOP(2) + CELL_GAP, BLUE, Font_Large);

    if (mShiftState == 0) display->DrawString("  Shift  ", 0.742F, ROW_TOP(3) + CELL_GAP, BLUE, Font_Large);
    else
    {
        display->RectFilled(0.742F, ROW_TOP(3), CELL_WIDTH, CELL_HEIGHT, GREY);
        display->DrawString("  Shift  ", 0.742F, ROW_TOP(3) + CELL_GAP, BLACK, Font_Large);
    }
}

//----------------------------------------------------------------------------
unsigned GetTextScreen::UpdateKeyboard(UIEvent *message)
{
    if (message->click.y < ROW_TOP(0)) return 0;
    unsigned row = (message->click.y - ROW_TOP(0)) / CELL_HEIGHT;

    //if (message->click.x < ROW_LEFT(row)) return 1;
    unsigned index = (message->click.x - ROW_LEFT(row)) / CELL_WIDTH;

    UIEvent km;
    km.message = Key;
    km.when    = message->when;
    km.kb.key  = 0;

    // Valid char click
    if (index < sLayoutRowCount[row])
    {
        km.kb.key = sKeyboardLayout[mShiftState][row][index];
        UI::QueueMessage(&km);
        return 1;
    }

    // Special clicks

    // backspace or ESC
    if (row == 0)
    {
        if (message->click.x < ROW_LEFT(0)) km.kb.key = KEY_ESC;
        else                                km.kb.key = KEY_BS;
        UI::QueueMessage(&km);
        return 1;
    }

    // Enter
    if (row == 1)
    {
        km.kb.key  = KEY_ENTER;
        UI::QueueMessage(&km);
        return 1;
    }

    // Cursor movement
    if (row == 2)
    {
        if (message->click.x < ROW_LEFT(0)) km.kb.key = KEY_LEFT;
        else                                km.kb.key = KEY_RIGHT;
        UI::QueueMessage(&km);
        return 1;
    }

    // Shift
    if (row == 3)
    {
        if (message->click.x < ROW_LEFT(0))
        {
            km.kb.key = KEY_SPACE;
            UI::QueueMessage(&km);
        }
        else
        {
            mShiftState ^= 0x01;
        }
        return 1;
    }

    return 0;
}

//------------------------------------------------------------------
ScreenBase::ScreenResult GetTextScreen::Update(void)
{
    return ScreenOk;
}

//------------------------------------------------------------------
void GetTextScreen::Draw(DisplayController *display)
{
    float textWidth, textHeight;
    display->GetFontSize(Font_Large, textWidth, textHeight);

    if (mTitle == NULL)
    {
        display->DrawString("Enter Text", 0.117, TITLE_Y, WHITE, Font_Large);
    }
    else
    {
        unsigned len = strlen(mTitle);
        float x = (1.0F - (len * textWidth)) / 2.0F;
        display->DrawString(mTitle, x, TITLE_Y, WHITE, Font_Large);
    }

    DrawKeyboard(display);

    unsigned len = strlen(mText);
    float x = (1.0F - (len * textWidth)) / 2.0F;
    display->DrawString(mText, x, TEXT_Y, YELLOW, Font_Large);

    uint32_t cursorColor = LIGHT_GREY;
    //if ((gSysTickCount % 1000) < 500) cursorColor = BLACK;
    display->RectFilled(x + textWidth * (float)mCursorPos, TEXT_Y + textHeight + 0.005F, textWidth, 0.0075F, cursorColor);
}

//------------------------------------------------------------------
ScreenBase::ScreenResult GetTextScreen::ProcessUIEvent(UIEvent *message)
{
    unsigned handled = UpdateKeyboard(message);
    if (handled) return ScreenOk;

    // We only care about  key messages here
    if (message->message != Key) return ScreenOk;

    unsigned len = strlen(mText);

    // CUrsor keys
    if ((message->kb.key == KEY_LEFT)  && (mCursorPos > 0))   --mCursorPos;
    if ((message->kb.key == KEY_RIGHT) && (mCursorPos < len)) ++mCursorPos;

    if ((message->kb.key == KEY_BS) && (len > 0) && (mCursorPos > 0))
    {
        mText[MAX_TEXT_LEN] = 0;
        --mCursorPos;
        memmove(mText + mCursorPos, mText + mCursorPos + 1, MAX_TEXT_LEN - mCursorPos);
    }

    if (message->kb.key == KEY_ENTER)
    {
        if (len > 0)
        {
            UIEvent e;
            e.message = GettextResult;
            e.when    = message->when;
            e.gt.id   = mID;
            e.gt.text = strdup(mText);
            UI::QueueMessage(&e);
        }
        return ScreenPop;
    }

    if (message->kb.key == KEY_ESC)
    {
        return ScreenPop;
    }

    if ((isspace((int)message->kb.key) || isgraph((int)message->kb.key)) && (len < MAX_TEXT_LEN))
    {
        if (mCursorPos < len) memmove(mText + mCursorPos + 1, mText + mCursorPos, MAX_TEXT_LEN - mCursorPos);
        mText[mCursorPos] = message->kb.key;
        ++mCursorPos;
    }

    return ScreenOk;
}

//----------------------------------------------------------------------------
void GetTextScreen::Push(const char *title, uint16_t id, const char *preload)
{
    GetTextScreen *gt = new GetTextScreen(true, title, id, preload);
    (void) gt;
}
