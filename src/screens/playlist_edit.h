#ifndef PLAYLIST_EDIT_H
#define PLAYLIST_EDIT_H

#include <stdint.h>

#include "ui/screen.h"
#include "ui/listbox.h"
#include "ui/button.h"

#include "audio/playlist.h"

class MusicPlayer;

//----------------------------------------------------------------------------
class PlaylistEdit : public ScreenBase
{
 public:

    PlaylistEdit(MusicPlayer *owner, Playlist *pl);
    virtual ~PlaylistEdit();

    virtual ScreenBase::ScreenResult Update(void);
    virtual void                     Draw(DisplayController *display);
    virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

    static void Push(MusicPlayer *owner, Playlist *pl);

 private:

   unsigned mSelectedGenreID;
   char     mSelectedGenre[28];
   char     mSelectedArtist[128];
   char     mSelectedAlbum[128];

   Playlist *mPlaylist;

   Button  *mOkButton;
   Button  *mAddButton;
   Button  *mRemoveButton;

   Button  *mSaveButton;
   Button  *mSaveAsButton;

   Button  *mClassicalButton;
   Button  *mChristmasButton;
   Button  *mModernButton;
   Button  *mOtherButton;

   Listbox *mArtist;
   Listbox *mAlbum;
   Listbox *mAlbumFiles;
   Listbox *mPlaylistFiles;

   MusicPlayer *mOwner;

   void UpdateGenre(void);
   void UpdateArtist(void);
   void FillPlaylistFiles(Playlist *pl);
   void AddAlbumfilesToPlaylist(Playlist *pl);
   void RemoveSelectedFiles(Playlist *pl);

};



#endif // PLAYLIST_EDIT_H

