#include "screens/remotedash.h"

//#include "sensors.h"
//#include "acc.h"

#include "util/util.h"

#include "screens/musicplayer.h"
#include "screens/pitrequest.h"
#include "screens/selecttrack.h"

#include "gnss/nmeainterface.h"
#include "sensors/sensormanager.h"
#include "track/track.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

enum Control_IDs { MusicPlayerScreen,
                   GaugeSpeedBar,
                   GaugeSpeedNumeric,
                   GaugeRPM,
                   GaugeWaterTemp,
                   GaugeOilPressure,
                   GaugeAFR,
                   GaugeBattery,
                   GaugeFuel,
                   PitRequestScreenButton,
                   PitConfirmButton
                 };

static ScreenElement::VisualStyle sButtonStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    MEDIUM_GREY,    // Border Color
    DARK_BLUE,      // Select Color
    48.0F,          // Font size in pixels
    Font_Mono,      // Font face
    2.0F,           // Border Size
    0.008F          // Border Radius
};


static ScreenElement::VisualStyle sUnitsStyle =
{
    CYAN,           // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    32.0F,          // Font size in pixels
    Font_Mono,      // Font face
    0,              // Border Size
    0               // Border Radius
};

static ScreenElement::VisualStyle sNumericLabelStyle =
{
    WHITE,          // FG Color
    BLACK,          // BG Color
    CLEAR,          // Border Color
    DARK_BLUE,      // Select Color
    28.0F,          // Font size in pixels
    Font_Mono,      // Font face
    0,              // Border Size
    0               // Border Radius
};

static ScreenElement::VisualStyle sNumericStyle =
{
    GREEN,          // FG Color
    BLACK,          // BG Color
    MEDIUM_GREY,    // Border Color
    DARK_BLUE,      // Select Color
    72.0F,          // Font size in pixels
    Font_Mono,      // Font face
    2.0F,           // Border Size
    0.008F          // Border Radius
};

static ScreenElement::VisualStyle sNumericTachSpeedoStyle =
{
    GREEN,          // FG Color
    BLACK,          // BG Color
    CLEAR,    // Border Color
    DARK_BLUE,      // Select Color
    128.0F,         // Font size in pixels
    Font_Mono,      // Font face
    0.0F,           // Border Size
    0.008F          // Border Radius
};

static NumericGauge::Presentation sNumericPres =
{
    NumericGauge::Presentation::LP_Top,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    3,            // wholeDigits
    0,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericTachSpeedoPres =
{
    NumericGauge::Presentation::LP_None,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    3,            // wholeDigits
    0,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericPresTenths =
{
    NumericGauge::Presentation::LP_Top,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    3,            // wholeDigits
    1,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static NumericGauge::Presentation sNumericPresHundreths =
{
    NumericGauge::Presentation::LP_Top,       // labelPos
    NumericGauge::Presentation::LJ_Center,    // labelJustify
    true,         // showUnits
    0,            // nameAlignLength
    2,            // wholeDigits
    2,            // decimalDigits
    &sNumericLabelStyle, // labelStyle
    &sUnitsStyle  // unitsStyle
};

static ScreenElement::VisualStyle sSpeedoStyle =
{
    BROWN,          // FG Color
    BLACK,          // BG Color
    LIGHT_GREY,     // Border Color
    DARK_BLUE,      // Select Color
    30.0F,          // Font size in pixels
    Font_Mono,      // Font face
    2.0F,           // Border Size
    0.008F          // Border Radius
};

static BarGauge::Presentation sSpeedoPres =
{
    false,           // showLabel
    true,            // showUnits
    false,           // showValue
    3,               // wholeDigits
    0,               // decimalDigits
    BarGauge::Right, // orientation
    5.0F,            // tickSpacing
    1,               // numMinorTicks
    0,               // numSubTicks;
    BarGauge::RightBottom, // tickOrientation
    true,            // Show tick value
    &sSpeedoStyle,   // labelStyle
    &sUnitsStyle     // unitsStyle
};


#define EXTRA_X 0.01562F
#define EXTRA_Y 0.01F


//-----------------------------------------------------------------------------
RemoteDashboard::RemoteDashboard(void)
{
    mPrevPitAck = PitAck_NoRequest;
// No Buttons for now
#if 0
    mMusicPlayerScreenButton = new Button("Music Player", MusicPlayerScreen, &sButtonStyle);
    mMusicPlayerScreenButton->SetSizeToContent(EXTRA_X, EXTRA_Y);
    mMusicPlayerScreenButton->SetPosition(0.75F, 0.8F);
    AddElement(mMusicPlayerScreenButton);
#endif

    mPitScreenButton = new Button("Send Pit Request", PitRequestScreenButton, &sButtonStyle);
    mPitScreenButton->SetSize(0.25F, 0.10F);
    mPitScreenButton->SetPosition(0.75F, 0.90F);
    AddElement(mPitScreenButton);

    mPitConfirmButton = new Button("Pit Confirm", PitConfirmButton, &sButtonStyle);
    mPitConfirmButton->SetSize(0.25F, 0.1F);
    mPitConfirmButton->SetPosition(0.5F, 0.9F);
    mPitConfirmButtonAdded = false;

    mSpeedo = new BarGauge("speed", "km/h", GaugeSpeedBar, &sSpeedoStyle, &sSpeedoPres);
    mSpeedo->SetPosition(0.0F, 0.0F);
    mSpeedo->SetSize(1.0F, 0.100F);
    mSpeedo->SetLabel("km/h");
    mSpeedo->SetRange(0, 200);
    AddElement(mSpeedo);

    // Row 1
    mRPM = new NumericGauge("ECU RPM", "rpm", GaugeRPM, &sNumericTachSpeedoStyle, &sNumericTachSpeedoPres);
    mRPM->SetPosition(0.65F, 0.2F);
    mRPM->SetSize(0.15F, 0.10F);
    mRPM->SetLabel("RPM");
    mRPM->SetRange(0, 9999);
    AddElement(mRPM);

    mSpeedoNumeric = new NumericGauge("speed", "km/h", GaugeSpeedNumeric, &sNumericStyle, &sNumericPres);
    mSpeedoNumeric->SetPosition(0.85F, 0.20F);
    mSpeedoNumeric->SetSize(0.15F, 0.10F);
    mSpeedoNumeric->SetLabel("Speed");
    mSpeedoNumeric->SetRange(0, 200);
    AddElement(mSpeedoNumeric);

    mWaterTemp = new NumericGauge("ECU CLT", "c", GaugeWaterTemp, &sNumericStyle, &sNumericPres);
    mWaterTemp->SetPosition(0.85F, 0.30F);
    mWaterTemp->SetSize(0.15F, 0.10F);
    mWaterTemp->SetLabel("Water Temp");
    mWaterTemp->SetRange(-20, 150);
    AddElement(mWaterTemp);

    mOilPressure = new NumericGauge("ECU OilPressure", "psi", GaugeOilPressure, &sNumericStyle, &sNumericPres);
    mOilPressure->SetPosition(0.85F, 0.40F);
    mOilPressure->SetSize(0.15F, 0.10F);
    mOilPressure->SetLabel("Oil Pres");
    mOilPressure->SetRange(0, 150);
    AddElement(mOilPressure);


    mBattery = new NumericGauge("ECU VBat", "v", GaugeBattery, &sNumericStyle, &sNumericPresTenths);
    mBattery->SetPosition(0.85F, 0.50F);
    mBattery->SetSize(0.15F, 0.10F);
    mBattery->SetLabel("Battery");
    mBattery->SetRange(0, 16);
    AddElement(mBattery);

    mAFR = new NumericGauge("ECU O2", "", GaugeAFR, &sNumericStyle, &sNumericPresTenths);
    mAFR->SetPosition(0.85F, 0.60F);
    mAFR->SetSize(0.15F, 0.10F);
    mAFR->SetLabel("AFR");
    mAFR->SetRange(7, 22);
    AddElement(mAFR);

    mFuelGauge = new NumericGauge("fuel level", "gal", GaugeFuel, &sNumericStyle, &sNumericPresHundreths);
    mFuelGauge->SetPosition(0.85F, 0.70F);
    mFuelGauge->SetSize(0.15F, 0.10F);
    mFuelGauge->SetLabel("Fuel Level");
    mFuelGauge->SetRange(0, 20);
    AddElement(mFuelGauge);
}

//-----------------------------------------------------------------------------
RemoteDashboard::~RemoteDashboard()
{
    delete mPitScreenButton;
    delete mPitConfirmButton;
    delete mMusicPlayerScreenButton;
    //delete mTach;
    delete mSpeedo;
    delete mSpeedoNumeric;
    delete mRPM;
    delete mWaterTemp;
    delete mOilPressure;
    delete mAFR;
    delete mBattery;
    delete mFuelGauge;
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult RemoteDashboard::Update(void)
{
    //NMEANavData initial, last;

    //mTach->UpdateValue();
    mRPM->UpdateValue();
    mSpeedo->UpdateValue();
    mSpeedoNumeric->UpdateValue();
    mWaterTemp->UpdateValue();
    mOilPressure->UpdateValue();
    mAFR->UpdateValue();
    mBattery->UpdateValue();
    mFuelGauge->UpdateValue();

    SensorManager *sm = SensorManager::Get();
    if (sm->WarningActive(Warn_OilPressure)) mOilPressure->SetForegroundColor(RED);
    else                                     mOilPressure->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_CoolantTemperature)) mWaterTemp->SetForegroundColor(RED);
    else if (mWaterTemp->GetClampedValue() > 110.9) mWaterTemp->SetForegroundColor(YELLOW);
    else                                            mWaterTemp->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_AFR)) mAFR->SetForegroundColor(RED);
    else                             mAFR->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_BatteryVoltage)) mBattery->SetForegroundColor(RED);
    else                                        mBattery->SetForegroundColor(GREEN);

    if (sm->WarningActive(Warn_FuelRemaining))     mFuelGauge->SetForegroundColor(RED);
    else if (mFuelGauge->GetClampedValue() <= 2.0) mFuelGauge->SetForegroundColor(YELLOW);
    else                                           mFuelGauge->SetForegroundColor(GREEN);

    double pitAckRaw;
    sm->GetRemoteStateValue("PIT ack", pitAckRaw);
    PitAcknowledge pitAck = (PitAcknowledge) pitAckRaw;

    if (mPrevPitAck != pitAck)
    {
        mPrevPitAck = pitAck;
        switch (pitAck)
        {
            // This is an error state if the button is visible
            case PitAck_NoRequest:
                mPitConfirmButton->SetName("Request None");
                break;

            case PitAck_Pending:
                mPitConfirmButton->SetName("Request Pending");
                break;

            case PitAck_OK:
                mPitConfirmButton->SetName("Request ACK");
                break;

            case PitAck_Timeout:
                mPitConfirmButton->SetName("Request Timeout");
                break;
        }

        if (!mPitConfirmButtonAdded && (pitAck != PitAck_NoRequest))
        {
            AddElement(mPitConfirmButton);
            mPitConfirmButtonAdded = true;
        }
    }

    return ScreenOk;
}


//-----------------------------------------------------------------------------
void RemoteDashboard::Draw(DisplayController *display)
{
    SensorManager *sm = SensorManager::Get();
    if (sm == NULL)
    {
        printf("SM IS NULL\n");
        return;
    }

    char s[80], s2[60];
    uint32_t color;

    //double fuelLapsRemaining;
    //sm->GetNewestStateValue("LAP fuelLapsRemaining", fuelLapsRemaining);

    // Current lap
    double lastDuration, lastLap;
    sm->GetNewestStateValue("LAP lastLap", lastLap);
    sm->GetNewestStateValue("LAP lastDuration", lastDuration);

    if (lastLap > 0)
    {
        sprintf(s, "Lap: %2d", (unsigned) lastLap);
        display->DrawString(s, 0.01F, 0.14F, CYAN, Font_Large);

        //color = CYAN;
        //if (fuelLapsRemaining < 4) color = RED;
        //else if (fuelLapsRemaining < 6) color = YELLOW;
        //sprintf(s, "FuelRemain: %.1f", fuelLapsRemaining);
        //display->DrawString(s, 0.175F, 0.14F, color, Font_Large);
    }
    else
    {
        sprintf(s, "Lap: Out");
        display->DrawString("Lap: Out", 0.01F, 0.14F, CYAN, Font_Large);
    }

    TimeToString(s, lastDuration, 2, false);
    display->DrawString(s, 0.01F, 0.20F, YELLOW, 85.0);

    // Stint
    double runtime;
    sm->GetNewestStateValue("runtime", runtime);
    TimeToString(s2, runtime, 0, true);
    sprintf(s, "Stint:%s", s2);
    color = GREEN;
    if (sm->WarningActive(Warn_StintTime)) color = RED;
    else if (runtime > (110 * 60))         color = YELLOW;
    display->DrawString(s, 0.76F, 0.80F, color, 64.0);

    double rssi, snr;
    sm->GetNewestStateValue("LORA rssi", rssi);
    sm->GetNewestStateValue("LORA snr", snr);

    sprintf(s, "LoRa rssi:%ddb  snr:%.1fdb", (int)rssi, snr);
    display->DrawString(s, 0.78F, 0.145F, CYAN, 32.0);

    if (gTrackManager->SelectedTrack() != NULL)
    {
        // Track name
        display->DrawString(gTrackManager->SelectedTrack()->Name(), 0.20, 0.14, YELLOW, Font_Large);

        gTrackManager->DrawSelectedTrack(display, 0.005F, 0.30F, 0.70F, 0.70F);

        double lat, lon;
        sm->GetNewestStateValue("latitude", lat);
        sm->GetNewestStateValue("longitude", lon);

        gTrackManager->DrawTrackMarker(display, 0.005F, 0.30F, 0.70F, 0.70F, lat, lon, MAGENTA);
    }
}


//-----------------------------------------------------------------------------
ScreenBase::ScreenResult RemoteDashboard::ProcessUIEvent(UIEvent *message)
{
    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case PitRequestScreenButton:
                PitRequestScreen::Push();
                break;

            case PitConfirmButton:
                RemoveElement(mPitConfirmButton);
                mPitConfirmButtonAdded = false;
                //SensorManager::Get()->ClearPitAcknowledge();
                break;

            case MusicPlayerScreen:
                MusicPlayer::Show();
                break;
        }
    }
    else if (message->message == DoubleClick)
    {
        if ((message->click.x < 0.7) && (message->click.y > 0.3))
        {
            SensorManager *sm = SensorManager::Get();
            double lat, lon;
            sm->GetNewestStateValue("latitude", lat);
            sm->GetNewestStateValue("longitude", lon);
            SelectTrackScreen::Push(true, lat, lon);
            return ScreenOk;
        }
    }

    return ScreenOk;
}

static RemoteDashboard *sRemoteDashboard = NULL;
//-----------------------------------------------------------------------------
void RemoteDashboard::Show(void)
{
    if (sRemoteDashboard == NULL) sRemoteDashboard = new RemoteDashboard;
    UI::SetScreen(sRemoteDashboard, true);
}


