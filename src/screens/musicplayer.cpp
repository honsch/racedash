#include "screens/musicplayer.h"

#include "display.h"

#include "audio/audio.h"

#include "util/util.h"

#include "ui/listbox.h"
#include "ui/button.h"
#include "ui/slider.h"

#include "screens/debug.h"
#include "screens/dashboardgauges.h"
#include "screens/gettext.h"
#include "screens/playlist_edit.h"
#include "screens/playlist_load.h"

#include "audio/playlist.h"

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


enum Control_IDs {  Nav_SkipPrev,
                    Nav_Stop,
                    Nav_PlayPause,
                    Nav_SkipNext,
                    VolumeSlider,
                    PlaylistLoad,
                    PlaylistEdit,
                    PlayOrder,
                    DashboardScreen,
                    DebugScreen
                 };

static ScreenElement::VisualStyle sNavStyle = { CYAN,           // FG Color
                                                BLACK,          // BG Color
                                                CLEAR,          // Border Color
                                                DARK_BLUE,      // Select Color
                                                150.0F,         // BIG buttons!
                                                Font_PlayerSymbols,
                                                2,              // Border Size
                                                10              // Border Radius
                                              };


static const char *sPlayOrderStrings[3] = { "Playlist       ",
                                            "Random by Album",
                                            "Random by Song "
                                          };


enum MPGlyphs {
                MPS_Stop            = 'a',
                MPS_Play            = 'C',
                MPS_Pause           = 'c',
                MPS_SkipPrev        = 'U',
                MPS_SkipNext        = 'T',
                MPS_Mute            = 'R',
                MPS_StopActive      = 'v',
                MPS_PlayActive      = 'u',
                MPS_PauseActive     = 'x',
                MPS_SkipPrevActive  = 'z',
                MPS_SkipNextActive  = '{',
                MPS_MuteActive      = 'y'
             };

static const char *NextFileCallback(bool skipToNext, void *context);


//-----------------------------------------------------------------------------
MusicPlayer::MusicPlayer(void)
{
    mPlayer = new VorbisPlayer;

    mPlaylist = new Playlist;

    mAutoStart = true;
    mPlayOrder = Playlist::Sequential;

    mDebugScreenButton  = new Button("Debug ScreenBase", DebugScreen, NULL);
    mDashboardScreenButton = new Button("Dashboard",        DashboardScreen, NULL);
    mPlaylistLoadButton = new Button("Load Playlist",    PlaylistLoad, NULL); // Edit &  load are the longest names
    mPlaylistEditButton = new Button("Edit Playlist",    PlaylistEdit, NULL);

    // Set all the buttons to the largest size
    mPlaylistLoadButton->SetSizeToContent(.036F, 0.0575F);
    mDebugScreenButton->SetSize(mPlaylistLoadButton->Width(), mPlaylistLoadButton->Height());
    mDashboardScreenButton->SetSize(mPlaylistLoadButton->Width(), mPlaylistLoadButton->Height());
    mPlaylistEditButton->SetSize(mPlaylistLoadButton->Width(), mPlaylistLoadButton->Height());

    float xPos = 1.0F - mPlaylistLoadButton->Width();
    mPlaylistLoadButton->SetPosition(xPos, 0.34375F);
    mPlaylistEditButton->SetPosition(xPos, 0.5F);
    mDashboardScreenButton->SetPosition(xPos, 0.74375F);
    mDebugScreenButton->SetPosition(xPos, 0.9F);

    mPlayOrderButton = new Button("Play Order",    PlayOrder, NULL);
    mPlayOrderButton->SetSizeToContent(0.004F, 0.008F);
    mPlayOrderButton->SetPosition(0.09375F, 0.7125F);

    AddElement(mDebugScreenButton);
    AddElement(mDashboardScreenButton);
    AddElement(mPlaylistLoadButton);
    AddElement(mPlaylistEditButton);
    AddElement(mPlayOrderButton);


    mVolumeSlider = new Slider("Volume", VolumeSlider, NULL, Slider::Vertical);
    mVolumeSlider->SetPosition(0.01F, 0.01F);
    mVolumeSlider->SetSize(0.047F, 0.9F);
    mVolumeSlider->SetRange(mPlayer->GetMaxVolumeDB(), mPlayer->GetMinVolumeDB());
    mVolumeSlider->SetStepSize(2.0F);
    mVolumeSlider->SetValue(-35.0F);
    AddElement(mVolumeSlider);
    mPlayer->SetMasterVolumeDB(-35.0F);

    char n[2];
    n[1] = 0;

    n[0] = MPS_Play;        mNavButtons[0] = new Button(n, Nav_PlayPause, &sNavStyle);
    n[0] = MPS_SkipPrev;    mNavButtons[1] = new Button(n, Nav_SkipPrev,  &sNavStyle);
    n[0] = MPS_Stop;        mNavButtons[2] = new Button(n, Nav_Stop,      &sNavStyle);
    n[0] = MPS_SkipNext;    mNavButtons[3] = new Button(n, Nav_SkipNext,  &sNavStyle);
    float x = 0.09375;
    unsigned a;
    for (a = 0; a < 4; a++)
    {
        mNavButtons[a]->SetSizeToContent(0, 0);
        float y = 1.0F - mNavButtons[a]->Height();

        mNavButtons[a]->SetPosition(x, y);
        x += mNavButtons[a]->Width();

        // Set the play pause away from the rest by an extra button width
        if (a == 0) x += mNavButtons[a]->Width() / 2.0F;

        AddElement(mNavButtons[a]);
    }

    // Now that the GUI fiddling is done, setup the playlist and start everything going
    mPlayer->SetFilenameCallback(&NextFileCallback, this);
}

//--------------------------------------------------------------------
MusicPlayer::~MusicPlayer()
{
    mPlayer->Stop();

    delete mPlaylist;
    delete mVolumeSlider;
    delete mNavButtons[0];
    delete mNavButtons[1];
    delete mNavButtons[2];
    delete mNavButtons[3];

    delete mDebugScreenButton;
    delete mDashboardScreenButton;
    delete mPlaylistLoadButton;
    delete mPlaylistEditButton;
    delete mPlayOrderButton;

    delete mPlayer;
}

//--------------------------------------------------------------------
static const char *NextFileCallback(bool skipToNext, void *context)
{
    if (context == NULL) return NULL;
    MusicPlayer *mp = (MusicPlayer *)context;
    return mp->NextFile(skipToNext);
}

//--------------------------------------------------------------------
const char *MusicPlayer::NextFile(bool skipToNext)
{
    if (skipToNext) mPlaylist->Next();
    return mPlaylist->CurrentFilename();
}

//-----------------------------------------------------------------------------
void MusicPlayer::SetPlaylist(Playlist *pl, bool match)
{
    if (pl == NULL) return;

    char curfname[_MAX_LFN];
    if (match != 0) strcpy(curfname, mPlaylist->CurrentFilename());
    else            curfname[0] = 0;

    Playlist *old = mPlaylist;

    pl->SetOrder(mPlayOrder);
    pl->Reset();

    pl->SetCurrent(curfname);
    mPlaylist = pl;

    delete old;
}

//-----------------------------------------------------------------------------
void MusicPlayer::OpenPlaylist(const char *fname, bool match)
{
    if (fname == NULL) return;
    char curfname[_MAX_LFN];
    if (match != 0) strcpy(curfname, mPlaylist->CurrentFilename());
    else            curfname[0] = 0;

    mPlayer->Stop();
    delete mPlaylist;
    mPlaylist = new Playlist(fname, curfname);
    if (mPlaylist == NULL) return;

    mPlaylist->SetOrder(mPlayOrder);
    mPlayer->Play();
}

//-----------------------------------------------------------------------------
void MusicPlayer::Beep(void)
{
    mPlayer->Beep();
}

//-----------------------------------------------------------------------------
void MusicPlayer::UpdateAudioInternal(void)
{
    mPlayer->Update();
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ScreenBase::ScreenResult MusicPlayer::Update(void)
{
    return ScreenOk;
}

//-----------------------------------------------------------------------------
void MusicPlayer::Draw(DisplayController *display)
{
    char s[200];

    float fontSize = Font_Large;
    float textWidth, textHeight;

#if 0
    sprintf(s, "Decoder Error %d   %d", mPlayer->DecoderError(), mPlayer->DecoderResetCount());
    display->DrawString(s, 1000, 80, BLUE, Small);
    sprintf(s, "Val %d", mPlayer->Value());
    display->DrawString(s, 1000, 110, BLUE, Small);
    sprintf(s, "Player Buffer %d", mPlayer->FileBufferState());
    display->DrawString(s, 1000, 140, BLUE, Small);

    sprintf(s, "Decode Late: %d", mPlayer->DecodeLate());
    display->DrawString(s, 1000, 170, BLUE, Small);

    sprintf(s, "Decoded: %d   ", mPlayer->NumDecodedBuffers());
    display->DrawString(s, 1000, 200, BLUE, Small);
#endif

    // Set up the correct playing state buttons
    char n[2] = { 0, 0 };
    switch (mPlayer->GetPlayingState())
    {
        case VorbisPlayer::Vorbis_Stopped:
            n[0] = MPS_StopActive;  mNavButtons[2]->SetName(n);
            n[0] = MPS_Play;        mNavButtons[0]->SetName(n);
            break;

        case VorbisPlayer::Vorbis_Playing:
            n[0] = MPS_Stop;        mNavButtons[2]->SetName(n);
            n[0] = MPS_Pause;       mNavButtons[0]->SetName(n);
            break;

        case VorbisPlayer::Vorbis_Paused:
            n[0] = MPS_Stop;        mNavButtons[2]->SetName(n);
            n[0] = MPS_Play;        mNavButtons[0]->SetName(n);
            break;
    }

    display->DrawString(sPlayOrderStrings[(int) mPlayOrder],
                        0.01F + mPlayOrderButton->X() + mPlayOrderButton->Width(),
                        0.0125F + mPlayOrderButton->Y(), GREEN, fontSize);

    sprintf(s, "%+4.1f  ", mVolumeSlider->GetValue());
    display->DrawString(s, 0.01F, 0.91F, BLUE, fontSize);


    if (mPlayer->GetPlayingState() == VorbisPlayer::Vorbis_Stopped)
    {
        display->DrawString("Vorbis Player Stopped", 0.078125F, 0.025F, BLUE, fontSize);
    }
    else
    {
        sprintf(s, "%3d of %d", mPlaylist->CurrentIndex() + 1, mPlaylist->ItemCount());
        //unsigned sw = textWidth * 17; // Length of "Whats spinning"
        display->GetStringExtents(fontSize, Font_LiberationSans, "What's Spinning:", textWidth, textHeight);

        display->DrawString("What's Spinning:", 0.078125F,               0.0125F, BLUE,        fontSize, Font_LiberationSans);
        display->DrawString(s,                  0.078125F + textWidth,   0.0125F, MEDIUM_GREY, fontSize, Font_LiberationSans);
        display->DrawString(mPlayer->CurrentTitle(),  0.09F,             0.0875F, YELLOW,      fontSize, Font_Bookman);

        sprintf(s, "%s - %s", mPlayer->CurrentArtist(), mPlayer->CurrentAlbum());
        display->DrawString(s, 0.09F,        0.15F, MEDIUM_CYAN, fontSize, Font_Bookman);

        // Percent is a float i.e. 0-1.0F
        float progressWidth = 0.625F;
        float percent = mPlayer->Progress();
        float played = progressWidth * percent;
        float remain = progressWidth - (played + 0.00078125F);
        unsigned aa = 0x22 + (unsigned) (221.0F * ((progressWidth * percent) - played));
        aa |= (aa << 8) | (aa << 16);

        display->RectFilled(0.078125F,              0.225F, played,      0.0075F, WHITE);
        display->RectFilled(0.078125F + played,     0.225F, remain,      0.0075F, MEDIUM_GREY);
        display->RectFilled(0.078125F + played,     0.225F, 0.00078125F, 0.0075F, aa);
        display->RectFilled(0.078125F + played + 0.00078125F, 0.225F, remain, 0.0075F, MEDIUM_GREY);

        // draw times under progress bar
        int seconds = (int)mPlayer->ProgressSeconds();
        int minutes = seconds / 60;
        seconds = seconds % 60;
        sprintf(s, "%01d:%02d", minutes, seconds);
        display->DrawString(s, 0.078125F, 0.2375F, MEDIUM_CYAN, fontSize, Font_LiberationSans);

        seconds = (int)mPlayer->LengthSeconds();
        minutes = seconds / 60;
        seconds = seconds % 60;
        sprintf(s, "%01d:%02d", minutes, seconds);
        display->GetStringExtents(fontSize, Font_LiberationSans, s, textWidth, textHeight);
        display->DrawString(s, 0.70F - textWidth, 0.2375F, MEDIUM_CYAN, fontSize, Font_LiberationSans);

        mPlaylist->UpdateTags();

        if (mPlaylist->NextTitle()[0] != 0)
        {
            sprintf(s, "%s - %s", mPlaylist->NextArtist(), mPlaylist->NextAlbum());

            display->DrawString("Next Up:",              0.078125F, 0.30F, BLUE,        Font_Regular, Font_LiberationSans);
            display->DrawString(mPlaylist->NextTitle(),  0.09F,     0.35F, YELLOW,      Font_Regular, Font_Bookman);
            display->DrawString(s,                       0.09F,     0.40F, MEDIUM_CYAN, Font_Regular, Font_Bookman);
        }
    }
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult MusicPlayer::ProcessUIEvent(UIEvent *message)
{
    VorbisPlayer::PlayingState playingState = mPlayer->GetPlayingState();

    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case DebugScreen:
                DebugScreen::Push();
                Beep();
                break;

            case DashboardScreen:
                DashboardGauges::Show();
                Beep();
                break;

            case PlaylistLoad:
                PlaylistLoad::Push(this);
                Beep();
                break;

            case PlaylistEdit:
                PlaylistEdit::Push(this, mPlaylist);
                Beep();
                break;

            case PlayOrder:
                if      (mPlayOrder == Playlist::Sequential)  mPlayOrder = Playlist::RandomAlbum;
                else if (mPlayOrder == Playlist::RandomAlbum) mPlayOrder = Playlist::RandomSong;
                else if (mPlayOrder == Playlist::RandomSong)  mPlayOrder = Playlist::Sequential;
                mPlaylist->SetOrder(mPlayOrder);
                Beep();
                break;

            case Nav_PlayPause:
                if (playingState == VorbisPlayer::Vorbis_Playing) mPlayer->Pause();
                else                                              mPlayer->Play();
                Beep();
                break;

            case Nav_Stop:
                Beep();
                mPlayer->Stop();
                mAutoStart = 0;
                break;

            case Nav_SkipPrev:
                // Skip to the prev song only if we are within the first 1% of the song
                if (mPlayer->Progress() < 1) mPlaylist->Prev();
                mPlayer->StartFile(false, true);
                Beep();
                break;

            case Nav_SkipNext:
                mPlaylist->Next();
                mPlayer->StartFile(false, true);
                Beep();
                break;

        }
    }

    if (message->message == SliderChanged)
    {
        if (message->sl.id == VolumeSlider) mPlayer->SetMasterVolumeDB(message->sl.newValue);
    }

    return ScreenOk;
}


MusicPlayer *MusicPlayer::sMusicPlayerScreen = NULL;
//-----------------------------------------------------------------------------
void MusicPlayer::Show(void)
{
    if (sMusicPlayerScreen == NULL) sMusicPlayerScreen = new MusicPlayer;
    UI::SetScreen(sMusicPlayerScreen, true);
}

//-----------------------------------------------------------------------------
void MusicPlayer::UpdateAudio(void)
{
    if (sMusicPlayerScreen == NULL) return;
    sMusicPlayerScreen->UpdateAudioInternal();
}

