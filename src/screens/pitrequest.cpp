#include "screens/pitrequest.h"

#include "sensors/sensormanager.h"
#include "telemetry/telemetry.h"

#include <stdint.h>

enum Control_IDs { PitNextLap   = 0,
                   PitOneLap    = 1,
                   PitTwoLaps   = 2,
                   PitThreeLaps = 3,
                   Send,
                   GoBack
                 };

static ScreenElement::VisualStyle sButtonStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    GREY,           // Border Color
    MEDIUM_BLUE,    // Select Color
    75.0F,          // Font size in pixels
    Font_Mono,      // Font face
    3.0F,           // Border Size
    0.008F          // Border Radius
};



//-----------------------------------------------------------------------------
PitRequestScreen::PitRequestScreen(void)
{
    mGoBackButton = new Button("Go Back", GoBack, &sButtonStyle);
    mGoBackButton->SetSize(0.4F, 0.175F);
    mGoBackButton->SetPosition(0.55F, 0.725F);
    AddElement(mGoBackButton);

    mSendButton = new Button("Send Request", Send, &sButtonStyle);
    mSendButton->SetSize(0.4F, 0.25F);
    mSendButton->SetPosition(0.55F, 0.175F);
    mSendAdded = false;

    mPitNextButton = new Button("Pit Next", PitNextLap, &sButtonStyle);
    mPitNextButton->SetSize(0.45F, 0.175F);
    mPitNextButton->SetPosition(0.025F, 0.125F);
    mPitNextButton->SetStyle(Button::Toggle);
    AddElement(mPitNextButton);

    mPitOneButton = new Button("Pit In One", PitOneLap, &sButtonStyle);
    mPitOneButton->SetSize(0.45F, 0.175F);
    mPitOneButton->SetPosition(0.025F, 0.325F);
    mPitOneButton->SetStyle(Button::Toggle);
    AddElement(mPitOneButton);

    mPitTwoButton = new Button("Pit In Two", PitTwoLaps, &sButtonStyle);
    mPitTwoButton->SetSize(0.45F, 0.175F);
    mPitTwoButton->SetPosition(0.025F, 0.525F);
    mPitTwoButton->SetStyle(Button::Toggle);
    AddElement(mPitTwoButton);

    mPitThreeButton = new Button("Pit In Three", PitThreeLaps, &sButtonStyle);
    mPitThreeButton->SetSize(0.45F, 0.175F);
    mPitThreeButton->SetPosition(0.025F, 0.725F);
    mPitThreeButton->SetStyle(Button::Toggle);
    AddElement(mPitThreeButton);

    mRequestLaps = -1;
}

//-----------------------------------------------------------------------------
PitRequestScreen::~PitRequestScreen()
{
    delete mGoBackButton;
    delete mSendButton;
    delete mPitNextButton;
    delete mPitOneButton;
    delete mPitTwoButton;
    delete mPitThreeButton;
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PitRequestScreen::Update(void)
{
    if ((mRequestLaps >= 0) && !mSendAdded)
    {
        mSendAdded = true;
        AddElement(mSendButton);
    }
    else if ((mRequestLaps < 0) && mSendAdded)
    {
        mSendAdded = false;
        RemoveElement(mSendButton);
    }

    return ScreenOk;
}


//-----------------------------------------------------------------------------
void PitRequestScreen::Draw(DisplayController *display)
{
    display->DrawString("Choose When:", 0.01F, 0.01F, CYAN, 85.0);
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PitRequestScreen::ProcessUIEvent(UIEvent *message)
{
    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case GoBack:
                return ScreenPop;
                break;

            case PitNextLap:
                mPitOneButton->SetState(Button::Off, false);
                mPitTwoButton->SetState(Button::Off, false);
                mPitThreeButton->SetState(Button::Off, false);
                if (message->bn.newState == Button::On) mRequestLaps = 0;
                else mRequestLaps = -1;
                break;

            case PitOneLap:
                mPitNextButton->SetState(Button::Off, false);
                mPitTwoButton->SetState(Button::Off, false);
                mPitThreeButton->SetState(Button::Off, false);
                if (message->bn.newState == Button::On) mRequestLaps = 1;
                else mRequestLaps = -1;
                break;

            case PitTwoLaps:
                mPitNextButton->SetState(Button::Off, false);
                mPitOneButton->SetState(Button::Off, false);
                mPitThreeButton->SetState(Button::Off, false);
                if (message->bn.newState == Button::On) mRequestLaps = 2;
                else mRequestLaps = -1;
                break;

            case PitThreeLaps:
                mPitNextButton->SetState(Button::Off, false);
                mPitOneButton->SetState(Button::Off, false);
                mPitTwoButton->SetState(Button::Off, false);
                if (message->bn.newState == Button::On) mRequestLaps = 3;
                else mRequestLaps = -1;
                break;

            case Send:
                SendRequest();
                return ScreenPop;
                break;
        }
    }

    return ScreenOk;
}

//-----------------------------------------------------------------------------
void PitRequestScreen::SendRequest(void)
{
    SensorManager *sm = SensorManager::Get();
    sm->PitRequest(mRequestLaps);
}


//-----------------------------------------------------------------------------
void PitRequestScreen::Push(void)
{
    PitRequestScreen *screen = new PitRequestScreen;
    UI::PushScreen(screen);
}


