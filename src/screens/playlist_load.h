#ifndef PLAYLIST_LOAD_H
#define PLAYLIST_LOAD_H

#include "ui/screen.h"
#include "audio/playlist.h"

class MusicPlayer;
class Button;
class Listbox;

class PlaylistLoad : public ScreenBase
{
 public:


typedef enum { PLL_Ok,
               PLL_Cancel,
               PLL_Delete,
               PLL_Files,
             } Control_IDs;


   PlaylistLoad(MusicPlayer *owner);
   virtual ~PlaylistLoad();

   static void Push(MusicPlayer *owner);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);


 private:

   Button *mDeleteButton;;
   Button *mOkButton;
   Button *mCancelButton;
   Listbox *mFiles;

   MusicPlayer *mOwner;
};







#endif //PLAYLIST_LOAD_H
