#include "screens/engine.h"

//#include "sensors.h"
//#include "acc.h"

#include "util/util.h"

#include "screens/debug.h"
#include "screens/musicplayer.h"


#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

//-----------------------------------------------------------------------------
EngineScreenConsult::EngineScreenConsult(void)
{
    mConsultInterface = new ConsultInterface("/dev/stty1");

    mDebugScreenButton = new Button("Debug ScreenBase", DebugScreen, NULL);
    mDebugScreenButton->SetSizeToContent(20, 6);
    mDebugScreenButton->SetPosition(600, 440);
    AddElement(mDebugScreenButton);

    mMusicPlayerScreenButton = new Button("Music Player", MusicPlayerScreen, NULL);
    mMusicPlayerScreenButton->SetSizeToContent(20, 6);
    mMusicPlayerScreenButton->SetPosition(600, 365);
    AddElement(mMusicPlayerScreenButton);

    mLiveDataButton = new Button("Live Data", LiveData, NULL);
    mLiveDataButton->SetSizeToContent(20, 6);
    mLiveDataButton->SetPosition(600, 165);
    mLiveDataButton->SetStyle(Button::Toggle);
    AddElement(mLiveDataButton);

    mCheckCodesButton = new Button("Check Codes", CheckCodes, NULL);
    mCheckCodesButton->SetSizeToContent(20, 6);
    mCheckCodesButton->SetPosition(600, 230);
    mCheckCodesButton->SetStyle(Button::Toggle);
    AddElement(mCheckCodesButton);

    mAdjustmentsButton = new Button("Adjustments", Adjustments, NULL);
    mAdjustmentsButton->SetSizeToContent(20, 6);
    mAdjustmentsButton->SetPosition(600, 295);
    mAdjustmentsButton->SetStyle(Button::Toggle);
    AddElement(mAdjustmentsButton);

    // Controls for tests

    mCoolantTemp   = new Button("Coolant Temp ", Test_SetCoolantTemp,       NULL);
    mFuelInjection = new Button(" Adjust Fuel ", Test_SetFuelInjection,     NULL);
    mTiming        = new Button("Adjust Timing", Test_SetTiming,            NULL);
    mIACV          = new Button(" Adjust IACV ", Test_SetIACVOpening,       NULL);
    mFICD          = new Button("  Test FICD  ", Test_SetIACV_FICDSolenoid, NULL);
    mBalance       = new Button("Power Balance", Test_SetPowerBalance,      NULL);
    mFuelPump      = new Button("Test FP Relay", Test_SetFuelPumpRelay,     NULL);
    mSelfLearn     = new Button(" Reset Trims ", Test_SetSelfLearn,         NULL);
    mCoolantFan    = new Button("  Test Fan   ", Test_SetCoolantFan,        NULL);

    mCoolantTemp->SetSizeToContent(4, 4);
    mFuelInjection->SetSizeToContent(4, 4);
    mTiming->SetSizeToContent(4, 4);
    mIACV->SetSizeToContent(4, 4);
    mFICD->SetSizeToContent(4, 4);
    mBalance->SetSizeToContent(4, 4);
    mFuelPump->SetSizeToContent(4, 4);
    mSelfLearn->SetSizeToContent(4, 4);
    mCoolantFan->SetSizeToContent(4, 4);

    mCoolantTemp->SetStyle(Button::Toggle);
    mFuelInjection->SetStyle(Button::Toggle);
    mTiming->SetStyle(Button::Toggle);
    mIACV->SetStyle(Button::Toggle);
    mFICD->SetStyle(Button::Toggle);
    mBalance->SetStyle(Button::Toggle);
    mFuelPump->SetStyle(Button::Toggle);
    mSelfLearn->SetStyle(Button::Toggle);
    mCoolantFan->SetStyle(Button::Toggle);

    mCoolantTemp->SetPosition(  390,  15);
    mFuelInjection->SetPosition(390,  69);
    mTiming->SetPosition(       390, 123);
    mIACV->SetPosition(         390, 177);
    mFICD->SetPosition(         390, 231);
    mBalance->SetPosition(      390, 285);
    mFuelPump->SetPosition(     390, 339);
    mSelfLearn->SetPosition(    390, 393);
    mCoolantFan->SetPosition(   390, 442);

    mTestValUnits ="";
    mTestValValid = 0;
    mTestValAdj = new Slider("Val", TestSlider, NULL, Slider::Vertical);
    mTestValAdj->SetPosition(175, 123);
    mTestValAdj->SetSize(50, 300);

    unsigned a;
    for (a = 0; a < 4; a++)
    {
        mTestToggleAdj[a] = new Button("B", TestButton1 + a, NULL);
        mTestToggleAdj[a]->SetPosition(50, 123 + a * 85);
        mTestToggleAdj[a]->SetSize(300, 75);
    }

    // Initialize everything and start reading data
    //mConsultInterface->loadConfig("Consult/default.cfg");
    SetConsultMode(ShowLiveData);
    mConsultInterface->RegistersReadStart();
}

//-----------------------------------------------------------------------------
EngineScreenConsult::~EngineScreenConsult()
{
    mConsultInterface->RegistersReadStop();

    delete mDebugScreenButton;
    delete mMusicPlayerScreenButton;
    delete mLiveDataButton;
    delete mCheckCodesButton;
    delete mAdjustmentsButton;
    delete mCoolantTemp;
    delete mFuelInjection;
    delete mTiming;
    delete mIACV;
    delete mFICD;
    delete mBalance;
    delete mFuelPump;
    delete mSelfLearn;
    delete mCoolantFan;

    delete mTestToggleAdj[0];
    delete mTestToggleAdj[1];
    delete mTestToggleAdj[2];
    delete mTestToggleAdj[3];

    delete mConsultInterface;
}

//-----------------------------------------------------------------------------
void EngineScreenConsult::SetTest(Test_ID test)
{
    Test_ID prev = mCurrentTest;
    mCurrentTest = test;

    unsigned a;
    // Cleanup old control
    if (prev != Test_Invalid) mConsultInterface->TestStop();
    mTestValValid = 0;
    switch (prev)
    {
        case Test_SetCoolantTemp:
            RemoveElement(mTestValAdj);
            mCoolantTemp->SetState(Button::Off, 0);
            break;

        case Test_SetFuelInjection:
            RemoveElement(mTestValAdj);
            mFuelInjection->SetState(Button::Off, 0);
            break;

        case Test_SetTiming:
            RemoveElement(mTestValAdj);
            mTiming->SetState(Button::Off, 0);
            break;

        case Test_SetIACVOpening:
            RemoveElement(mTestValAdj);
            mIACV->SetState(Button::Off, 0);
            break;

        case Test_SetIACV_FICDSolenoid:
            RemoveElement(mTestToggleAdj[0]);
            mFICD->SetState(Button::Off, 0);
            break;

        case Test_SetPowerBalance:
            for (a = 0; a < 4; a++)
            {
                RemoveElement(mTestToggleAdj[a]);
            }
            mBalance->SetState(Button::Off, 0);
            break;

        case Test_SetFuelPumpRelay:
            RemoveElement(mTestToggleAdj[0]);
            mFuelPump->SetState(Button::Off, 0);
            break;

        case Test_SetSelfLearn:
            RemoveElement(mTestToggleAdj[0]);
            mSelfLearn->SetState(Button::Off, 0);
            break;

        case Test_SetCoolantFan:
            RemoveElement(mTestToggleAdj[0]);
            mCoolantFan->SetState(Button::Off, 0);
            break;

        default:
            break;
    }
    // Setup controls for this test
    switch (mCurrentTest)
    {
        case Test_SetCoolantTemp:
            mTestValUnits = "*C";
            mTestValAdj->SetRange(150.0F, -50.0F);
            mTestValAdj->SetStepSize(1.0F);
            mTestValAdj->SetValue(80.0F);
            AddElement(mTestValAdj);
            break;

        case Test_SetFuelInjection:
            mTestValUnits = "%";
            mTestValAdj->SetRange(150.0F, 50.0F);
            mTestValAdj->SetStepSize(1.0F);
            mTestValAdj->SetValue(100.0F);
            AddElement(mTestValAdj);
            break;

        case Test_SetTiming:
            mTestValUnits = "*";
            mTestValAdj->SetRange(20.0F, -20.0F);
            mTestValAdj->SetStepSize(1.0F);
            mTestValAdj->SetValue(0.0F);
            AddElement(mTestValAdj);
            break;

        case Test_SetIACVOpening:
            mTestValUnits = "%";
            mTestValAdj->SetRange(100.0F, 0.0F);
            mTestValAdj->SetStepSize(1.0F);
            mTestValAdj->SetValue(20.0F);
            AddElement(mTestValAdj);
            break;

        case Test_SetIACV_FICDSolenoid:
            mTestValUnits = "%";
            AddElement(mTestToggleAdj[0]);
            mTestToggleAdj[0]->SetName("FICD On");
            mTestToggleAdj[0]->SetStyle(Button::Toggle);
            mTestToggleAdj[0]->SetState(Button::Off, true);
            break;

        case Test_SetPowerBalance:
            mCylinderEnable = 0xFF;
            for (a = 0; a < 4; a++)
            {
                AddElement(mTestToggleAdj[a]);
                char s[15];
                sprintf(s, "Cylinder %d On", a + 1);
                mTestToggleAdj[a]->SetName(s);
                mTestToggleAdj[a]->SetStyle(Button::Toggle);
                mTestToggleAdj[a]->SetState(Button::On, true);
            }
            break;

        case Test_SetFuelPumpRelay:
            AddElement(mTestToggleAdj[0]);
            mTestToggleAdj[0]->SetName("FuelPump On");
            mTestToggleAdj[0]->SetStyle(Button::Toggle);
            mTestToggleAdj[0]->SetState(Button::On, true);
            break;

        case Test_SetSelfLearn:
            AddElement(mTestToggleAdj[0]);
            mTestToggleAdj[0]->SetName("Reset Trims");
            mTestToggleAdj[0]->SetStyle(Button::Momentary);
            break;

        case Test_SetCoolantFan:
            AddElement(mTestToggleAdj[0]);
            mTestToggleAdj[0]->SetName("Radiator Fan On");
            mTestToggleAdj[0]->SetStyle(Button::Toggle);
            mTestToggleAdj[0]->SetState(Button::Off, true);
            break;

        default:
            break;
    }
}

//-----------------------------------------------------------------------------
void EngineScreenConsult::SetConsultMode(ConsultMode newMode)
{
    //assert(newMode != ShowInvalid);

    ConsultMode prev = mConsultMode;
    mConsultMode = newMode;

    if (newMode == ShowLiveData)    mLiveDataButton->SetState(Button::On, false);
    else                            mLiveDataButton->SetState(Button::Off, false);

    if (newMode == ShowCodes)       mCheckCodesButton->SetState(Button::On, false);
    else                            mCheckCodesButton->SetState(Button::Off, false);

    if (newMode == ShowAdjustments) mAdjustmentsButton->SetState(Button::On, false);
    else                            mAdjustmentsButton->SetState(Button::Off, false);

    if (prev != mConsultMode)
    {
        switch (prev)
        {
            case ShowLiveData:
                mConsultStopError = mConsultInterface->RegistersReadStop();

                break;

            case ShowCodes:
                mConsultStopError = mConsultInterface->FaultsReadStop();
                break;

            case ShowAdjustments:
                SetTest(Test_Invalid);
                RemoveElement(mCoolantTemp);
                RemoveElement(mFuelInjection);
                RemoveElement(mTiming);
                RemoveElement(mIACV);
                RemoveElement(mFICD);
                RemoveElement(mBalance);
                RemoveElement(mFuelPump);
                RemoveElement(mSelfLearn);
                RemoveElement(mCoolantFan);
                break;

            default:
                break;
        }

        switch (mConsultMode)
        {
            case ShowLiveData:
                mConsultStartError = mConsultInterface->RegistersReadStart();
                break;

            case ShowCodes:
                mConsultStartError = mConsultInterface->FaultsReadStart();
                mCodesRefreshTime = SystemTimeUTC() + 1.0;
                break;

            case ShowAdjustments:
                AddElement(mCoolantTemp);
                AddElement(mFuelInjection);
                AddElement(mTiming);
                AddElement(mIACV);
                AddElement(mFICD);
                AddElement(mBalance);
                AddElement(mFuelPump);
                AddElement(mSelfLearn);
                AddElement(mCoolantFan);
                break;

            default:
                break;
        }
    }
}



//#define G_METER_X    400
//#define G_METER_Y    20

//-----------------------------------------------------------------------------
//void g_to_pos(unsigned *x, unsigned *y, float gX, float gY)
//{
//    *x = (G_METER_X - 4 + (GMeterImage.width  / 2)) + (int) (gX * 0.8F * (float) (GMeterImage.width  / 2));
//    *y = (G_METER_Y - 4 + (GMeterImage.height / 2)) + (int) (gY * 0.8F * (float) (GMeterImage.height / 2));
//}

//-----------------------------------------------------------------------------
void EngineScreenConsult::DrawLiveData(DisplayController *display)
{
    //unsigned dx, dy;
    char s[30];

    //g_to_pos(&dx, &dy, gSensorState.gX , gSensorState.gY);
    //LCD_blit_rle(&GMeterImage, G_METER_X, G_METER_Y);
    //display->Rect(dx, dy, 7, 7, RED);

    //sprintf(s, "%.1fV ", gSensorState.batteryVoltage);
    //display->DrawString(s, 200, 20, GREEN, BLACK, Font_Regular);
    //sprintf(s, "%3dpsi", (int)gSensorState.oilPressure);
    //display->DrawString(s, 200, 52, GREEN, BLACK, Font_Regular);
    //sprintf(s, "%3d*C ", (int)gSensorState.oilTemperature);
    //display->DrawString(s, 200, 84, GREEN, BLACK, Font_Regular);
    //sprintf(s, "%.1f ", gSensorState.afRatio);
    //display->DrawString(s, 200, 116, GREEN, BLACK, Font_Regular);
    //sprintf(s, "%-.2fb ", gSensorState.intakePressure);
    //display->DrawString(s, 200, 148, GREEN, BLACK, Font_Regular);

    //sprintf(s, "X: %-.2fg ", gSensorState.gX);
    //display->DrawString(s, 620,  75, BLUE, BLACK, Font_Small);
    //sprintf(s, "Y: %-.2fg ", gSensorState.gY);
    //display->DrawString(s, 620, 100, BLUE, BLACK, Font_Small);
    //sprintf(s, "Z: %-.2fg ", gSensorState.gZ);
    //display->DrawString(s, 620, 125, BLUE, BLACK, Font_Small);

    unsigned a;
    display->DrawString("Battery",   10, 20, GREEN, BLACK, Font_Regular);
    display->DrawString("Oil Pr",    10, 52, GREEN, BLACK, Font_Regular);
    display->DrawString("Oil Temp",  10, 84, GREEN, BLACK, Font_Regular);
    display->DrawString("AF Ratio",  10, 116, GREEN, BLACK, Font_Regular);
    display->DrawString("Intake Pr", 10, 148, GREEN, BLACK, Font_Regular);

    for (a = 0; a < mConsultInterface->RegistersCount(); a++)
    {
        uint8_t index = mConsultInterface->RegistersRegister(a);
        display->DrawString(mConsultInterface->RegisterName(index),  10, 250 + (a * 25), GREEN,  BLACK, Font_Small);
    }

    for (a = 0; a < mConsultInterface->RegistersCount(); a++)
    {
        uint8_t index = mConsultInterface->RegistersRegister(a);
        mConsultInterface->RegisterFormatValue(index, s, 28);
        strcat(s, " ");
        display->DrawString(s, 250, 250 + (a * 25), YELLOW, BLACK, Font_Small);
    }
}

//-----------------------------------------------------------------------------
void EngineScreenConsult::DrawCodes(DisplayController *display)
{
    unsigned count = mConsultInterface->FaultsCount();

    char s[100];
    sprintf(s, "%d Fault Codes", count);
    display->DrawString(s, 10, 10, CYAN, BLACK, Font_Regular);

    unsigned a;
    for (a = 0; a < count; a++)
    {
        const ConsultFaultState &fs = mConsultInterface->FaultsGet(a);
        sprintf(s,"%2d %s", fs.FaultID(), fs.Fault());
        display->DrawString(s,  10, 50 + (a * 35), YELLOW, BLACK, Font_Regular);
    }
}

//-----------------------------------------------------------------------------
void EngineScreenConsult::DrawAdjustments(DisplayController *display)
{
    char s[50];
    float mw, mh;
    display->GetFontSize(Font_Regular, mw, mh);

    const char *name = "Choose Test";
    if (mCurrentTest != Test_Invalid) name = sTestNames[mCurrentTest - Test_SetCoolantTemp];

    unsigned namelen = strlen(name);
    float namew = namelen * mw;
    float namex = (380 - namew) / 2;

    display->DrawString(name, namex, 15, CYAN, BLACK, Font_Regular);

    if (mTestValValid != 0)
    {
        float min, max;
        mTestValAdj->GetRange(max, min);

        sprintf(s, "%d%s", (int)max, mTestValUnits);
        unsigned x = 200 - ((strlen(s) * mw) / 2);
        display->DrawString(s, x, 73, YELLOW, BLACK, Font_Regular);

        sprintf(s, "%d%s", (int)min, mTestValUnits);
        x = 200 - ((strlen(s) * mw) / 2);
        display->DrawString(s, x, 427, YELLOW, BLACK, Font_Regular);
    }

    sprintf(s, "%d%s", (int)mTestValAdj->GetValue(), mTestValUnits);
    unsigned x = 165 - (strlen(s) * mw);
    display->DrawString(s, x, 255, GREEN, BLACK, Font_Regular);

    mTestValChanged = 0;
}


//-----------------------------------------------------------------------------
void EngineScreenConsult::Draw(DisplayController *display)
{
    char s[100];

    sprintf(s, "Time: %s", TimeString());
    display->DrawString(s, 600, 10, BLUE, BLACK, Font_Small);
    //sprintf(s, "Max Update: %dms  ", (int) gMaxUpdateTime);
    //display->DrawString(s, 600, 35, BLUE, BLACK, Font_Tiny);
    //sprintf(s, "ConsultStopError: %d  ", sConsultStopError);
    //display->DrawString(s, 600, 50, BLUE, BLACK, Font_Tiny);
    //sprintf(s, "ConsultStartError: %d  ", sConsultStartError);
    //display->DrawString(s, 600, 65, BLUE, BLACK, Font_Tiny);

    //sprintf(s, "Log Opened %d  %4d ", sLogOpened, sLogCacheUsed);
    //display->DrawString(s, 600, 50, BLUE, BLACK, Font_Tiny);
    //sprintf(s, "File Error %d %d  ", gFileError, sLogSyncError);
    //display->DrawString(s, 600, 63, BLUE, BLACK, Font_Tiny);

    //display->DrawString(mLogFileName, 400, 250, BLUE, BLACK, Font_Tiny);

    switch (mConsultMode)
    {
        case ShowLiveData:
            DrawLiveData(display);
            break;

        case ShowCodes:
            DrawCodes(display);
            break;

        case ShowAdjustments:
            DrawAdjustments(display);
            break;

        default:
            break;
    }

    // Refresh the list of codes available every second, they can change but only when NOT streaming the codes
    if ((mConsultMode == ShowCodes) && (SystemTimeUTC() >= mCodesRefreshTime))
    {
        mCodesRefreshTime += 1.0;
        mConsultInterface->FaultsReadStop();
        mConsultInterface->FaultsReadStart();
    }
}

//-----------------------------------------------------------------------------
void EngineScreenConsult::TestsUI(UIEvent *message)
{
    uint8_t val;
    if (message->message == SliderChanged)
    {
        switch (mCurrentTest)
        {
            case Test_SetCoolantTemp:
                mConsultInterface->TestStop();
                val = (uint8_t) (message->sl.newValue + 50.0F);
                mConsultInterface->TestStart(SetCoolantTemp, val);
                mTestValValid = true;
                mTestValChanged = true;
                break;

            case Test_SetFuelInjection:
                mConsultInterface->TestStop();
                val = (uint8_t) message->sl.newValue;
                mConsultInterface->TestStart(SetFuelInjection, val);
                mTestValValid = true;
                mTestValChanged = true;
                break;

            case Test_SetTiming:
                mConsultInterface->TestStop();
                val = (uint8_t) ((int8_t) message->sl.newValue);
                mConsultInterface->TestStart(SetIgnitionTiming, val);
                mTestValValid = true;
                mTestValChanged = true;
                break;

            case Test_SetIACVOpening:
                mConsultInterface->TestStop();
                val = (uint8_t) message->sl.newValue;
                mConsultInterface->TestStart(SetIACVOpening, val);
                mTestValValid = true;
                mTestValChanged = true;
                break;

            default:
                //assert(0 && "Bad test for slider value!");
                break;
        }
    }

    if (message->message == ButtonClicked)
    {
        uint8_t val;
        uint16_t cylinder;
        switch (mCurrentTest)
        {
            case Test_SetIACV_FICDSolenoid:
                mConsultInterface->TestStop();
                val = (uint8_t) message->bn.newState;
                mConsultInterface->TestStart(SetIACV_FICDSolenoid, val);
                break;

            case Test_SetPowerBalance:
                cylinder = message->bn.id - TestButton1;
                if (cylinder < 4)
                {
                    mConsultInterface->TestStop();
                    if (message->bn.newState == Button::Off) mCylinderEnable &= ~(1 << cylinder);
                    else                                mCylinderEnable |=  (1 << cylinder);
                    mConsultInterface->TestStart(SetPowerBalance, mCylinderEnable ^ 0xFF);
                }
                break;

            case Test_SetFuelPumpRelay:
                mConsultInterface->TestStop();
                val = (uint8_t) message->bn.newState;
                mConsultInterface->TestStart(SetFuelPumpRelay, val);
                break;

            case Test_SetSelfLearn:
                mConsultInterface->TestStop();
                val = (uint8_t) message->bn.newState;
                mConsultInterface->TestStart(SetSelfLearn, val);
                break;

            case Test_SetCoolantFan:
                mConsultInterface->TestStop();
                val = (uint8_t) message->bn.newState;
                mConsultInterface->TestStart(SetCoolantFanLow, val);
                //Beep();
                break;

            default:
                break;
        }
    }
}

//-----------------------------------------------------------------------------
void EngineScreenConsult::LiveDataUI(UIEvent *message)
{
    if (message->message == Holding)
    {
        //int16_t top    = G_METER_Y + (GMeterImage.height / 4);
        //int16_t bottom = G_METER_Y +  GMeterImage.height - (GMeterImage.height / 4);

        //int16_t left  = G_METER_X + (GMeterImage.width / 4);
        //int16_t right = G_METER_X +  GMeterImage.width - (GMeterImage.width / 4);

        //if ((message->click.x >= left) && (message->click.x <= right) &&
        //    (message->click.y >= top)  && (message->click.y <= bottom))
        //{
        //    Beep();
        //    ACC_Zero();
        //}
    }
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult EngineScreenConsult::ProcessUIEvent(UIEvent *message)
{
    if (mConsultMode == ShowAdjustments)
    {
        TestsUI(message);
    }

    if (mConsultMode == ShowLiveData)
    {
        LiveDataUI(message);
    }


    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case LiveData:
                SetConsultMode(ShowLiveData);
                break;

            case CheckCodes:
                SetConsultMode(ShowCodes);
                break;

            case Adjustments:
                SetConsultMode(ShowAdjustments);
                break;

            case MusicPlayerScreen:
                MusicPlayer::Show();
                break;

            case DebugScreen:
                DebugScreen::Push();
                break;

            case Test_SetCoolantTemp:
            case Test_SetFuelInjection:
            case Test_SetTiming:
            case Test_SetIACVOpening:
            case Test_SetIACV_FICDSolenoid:
            case Test_SetPowerBalance:
            case Test_SetFuelPumpRelay:
            case Test_SetSelfLearn:
            case Test_SetCoolantFan:
                SetTest((Test_ID)message->bn.id);
                break;

        }
    }

    if (message->message == ButtonHolding)
    {
        if ((message->bn.id == CheckCodes) && (mConsultMode == ShowCodes))
        {
            //Beep();
            mConsultInterface->FaultsClear();
        }
    }

    return ScreenOk;
}


static EngineScreenConsult *sEngineScreenConsult = NULL;
//-----------------------------------------------------------------------------
void EngineScreenConsult::Show(void)
{
    if (sEngineScreenConsult == NULL) sEngineScreenConsult = new EngineScreenConsult;
    UI::SetScreen(sEngineScreenConsult, true);

    //if (mConsultMode == ShowInvalid) mLiveDataButton->SetState(Button::On, true);
}


