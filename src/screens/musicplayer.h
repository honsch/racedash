#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include "ui/screen.h"
#include "audio/playlist.h"

class Button;
class Listbox;
class Slider;
class VorbisPlayer;

class MusicPlayer : public ScreenBase
{
 public:

   MusicPlayer(void);
   virtual ~MusicPlayer();

   void SetPlaylist(Playlist *pl, bool match);
   void OpenPlaylist(const char *fname, bool match);

   void Beep(void);

   static void Show(void);
   static void UpdateAudio(void);

   const char *NextFile(bool skipToNext);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

 private:

   VorbisPlayer    *mPlayer;

   Playlist     *mPlaylist;
   bool          mAutoStart;

   Playlist::PlaylistOrder mPlayOrder;

   Slider *mVolumeSlider;
   Button *mNavButtons[4];

   Button *mDebugScreenButton;
   Button *mDashboardScreenButton;
   Button *mPlaylistLoadButton;
   Button *mPlaylistEditButton;
   Button *mPlayOrderButton;

   static MusicPlayer *sMusicPlayerScreen;

   void UpdateAudioInternal(void);
};




#endif  //MUSICPLAYER_H
