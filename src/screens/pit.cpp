#include "screens/pit.h"

#include "util/util.h"

#include "audio/audio.h"

#include "screens/gettext.h"

#include "sensors/sensormanager.h"

#include <stdio.h>

enum Pit_Controls
{
    Pit_Accept
};

//-----------------------------------------------------------------------------
PitConfirmScreen::PitConfirmScreen(void)
{
    mPushTime = SystemTimeUTC();
}

//-----------------------------------------------------------------------------
PitConfirmScreen::~PitConfirmScreen()
{
}


//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PitConfirmScreen::Update(void)
{
    int timeLeft = kMaxDisplayTime - (int) (SystemTimeUTC() - mPushTime);
    if (timeLeft < 0)
    {
        // Clear the pit request
        SensorManager *sm = SensorManager::Get();
        sm->PitAcknowledged(PitAck_Timeout);
        return ScreenPop;
    }
    return ScreenOk;
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PitConfirmScreen::ProcessUIEvent(UIEvent *message)
{
    if ((message->message == Click) || (message->message== DoubleClick))
    {
        // Clear the pit request
        SensorManager *sm = SensorManager::Get();
        sm->PitAcknowledged(PitAck_OK);
        return ScreenPop;
    }

    return ScreenOk;
}


//-----------------------------------------------------------------------------
void PitConfirmScreen::Draw(DisplayController *display)
{
    SensorManager *sm = SensorManager::Get();
    if (sm == NULL)
    {
        printf("SM IS NULL\n");
        return;
    }

    char s[100];
    double pitLaps;
    sm->GetRemoteStateValue("PIT laps", pitLaps);
    pitLaps = 3;
    if (pitLaps == 0) sprintf(s, "This Lap");
    else              sprintf(s, "In %d Laps", (int)pitLaps);

    float w, h;
    if (Fraction(SystemTimeUTC()) > 0.15)
    {
        display->GetStringExtents(175, Font_Mono, "PIT PIT PIT", w, h);
        display->DrawString("PIT PIT PIT", Center(1.0F, w), -0.1F, WHITE, 175.0F);
    }

    display->GetStringExtents(150, Font_Mono, s, w, h);
    display->DrawString(s, Center(1.0F, w), 0.35F, WHITE, 150.0F);

    display->DrawString("Tap screen to acknowledge", 0.20F, 0.75F, CYAN, 72.0F);

    int timeLeft = kMaxDisplayTime - (int) (SystemTimeUTC() - mPushTime);
    sprintf(s, "%2d", timeLeft);
    display->DrawString(s, 0.05F, 0.75F, CYAN, 72.0F);
}


//-----------------------------------------------------------------------------
void PitConfirmScreen::Push(void)
{
    PitConfirmScreen *pit = new PitConfirmScreen;
    UI::PushScreen(pit);
}


