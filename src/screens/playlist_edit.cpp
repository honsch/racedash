#include "screens/playlist_edit.h"

#include "util/util.h"


#include "screens/gettext.h"
#include "screens/musicplayer.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define PLAYLIST_FOLDER "Playlists"

//#define WANT_SCAN_CONTROLS

enum Control_IDs { PLE_Ok,
                   PLE_Add,
                   PLE_Remove,
                   PLE_Save,
                   PLE_SaveAs,
                   PLE_Christmas,
                   PLE_Classical,
                   PLE_Modern,
                   PLE_Other,
                   PLE_Artist,
                   PLE_Album,
                   PLE_AlbumFiles,
                   PLE_Playlist,
                   PLE_NewFilename
                 };

#define MUSIC_FOLDER "/home/alarm/Music"


//-----------------------------------------------------------------------------
PlaylistEdit::PlaylistEdit(MusicPlayer *owner, Playlist *pl) :
    mOwner(owner)
{

    float smallSpaceX = 0.0078125F;
    float smallSpaceY = 0.0125F;

    mOkButton = new Button("   Ok   ", (uint16_t)PLE_Ok, NULL);
    mOkButton->SetSizeToContent(smallSpaceX, smallSpaceY);
    mOkButton->SetPosition(0.0F, 1.0F - mOkButton->Height());
    AddElement(mOkButton);

    mSaveButton = new Button("  Save  ", (uint16_t)PLE_Save, NULL);
    mSaveButton->SetSizeToContent(smallSpaceX, smallSpaceY);
    mSaveButton->SetPosition(0.15625F, 1.0F - mSaveButton->Height());
    AddElement(mSaveButton);

    mSaveAsButton = new Button("Save As", (uint16_t)PLE_SaveAs, NULL);
    mSaveAsButton->SetSizeToContent(smallSpaceX, smallSpaceY);
    mSaveAsButton->SetPosition(0.320F, 1.0F - mSaveAsButton->Height());
    AddElement(mSaveAsButton);

    mAddButton = new Button(">>>", (uint16_t)PLE_Add, NULL);
    mAddButton->SetPosition(0.598F, 0.219F);
    mAddButton->SetSizeToContent(smallSpaceX * 3.0F, smallSpaceY * 3.0F);
    AddElement(mAddButton);

    mRemoveButton = new Button("<<<", (uint16_t)PLE_Remove, NULL);
    mRemoveButton->SetPosition(0.598, 0.5F);
    mRemoveButton->SetSizeToContent(smallSpaceX * 3.0F, smallSpaceY * 3.0F);
    AddElement(mRemoveButton);

    mModernButton = new Button(" Modern  ", (uint16_t)PLE_Modern, NULL);
    mModernButton->SetPosition(0, 0);
    mModernButton->SetSizeToContent(smallSpaceX, smallSpaceY);
    mModernButton->SetStyle(Button::Toggle);
    AddElement(mModernButton);

    mClassicalButton = new Button("Classical", (uint16_t)PLE_Classical, NULL);
    mClassicalButton->SetPosition(0.1953125F, 0);
    mClassicalButton->SetSizeToContent(smallSpaceY, smallSpaceY);
    mClassicalButton->SetStyle(Button::Toggle);
    AddElement(mClassicalButton);

    mChristmasButton = new Button("Christmas", (uint16_t)PLE_Christmas, NULL);
    mChristmasButton->SetPosition(0.390625F, 0);
    mChristmasButton->SetSizeToContent(smallSpaceX, smallSpaceY);
    mChristmasButton->SetStyle(Button::Toggle);
    AddElement(mChristmasButton);

    mOtherButton = new Button("  Other  ", (uint16_t)PLE_Other, NULL);
    mOtherButton->SetPosition(0.5859375F, 0);
    mOtherButton->SetSizeToContent(smallSpaceX, smallSpaceY);
    mOtherButton->SetStyle(Button::Toggle);
    AddElement(mOtherButton);

    mArtist = new Listbox("Artist", (uint16_t)PLE_Artist, NULL, false);
    mArtist->SetPosition(0, 0.09375F);
    mArtist->SetSize(0.293F, 0.8125F);
    AddElement(mArtist);

    mAlbum = new Listbox("Album", (uint16_t)PLE_Album, NULL, true);
    mAlbum->SetPosition(0.3125F, 0.09375F);
    mAlbum->SetSize(0.2734375F, 0.8125F);
    AddElement(mAlbum);

    // Used as a nice way to get filenames
    // Never added to the elements list
    mAlbumFiles = new Listbox("AlbumFiles", 0, NULL, false);

    mPlaylistFiles = new Listbox("Playlist", (uint16_t)PLE_Playlist, NULL, true);
    mPlaylistFiles->SetPosition(0.6875, 0.09375F);
    mPlaylistFiles->SetSize(0.3125F, 0.8125);
    AddElement(mPlaylistFiles);

    strcpy(mSelectedGenre, "Modern");
    memset(mSelectedArtist, 0, sizeof(mSelectedArtist));
    memset(mSelectedAlbum,  0, sizeof(mSelectedAlbum));

    mModernButton->SetState(Button::On, false);
    UpdateGenre();

    mPlaylist = pl->Clone();
}

//-----------------------------------------------------------------------------
PlaylistEdit::~PlaylistEdit()
{
   delete mOkButton;
   delete mAddButton;
   delete mRemoveButton;

   delete mSaveButton;
   delete mSaveAsButton;

   delete mClassicalButton;
   delete mChristmasButton;
   delete mModernButton;
   delete mOtherButton;

   delete mArtist;
   delete mAlbum;
   delete mAlbumFiles;
   delete mPlaylistFiles;

   delete mPlaylist;
}


//-----------------------------------------------------------------------------
void PlaylistEdit::UpdateGenre(void)
{
    mArtist->Clear();
    mAlbum->Clear();
    mAlbumFiles->Clear();
    char s[_MAX_LFN + 1];
    sprintf(s, "%s/%s", MUSIC_FOLDER, mSelectedGenre);

    FillListboxFromDir(mArtist, s, FL_DirOnly, 0);
}

//-----------------------------------------------------------------------------
void PlaylistEdit::UpdateArtist(void)
{
    mAlbumFiles->Clear();

    char s[_MAX_LFN + 1];
    sprintf(s, "%s/%s/%s", MUSIC_FOLDER, mSelectedGenre, mSelectedArtist);

    FillListboxFromDir(mAlbum, s, FL_DirOnly, 1);
}

//-----------------------------------------------------------------------------
void PlaylistEdit::FillPlaylistFiles(Playlist *pl)
{
    if (pl == NULL) return;
    mPlaylistFiles->Clear();

    char s[_MAX_LFN];
    unsigned a;
    for (a = 0; a < pl->ItemCount(); a++)
    {
        strcpy(s, pl->GetFilename(a) + 3); // skip the song index at the front of the filename
        unsigned len = strlen(s);
        if (len > 4) s[len - 4] = 0; // chop the '.mp3' at the end
        mPlaylistFiles->AddString(s, false);
    }
}

//-----------------------------------------------------------------------------
void PlaylistEdit::AddAlbumfilesToPlaylist(Playlist *pl)
{
    unsigned a;
    for (a = 0; a < mAlbum->ItemCount(); a++)
    {
        if (mAlbum->IsSelected(a))
        {
            char dir[_MAX_LFN + 1];
            sprintf(dir, "%s/%s/%s/%s", MUSIC_FOLDER, mSelectedGenre, mSelectedArtist, mAlbum->GetString(a));

            FillListboxFromDir(mAlbumFiles, dir, FL_FileOnly, 1);

            unsigned count = mAlbumFiles->ItemCount();

            strcat(dir, "/");

            unsigned b;
            for (b = 0; b < count; b++)
            {
                pl->AddItem(dir, mAlbumFiles->GetString(b));
            }
        }
    }

    FillPlaylistFiles(pl);
}

//-----------------------------------------------------------------------------
// This function fiddles with the innards of the playlist!
// It will break if anything interesting changes
void PlaylistEdit::RemoveSelectedFiles(Playlist *pl)
{
    if (pl == NULL) return;

    unsigned a;
    for (a = pl->ItemCount(); a > 0; a--)
    {
        if (mPlaylistFiles->IsSelected(a - 1))
        {
            pl->RemoveItem(a - 1);
        }
    }

    // Force a reordering and cleanup
    pl->SetOrder(pl->GetOrder());
}


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PlaylistEdit::Update(void)
{
    return ScreenOk;
}

//-----------------------------------------------------------------------------
void PlaylistEdit::Draw(DisplayController *display)
{
    // Add my debugging
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult PlaylistEdit::ProcessUIEvent(UIEvent *message)
{
    //static uint16_t selectedIndex IN_CCM;
    if (message->message == ListboxChanged)
    {
        switch (message->lb.id)
        {
            case PLE_Artist:
                if (message->lb.newState != 0)
                {
                    strcpy(mSelectedArtist, mArtist->GetString(message->lb.itemIndex));
                    UpdateArtist();
                }
                break;
        }
    }

    if (message->message == GettextResult)
    {
        if (message->gt.id == PLE_NewFilename)
        {
            //display->DrawString("Saving as", 10, 75, GREEN, Small);
            printf("Saving playlist as: %s\n", message->gt.text);
            Playlist::SaveError err = mPlaylist->SaveAs(message->gt.text);
            free(message->gt.text);
            if (err == Playlist::SE_None)
            {
                char fname[_MAX_LFN + 1];
                strcpy(fname, mPlaylist->CurrentFilename());
                mPlaylist->Close();
                mOwner->OpenPlaylist(fname, true);
                printf("SaveAs() success\n");
                return ScreenPop;
            }
        }
    }

    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case PLE_Modern:
                if (message->bn.newState == Button::On)
                {
                    mChristmasButton->SetState(Button::Off, false);
                    mClassicalButton->SetState(Button::Off, false);
                    mOtherButton->SetState(Button::Off, false);
                    strcpy(mSelectedGenre, "Modern");
                    UpdateGenre();
                    mSelectedGenreID = PLE_Modern;
                }
                break;

            case PLE_Classical:
                if (message->bn.newState == Button::On)
                {
                    mChristmasButton->SetState(Button::Off, false);
                    mModernButton->SetState(Button::Off, false);
                    mOtherButton->SetState(Button::Off, false);
                    strcpy(mSelectedGenre, "Classical");
                    UpdateGenre();
                    mSelectedGenreID = PLE_Classical;
                }
                break;

            case PLE_Christmas:
                if (message->bn.newState == Button::On)
                {
                    mModernButton->SetState(Button::Off, false);
                    mClassicalButton->SetState(Button::Off, false);
                    mOtherButton->SetState(Button::Off, false);
                    strcpy(mSelectedGenre, "Christmas");
                    UpdateGenre();
                    mSelectedGenreID = PLE_Christmas;
                }
                break;

            case PLE_Other:
                if (message->bn.newState == Button::On)
                {
                    mChristmasButton->SetState(Button::Off, false);
                    mClassicalButton->SetState(Button::Off, false);
                    mModernButton->SetState(Button::Off, false);
                    strcpy(mSelectedGenre, "Other");
                    UpdateGenre();
                    mSelectedGenreID = PLE_Other;
                }
                break;

            case PLE_Add:
                AddAlbumfilesToPlaylist(mPlaylist);
                break;

            case PLE_Remove:
                RemoveSelectedFiles(mPlaylist);
                FillPlaylistFiles(mPlaylist);
                break;

            case PLE_SaveAs:
                GetTextScreen::Push("Enter New Playlist Filename", PLE_NewFilename, mPlaylist->GetPlaylistFilename());
                break;

            case PLE_Save:
            {
                Playlist::SaveError err = mPlaylist->Save();
                if (err == Playlist::SE_None)
                {
                    char fname[_MAX_LFN + 1];
                    strcpy(fname, mPlaylist->CurrentFilename());
                    mPlaylist->Close();
                    mOwner->OpenPlaylist(fname, true);
                    return ScreenPop;
                }
                else if (err == Playlist::SE_NoFilename)
                {
                    GetTextScreen::Push("Enter New Playlist Filename", PLE_NewFilename, mPlaylist->GetPlaylistFilename());
                }
                break;
            }

            case PLE_Ok:
            {
                if (mPlaylist->HasEdits())
                {
                    mOwner->SetPlaylist(mPlaylist, true);
                    mPlaylist = NULL;
                }
                else
                {
                    mPlaylist->Close();
                }
                return ScreenPop;
                break;
            }
        }
    }

    return ScreenOk;
}


//-----------------------------------------------------------------------------
void PlaylistEdit::Push(MusicPlayer *owner, Playlist *pl)
{
    PlaylistEdit *ple = new PlaylistEdit(owner, pl);
    ple->FillPlaylistFiles(ple->mPlaylist);
    UI::PushScreen(ple);
}


