#include "screens/selecttrack.h"

#include "track/track.h"
#include "sensors/sensormanager.h"

#include <stdint.h>

enum Control_IDs { TrackList  = 0,
                   SelectTrack,
                   Cancel
                 };

static ScreenElement::VisualStyle sListStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    GREY,           // Border Color
    MEDIUM_BLUE,    // Select Color
    35.0F,          // Font size in pixels
    Font_Mono,      // Font face
    3.0F,           // Border Size
    0.008F          // Border Radius
};

static ScreenElement::VisualStyle sButtonStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    GREY,           // Border Color
    MEDIUM_BLUE,    // Select Color
    25.0F,          // Font size in pixels
    Font_Mono,      // Font face
    3.0F,           // Border Size
    0.008F          // Border Radius
};

static ScreenElement::VisualStyle sRemoteListStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    GREY,           // Border Color
    MEDIUM_BLUE,    // Select Color
    75.0F,          // Font size in pixels
    Font_Mono,      // Font face
    3.0F,           // Border Size
    0.008F          // Border Radius
};

static ScreenElement::VisualStyle sRemoteButtonStyle =
{
    YELLOW,         // FG Color
    BLACK,          // BG Color
    GREY,           // Border Color
    MEDIUM_BLUE,    // Select Color
    75.0F,          // Font size in pixels
    Font_Mono,      // Font face
    3.0F,           // Border Size
    0.008F          // Border Radius
};


//-----------------------------------------------------------------------------
SelectTrackScreen::SelectTrackScreen(bool isRemote, double lat, double lon) :
    mIsRemote(isRemote)
{
    if (mIsRemote)
    {
        mSelectButton = new Button("Select", SelectTrack, &sRemoteButtonStyle);
        mCancelButton = new Button("Cancel", Cancel, &sRemoteButtonStyle);
        mTrackList = new Listbox("TrackList", TrackList, &sRemoteListStyle, false);
    }
    else
    {
        mSelectButton = new Button("Select", SelectTrack, &sButtonStyle);
        mCancelButton = new Button("Cancel", Cancel, &sButtonStyle);
        mTrackList = new Listbox("TrackList", TrackList, &sListStyle, false);
    }

    mSelectButton->SetSize(0.25F, 0.1F);
    mSelectButton->SetPosition(0.125, 0.85F);
    AddElement(mSelectButton);

    mCancelButton->SetSize(0.25F, 0.1F);
    mCancelButton->SetPosition(0.675F, 0.85F);
    AddElement(mCancelButton);

    mTrackList->SetSize(0.8F, 0.65F);
    mTrackList->SetPosition(0.05F, 0.15F);
    AddElement(mTrackList);

    std::vector<const Track *> tracks;
    gTrackManager->ClosestTracks(lat, lon, tracks);
    unsigned a;
    for (a = 0; a < tracks.size(); a++)
    {
        mTrackList->AddString(tracks[a]->Name(), a == 0);
    }
}

//-----------------------------------------------------------------------------
SelectTrackScreen::~SelectTrackScreen()
{
    delete mSelectButton;
    delete mCancelButton;
    delete mTrackList;
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult SelectTrackScreen::Update(void)
{
    return ScreenOk;
}


//-----------------------------------------------------------------------------
void SelectTrackScreen::Draw(DisplayController *display)
{
    if (mIsRemote)
    {
        display->DrawString("Select Track:", 0.01F, 0.01F, CYAN, 85.0);
    }
}

//-----------------------------------------------------------------------------
ScreenBase::ScreenResult SelectTrackScreen::ProcessUIEvent(UIEvent *message)
{
    if (message->message == ButtonClicked)
    {
        switch (message->bn.id)
        {
            case Cancel:
                return ScreenPop;
                break;

            case SelectTrack:
                SelectTrackInternal();
                return ScreenPop;
                break;
        }
    }

    return ScreenOk;
}

//-----------------------------------------------------------------------------
void SelectTrackScreen::SelectTrackInternal(void)
{
    uint32_t selected = mTrackList->SelectedIndex(0);
    if (selected != Listbox::BAD_INDEX)
    {
        gTrackManager->SelectTrack(mTrackList->GetString(selected));
    }
}

//-----------------------------------------------------------------------------
void SelectTrackScreen::Push(bool isRemote, double lat, double lon)
{
    SelectTrackScreen *screen = new SelectTrackScreen(isRemote, lat, lon);
    UI::PushScreen(screen);
}


