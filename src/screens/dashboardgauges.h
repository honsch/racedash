#ifndef DASHBOARDGAUGES_H
#define DASHBOARDGAUGES_H

#include "display.h"

#include "ui/listbox.h"
#include "ui/button.h"
#include "ui/slider.h"
#include "ui/bargauge.h"
#include "ui/dialgauge.h"
#include "ui/numericgauge.h"
#include "ui/stripgauge.h"


//-------------------------------------------------------
class DashboardGauges : public ScreenBase
{
  public:
    DashboardGauges(void);
   ~DashboardGauges();

   static void Show(void);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

  private:
    Button *mDebugScreenButton;
    Button *mMusicPlayerScreenButton;
    Button *mStintTimerButton;

    //StripGauge   *mTach;
    NumericGauge *mRPM;
    NumericGauge *mSpeed;

    NumericGauge *mWaterTemp;
    NumericGauge *mOilPressure;

    NumericGauge *mBoost;
    NumericGauge *mAFR;
    NumericGauge *mBattery;
    NumericGauge *mFuelLevel;

    void DrawLiveData(DisplayController *display);
 };

#endif // DASHBOARDGAUGES_H
