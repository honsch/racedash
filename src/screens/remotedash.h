#ifndef REMOTEDASHBOARD_H
#define REMOTEDASHBOARD_H

#include "display.h"

#include "ui/listbox.h"
#include "ui/button.h"
#include "ui/slider.h"
#include "ui/bargauge.h"
#include "ui/dialgauge.h"
#include "ui/numericgauge.h"
#include "ui/stripgauge.h"

//-------------------------------------------------------
class RemoteDashboard : public ScreenBase
{
  public:
    RemoteDashboard(void);
   ~RemoteDashboard();

   static void Show(void);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

  private:
    Button *mMusicPlayerScreenButton;

    BarGauge     *mSpeedo;
    NumericGauge *mRPM;

    NumericGauge *mWaterTemp;
    NumericGauge *mOilPressure;

    NumericGauge *mAFR;
    NumericGauge *mBattery;
    NumericGauge *mFuelGauge;
    NumericGauge *mSpeedoNumeric;

    Button *mPitScreenButton;
    Button *mPitConfirmButton;
    bool    mPitConfirmButtonAdded;

    void DrawLiveData(DisplayController *display);

    PitAcknowledge mPrevPitAck;
 };

#endif // REMOTEDASHBOARD_H
