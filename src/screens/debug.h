#ifndef DEBUG_H
#define DEBUG_H

#include <stdint.h>

#include "ui/screen.h"

#include "ui/button.h"
#include "ui/listbox.h"
#include "ui/slider.h"

//-----------------------------------------------------------------------------
class DebugScreen : public ScreenBase
{
  public:
    DebugScreen(void);
    virtual ~DebugScreen();

    virtual ScreenBase::ScreenResult Update(void);
    virtual void                     Draw(DisplayController *display);
    virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

    static void Push(void);

  private:

   Button *mReturnButton;
   Button *mQuitButton;

   ImageHandle mImage;
   double mPrevSysTime;
};






#endif //DEBUG_H
