#ifndef ENGINECONSULT_H
#define ENGINECONSULT_H

#include "display.h"

#include "ui/listbox.h"
#include "ui/button.h"
#include "ui/slider.h"

#include "consult/consult.h"

enum ConsultMode { ShowInvalid,
                   ShowLiveData,
                   ShowCodes,
                   ShowAdjustments
                 };

enum Control_IDs { LiveData,
                   CheckCodes,
                   Adjustments,
                   MusicPlayerScreen,
                   DebugScreen,
                   TestSlider,
                   TestButton1,
                   TestButton2,
                   TestButton3,
                   TestButton4,
                 };

enum Test_ID {  Test_Invalid = 0,
                Test_SetCoolantTemp = 100,
                Test_SetFuelInjection,
                Test_SetTiming,
                Test_SetIACVOpening,
                Test_SetIACV_FICDSolenoid,
                Test_SetPowerBalance,
                Test_SetFuelPumpRelay,
                Test_SetSelfLearn,
                Test_SetCoolantFan
            };

static const char *sTestNames[] = {  "Set Coolant Temp",
                                     "Adjust Fueling",
                                     "Adjust Timing",
                                     "Set IACV",
                                     "Test FICD",
                                     "Cylinder Balance",
                                     "Fuel Pump",
                                     "Reset Trims",
                                     "Test Coolant Fan"
                                  };

//-------------------------------------------------------
class EngineScreenConsult : public ScreenBase
{
  public:
    EngineScreenConsult(void);
   ~EngineScreenConsult();

   static void Show(void);

   virtual ScreenBase::ScreenResult Update(void);
   virtual void                     Draw(DisplayController *display);
   virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

  private:
    Test_ID mCurrentTest;

    ConsultInterface *mConsultInterface;

    ConsultMode mConsultMode;
    int mConsultStartError;
    int mConsultStopError;

    double mCodesRefreshTime;

    Button *mDebugScreenButton;
    Button *mMusicPlayerScreenButton;

    Button *mLiveDataButton;
    Button *mCheckCodesButton;
    Button *mAdjustmentsButton;

    Slider *mTestValAdj;
    Button *mTestToggleAdj[4];

    bool  mTestValValid;
    bool  mTestValChanged;
    const char *mTestValUnits;

    uint8_t mCylinderEnable;

    Button *mCoolantTemp;
    Button *mFuelInjection;
    Button *mTiming;
    Button *mIACV;
    Button *mFICD;
    Button *mBalance;
    Button *mFuelPump;
    Button *mSelfLearn;
    Button *mCoolantFan;

    void SetTest(Test_ID test);
    void SetConsultMode(ConsultMode mode);

    void DrawLiveData(DisplayController *display);
    void DrawCodes(DisplayController *display);
    void DrawAdjustments(DisplayController *display);

    void TestsUI(UIEvent *event);
    void LiveDataUI(UIEvent *event);

 };



#endif // ENGINECONSULT_H
