#ifndef GETTEXT_H
#define GETTEXT_H

#include <stdint.h>

#include "ui/screen.h"

#define MAX_TEXT_LEN 512
enum GTResultStatus { GT_OK, GT_IN_PROGRESS, GT_CANCELLED };

//----------------------------------------------------------------------------
class GetTextScreen : public ScreenBase
{
 public:

    GetTextScreen(bool push, const char *title, uint16_t id, const char *preload);
    virtual ~GetTextScreen();

    virtual ScreenBase::ScreenResult Update(void);
    virtual void                     Draw(DisplayController *display);
    virtual ScreenBase::ScreenResult ProcessUIEvent(UIEvent *event);

    static void Push(const char *title, uint16_t id, const char *preload);

 private:

   bool      mShiftState;
   char     *mTitle;
   uint16_t  mID;
   char      mText[MAX_TEXT_LEN + 1];
   unsigned  mCursorPos;

    void     DrawKeyboard(DisplayController *display);
    unsigned UpdateKeyboard(UIEvent *event);
};

#endif // GETTEXT_H
