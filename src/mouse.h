#ifndef MOUSE_H
#define MOUSE_H

#include <stdint.h>

#include "ui/inputevent.h"

class Mouse
{
 public:
   Mouse(void);
  ~Mouse();

   int  Init(const char *device);

   void RegisterCallback(InputEventCallback callback, void *context);

   int  Update(void);

   unsigned LastReport(InputEvent &dest) const { dest = mPreviousEvent; return mTotalEvents; }

   bool Ready(void) const { return mDevFD > 0; }

 private:

   int InitInternal(void);

   char *mDeviceName;
   int mDevFD;
   InputEventCallback mCallback;
   void              *mCallbackContext;

   float mCursorX;
   float mCursorY;

   InputEvent mPreviousEvent;

   unsigned mTotalEvents;
};

#endif // MOUSE_H