#include "radio_1276_lora.h"
#include "util/util.h"

#include <math.h>
#include <unistd.h>

#include "log/log.h"

//-----------------------------------------------------
LoraRadio::LoraRadio(void) :
    mSpi(NULL),
    mRadioMode(Sleep),
    mFrequency(434000000),
    mNewRXTimeout(0)
{
    mSpi = new SpiDevice("LoraRadio", "/dev/spidev0.0");
    mSpi->SetMaxHz(10000);

    mTXPacket.len = 0;
    mTXPacket.pos = 0;
    mTXPacket.data = new uint8_t[2048];
    mTXPacket.allocLen = 2048;

    mTXRequest = false;

    pthread_mutex_init(&mRXLock, NULL);

    mEventThreadQuit = false;
    mEventThread = std::thread(&EventThreadFunc, (void *) this);
    pthread_setname_np(mEventThread.native_handle(), "LoRa Events");
}

//-----------------------------------------------------
LoraRadio::~LoraRadio()
{
    mEventThreadQuit = true;
    mEventThread.join();

    delete[] mTXPacket.data;

    while (!mRXPackets.empty())
    {
        delete[] mRXPackets.front().data;
        mRXPackets.pop();
    }
    delete mSpi;
    pthread_mutex_destroy(&mRXLock);
}

//-----------------------------------------------------
RadioStatus LoraRadio::GetStatus(void)
{
    switch (mRadioMode)
    {
        case Sleep:   return Ready;
        case Standby: return Ready;
        case RX:      return BusyRX;
        case TX:      return BusyTX;
        case CAD:     return BusyRX;
    }
    return Error;
}

//-----------------------------------------------------
// Queue a transmission for the next TX time
// returns failure if there's already a packet queued
bool LoraRadio::Send(const uint8_t *data, unsigned len)
{
    if (TXPacketQueued()) return false;
    mTXPacket.len = len;
    mTXPacket.pos = 0;
    if (len > mTXPacket.allocLen)
    {
        delete[] mTXPacket.data;
        mTXPacket.allocLen = ((len + 2047) & ~2047);
        mTXPacket.data = new uint8_t[mTXPacket.allocLen];
    }
    memcpy(mTXPacket.data, data, len);
    mTXRequest = true;
    return true;
}

//-----------------------------------------------------
bool LoraRadio::TXPacketQueued(void)
{
    return mTXPacket.len != 0;
}

//-----------------------------------------------------
// caller is responsible for deleting the data after use!
bool LoraRadio::Receive(RadioRXPacket &rx)
{
    pthread_mutex_lock(&mRXLock);
    if (mRXPackets.empty())
    {
        pthread_mutex_unlock(&mRXLock);
        return false;
    }

    rx.data = mRXPackets.front().data;
    rx.len  = mRXPackets.front().len;
    rx.rssi = mRXPackets.front().rssi;
    rx.snr  = mRXPackets.front().snr;

    mRXPackets.pop();
    pthread_mutex_unlock(&mRXLock);
    return true;
}

//-----------------------------------------------------
bool LoraRadio::SetFrequency(uint32_t Hz)
{
    // Our radio RF is tuned for the 433MHz band, use the North America 70cm band
    if (Hz < 430000000) return false;
    if (Hz > 449999999) return false;
    mFrequency = Hz;
    return true;
}

//-----------------------------------------------------
void LoraRadio::SetRXTimeout(unsigned symbols)
{
    mNewRXTimeout = symbols;
}

//-----------------------------------------------------
bool LoraRadio::InitRadio(void)
{
    AddRegister("OpMode",                   REG_LR_OPMODE, RFLR_OPMODE_LONGRANGEMODE_ON);
    AddRegister("Frequency MSB",            REG_LR_FRFMSB);
    AddRegister("Frequency Mid",            REG_LR_FRFMID);
    AddRegister("Frequency LSB",            REG_LR_FRFLSB);
    AddRegister("PowerAmp Config",          REG_LR_PACONFIG);
    AddRegister("PowerAmp Ramp",            REG_LR_PARAMP);
    AddRegister("Over Current Protection",  REG_LR_OCP);
    AddRegister("LNA Settings",             REG_LR_LNA);
    //AddRegister("TX Fifo Base Address",     REG_LR_FIFOTXBASEADDR);
    //AddRegister("RX Fifo Base Address",     REG_LR_FIFORXBASEADDR);
    AddRegister("IRQ Masks",                REG_LR_IRQFLAGSMASK);
    //AddRegister("IRQ Clears",               REG_LR_IRQFLAGS); // Don't cache the dynamic flags!
    AddRegister("Modem Config 1",           REG_LR_MODEMCONFIG1);
    AddRegister("Modem Config 2",           REG_LR_MODEMCONFIG2);
    AddRegister("Preamble Symbol Timeout",  REG_LR_SYMBTIMEOUTLSB);
    AddRegister("Preamble Len MSB",         REG_LR_PREAMBLEMSB);
    AddRegister("Preamble Len LSB",         REG_LR_PREAMBLELSB);
    AddRegister("Payload Max Length",       REG_LR_PAYLOADMAXLENGTH);
    AddRegister("Hop Period",               REG_LR_HOPPERIOD);
    AddRegister("Modem Config 3",           REG_LR_MODEMCONFIG3);
    AddRegister("PPM Correction",           0x27);
    AddRegister("IF Frequency 1",           REG_LR_TEST2F);
    AddRegister("IF Frequency 2",           REG_LR_TEST30);
    AddRegister("Detector Optimize SF6",    REG_LR_DETECTOPTIMIZE);
    AddRegister("Invert IQ 1",              REG_LR_INVERTIQ);
    AddRegister("High BW Optimize 1",       REG_LR_TEST36);
    AddRegister("Detection Theashol SF6",   REG_LR_DETECTIONTHRESHOLD);
    AddRegister("Lora Sync Word",           REG_LR_SYNCWORD);
    AddRegister("High BW Optimize 2",       REG_LR_TEST3A);
    AddRegister("Invert IQ 2",              REG_LR_INVERTIQ2);
    AddRegister("DIO Mapping 1",            REG_LR_DIOMAPPING1);
    AddRegister("DIO Mapping 2",            REG_LR_DIOMAPPING2);
    AddRegister("Version",                  REG_LR_VERSION);

    // put modem in Lora mode FIRST!

    WriteRegisterHW(REG_LR_OPMODE, RFLR_OPMODE_SLEEP);
    usleep(1000);
    log_debug("Opmode after setting is 0x%02X", ReadRegisterHW(REG_LR_OPMODE));

    WriteRegisterHW(REG_LR_OPMODE, RFLR_OPMODE_LONGRANGEMODE_ON | RFLR_OPMODE_SLEEP);
    usleep(1000);
    log_debug("Opmode after setting is 0x%02X", ReadRegisterHW(REG_LR_OPMODE));

    WriteRegisterHW(REG_LR_OPMODE, RFLR_OPMODE_LONGRANGEMODE_ON | RFLR_OPMODE_STANDBY);
    usleep(1000);
    log_debug("Opmode after setting is 0x%02X", ReadRegisterHW(REG_LR_OPMODE));

    ReadHWRegisters(true);

    //while (!mEventThreadQuit) ReadRegisterHW(REG_LR_OPMODE);

    //exit(0);
    SetOpMode(RFLR_OPMODE_STANDBY);
    mRadioMode = Standby;
    UpdateHWRegisters();

    SetOpLFMode(RFLR_OPMODE_FREQMODE_ACCESS_LF);
    //SetOpModulation(); // ??
    SetOpLRMode(RFLR_OPMODE_LONGRANGEMODE_ON);

    UpdateHWRegisters();

    SetOpMode(RFLR_OPMODE_STANDBY);
    mRadioMode = Standby;
    UpdateHWRegisters();

    SetPATXPower(15); // set to max power
    SetPAMaxPower(0x05); //13.8db
    SetPABoost(RFLR_PACONFIG_PASELECT_RFO);
    SetPARampTime(RFLR_PARAMP_0040_US);
    SetPARampTXBF(RFLR_PARAMP_TXBANDFORCE_AUTO);

    SetOCPEnable(RFLR_OCP_ON);
    SetOCPTrim(RFLR_OCP_TRIM_100_MA);

    SetLNAGain(RFLR_LNA_GAIN_G1); // Max gain
    SetLNALFBoost(RFLR_LNA_BOOST_LF_DEFAULT);
    SetLNAHFBoost(RFLR_LNA_BOOST_HF_OFF);

    SetModemConfigBW(sBandwidth);
    SetModemConfigCR(sCodingRate);
    SetModemConfigIH(RFLR_MODEMCONFIG1_IMPLICITHEADER_OFF);

    SetModemConfigSF(sSpreadingFactor);
    SetModemConfigCRC(RFLR_MODEMCONFIG2_RXPAYLOADCRC_ON);
    SetModemConfigTXCont(RFLR_MODEMCONFIG2_TXCONTINUOUSMODE_OFF);

    SetIRQFlagMask(0x00);  // mask nothing
    SetIRQFlagClear(0xFF); // clear everything

    SetSymTimeout(100); // What's a good number?  I think this is ~1s with 125KHz BW, SF9

    UpdateHWRegisters();

    SetOpMode(RFLR_OPMODE_STANDBY);
    mRadioMode = Standby;
    UpdateHWRegisters();

    return true;
}

//-----------------------------------------------------
void LoraRadio::SendQueuedPacket(void)
{
    log_trace("Start Send()");

    if (mTXPacket.pos == 0)
    {
        mTXPacket.time = MonotonicTime();
        mTXRequest     = false;
    }

    // copy packet into lora buffer
    SetFifoAddress(sTXFifoBase);
    SetFifoTXAddress(sTXFifoBase);
    SetDIOMapping03(RFLR_DIOMAPPING1_DIO0_01);
    SetDIOMapping45(RFLR_DIOMAPPING2_DIO5_00);

    UpdateHWRegisters(); // this should do nothing, but for correctness...

    unsigned sendLen = mTXPacket.len - mTXPacket.pos;
    if (sendLen > sMaxLoraTXSize) sendLen = sMaxLoraTXSize;

    mSpi->Send(REG_LR_FIFO | 0x80, mTXPacket.data + mTXPacket.pos, sendLen);
    mTXPacket.pos += sendLen;

    SetPayloadLen(sendLen);

    //SetIRQFlagMask(~RFLR_IRQFLAGS_TXDONE_MASK);
    //SetIRQFlagClear(0xFF); // clear everything
    SetIRQFlagClear(RFLR_IRQFLAGS_TXDONE);
    // Set radio to transmit
    SetOpMode(RFLR_OPMODE_TRANSMITTER);
    mRadioMode = TX;

    // Make it go!
    UpdateHWRegisters();

    double sendTime = PacketDuration(sendLen);
    log_debug("Packet len %d send time is %5.3fs", sendLen, sendTime);
}

//-----------------------------------------------------
void LoraRadio::HandleTXEvent(void)
{
    // Clear IRQ Flag
    //SetIRQFlagClear(RFLR_IRQFLAGS_TXDONE);
    SetIRQFlagClear(0xFF);
    UpdateHWRegisters();

    if (mTXPacket.pos < mTXPacket.len)
    {
        SendQueuedPacket();
    }
    else
    {
        double txTime = MonotonicTime() - mTXPacket.time;
        log_debug("TX Packet of %d bytes took %f to transmit", mTXPacket.len, txTime);
        mTXPacket.len = 0;
        mTXPacket.pos = 0;
        memset(mTXPacket.data, 0, mTXPacket.allocLen);
        SetOpMode(RFLR_OPMODE_STANDBY);
        mRadioMode = Standby;
        log_trace("Finshed Send()");

    }
}

//-----------------------------------------------------
void LoraRadio::StartListen(void)
{
    log_trace("StartListen()");

    if (mNewRXTimeout != 0)
    {
        SetSymTimeout(mNewRXTimeout);
        mNewRXTimeout = 0;
    }

    // Setup the RX write pointer
    SetFifoAddress(sRXFifoBase);
    SetFifoRXAddress(sRXFifoBase);

    // setup the interrupt
    //SetIRQFlagMask(~(RFLR_IRQFLAGS_RXDONE_MASK | RFLR_IRQFLAGS_RXTIMEOUT_MASK));
    SetIRQFlagClear(0xFF); // clear everything

    SetDIOMapping03(RFLR_DIOMAPPING1_DIO0_00 | RFLR_DIOMAPPING1_DIO1_00);
    SetDIOMapping45(RFLR_DIOMAPPING2_DIO5_00);

    // Set radio to transmit
    SetOpMode(RFLR_OPMODE_RECEIVER_SINGLE);
    mRadioMode = RX;

    // Make it go!
    UpdateHWRegisters();
    log_trace("StartListen() at %f", MonotonicTime());
}

//-----------------------------------------------------
void LoraRadio::HandleRXEvent(void)
{
    // Clear IRQ Flag
    //SetIRQFlagClear(RFLR_IRQFLAGS_RXDONE | RFLR_IRQFLAGS_RXTIMEOUT);
    SetIRQFlagClear(0xFF);
    UpdateHWRegisters();

    // Read the packet
    LoraPacket p;

    p.len  = ReadRegister(REG_LR_RXNBBYTES);
    p.data = new uint8_t[p.len];
    p.pos  = 0;
    p.time = SystemTimeUTC();
    p.allocLen = p.len;

    SetFifoAddress(sRXFifoBase);
    UpdateHWRegisters();
    mSpi->Receive(REG_LR_FIFO, p.data, p.len);

    p.snr  =    0.25F * (float)ReadRegister(REG_LR_PKTSNRVALUE);
    p.rssi = -164.0F + ((16.0F/15.0F)*(float)ReadRegister(REG_LR_PKTRSSIVALUE)) + p.snr;

    pthread_mutex_lock(&mRXLock);
    mRXPackets.push(p);
    pthread_mutex_unlock(&mRXLock);

    EndListen();
}

//-----------------------------------------------------
void LoraRadio::EndListen(void)
{
    SetIRQFlagClear(0xFF); // clear everything
    SetOpMode(RFLR_OPMODE_STANDBY);
    mRadioMode = Standby;
    UpdateHWRegisters();
    usleep(2000);
}

//-----------------------------------------------------
void LoraRadio::EventThread(void)
{
    InitRadio();

    while (!mEventThreadQuit)
    {
        RadioStatus status = GetStatus();
        if (status == Ready)
        {
            if (TXPacketQueued())
            {
                // Send a new packet
                SendQueuedPacket();
            }
            else
            {
                // Listen for a packet
                LoraRadio::StartListen();
            }
        }

        int irqs = ReadRegister(REG_LR_IRQFLAGS);

        int modemStatus = ReadRegister(REG_LR_MODEMSTAT);
        while (!mEventThreadQuit)
        {
            if (mTXRequest && ((modemStatus & 0x06) == 0))
            {
                if (mRadioMode == RX)
                {
                    EndListen();
                }
                break;
            }

            usleep(10000);

            irqs = ReadRegister(REG_LR_IRQFLAGS);
            log_trace("IRQs 0x%02X  Mode %d", irqs, (int) mRadioMode);

            if (mRadioMode == RX)
            {
                if ((irqs & RFLR_IRQFLAGS_RXDONE) != 0)
                {
                    log_trace("Got RXDone");
                    HandleRXEvent();
                    break;
                }
                if ((irqs & RFLR_IRQFLAGS_RXTIMEOUT) != 0)
                {
                    log_trace("Got RXTimeout");
                    EndListen();
                    break;
                }
            }
            else if (mRadioMode == TX)
            {
                if ((irqs & RFLR_IRQFLAGS_TXDONE) != 0)
                {
                    log_trace("Got TxDone");
                    HandleTXEvent();
                    break;
                }
            }

            modemStatus = ReadRegister(REG_LR_MODEMSTAT);
        }
    }
}

//-----------------------------------------------------
void LoraRadio::EventThreadFunc(void *context)
{
    LoraRadio *radio = (LoraRadio *) context;
    radio->EventThread();
}

//-----------------------------------------------------
void LoraRadio::AddRegister(const char *name, uint8_t reg, uint8_t sleepBits)
{
    LoraRegister lr;
    lr.reg       = reg;
    lr.sleepBits = sleepBits;
    lr.name      = strdup(name);
    lr.swValue   = 0;
    lr.hwValue   = 0;

    mRegs[reg] = lr;
}

//-----------------------------------------------------
void LoraRadio::UpdateHWRegisters(void)
{
    SetOptimizedErrata(mRadioMode);

    bool dirty = false;

    std::map<uint8_t, LoraRegister>::iterator it;
    for (it = mRegs.begin(); it != mRegs.end(); it++)
    {
        // Don't update opmode yet
        if ((*it).second.reg == REG_LR_OPMODE) continue;

        if ((*it).second.Dirty())
        {
            dirty = true;
            break;
        }
    }

    if (dirty)
    {
        uint8_t opmode = ReadRegister(REG_LR_OPMODE);
        uint8_t mode   = opmode & (~RFLR_OPMODE_MASK);
        if (mode != RFLR_OPMODE_SLEEP)
        {
            // put device in standby
            uint8_t newMode = (opmode & RFLR_OPMODE_MASK) | RFLR_OPMODE_STANDBY;
            WriteRegisterHW(REG_LR_OPMODE, newMode);
            usleep(1000);
            log_trace("Update put radio to standby  %02X", newMode);
        }

        std::map<uint8_t, LoraRegister>::iterator it;
        for (it = mRegs.begin(); it != mRegs.end(); it++)
        {
            // Don't update opmode yet
            if ((*it).second.reg == REG_LR_OPMODE) continue;

            (*it).second.Update(this);
        }
    }

    mRegs[REG_LR_OPMODE].Update(this, true);
    usleep(1000);
}

//-----------------------------------------------------
void LoraRadio::ReadHWRegisters(bool clearSet)
{
    std::map<uint8_t, LoraRegister>::iterator it;
    for (it = mRegs.begin(); it != mRegs.end(); it++)
    {
        uint8_t val = ReadRegisterHW((*it).second.reg);
        if (clearSet) (*it).second.swValue = val;
        (*it).second.hwValue = val;
        usleep(1000);
    }

#if 0
    for (it = mRegs.begin(); it != mRegs.end(); it++)
    {
        log_debug("Register 0x%02X %s is 0x%02X", (*it).second.reg, (*it).second.name, (*it).second.hwValue);
    }
#endif
}

//-----------------------------------------------------
void LoraRadio::WriteRegister(uint8_t reg, uint8_t val)
{
    std::map<uint8_t, LoraRegister>::iterator found = mRegs.find(reg);
    if (found != mRegs.end())
    {
        (*found).second.swValue = val;
    }
    else
    {
        UpdateHWRegisters(); // Preserve set ordering
        WriteRegisterHW(reg, val);
    }
}

//-----------------------------------------------------
uint8_t LoraRadio::ReadRegister(uint8_t reg)
{
    std::map<uint8_t, LoraRegister>::iterator found = mRegs.find(reg);
    if (found != mRegs.end())
    {
        return (*found).second.swValue;;
    }
    return ReadRegisterHW(reg);
}

//-----------------------------------------------------
void LoraRadio::WriteRegisterHW(uint8_t reg, uint8_t val)
{
    reg |= 0x80; // set write bit
    mSpi->Send(reg, &val, 1);
    usleep(1000);
}

//-----------------------------------------------------
uint8_t LoraRadio::ReadRegisterHW(uint8_t reg)
{
    uint8_t val = 0;
    mSpi->Receive(reg, &val, 1);
    return val;
}

//-----------------------------------------------------
// Deal with errata
// Also sets the frequency registers
void LoraRadio::SetOptimizedErrata(RadioMode mode)
{
    uint8_t bw = GetModemConfigBW();

    double freq = mFrequency;

    // Sensitivity Opt for 500KHz BW
    switch (bw)
    {
        case RFLR_MODEMCONFIG1_BW_7_81_KHZ:
        case RFLR_MODEMCONFIG1_BW_10_41_KHZ:
        case RFLR_MODEMCONFIG1_BW_15_62_KHZ:
        case RFLR_MODEMCONFIG1_BW_20_83_KHZ:
        case RFLR_MODEMCONFIG1_BW_31_25_KHZ:
        case RFLR_MODEMCONFIG1_BW_41_66_KHZ:
        case RFLR_MODEMCONFIG1_BW_62_50_KHZ:
        case RFLR_MODEMCONFIG1_BW_125_KHZ:
        case RFLR_MODEMCONFIG1_BW_250_KHZ:
            WriteRegister(REG_LR_TEST36, 0x03);
            break;

        case RFLR_MODEMCONFIG1_BW_500_KHZ:
            WriteRegister(REG_LR_TEST36, 0x03);
            WriteRegister(REG_LR_TEST3A, 0x65);
            break;
    }

    // Deal with IF and frequency offsets for spurious RX
    if (mode != TX)
    {
        // For spurious RX
        switch (bw)
        {
            case RFLR_MODEMCONFIG1_BW_7_81_KHZ:
                WriteRegister(REG_LR_DETECTOPTIMIZE, ReadRegister(REG_LR_DETECTOPTIMIZE) & ~0x80);
                WriteRegister(REG_LR_TEST2F, 0x48);
                WriteRegister(REG_LR_TEST30, 0x00);
                break;

            case RFLR_MODEMCONFIG1_BW_10_41_KHZ:
            case RFLR_MODEMCONFIG1_BW_15_62_KHZ:
            case RFLR_MODEMCONFIG1_BW_20_83_KHZ:
            case RFLR_MODEMCONFIG1_BW_31_25_KHZ:
            case RFLR_MODEMCONFIG1_BW_41_66_KHZ:
            case RFLR_MODEMCONFIG1_BW_62_50_KHZ:
                WriteRegister(REG_LR_DETECTOPTIMIZE, ReadRegister(REG_LR_DETECTOPTIMIZE) & ~0x80);
                WriteRegister(REG_LR_TEST2F, 0x44);
                WriteRegister(REG_LR_TEST30, 0x00);
                break;

            case RFLR_MODEMCONFIG1_BW_125_KHZ:
            case RFLR_MODEMCONFIG1_BW_250_KHZ:
                WriteRegister(REG_LR_DETECTOPTIMIZE, ReadRegister(REG_LR_DETECTOPTIMIZE) & ~0x80);
                break;

           case RFLR_MODEMCONFIG1_BW_500_KHZ:
                WriteRegister(REG_LR_DETECTOPTIMIZE, ReadRegister(REG_LR_DETECTOPTIMIZE) | 0x80);
                break;
        }

        switch (bw)
        {
            // Bandwidths under 62.5KHz need to be offset by the IF freq offset
            case RFLR_MODEMCONFIG1_BW_7_81_KHZ:  freq +=  7810.0; break;
            case RFLR_MODEMCONFIG1_BW_10_41_KHZ: freq += 10400.0; break;
            case RFLR_MODEMCONFIG1_BW_15_62_KHZ: freq += 15620.0; break;
            case RFLR_MODEMCONFIG1_BW_20_83_KHZ: freq += 20830.0; break;
            case RFLR_MODEMCONFIG1_BW_31_25_KHZ: freq += 31250.0; break;
            case RFLR_MODEMCONFIG1_BW_41_66_KHZ: freq += 41660.0; break;

            case RFLR_MODEMCONFIG1_BW_62_50_KHZ:
            case RFLR_MODEMCONFIG1_BW_125_KHZ:
            case RFLR_MODEMCONFIG1_BW_250_KHZ:
            case RFLR_MODEMCONFIG1_BW_500_KHZ:
                break;
        }
    }

    const double FrequencyResolution = 32000000.0 / 524288.0; // 32MHz / 2^19
    unsigned ft = (unsigned) (freq / FrequencyResolution);

    log_trace("FT set to %d (0x%06X) or %f", ft, ft, ft*FrequencyResolution);
    WriteRegister(REG_LR_FRFLSB, ft & 0xFF);
    ft >>= 8;
    WriteRegister(REG_LR_FRFMID, ft & 0xFF);
    ft >>= 8;
    WriteRegister(REG_LR_FRFMSB, ft & 0xFF);
}

//----------------------------------------------------
double LoraRadio::PacketDuration(unsigned len)
{
    double bw = 0.0;
    // REMARK: When using LoRa modem only bandwidths 125, 250 and 500 kHz are supported
    switch (GetModemConfigBW())
    {
        case RFLR_MODEMCONFIG1_BW_7_81_KHZ:  bw =   7810.0; break;
        case RFLR_MODEMCONFIG1_BW_10_41_KHZ: bw =  10400.0; break;
        case RFLR_MODEMCONFIG1_BW_15_62_KHZ: bw =  15620.0; break;
        case RFLR_MODEMCONFIG1_BW_20_83_KHZ: bw =  20830.0; break;
        case RFLR_MODEMCONFIG1_BW_31_25_KHZ: bw =  31250.0; break;
        case RFLR_MODEMCONFIG1_BW_41_66_KHZ: bw =  41660.0; break;
        case RFLR_MODEMCONFIG1_BW_62_50_KHZ: bw =  62500.0; break;
        case RFLR_MODEMCONFIG1_BW_125_KHZ:   bw = 125000.0; break;
        case RFLR_MODEMCONFIG1_BW_250_KHZ:   bw = 250000.0; break;
        case RFLR_MODEMCONFIG1_BW_500_KHZ:   bw = 500000.0; break;
    }

    // Symbol rate : time for one symbol (secs)
    unsigned sf = (GetModemConfigSF() >> 4);
    double rs = bw / (1 << sf);
    double ts = 1.0 / rs;
    // time of preamble
    double tPreamble = (GetPreambleLen() + 4.25) * ts;
    // Symbol length of payload and time
    double tmp = ceil(((8 * len) - (4 * sf) + 28 + GetCRCSize() - GetHeaderLen()) /
                       (double)(4 * sf - ((GetModemConfigLDROpt() != 0) ? 2 : 0))) *
                 ((GetModemConfigCR() >> 1) + 4);
    double nPayload = 8 + ((tmp > 0) ? tmp : 0);
    double tPayload = nPayload * ts;

    return tPreamble + tPayload;
}
