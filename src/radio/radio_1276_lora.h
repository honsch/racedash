#ifndef RADIO_1276_LORA_H
#define RADIO_1276_LORA_H

#include "radio.h"
#include "spi/spi.h"
#include "gpio/gpio.h"
#include "sx1276Regs-LoRa.h"

#include <string.h>

#include <thread>
#include <queue>
#include <map>

class LoraRadio : public Radio
{
  public:

    LoraRadio(void);
    virtual ~LoraRadio();

    RadioStatus GetStatus(void);

    bool        Send(const uint8_t *data, unsigned len);

    // caller is responsible for deleting the data after use!
    bool        Receive(RadioRXPacket &rx);

    bool        TXPacketQueued(void);

    // 430MHz-450MHz is 70cm band in US & Canada
    bool        SetFrequency(uint32_t Hz);

    void SetRXTimeout(unsigned symbols);

  protected:

    bool InitRadio(void);

    SpiDevice         *mSpi;

    enum RadioMode { Sleep, Standby, TX, RX, CAD };
    RadioMode mRadioMode;

    uint32_t mFrequency;
    uint32_t mNewRXTimeout;

    struct LoraPacket
    {
        double   time;
        uint8_t *data;
        unsigned len;
        float    rssi;
        float    snr;
        unsigned pos;
        unsigned allocLen;
    };

    LoraPacket mTXPacket;
    volatile bool mTXRequest;
    std::queue<LoraPacket> mRXPackets;
    pthread_mutex_t mRXLock;

    void EventThread(void);
    static void EventThreadFunc(void *context);
    bool mEventThreadQuit;
    std::thread mEventThread;

    static const unsigned sMaxLoraTXSize = 250;

    static const uint8_t sSpreadingFactor = RFLR_MODEMCONFIG2_SF_9;
    static const uint8_t sCodingRate      = RFLR_MODEMCONFIG1_CODINGRATE_4_5;
    static const uint8_t sBandwidth       = RFLR_MODEMCONFIG1_BW_500_KHZ;
    static const uint8_t sTXFifoBase      = 0x00;
    static const uint8_t sRXFifoBase      = 0x00;

    void SendQueuedPacket(void);
    void HandleTXEvent(void);

    void StartListen(void);
    void HandleRXEvent(void);
    void EndListen(void);

    double PacketDuration(unsigned len);

    void    UpdateHWRegisters(void);
    void    ReadHWRegisters(bool clearSet);

    void    WriteRegister(uint8_t reg, uint8_t val);
    uint8_t ReadRegister(uint8_t reg);

    struct LoraRegister
    {
        bool Dirty(void)   { return swValue != hwValue; }
        bool NeedSleep(void) { return (((swValue ^ hwValue) & sleepBits) != 0); }
        void Update(LoraRadio *lr, bool force = false)
        {
            if (force || Dirty())
            {
                lr->WriteRegisterHW(reg, swValue);
                hwValue = swValue;
                //if (reg) printf("LoraRegister %s (0x%02X) set to 0x%02X\n", name, reg, swValue);
            }
        }

        uint8_t reg;
        uint8_t swValue; // the SW view
        uint8_t hwValue; // what has been sent to the HW
        uint8_t sleepBits; // mask of bits that can only change in sleep mode
        const char *name;
    };

    void    WriteRegisterHW(uint8_t reg, uint8_t val);
    uint8_t ReadRegisterHW(uint8_t reg);

    void AddRegister(const char *name, uint8_t reg, uint8_t sleepBits = 0x00);
    std::map<uint8_t, LoraRegister> mRegs;

    uint8_t GetModemConfigBW(void)     { return ReadRegister(REG_LR_MODEMCONFIG1) & ~RFLR_MODEMCONFIG1_BW_MASK; }
    uint8_t GetModemConfigSF(void)     { return ReadRegister(REG_LR_MODEMCONFIG2) & ~RFLR_MODEMCONFIG2_SF_MASK; }
    uint8_t GetModemConfigLDROpt(void) { return ReadRegister(REG_LR_MODEMCONFIG3) & ~RFLR_MODEMCONFIG3_LOWDATARATEOPTIMIZE_MASK; }
    uint8_t GetModemConfigCR(void)     { return ReadRegister(REG_LR_MODEMCONFIG1) & ~RFLR_MODEMCONFIG1_CODINGRATE_MASK; }

    uint16_t GetPreambleLen(void)   { return (((uint16_t)ReadRegister(REG_LR_PREAMBLEMSB)) << 8) + ReadRegister(REG_LR_PREAMBLELSB); }
    uint8_t  GetHeaderLen(void)     { return 20 - (20 * (ReadRegister(REG_LR_MODEMCONFIG1) & ~RFLR_MODEMCONFIG1_IMPLICITHEADER_MASK)); }
    uint8_t  GetCRCSize(void)       { return ((ReadRegister(REG_LR_MODEMCONFIG2) & ~RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK) != 0) ? 16 : 0; } //Bug? shouldn't this be 2 or zero?

    void SetOpMode(uint8_t mode)       { WriteRegister(REG_LR_OPMODE, (ReadRegister(REG_LR_OPMODE) & RFLR_OPMODE_MASK)                 | mode); }
    void SetOpLFMode(uint8_t mode)     { WriteRegister(REG_LR_OPMODE, (ReadRegister(REG_LR_OPMODE) & RFLR_OPMODE_FREQMODE_ACCESS_MASK) | mode); }
    void SetOpModulation(uint8_t mode) { WriteRegister(REG_LR_OPMODE, (ReadRegister(REG_LR_OPMODE) & RFLR_OPMODE_ACCESSSHAREDREG_MASK) | mode); }
    void SetOpLRMode(uint8_t mode)     { WriteRegister(REG_LR_OPMODE, (ReadRegister(REG_LR_OPMODE) & RFLR_OPMODE_LONGRANGEMODE_MASK)   | mode); }

    void SetPATXPower(uint8_t mode)    { WriteRegister(REG_LR_PACONFIG, (ReadRegister(REG_LR_PACONFIG) & RFLR_PACONFIG_OUTPUTPOWER_MASK) | (mode << 0)); }
    void SetPAMaxPower(uint8_t mode)   { WriteRegister(REG_LR_PACONFIG, (ReadRegister(REG_LR_PACONFIG) & RFLR_PACONFIG_MAX_POWER_MASK)   | (mode << 4)); }
    void SetPABoost(uint8_t mode)      { WriteRegister(REG_LR_PACONFIG, (ReadRegister(REG_LR_PACONFIG) & RFLR_PACONFIG_PASELECT_MASK)    | mode); }

    void SetPARampTime(uint8_t mode)   { WriteRegister(REG_LR_PARAMP, (ReadRegister(REG_LR_PARAMP) & RFLR_PARAMP_MASK) | mode); }
    void SetPARampTXBF(uint8_t mode)   { WriteRegister(REG_LR_PARAMP, (ReadRegister(REG_LR_PARAMP) & RFLR_PARAMP_TXBANDFORCE_MASK) | mode); }

    void SetOCPEnable(uint8_t mode)    { WriteRegister(REG_LR_OCP, (ReadRegister(REG_LR_OCP) & RFLR_OCP_MASK)      | mode); }
    void SetOCPTrim(uint8_t mode)      { WriteRegister(REG_LR_OCP, (ReadRegister(REG_LR_OCP) & RFLR_OCP_TRIM_MASK) | mode); }

    void SetLNAGain(uint8_t mode)      { WriteRegister(REG_LR_LNA, (ReadRegister(REG_LR_LNA) & RFLR_LNA_GAIN_MASK)     | mode); }
    void SetLNALFBoost(uint8_t mode)   { WriteRegister(REG_LR_LNA, (ReadRegister(REG_LR_LNA) & RFLR_LNA_BOOST_LF_MASK) | mode); }
    void SetLNAHFBoost(uint8_t mode)   { WriteRegister(REG_LR_LNA, (ReadRegister(REG_LR_LNA) & RFLR_LNA_BOOST_HF_MASK) | mode); }

    void SetFifoAddress(uint8_t addr)   { WriteRegister(REG_LR_FIFOADDRPTR,    addr); }
    void SetFifoTXAddress(uint8_t addr) { WriteRegister(REG_LR_FIFOTXBASEADDR, addr); }
    void SetFifoRXAddress(uint8_t addr) { WriteRegister(REG_LR_FIFORXBASEADDR, addr); }

    void SetIRQFlagMask(uint8_t mask)   { WriteRegister(REG_LR_IRQFLAGSMASK, mask); }
    void SetIRQFlagClear(uint8_t mask)  { WriteRegister(REG_LR_IRQFLAGS,     mask); }


    void SetModemConfigBW(uint8_t mode) { WriteRegister(REG_LR_MODEMCONFIG1, (ReadRegister(REG_LR_MODEMCONFIG1) & RFLR_MODEMCONFIG1_BW_MASK)           | mode); }
    void SetModemConfigCR(uint8_t mode) { WriteRegister(REG_LR_MODEMCONFIG1, (ReadRegister(REG_LR_MODEMCONFIG1) & RFLR_MODEMCONFIG1_CODINGRATE_MASK)   | mode); }
    void SetModemConfigIH(uint8_t mode) { WriteRegister(REG_LR_MODEMCONFIG1, (ReadRegister(REG_LR_MODEMCONFIG1) & RFLR_MODEMCONFIG1_IMPLICITHEADER_MASK) | mode); }

    void SetModemConfigSF(uint8_t mode)     { WriteRegister(REG_LR_MODEMCONFIG2, (ReadRegister(REG_LR_MODEMCONFIG2) & RFLR_MODEMCONFIG2_SF_MASK) | mode); }
    void SetModemConfigCRC(uint8_t mode)    { WriteRegister(REG_LR_MODEMCONFIG2, (ReadRegister(REG_LR_MODEMCONFIG2) & RFLR_MODEMCONFIG2_RXPAYLOADCRC_MASK) | mode); }
    void SetModemConfigTXCont(uint8_t mode) { WriteRegister(REG_LR_MODEMCONFIG2, (ReadRegister(REG_LR_MODEMCONFIG2) & RFLR_MODEMCONFIG2_TXCONTINUOUSMODE_MASK) | mode); }

    void SetModemConfigAGCAuto(uint8_t mode) { WriteRegister(REG_LR_MODEMCONFIG3, (ReadRegister(REG_LR_MODEMCONFIG3) & RFLR_MODEMCONFIG3_AGCAUTO_MASK) | mode); }
    void SetModemConfigLDROpt(uint8_t mode)  { WriteRegister(REG_LR_MODEMCONFIG3, (ReadRegister(REG_LR_MODEMCONFIG3) & RFLR_MODEMCONFIG3_LOWDATARATEOPTIMIZE_MASK) | mode); }

    void SetSymTimeout(uint16_t syms)
    {
        syms &= 0x3FF;
        WriteRegister(REG_LR_MODEMCONFIG2, (ReadRegister(REG_LR_MODEMCONFIG2) & RFLR_MODEMCONFIG2_SYMBTIMEOUTMSB_MASK ) | (syms >> 8));
        WriteRegister(REG_LR_SYMBTIMEOUTLSB, syms & 0xFF);
    }

    void SetPreambleLen(uint16_t len)
    {
        WriteRegister(REG_LR_PREAMBLEMSB, len >> 8);
        WriteRegister(REG_LR_PREAMBLELSB, len & 0xFF);
    }

    void SetPayloadLen(uint8_t len)    { WriteRegister(REG_LR_PAYLOADLENGTH,    len); }
    void SetPayloadMaxLen(uint8_t len) { WriteRegister(REG_LR_PAYLOADMAXLENGTH, len); }

    void SetHopPeriod(uint8_t len) { WriteRegister(REG_LR_HOPPERIOD, len); }

    void SetDIOMapping03(uint8_t map) { WriteRegister(REG_LR_DIOMAPPING1, map); }
    void SetDIOMapping45(uint8_t map) { WriteRegister(REG_LR_DIOMAPPING2, map); }



    void SetOptimizedErrata(RadioMode mode);

};


#endif // RADIO_1276_LORA_H
