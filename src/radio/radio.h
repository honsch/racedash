#ifndef RADIO_H
#define RADIO_H

#include <stdint.h>

enum RadioStatus { Ready, BusyTX, BusyRX, Error };

struct RadioRXPacket
{
    uint8_t *data;
    unsigned len;
    float rssi;
    float snr;

    void Free(void) { delete[] data; }
};

class Radio
{
  public:
    virtual RadioStatus GetStatus(void) = 0;
    virtual bool        Send(const uint8_t *data, unsigned len) = 0;
    virtual bool        Receive(RadioRXPacket &rx) = 0;

    virtual bool        SetFrequency(uint32_t Hz) = 0;

  protected:


};




#endif //RADIO_H