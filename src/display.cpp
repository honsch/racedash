/*********************************************************

   DisplayController Driver code

*************************************************************/

#include "display.h"

#include <linux/kd.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <math.h>

#include <gbm.h>

#include "nanovg_gl.h"
#include "nanovg_gl_utils.h"

#include "util/util.h"

#include "log/log.h"

DisplayController *gDisplay = NULL;

//-------------------------------------------------------------
DisplayController::DisplayController(const char *device, unsigned desiredWidth, unsigned desiredHeight, DisplayRotation rotation)
{
    mInitOK = false;
    mDrmFD = open(device, O_RDWR);

    log_debug("mDrmFD %d", mDrmFD);

    // Get DRM info and choose display info
    drmModeRes *res = drmModeGetResources(mDrmFD);
    if (res == NULL)
    {
        log_error("Can't get drmModeGetResources.");
        return;
    }

    // Loop though all available connectors and try to find one closest to the desired size
    unsigned closestDiff = 0xFFFFFF;
    log_debug("There are %d DRM connectors", res->count_connectors);
    int a;
    for (a = 0; a < res->count_connectors; a++)
    {
        drmModeConnector *conn = drmModeGetConnector(mDrmFD, res->connectors[a]);
        if (conn->connection != DRM_MODE_CONNECTED)
        {
            log_debug("DRM Connector %d not connected", a);
            drmModeFreeConnector(conn);
            conn = NULL;
            continue;
        }

        log_debug("Checking connector %d", conn->connector_id);
        log_debug("  Has %d modes", conn->count_modes);

        int m;
        for (m = 0; m < conn->count_modes; m++)
        {
            const drmModeModeInfo &mode = conn->modes[m];

            log_debug("  Mode %d: %s", m, mode.name);

            unsigned diff = (((mode.hdisplay - desiredWidth)  * (mode.hdisplay - desiredWidth)) +
                             ((mode.vdisplay - desiredHeight) * (mode.vdisplay - desiredHeight)));

            if (diff < closestDiff)
            {
                log_debug("  Mode is now best match");
                closestDiff      =  diff;
                mChosenConnector = *conn;
                mChosenMode      =  mode;
                int e;
                for (e = 0; e < res->count_encoders; e++)
                {
                    drmModeEncoder *encoder = drmModeGetEncoder(mDrmFD, res->encoders[e]);
                    if (encoder->encoder_id != conn->encoder_id)
                    {
                        drmModeFreeEncoder(encoder);
                        continue;
                    }
                    mCrtcID          = encoder->crtc_id;
                    mChosenEncoderID = encoder->encoder_id;
                    drmModeFreeEncoder(encoder);
                    break;
                }
            }
            if (closestDiff == 0) break;
        }
        //drmModeFreeConnector(conn);
        if (closestDiff == 0) break;
    }

    drmModeFreeResources(res);

    // No modes were found
    if (closestDiff == 0xFFFFFF)
    {
        log_error("No mode close to %dx%d were found", desiredWidth, desiredHeight);
        return;
    }

    mRealDisplayWidth  = (float) mChosenMode.hdisplay;
    mRealDisplayHeight = (float) mChosenMode.vdisplay;
    mRealDisplayAspectRatio = mRealDisplayWidth / mRealDisplayHeight;

    SetDisplayRotation(rotation);

    //------------------------------------------------------------------------------
    // Init gbm
    //------------------------------------------------------------------------------
    mDisplayedBO     = NULL;
    mFramebufferNext = 0;

    mGbm        = gbm_create_device(mDrmFD);
    mGbmFormat  = GBM_FORMAT_ARGB8888;
    mGbmSurface = gbm_surface_create(mGbm, mRealDisplayWidth, mRealDisplayHeight, mGbmFormat, GBM_BO_USE_SCANOUT | GBM_BO_USE_RENDERING);

    if (mGbmSurface == NULL)
    {
        log_error("Failed to create gbm surface");
        return;
    }

    //  Show the selected mode's info
    log_debug("CRTC ID   : %d", mCrtcID);
    log_debug("EncoderID : %d", mChosenEncoderID);
    log_debug("Connector info:");
    log_debug("  Connector ID      : %d", mChosenConnector.connector_id);
    log_debug("  Encoder ID        : %d", mChosenConnector.encoder_id);
    log_debug("  Connector type    : %d", mChosenConnector.connector_type);
    log_debug("  Connector Type ID : %d", mChosenConnector.connector_type_id);
    log_debug("  Connection        : %d", mChosenConnector.connection);

    log_debug("Mode info:");
    log_debug("  Name     : %s", mChosenMode.name);
    log_debug("  hdisplay : %d", mChosenMode.hdisplay);
    log_debug("  vdisplay : %d", mChosenMode.vdisplay);
    log_debug("  vrefresh : %d", mChosenMode.vrefresh);
    log_debug("  Clock    : %d", mChosenMode.clock);
    log_debug("  flags    : 0x%04X", mChosenMode.flags);
    log_debug("  type     : %d", mChosenMode.type);

    //------------------------------------------------------------------------------
    // Init EGL
    //------------------------------------------------------------------------------

    mInitOK = InitGL(mDrmFD);

    //------------------------------------------------------------------------------
    // Done basic setup
    //------------------------------------------------------------------------------

    mLastSwapTime = SystemTimeUTC();
    gDisplay = this;
}

//-------------------------------------------------------------
DisplayController::~DisplayController()
{
   eglTerminate(mEGLDisplay);
   close(mDrmFD);
}

void DisplayController::SetDisplayRotation(DisplayRotation rotation)
{
    mDisplayRotation = rotation;

    switch (mDisplayRotation)
    {
        case None:
            mDisplayWidth  = mRealDisplayWidth;
            mDisplayHeight = mRealDisplayHeight;
            mDisplayRotationAngle = 0.0;
            mDisplayTranslateX = 0.0F;
            mDisplayTranslateY = 0.0F;
            break;

        case CCW_90:
            mDisplayWidth  = mRealDisplayHeight;
            mDisplayHeight = mRealDisplayWidth;
            mDisplayRotationAngle  = M_PI + M_PI_2 - .002;
            mDisplayTranslateX = -mDisplayWidth;
            mDisplayTranslateY = 0.0F;
            break;

        case CW_90:
            mDisplayWidth  = mRealDisplayHeight;
            mDisplayHeight = mRealDisplayWidth;
            mDisplayRotationAngle = M_PI_2 - .002;
            mDisplayTranslateX    = 0.0;;
            mDisplayTranslateY    = -mDisplayHeight;
            break;

        case CW_180:
            mDisplayWidth  = mRealDisplayWidth;
            mDisplayHeight = mRealDisplayHeight;
            mDisplayRotationAngle = M_PI - 0.002;
            mDisplayTranslateX = -mDisplayWidth;
            mDisplayTranslateY = -mDisplayHeight;
            break;
    }

    mDisplayAspectRatio = mDisplayWidth / mDisplayHeight;
}

//-------------------------------------------------------------
void DisplayController::FramebufferDestroyCallback(struct gbm_bo *bo, void *data)
{
    FramebufferData *fbd = (FramebufferData *)gbm_bo_get_user_data(bo);
    if (fbd == NULL) return;
    if (fbd->fbID != 0)
    {
        int fd = gbm_device_get_fd(gbm_bo_get_device(bo));
        drmModeRmFB(fd, fbd->fbID);
    }
}

//-------------------------------------------------------------
bool DisplayController::InitGL(int displayFD)
{
    // 1. Initialize EGL
    PFNEGLGETPLATFORMDISPLAYEXTPROC eglGetPlatformDisplayEXT = NULL;
    eglGetPlatformDisplayEXT = (PFNEGLGETPLATFORMDISPLAYEXTPROC) eglGetProcAddress("eglGetPlatformDisplayEXT");
    if (eglGetPlatformDisplayEXT == NULL)
    {
        log_error("Failed to get eglGetPlatformDisplayEXT");
        return false;
    }

    mEGLDisplay = eglGetPlatformDisplayEXT(EGL_PLATFORM_GBM_KHR, mGbm, NULL);
    if (EGL_NO_DISPLAY == mEGLDisplay)
    {
        log_error("eglGetDisplay returned EGL_NO_DISPLAY");
        return false;
    }

    EGLint major, minor;
    if (eglInitialize(mEGLDisplay, &major, &minor) == EGL_FALSE)
    {
        log_error("Unable to initialize EGL using eglInitialize");
        return false;
    }


    log_info("EGL              %d.%d", major, minor);
    log_info("EGL_VENDOR:      %s", eglQueryString(mEGLDisplay, EGL_VENDOR));
    log_info("EGL_VERSION:     %s", eglQueryString(mEGLDisplay, EGL_VERSION));
    log_info("EGL_EXTENSIONS:  %s", eglQueryString(mEGLDisplay, EGL_EXTENSIONS));
    log_info("EGL_CLIENT_APIS: %s", eglQueryString(mEGLDisplay, EGL_CLIENT_APIS));


    // 1. Bind the API
    if (EGL_FALSE == eglBindAPI(EGL_OPENGL_ES_API))
    {
        log_error("Unable to bind EGL API to OpenGL");
        return false;
    }

    // 2. Select an appropriate configuration
    EGLint numConfigs;
    EGLConfig *eglCfg;
    EGLint const attrib_list[] =
    {
      EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
      EGL_RED_SIZE,      8,
      EGL_GREEN_SIZE,    8,
      EGL_BLUE_SIZE,     8,
      EGL_ALPHA_SIZE,    8,
      EGL_DEPTH_SIZE,   24,
      EGL_STENCIL_SIZE,  8,
      EGL_RENDERABLE_TYPE,    EGL_OPENGL_ES2_BIT,
      EGL_NONE
    };

    // get how many configs there are
    if (EGL_FALSE == eglChooseConfig(mEGLDisplay, attrib_list, NULL, 0, &numConfigs))
    {
        log_error("eglChooseConfig get num failed");
        return false;
    }

    // get the configs
    eglCfg = (EGLConfig *)malloc(sizeof(EGLConfig) * numConfigs);
    memset(eglCfg, 0, sizeof(EGLConfig) * numConfigs);

    if (EGL_FALSE == eglChooseConfig(mEGLDisplay, attrib_list, eglCfg, numConfigs, &numConfigs))
    {
        log_error("eglChooseConfig get configs failed");
        return false;
    }

    log_debug("eglChooseConfig returned %d configs", numConfigs);

    // Find one that matches the visual_id we want
    int chosenEGLConfig = -1;
    int a;
    for (a = 0; a < numConfigs; a++)
    {
        //DumpEGLConfig(eglCfg[a]);
        EGLint id;
        eglGetConfigAttrib(mEGLDisplay, eglCfg[a], EGL_NATIVE_VISUAL_ID, &id);
        if (id == mGbmFormat)
        {
            chosenEGLConfig = a;
            break;
        }
    }

    if (chosenEGLConfig < 0)
    {
        log_error("Unable to find matching EGL config");
        return false;
    }

    // 3. Create a context
    EGLint const context_attrib_list[] =
    {
      EGL_CONTEXT_CLIENT_VERSION, 2,
      EGL_NONE
    };

    mEGLContext = eglCreateContext(mEGLDisplay, eglCfg[chosenEGLConfig], EGL_NO_CONTEXT, context_attrib_list);
    if (EGL_NO_CONTEXT == mEGLContext)
    {
        log_error("Unable to initialize EGLContext");
        return false;
    }

    // 4. Create surface
    mEGLSurface = eglCreateWindowSurface(mEGLDisplay, eglCfg[chosenEGLConfig], (EGLNativeWindowType)mGbmSurface, NULL);
    if (mEGLSurface == EGL_NO_SURFACE)
    {
        log_error("failed to create egl surface");
        return false;
    }

    // 5. Make it current
    if (EGL_FALSE == eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext))
    {
        log_error("Unable to make EGL context current");
        return false;
    }

    // lastly, setup NanoVG
    mVG = nvgCreateGLES2(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
    if (mVG == NULL)
    {
        log_error("NanoVG init failed");
    }

    mFonts[Font_Mono]           = nvgCreateFont(mVG, "Mono",           "fonts/mono.ttf");
    mFonts[Font_Bookman]        = nvgCreateFont(mVG, "Bookman",        "fonts/Bookman.ttf");
    mFonts[Font_LiberationSans] = nvgCreateFont(mVG, "LiberationSans", "fonts/LiberationSans-Regular.ttf");
    mFonts[Font_PlayerSymbols]  = nvgCreateFont(mVG, "PlayerSymbols",  "fonts/PlayerSymbols.ttf");
    mFonts[Font_Cursors]        = nvgCreateFont(mVG, "Cursors",        "fonts/Cursors.ttf");

    // from now on use your OpenGL context

    glClearDepthf(1.0F);
    glClearStencil(0x00);
    glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // Setup the display
    eglSwapBuffers(mEGLDisplay, mEGLSurface);

    struct gbm_bo *nextBO = gbm_surface_lock_front_buffer(mGbmSurface);
    FramebufferData *nextFB = FramebufferFromBO(nextBO);
    int ret = drmModeSetCrtc(mDrmFD, mCrtcID, nextFB->fbID, 0, 0, &mChosenConnector.connector_id, 1, &mChosenMode);
    if (ret != 0)
    {
        log_error("Failed to set CRTC: %s", strerror(errno));
    }
    mDisplayedBO = nextBO;

    free(eglCfg);

    log_info("Display init success");
    return true;
}

//-------------------------------------------------------------
DisplayController::FramebufferData *DisplayController::FramebufferFromBO(struct gbm_bo *bo)
{
    if (bo == NULL) return NULL;

    FramebufferData *fbd = (FramebufferData *)gbm_bo_get_user_data(bo);
    if (fbd != NULL) return fbd;

    if (mFramebufferNext >= MAX_FRAMEBUFFERS)
    {
        log_error("Too many framebuffers!");
        return NULL;
    }

    // find the next fbd to use
    fbd = &mFramebuffers[mFramebufferNext];
    ++mFramebufferNext;

    // Add this to the framebuffers possible list
    uint32_t handles[4] = { gbm_bo_get_handle(bo).u32, 0, 0, 0 };
    uint32_t strides[4] = { gbm_bo_get_stride(bo),     0, 0, 0 };
    uint32_t offsets[4] = { 0, 0, 0, 0 };
    int ret = drmModeAddFB2(mDrmFD, mRealDisplayWidth, mRealDisplayHeight, mGbmFormat,
                            handles, strides, offsets, &fbd->fbID, 0);

    if (ret != 0)
    {
        log_debug("drmModeAddFB2 returned %s", strerror(errno));
    }

    gbm_bo_set_user_data(bo, fbd, &DisplayController::FramebufferDestroyCallback);
    return fbd;
}


//-------------------------------------------------------------
void DisplayController::BeginFrame(void)
{
    glViewport(0, 0, mRealDisplayWidth, mRealDisplayHeight);
    nvgBeginFrame(mVG, mRealDisplayWidth, mRealDisplayHeight, 1.0F);

    nvgRotate(mVG, mDisplayRotationAngle);
    nvgTranslate(mVG, mDisplayTranslateX, mDisplayTranslateY);

    mTransformStack.clear();
    PushCurrentTransform();

    //nvgScale(mVG, mDisplayWidth, mDisplayHeight);
}

//-------------------------------------------------------------
static void page_flip_handler(int fd, unsigned int frame,
      unsigned int sec, unsigned int usec, void *data)
{
  /* suppress 'unused parameter' warnings */
  (void)fd, (void)frame, (void)sec, (void)usec;

  int *waiting_for_flip = (int *)data;
  *waiting_for_flip = 0;
}

//-------------------------------------------------------------
int DisplayController::Swap(void)
{
    drmEventContext evctx;
    evctx.version            = 2;
    evctx.vblank_handler     = NULL;
    evctx.page_flip_handler  = &page_flip_handler;
    evctx.page_flip_handler2 = NULL;
    evctx.sequence_handler   = NULL;

    nvgResetScissor(mVG);
    nvgResetTransform(mVG);
    nvgEndFrame(mVG);
    int err = eglSwapBuffers(mEGLDisplay, mEGLSurface);

    struct gbm_bo *nextBO = gbm_surface_lock_front_buffer(mGbmSurface);
    FramebufferData *nextFB = FramebufferFromBO(nextBO);
    log_trace("NextBO: %8p   nextFB: %8p   nextFBID: %d", nextBO, nextFB, nextFB->fbID);

    //int ret = drmModePageFlipTarget(mDrmFD, mCrtcID, nextFB->fbID, DRM_MODE_PAGE_FLIP_TARGET_RELATIVE, NULL, 1);
    //int ret = drmModeSetCrtc(mDrmFD, mCrtcID, nextFB->fbID, 0, 0, &mChosenConnector.connector_id, 1, &mChosenMode);
    int waiting_for_flip = 1;
    int ret = drmModePageFlip(mDrmFD, mCrtcID, nextFB->fbID, DRM_MODE_PAGE_FLIP_EVENT, &waiting_for_flip);

    if (ret != 0)
    {
        log_warn("Failed to flip: %s", strerror(errno));
    }

    fd_set fds;
    while (waiting_for_flip)
    {
        FD_ZERO(&fds);
        FD_SET(0, &fds);
        FD_SET(mDrmFD, &fds);

        ret = select(mDrmFD + 1, &fds, NULL, NULL, NULL);
        if (ret < 0)
        {
            log_warn("select err: %s", strerror(errno));
            //return ret;
        }
        else if (ret == 0)
        {
            log_warn("select timeout!");
            return -1;
        }
        else if (FD_ISSET(0, &fds))
        {
            //log_warn("user interrupted!");
            //return 0;
        }
        drmHandleEvent(mDrmFD, &evctx);
    }

    gbm_surface_release_buffer(mGbmSurface, mDisplayedBO);
    mDisplayedBO = nextBO;

    double swapTime = SystemTimeUTC();
    mLastFrameTime = swapTime - mLastSwapTime;
    mLastSwapTime  = swapTime;

   return err;
}

//-------------------------------------------------------------
void DisplayController::Clear(uint32_t color)
{
    float r = RedF(color);
    float g = GreenF(color);
    float b = BlueF(color);
    float a = AlphaF(color);

    glScissor(0, 0, mRealDisplayWidth, mRealDisplayHeight),
    glClearColor(r, g, b, a);
    glClearDepthf(1.0F);
    glClearStencil(0x00);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}


//------------------------------------------------------------------------------------------------------------------------
void DisplayController::DrawString(const char *s, float xpos, float ypos, uint32_t _fgcolor, float fontsize, int fontFace)
{
    xpos *= mDisplayWidth;
    ypos *= mDisplayHeight;

    int faceID = mFonts[Font_Mono];
    if ((fontFace >= 0) && (fontFace < _Font_Max)) faceID = mFonts[fontFace];
    nvgFontSize(mVG, fontsize);
    nvgFontFaceId(mVG, faceID);
    nvgTextAlign(mVG, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);

    nvgFillColor(mVG, nvgRGBAf(RedF(_fgcolor), GreenF(_fgcolor), BlueF(_fgcolor), AlphaF(_fgcolor)));
    nvgText(mVG, xpos, ypos, s, NULL);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::DrawStringRect(const char *s, float xpos, float ypos, float width, float height, uint32_t _fgcolor, float fontsize, int fontFace )
{
    xpos *= mDisplayWidth;
    ypos *= mDisplayHeight;

    int faceID = mFonts[Font_Mono];
    if ((fontFace >= 0) && (fontFace < _Font_Max)) faceID = mFonts[fontFace];
    nvgFontSize(mVG, fontsize);
    nvgFontFaceId(mVG, faceID);
    nvgTextAlign(mVG, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);

    NVGtextRow rows;
    nvgTextBreakLines(mVG, s, NULL, width * mDisplayWidth, &rows, 1);

    nvgFillColor(mVG, nvgRGBAf(RedF(_fgcolor), GreenF(_fgcolor), BlueF(_fgcolor), AlphaF(_fgcolor)));
    nvgText(mVG, xpos, ypos, rows.start, rows.end);
}


//------------------------------------------------------------------------------------------------------------------------
void DisplayController::Polygon(VertexData *verts, unsigned count, float thickness, uint32_t outlineColor, uint32_t fillColor)
{
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(outlineColor), GreenF(outlineColor), BlueF(outlineColor), AlphaF(outlineColor)));
    nvgFillColor(mVG, nvgRGBAf(RedF(fillColor), GreenF(fillColor), BlueF(fillColor), AlphaF(fillColor)));
    nvgStrokeWidth(mVG, thickness);

    nvgMoveTo(mVG, verts[count-1].x * mDisplayWidth, verts[count-1].y * mDisplayHeight);

    unsigned a;
    for (a = 0; a < count; a++)
    {
        nvgLineTo(mVG, verts[a].x * mDisplayWidth, verts[a].y * mDisplayHeight);
    }
    if (fillColor != CLEAR) nvgFill(mVG);
    nvgStroke(mVG);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::Line(LineData *lines, unsigned count, float thickness, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);

    unsigned a;
    for (a = 0; a < count; a++)
    {
        nvgMoveTo(mVG, lines[a].x1 * mDisplayWidth, lines[a].y1 * mDisplayHeight);
        nvgLineTo(mVG, lines[a].x2 * mDisplayWidth, lines[a].y2 * mDisplayHeight);
    }
    nvgStroke(mVG);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::Line(float x1, float y1, float x2, float y2, float thickness, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);
    nvgMoveTo(mVG, x1 * mDisplayWidth, y1 * mDisplayHeight);
    nvgLineTo(mVG, x2 * mDisplayWidth, y2 * mDisplayHeight);
    nvgStroke(mVG);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::Rect(float x1, float y1, float width, float height, float thickness, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);
    nvgRect(mVG, x1 * mDisplayWidth, y1 * mDisplayHeight, width * mDisplayWidth, height * mDisplayHeight);
    nvgStroke(mVG);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::RectRounded(float x1, float y1, float width, float height, float radius, float thickness, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);
    nvgRoundedRect(mVG, x1 * mDisplayWidth, y1 * mDisplayHeight, width * mDisplayWidth, height * mDisplayHeight, radius * mDisplayWidth);
    nvgStroke(mVG);
    //nvgFill(mVG);
}

//-------------------------------------------------------------------
void DisplayController::Circle(float cx, float cy, float r, float thickness, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);
    nvgCircle(mVG, cx * mDisplayWidth, cy * mDisplayHeight, r * mDisplayWidth);
    nvgStroke(mVG);
}

//-------------------------------------------------------------------
void DisplayController::Ellipse(float cx, float cy, float rx, float ry, float thickness, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgFillColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);
    nvgEllipse(mVG, cx * mDisplayWidth, cy * mDisplayHeight, rx * mDisplayWidth, ry * mDisplayHeight);
    nvgStroke(mVG);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::RectFilled(float x1, float y1, float width, float height, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgFillColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgRect(mVG, x1 * mDisplayWidth, y1 * mDisplayHeight, width * mDisplayWidth, height * mDisplayHeight);
    nvgFill(mVG);
}

//------------------------------------------------------------------------------------------------------------------------
void DisplayController::RectRoundedFilled(float x1, float y1, float width, float height, float radius, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgFillColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgRoundedRect(mVG, x1 * mDisplayWidth, y1 * mDisplayHeight, width * mDisplayWidth, height * mDisplayHeight, radius * mDisplayWidth);
    nvgFill(mVG);
}

//-------------------------------------------------------------------
void DisplayController::CircleFilled(float cx, float cy, float r, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgFillColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgCircle(mVG, cx * mDisplayWidth, cy * mDisplayHeight, r * mDisplayWidth);
    nvgFill(mVG);
}

//-------------------------------------------------------------------
void DisplayController::EllipseFilled(float cx, float cy, float rx, float ry, uint32_t color)
{
    nvgBeginPath(mVG);
    nvgFillColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgEllipse(mVG, cx * mDisplayWidth, cy * mDisplayHeight, rx * mDisplayWidth, ry * mDisplayHeight);
    nvgFill(mVG);
}

//-------------------------------------------------------------------
void DisplayController::Arc(float cx, float cy, float r, float radStart, float radEnd, int dir, float thickness, uint32_t color)
{
    int nvgDir = (dir < 0) ? NVG_CW : NVG_CCW;
    nvgBeginPath(mVG);
    nvgStrokeColor(mVG, nvgRGBAf(RedF(color), GreenF(color), BlueF(color), AlphaF(color)));
    nvgStrokeWidth(mVG, thickness);
    nvgArc(mVG, cx * mDisplayWidth, cy * mDisplayHeight, r * mDisplayWidth, radStart, radEnd, nvgDir);
    nvgStroke(mVG);
}

//-------------------------------------------------------------------
void DisplayController::GetFontSize(float fontsize, float &width, float &height, int fontFace)
{
    width  = 0.0F;
    height = 0.0F;

    int faceID = mFonts[Font_Mono];
    if ((fontFace >= 0) && (fontFace < _Font_Max)) faceID = mFonts[fontFace];

    nvgFontSize(mVG, fontsize);
    nvgFontFaceId(mVG, faceID);
    nvgTextAlign(mVG, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);

    float bounds[4];

    nvgTextBounds(mVG, 0.0F, 0.0F, "W", NULL, bounds);

    width  = bounds[2] / mDisplayWidth;
    height = bounds[3] / mDisplayHeight;
}

//-------------------------------------------------------------------
void DisplayController::GetStringExtents(float fontsize, int fontFace, const char *string, float &width, float &height)
{
    width  = 0.0F;
    height = 0.0F;

    if (string == NULL) string = "W";  // use a single wide char if there's no string
    int faceID = mFonts[Font_Mono];
    if ((fontFace >= 0) && (fontFace < _Font_Max)) faceID = mFonts[fontFace];

    nvgFontSize(mVG, fontsize);
    nvgFontFaceId(mVG, faceID);
    nvgTextAlign(mVG, NVG_ALIGN_LEFT | NVG_ALIGN_TOP);

    float bounds[4];

    nvgTextBounds(mVG, 0.0F, 0.0F, string, NULL, bounds);

    width  = bounds[2] / mDisplayWidth;
    height = bounds[3] / mDisplayHeight;
}

//-------------------------------------------------------------------
ImageHandle DisplayController::LoadImage(const char *fname)
{
    Image *image = new Image;
    image->mHandle = nvgCreateImage(mVG, fname, 0);
    if (image->mHandle < 0)
    {
        log_warn("LoadImage(%s) failed %d", fname, image->mHandle);
        delete image;
        return NULL;
    }
    nvgImageSize(mVG, image->mHandle, &image->mWidth, &image->mHeight);
    log_info("LoadImage(%s) OK %d   %d %d", fname, image->mHandle, image->mWidth, image->mHeight);

    return image;
}

//-------------------------------------------------------------------
void DisplayController::UnloadImage(ImageHandle handle)
{
    if (handle == NULL) return;
    Image *image = (Image *) handle;
    nvgDeleteImage(mVG, image->mHandle);
}

//-------------------------------------------------------------------
void DisplayController::DrawImage(ImageHandle handle, float x, float y, float alpha)
{
    if (handle == NULL) return;
    Image *image = (Image *) handle;
    NVGpaint pattern = nvgImagePattern(mVG, 0.0F, 0.0F, image->mWidth, image->mHeight, 0.0F, image->mHandle, alpha);

    nvgBeginPath(mVG);
    nvgRect(mVG, x * mDisplayWidth, y * mDisplayHeight, image->mWidth, image->mHeight);
    nvgFillPaint(mVG, pattern);
    nvgFill(mVG);
}

//-------------------------------------------------------------------
void DisplayController::RotateCoord(float &x, float &y) const
{
    float outx = 0, outy = 0;
    switch (mDisplayRotation)
    {
        case None:
            outx = x;
            outy = y;
            break;

        case CCW_90:
            outx = 1.0F - y;
            outy = x;
            break;

        case CW_90:
            outx = y;
            outy = 1.0F - x;
            break;

        case CW_180:
            outx = 1.0F - x;
            outy = 1.0F - y;
            break;

    }
    x = outx;
    y = outy;
}

//-------------------------------------------------------------------
void DisplayController::PopTransform(void)
{
    mTransformStack.pop_back();
    nvgSetCurrentTransform(mVG, mTransformStack[mTransformStack.size() - 1].t);
}

//-------------------------------------------------------------------
void DisplayController::PushScale(float sx, float sy)
{
    nvgScale(mVG, sx, sy);
    PushCurrentTransform();
}

//-------------------------------------------------------------------
void DisplayController::PushTranslate(float tx, float ty)
{
    nvgTranslate(mVG, tx * mDisplayWidth, ty * mDisplayHeight);
    PushCurrentTransform();
}

//-------------------------------------------------------------------
void DisplayController::PushRotation(float angleRad)
{
    nvgRotate(mVG, angleRad);
    PushCurrentTransform();
}

//-------------------------------------------------------------------
void DisplayController::DumpEGLConfig(void *conf)
{
    EGLConfig eglCfg = (EGLConfig)conf;
    EGLint r, g, b, a, depth, stencil, configID, native;
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_CONFIG_ID,    &configID);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_RED_SIZE,     &r);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_GREEN_SIZE,   &g);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_BLUE_SIZE,    &b);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_ALPHA_SIZE,   &a);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_DEPTH_SIZE,   &depth);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_STENCIL_SIZE, &stencil);
    eglGetConfigAttrib(mEGLDisplay, eglCfg, EGL_NATIVE_VISUAL_ID, &native);

    log_info("EGL Config %d:", configID);
    log_info("   R Bits:        %d", r);
    log_info("   G Bits:        %d", g);
    log_info("   B Bits:        %d", b);
    log_info("   A Bits:        %d", a);
    log_info("   Depth Bits:    %d", depth);
    log_info("   Stencil Bits:  %d", stencil);
    log_info("   Native Format: %08X  (GBM Format: %08X)", native, mGbmFormat);
}
