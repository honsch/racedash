#include "mtkgps.h"

#include <unistd.h>
#include <stdio.h>
#include <time.h>

//------------------------------------------
MtkGPS::MtkGPS(const char *device, NMEAInterface::NavBuffer &navBuffer) :
    mNmea(device, 9600, navBuffer)
{
    if (!mNmea.Initialized()) return;

    printf("NMEAInterface initialized\n");

    // Setup the MTK to the 'best' settings for what we're doing
    mNmea.SendString("PMTK251,115200"); // fast baud rate
    mNmea.ResetBaudrate(115200);
    usleep(500000);
    mNmea.SendString("PMTK220,200"); // Set update to 5Hz in milliseconds
    mNmea.SendString("PMTK313,1");   // enable SBAS (WAAS)
    mNmea.SendString("PMTK314,0,1,0,1,5,5,0,0,0,0,0,0,0,0,0,0,0,0,0"); // enable RMC,VTG,GGA,GSA,GSV
    mNmea.SendString("PMTK397,0.2"); // 0.2 m/s minimum speed
}

//------------------------------------------
MtkGPS::~MtkGPS()
{

}

