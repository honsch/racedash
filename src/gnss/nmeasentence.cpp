#include "nmeasentence.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <termios.h>
#include <ctype.h>
#include <stdint.h>

#include <syslog.h>

#include "util/util.h"


//--------------------------------------------------------------
NMEASentence::NMEASentence(void) :
    mSystemTime(0.0),
    mNumSentenceValues(0)
{
    memset(mSentence, 0, cMaxSentenceLen + 1);
    memset(mValues, 0, sizeof(char *) * cMaxSentenceValues);
}

//--------------------------------------------------------------
// Make a copy making sure the mValues point to my own mSentence!
NMEASentence::NMEASentence(const NMEASentence &rhs) :
    mSystemTime(rhs.mSystemTime),
    mNumSentenceValues(rhs.mNumSentenceValues)
{
    memcpy(mSentence, rhs.mSentence, cMaxSentenceLen + 1);
    memset(mValues, 0, sizeof(char *) * cMaxSentenceValues);

    unsigned a;
    for (a = 0; a < mNumSentenceValues; a++)
    {
        mValues[a] = mSentence + (rhs.mValues[a] - rhs.mSentence);
    }
}

//--------------------------------------------------------------
NMEASentence::NMEASentence(double systemTime, const char *sentence) :
    mSystemTime(systemTime)
{
    memset(mValues, 0, sizeof(char *) * cMaxSentenceValues);
    strncpy(mSentence, sentence, cMaxSentenceLen);
    mNumSentenceValues = 0;

    unsigned len = strlen(mSentence);
    unsigned pos = 0;
    while (pos < len)
    {
        mValues[mNumSentenceValues] = &mSentence[pos];
        ++mNumSentenceValues;
        while ((pos < len) && (mSentence[pos] != ','))
        {
            ++pos;
        }
        mSentence[pos] = 0;
        ++pos;
    }
}

//--------------------------------------------------------------
NMEASentence::~NMEASentence()
{

}

//--------------------------------------------------------------
const char *NMEASentence::Value(unsigned index) const
{
    if (index > mNumSentenceValues) return "INVALID";
    return mValues[index];
}

//--------------------------------------------------------------
bool NMEASentence::ValueChars(unsigned index, char *dest, unsigned first, unsigned len) const
{
    if (dest == NULL) return false;
    *dest = 0;
    if (index > mNumSentenceValues) return false;

    unsigned vl = strlen(mValues[index]);
    if (first >= vl) return false;
    vl -= first;

    if (len == 0) len = vl;
    else if (len > vl) return false;

    memcpy(dest, mValues[index] + first, len);
    dest[len] = 0;
    return true;
}


//--------------------------------------------------------------
int NMEASentence::ValueAsInt(unsigned index) const
{
    if (index > mNumSentenceValues) return 0;
    return atoi(mValues[index]);
}

//--------------------------------------------------------------
int NMEASentence::ValueAsInt(unsigned index, unsigned first, unsigned len) const
{
    char temp[80];
    bool ok = ValueChars(index, temp, first, len);
    if (!ok) return 0;
    return atoi(temp);
}

//--------------------------------------------------------------
double NMEASentence::ValueAsDouble(unsigned index) const
{
    if (index > mNumSentenceValues) return 0.0;
    return strtod(mValues[index], NULL);
}

//--------------------------------------------------------------
double NMEASentence::ValueAsDouble(unsigned index, unsigned first, unsigned len) const
{
    char temp[80];
    bool ok = ValueChars(index, temp, first, len);
    if (!ok) return 0.0;
    return strtod(temp, NULL);
}

//--------------------------------------------------------------
double NMEASentence::ValueAsDegreesDecimal(unsigned index) const
{
    if (index > mNumSentenceValues) return 0.0;

    unsigned leadingCount = 0;
    const char *s = mValues[index];
    if (s[4] == '.') leadingCount = 2;
    if (s[5] == '.') leadingCount = 3;

    if (leadingCount == 0) return 0.0;

    double degrees = ValueAsDouble(index, 0, leadingCount);
    double minutes = ValueAsDouble(index, leadingCount);

    degrees += (minutes / 60.0);
    return degrees;
}
