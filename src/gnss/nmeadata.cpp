#include "nmeadata.h"

#include <string.h>
#include <math.h>
#include <stdio.h>

#include "util/util.h"

int  NMEAData::tmYear  = 0;
int  NMEAData::tmMonth = 0;
int  NMEAData::tmDay   = 0;

//------------------------------------------
NMEAData::NMEAData(void)
{
    mType = Invalid;
}

//------------------------------------------
void NMEAData::Clear(void)
{
    memset(this, 0, sizeof(*this));
}

//------------------------------------------
void NMEAData::Dump(void)
{
    enum NMEADataType { Invalid, GGA, GLL, GSA, GSV, MSS, RMC, VTG };

    switch (mType)
    {
        case Invalid:
            printf("NMEAData : Invalid\n");
            break;

        case GGA:
            DumpGGA();
            break;

        case GLL:
            DumpGLL();
            break;

        case GSA:
            DumpGSA();
            break;

        case GSV:
            DumpGSV();
            break;

        case MSS:
            DumpMSS();
            break;

        case RMC:
            DumpRMC();
            break;

        case VTG:
            DumpVTG();
            break;
    }

}

//------------------------------------------
bool NMEAData::Parse(const NMEASentence &s, bool &more)
{
    if      (0 == strcmp(s.Value(0), "GPGGA")) return ParseGGA(s, more);
    else if (0 == strcmp(s.Value(0), "GPGLL")) return ParseGLL(s, more);
    else if (0 == strcmp(s.Value(0), "GPGSA")) return ParseGSA(s, more);
    else if (0 == strcmp(s.Value(0), "GPGSV")) return ParseGSV(s, more);
    else if (0 == strcmp(s.Value(0), "GPMSS")) return ParseMSS(s, more);
    else if (0 == strcmp(s.Value(0), "GPRMC")) return ParseRMC(s, more);
    else if (0 == strcmp(s.Value(0), "GPVTG")) return ParseVTG(s, more);

    return false;
}

//------------------------------------------
bool NMEAData::ParseGGA(const NMEASentence &s, bool &more)
{
    more = false;
    if (s.NumValues() != 15)
    {
        printf("GGA sentence had %d values, not 15\n", s.NumValues());
        return false;
    }

    // If the date isn't set we can't use time properly
    if (tmYear == 0) return false;

    mType = GGA;
    gga.utc            = UTCTime(s, 1);
    gga.latitude       = LatLong(s, 2);
    gga.longitude      = LatLong(s, 4);
    gga.fix            = s.ValueAsInt(6);
    gga.satellitesUsed = s.ValueAsInt(7);
    gga.hdop           = s.ValueAsDouble(8);
    gga.altitude       = s.ValueAsDouble(9);
    gga.geoidSep       = s.ValueAsDouble(11);

    return true;
}

//------------------------------------------
bool NMEAData::ParseGLL(const NMEASentence &s, bool &more)
{
    more = false;
    // Maybe should be 7?  Mode is only on NMEA V3.00
    if (s.NumValues() != 8)
    {
        printf("GLL sentence had %d values, not 8\n", s.NumValues());
        return false;
    }

    gll.utc            = UTCTime(s, 5);
    gll.latitude       = LatLong(s, 1);
    gll.longitude      = LatLong(s, 3);
    gll.valid          = s.Value(6)[0] == 'A';
    gll.mode           = s.Value(7)[0];

    return true;
}

//------------------------------------------
bool NMEAData::ParseGSA(const NMEASentence &s, bool &more)
{
    more = false;
    if (s.NumValues() != 18)
    {
        printf("GSA sentence had %d values, not 18\n", s.NumValues());
        return false;
    }

    mType = GSA;
    gsa.forceManualMode = s.Value(1)[0] == 'M';
    gsa.pdop = s.ValueAsDouble(15);
    gsa.hdop = s.ValueAsDouble(16);
    gsa.vdop = s.ValueAsDouble(17);

    gsa.fix = (NMEA_GSA::FixType) s.ValueAsInt(2);

    unsigned a;
    for (a = 0; a < 12; a++)
    {
        gsa.satellitesUsed[a] = s.ValueAsInt(a + 3);
    }

    return true;
}

//------------------------------------------
bool NMEAData::ParseGSV(const NMEASentence &s, bool &more)
{
    more = false;

    int numMessages = s.ValueAsInt(1);
    int thisMessage = s.ValueAsInt(2);
    int count       = s.ValueAsInt(3);
    int startIndex = (thisMessage - 1) * 4;

    int remain = count - startIndex;
    if (remain > 4) remain = 4;

    int computedNumValues = (4 + (remain * 4));
    if (s.NumValues() != (unsigned) computedNumValues)
    {
        printf("GSV sentence %d of %d had %d values, not %d\n", thisMessage, numMessages, s.NumValues(), computedNumValues);
        return false;
    }

    if (numMessages != thisMessage) more = true;

    mType = GSV;
    gsv.numSatellites = count;

    int a;
    for (a = 0; a < remain; a++)
    {
        int baseIndex = 4 + (a * 4);
        gsv.satellites[startIndex + a].id        = s.ValueAsInt(baseIndex + 0);
        gsv.satellites[startIndex + a].elevation = s.ValueAsInt(baseIndex + 1);
        gsv.satellites[startIndex + a].azimuth   = s.ValueAsInt(baseIndex + 2);
        gsv.satellites[startIndex + a].snr       = s.ValueAsInt(baseIndex + 3);
    }

    return true;
}

//------------------------------------------
bool NMEAData::ParseMSS(const NMEASentence &s, bool &more)
{
    more = false;
    if (s.NumValues() != 6)
    {
        printf("MSS sentence had %d values, not 6\n", s.NumValues());
        return false;
    }

    mType = MSS;
    mss.strength   = s.ValueAsInt(1);
    mss.snr        = s.ValueAsInt(2);
    mss.frequency  = s.ValueAsDouble(3);
    mss.beaconRate = s.ValueAsInt(4);
    mss.channel    = s.ValueAsInt(5);

    return true;
}

//------------------------------------------
bool NMEAData::ParseRMC(const NMEASentence &s, bool &more)
{
    more = false;
    if (s.NumValues() != 13)
    {
        printf("RMC sentence had %d values, not 13\n", s.NumValues());
        return false;
    }

    mType = RMC;
    rmc.utc       = UTCTime(s, 1, 9);
    rmc.latitude  = LatLong(s, 3);
    rmc.longitude = LatLong(s, 5);
    rmc.valid     = (s.Value(2)[0] == 'A');
    rmc.mode      = s.Value(12)[0];
    rmc.speed     = s.ValueAsDouble(7) * 0.514444; // convert knots to m/sec
    rmc.course    = s.ValueAsDegreesDecimal(8);

    return true;
}

//------------------------------------------
bool NMEAData::ParseVTG(const NMEASentence &s, bool &more)
{
    more = false;
    if (s.NumValues() != 10)
    {
        printf("VTG sentence had %d values, not 10\n", s.NumValues());
        return false;
    }

    mType = VTG;
    vtg.course = s.ValueAsDegreesDecimal(1);
    vtg.speed  = s.ValueAsDouble(5) * 0.514444; // convert knots to m/sec
    vtg.mode   = s.Value(9)[0];

    return true;
}

//------------------------------------------
double NMEAData::UTCTime(const NMEASentence &s, int tIndex, int dIndex)
{
    if ((dIndex == -1) && (tmYear == 0)) return -1.0;

    // extract time and date
    struct tm now_tm;

    double seconds = s.ValueAsDouble(tIndex, 4);
    now_tm.tm_sec   = (int) seconds;
    now_tm.tm_min   = s.ValueAsInt(tIndex, 2, 2);
    now_tm.tm_hour  = s.ValueAsInt(tIndex, 0, 2);

    if (dIndex > 0)
    {
        tmDay   = s.ValueAsInt(dIndex, 0, 2);
        tmMonth = s.ValueAsInt(dIndex, 2, 2) - 1;
        tmYear  = s.ValueAsInt(dIndex, 4, 2) + 100;
    }

    now_tm.tm_mday  = tmDay;
    now_tm.tm_mon   = tmMonth;
    now_tm.tm_year  = tmYear;

    now_tm.tm_wday  =  0;       // output, day of the week
    now_tm.tm_yday  =  0;       // output, day in the year */
    now_tm.tm_isdst = -1;       // in, -1 is use TZ table, out daylight saving time

    time_t now_time = mktime(&now_tm);

    double now = Fraction(seconds) + (double)now_time;

    return now;
}

//------------------------------------------
// Assumes the next values is either N, S, E, W
double NMEAData::LatLong(const NMEASentence &s, int index)
{
    double l = s.ValueAsDegreesDecimal(index);
    char c = s.Value(index + 1)[0];
    if ((c == 'S') || (c == 'W')) l = -l;
    return l;
}

//-----------------------------------------
void NMEAData::DumpGGA(void)
{
    printf("GGA message at %f\n", gga.utc);
    printf("    Fix : %2d\n", gga.fix);
    printf("    Sats: %2d\n", gga.satellitesUsed);;
    printf("    At  : %.5f, %.5f  %.2f\n", gga.latitude, gga.longitude, gga.altitude);
    printf("    HDOP: %.2f\n", gga.hdop);
    printf("    GSep: %.2f\n", gga.geoidSep);
}

//-----------------------------------------
void NMEAData::DumpGLL(void)
{
    printf("GLL message at %f\n", gll.utc);
    printf("    Valid : %s\n", gll.valid ? "Yes" : "No");
    printf("    At    : %.5f, %.5f\n", gll.latitude, gll.longitude);
    printf("    Mode  : %c\n", gll.mode);
}

//-----------------------------------------
void NMEAData::DumpGSA(void)
{
    const char *fixes[] = { "Invalid", "No Fix", "Fix 2D", "Fix 3D" };

    printf("GSA message:\n");
    printf("    Fix  : %s  Mode : %s", fixes[(int) gsa.fix], gsa.forceManualMode ? "Manual" : "Auto");
    printf("    Sats : ");
    int a;
    for (a = 0; a < 12; a++)
    {
        int id = gsa.satellitesUsed[a];
        if (a == 0) break;
        printf("%3d, ", id);
    }
    printf("\n");
    printf("    PDOP: %.2f", gsa.pdop);
    printf("    HDOP: %.2f", gsa.hdop);
    printf("    VDOP: %.2f", gsa.vdop);
}

//-----------------------------------------
void NMEAData::DumpGSV(void)
{
    printf("GSV message with %d satellites:\n", gsv.numSatellites);
    int a;
    for (a = 0; a < gsv.numSatellites; a++)
    {
        printf("  Sat ID: %3d  SNR: %2d  Ele: %2d  Azm: %3d\n",
            gsv.satellites[a].id, gsv.satellites[a].snr, gsv.satellites[a].elevation, gsv.satellites[a].azimuth);
    }
}

//-----------------------------------------
void NMEAData::DumpMSS(void)
{
    printf("MSS Message\n");
    printf("    Strength : %3d\n", mss.strength);
    printf("    SNR      : %3d\n", mss.snr);
    printf("    Freq     : %6.2fKHz\n", mss.frequency);
    printf("    BRate    : %3d\n", mss.beaconRate);
    printf("    Channel  : %3d\n", mss.channel);
}

//-----------------------------------------
void NMEAData::DumpRMC(void)
{
    printf("RMC message at %f\n", rmc.utc);
    printf("    At    : %.5f, %.5f\n", rmc.latitude, rmc.longitude);
    printf("    Speed : %.2f\n", rmc.speed);
    printf("    Course: %.2f\n", rmc.course);
    printf("    Data  : %s\n", rmc.valid ? "Valid" : "Invalid");
    printf("    Mode  : %c\n", rmc.mode);
}

//-----------------------------------------
void NMEAData::DumpVTG(void)
{
    printf("VTG Message\n");
    printf("    Speed : %.2f\n", vtg.speed);
    printf("    Course: %.2f\n", vtg.course);
    printf("    Mode  : %c\n", vtg.mode);
}

