#ifndef MTKGPS_H
#define MTKGPS_H

#include <vector>
#include "nmeainterface.h"


//------------------------------------------
class MtkGPS
{
  public:
    MtkGPS(const char *device, NMEAInterface::NavBuffer &navBuffer);
   ~MtkGPS();

    bool GetInitialNavData(NMEANavData &out) const
    {
        return mNmea.GetInitialNavData(out);
    }

    bool GetLastNavData(NMEANavData &out) const
    {
        return mNmea.GetLastNavData(out);
    }

    int SampleAt(double time, NMEANavData &out) const
    {
        return mNmea.SampleAt(time, out);
    }

    double NewestSampleTime(void) const
    {
        return mNmea.NewestSampleTime();
    }

    void NewestSample(size_t &count, NMEANavData &out) const
    {
        return mNmea.NewestSample(count, out);
    }

  private:

    NMEAInterface mNmea;
};


#endif //MTKGPS_H