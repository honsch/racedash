#ifndef NEMAINTERFACE_H
#define NEMAINTERFACE_H

#include <stdint.h>
#include <string.h>
#include <thread>

#include "nmeasentence.h"
#include "nmeadata.h"
#include "util/circularbuffer.h"
#include "util/util.h"

#include "datalogger/datalogger.h"

//--------------------------------------------------------------
struct NMEANavData
{
    double utc;
    double latitude;
    double longitude;
    double altitude;
    double geoidSep;
    double velocity;
    double course;
    double hdop;

    #define HAS_UTC       0x0001
    #define HAS_LATLONG   0x0002
    #define HAS_ALTGSEP   0x0004
    #define HAS_VELCOURSE 0x0008
    #define HAS_HDOP      0x0010
    #define NAV_COMPLETE  (HAS_UTC | HAS_LATLONG | HAS_ALTGSEP | HAS_VELCOURSE | HAS_HDOP)


    bool Interpolate(NMEANavData &out, double time, const NMEANavData &late) const
    {
        if (time < utc) return false;
        if (time > late.utc) return false;

        double timeDiff = late.utc - utc;
        if (timeDiff < 0.0) return false;
        double t = (time - utc) / timeDiff;
        out.utc = time;
        out.latitude  = Interp(latitude,  late.latitude,  t);
        out.longitude = Interp(longitude, late.longitude, t);
        out.altitude  = Interp(altitude,  late.altitude,  t);
        out.geoidSep  = Interp(geoidSep,  late.geoidSep,  t);
        out.velocity  = Interp(velocity,  late.velocity,  t);
        out.course    = Interp(course,    late.course,    t);
        out.hdop      = Interp(hdop,      late.hdop,      t);
        return true;
    }

    inline bool IsComplete(void) const { return flags == NAV_COMPLETE; }
    inline void Clear(void)
    {
       memset(this, 0, sizeof(*this));
    }

    inline void WriteUTC(double _utc)
    {
        if ((flags & HAS_UTC) == 0)
        {
            utc = _utc;
            flags |= HAS_UTC;
        }
        else if (!Compare(utc, _utc))
        {
            printf("ERROR: NavData has conflicting times!  cur: %f  new: %f\n", utc, _utc);
        }
    }

    inline void WriteLatLong(double lat, double lng)
    {
        if ((flags & HAS_LATLONG) == 0)
        {
            latitude  = lat;
            longitude = lng;
            flags |= HAS_LATLONG;
        }
    }

    inline void WriteAltGSep(double alt, double gsep)
    {
        if ((flags & HAS_ALTGSEP) == 0)
        {
            altitude  = alt;
            geoidSep  = gsep;
            flags |= HAS_ALTGSEP;
        }
    }

    inline void WriteVelCourse(double vel, double crs)
    {
        if ((flags & HAS_VELCOURSE) == 0)
        {
            velocity = vel;
            course = crs;
            flags |= HAS_VELCOURSE;
        }
    }

    inline void WriteHdop(double _hdop)
    {
        if ((flags & HAS_HDOP) == 0)
        {
            hdop = _hdop;
            flags |= HAS_HDOP;
        }
    }

    uint32_t flags;
};


//--------------------------------------------------------------
class NMEAInterface
{
  public:
    typedef CircularBuffer<NMEANavData, 10> NavBuffer;

    NMEAInterface(const char *dev, uint32_t baudrate, NavBuffer &navBuffer);
   ~NMEAInterface();

   bool Initialized(void) const { return mDevice > 0; }
   bool ResetBaudrate(uint32_t baudrate);

   // Sends a NEMA string.  Don't include the leading $ or trailing * and CRC
   bool SendString(const char *string);

   bool GetInitialNavData(NMEANavData &out) const;
   bool GetLastNavData(NMEANavData &out) const;

   int SampleAt(double time, NMEANavData &out) const;
   double NewestSampleTime(void) const
   {
       return (mInitialNavValid) ? mNavBuffer.NewestSampleTime() : 0.0;
   }

   void NewestSample(size_t &count, NMEANavData &navData) const
   {
        mNavBuffer.NewestSample(count, navData);
   }

  private:

    int         mDevice;
    double      mSecsPerByte;
    std::thread mInputThread;
    bool        mWantQuit;

    bool        mInitialNavValid;
    NMEANavData mInitialNavData;
    NMEANavData mLastNavData;

    uint8_t CalculateCRC(const char *string, size_t len);

    static void *InputThreadFunc(void *data);
    void        *InputThreadReal(void);

    NMEANavData mCurNavData;
    NavBuffer  &mNavBuffer;
    void UpdateNavData(NMEAData *nmea);

    DataLoggerID mLatitudeID;
    DataLoggerID mLongitudeID;
    DataLoggerID mAltitudeID;
    DataLoggerID mVelocityID;
    DataLoggerID mCourseID;
    DataLoggerID mHdopID;

    char mDevicePath[128];
    uint32_t mBaudrate;
};

#endif // NEMAINTERFACE_H
