#ifndef NMEADATA_H
#define NMEADATA_H

#include <time.h>
#include "nmeasentence.h"
#include "util/util.h"

// NOTE:  All times are in time_t format except as a double
//        We need fractional seconds!

//---------------------------------------
struct NMEA_GGA
{
    double utc;
    int    fix;
    int    satellitesUsed;
    double latitude;  // + for N, - for S
    double longitude; // + for E, - for W
    double hdop;
    double altitude; // always in meters
    double geoidSep; // always in meters
};

//---------------------------------------
struct NMEA_GLL
{
    double utc;
    double latitude;  // + for N, - for S
    double longitude; // + for E, - for W
    bool   valid;
    char   mode;
};

//---------------------------------------
struct NMEA_GSA
{
    int     satellitesUsed[12];
    double  pdop;
    double  hdop;
    double  vdop;
    enum FixType { NoFix = 1, Fix_2D = 2, Fix_3D = 3 };
    FixType fix;
    bool    forceManualMode;
};

//---------------------------------------
struct NMEA_GSV
{
    struct Satellite
    {
        uint32_t id        : 8;
        uint32_t snr       : 8;
        uint32_t elevation : 7;
        uint16_t azimuth   : 9;
    };

    static const int cMaxSatellites = 33;
    int       numSatellites;
    Satellite satellites[cMaxSatellites];
};

//---------------------------------------
struct NMEA_MSS
{
    int    strength;
    int    snr;
    double frequency; // in KHz
    int    beaconRate;
    int    channel;
};

//---------------------------------------
struct NMEA_RMC
{
    double utc;
    double latitude;  // + for N, - for S
    double longitude; // + for E, - for W
    bool   valid;
    char   mode;
    double speed; // in meters/sec
    double course;
};

//---------------------------------------
struct NMEA_VTG
{
    double course;
    double speed; // meters/sec
    char   mode;
};


//---------------------------------------
class NMEAData
{
  public:
    NMEAData(void);
    bool Parse(const NMEASentence &s, bool &more);
    void Dump(void);

    void Clear(void);

    enum NMEADataType { Invalid, GGA, GLL, GSA, GSV, MSS, RMC, VTG };
    NMEADataType mType;
    union
    {
        NMEA_GGA gga;
        NMEA_GLL gll;
        NMEA_GSA gsa;
        NMEA_GSV gsv;
        NMEA_MSS mss;
        NMEA_RMC rmc;
        NMEA_VTG vtg;
    };

  private:
    // these are set every RMC message,
    // used for completing UTC on messages that don't have date info
    static int  tmYear;
    static int  tmMonth;
    static int  tmDay;

    bool ParseGGA(const NMEASentence &s, bool &more);
    bool ParseGLL(const NMEASentence &s, bool &more);
    bool ParseGSA(const NMEASentence &s, bool &more);
    bool ParseGSV(const NMEASentence &s, bool &more);
    bool ParseMSS(const NMEASentence &s, bool &more);
    bool ParseRMC(const NMEASentence &s, bool &more);
    bool ParseVTG(const NMEASentence &s, bool &more);

    double UTCTime(const NMEASentence &s, int tIndex, int dIndex = -1);

    // Assumes the next values is either N, S, E, W
    double LatLong(const NMEASentence &s, int index);

    void DumpGGA(void);
    void DumpGLL(void);
    void DumpGSA(void);
    void DumpGSV(void);
    void DumpMSS(void);
    void DumpRMC(void);
    void DumpVTG(void);

};




#endif //NMEADATA_H