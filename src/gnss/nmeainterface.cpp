#include "nmeainterface.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
//#include <errno.h>
//#include <signal.h>
//#include <termios.h>
#include <ctype.h>
#include <stdint.h>

#include <poll.h>

#include "util/util.h"

#include "log/log.h"

//--------------------------------------------------------------
NMEAInterface::NMEAInterface(const char *dev, uint32_t baudrate, NavBuffer &navBuffer) :
    mInitialNavValid(false),
    mNavBuffer(navBuffer),
    mBaudrate(baudrate)
{
    strncpy(mDevicePath, dev, 128);
    mSecsPerByte = 10.0 / (double)mBaudrate;

    mWantQuit = false;
    mInputThread = std::thread(&InputThreadFunc, (void *) this);
    pthread_setname_np(mInputThread.native_handle(), "NMEA");
}

//--------------------------------------------------------------
NMEAInterface::~NMEAInterface()
{
    mWantQuit = true;
    mInputThread.join();

    if (mDevice > 0)
    {
        close(mDevice);
        mDevice = 0;
    }
}

//--------------------------------------------------------------
bool NMEAInterface::ResetBaudrate(uint32_t baudrate)
{
    if (mDevice <= 0) return false;
    return SetSerialParams(mDevice, baudrate);
}


//--------------------------------------------------------------
bool NMEAInterface::SendString(const char *string)
{
    if (mDevice <= 0) return false;

    size_t len = strlen(string);
    if (len > 120) return false;
    uint8_t crc = CalculateCRC(string, len);

    char command[128];
    command[0] = '$';
    memcpy(command  + 1, string, len);
    command[len + 1] = '*';
    sprintf(command + len + 2, "%02X\r\n", (int) crc);
    ssize_t written = write(mDevice, command, len + 6);
    if (written != (ssize_t) (len + 6))
    {
        return false;
    }
    return true;
}

//--------------------------------------------------------------
uint8_t NMEAInterface::CalculateCRC(const char *string, size_t len)
{
    if (len == 0) return 0;
    uint8_t crc = string[0];
    size_t a;
    for (a = 1; a < len; a++) crc ^= string[a];
    return crc;
}

//--------------------------------------------------------------
void *NMEAInterface::InputThreadFunc(void *data)
{
    NMEAInterface *nmea = (NMEAInterface*)data;
    if (nmea == NULL) return NULL;

    return nmea->InputThreadReal();
}

//--------------------------------------------------------------
uint8_t hexval(char c)
{
    c = toupper(c);
    if ((c >= '0') && (c <= '9')) return c - '0';
    if ((c >= 'A') && (c <= 'F')) return c - ('A' - 10);
    return 0;
}

//--------------------------------------------------------------
unsigned decimalval(char c)
{
    unsigned val = (unsigned) (c - '0');
    if (val > 9) return 0;
    return val;
}

//--------------------------------------------------------------
void *NMEAInterface::InputThreadReal(void)
{
    bool inSentence = false;
    char sentence[256];
    int  sentenceLen = 0;
    double sentenceStartTime = 0.0;

    mDevice = 0;

    mCurNavData.Clear();
    NMEAData curData;
    curData.Clear();

    mLatitudeID  = DataLogger::AddVariable("GPS Latitude",  "deg", Double);
    mLongitudeID = DataLogger::AddVariable("GPS Longitude", "deg", Double);
    mAltitudeID  = DataLogger::AddVariable("GPS Altitude",  "m",   Float);
    mVelocityID  = DataLogger::AddVariable("GPS Velocity",  "m/s", Float);
    mCourseID    = DataLogger::AddVariable("GPS Course",    "deg", Float);
    mHdopID      = DataLogger::AddVariable("GPS HDOP",      "m",   Float);

    while (!mWantQuit)
    {
        while ((mDevice == 0) && !mWantQuit)
        {
            mDevice = open(mDevicePath, O_RDWR | O_NDELAY);
            if (mDevice < 0)
            {
                log_error("Can't open GPS serial port (%s), retrying in one second.", mDevicePath);
                sleep(1);
            }
            else
            {
                bool ok = SetSerialParams(mDevice, mBaudrate);
                if (!ok)
                {
                    close(mDevice);
                    log_error("Can't setup GPS serial port (%s), retrying in one second.", mDevicePath);
                    sleep(1);
                }
            }
        }

        struct pollfd fds;
        fds.fd = mDevice;
        fds.events = POLLIN;

        int ready = poll(&fds, 1, 1000); // wait up to 1s for data
        if ((ready != 1) || (fds.revents != POLLIN)) continue;

        // now it the time that the LAST byte finished!
        double now = SystemTimeUTC();

        char buf[32];
        ssize_t amount = read(mDevice, buf, 32);
        if (amount <= 0) continue;

        ssize_t pos = 0;
        while (pos < amount)
        {
            if (!inSentence)
            {
                while(pos < amount)
                {
                    if (buf[pos] == '$')
                    {
                        inSentence = true;
                        sentenceLen = 0;
                        sentenceStartTime = now - (((amount - pos) + 1) * mSecsPerByte);
                        ++pos;
                        break;
                    }
                    ++pos;
                }
            }

            // I'm in a sentence here or I'm done parsing input data
            while (pos < amount)
            {
                // compute checksum and check sentence
                if (buf[pos] == 0x0D)
                {
                    uint8_t calcCRC = CalculateCRC(sentence, sentenceLen - 3);
                    uint8_t rxCRC   = (hexval(sentence[sentenceLen - 2]) << 4) |
                                       hexval(sentence[sentenceLen - 1]);
                    // Adding a space at the end of the line handles the
                    // dangling comma at the end properly inside NMEASentence
                    sentence[sentenceLen - 3] = ' ';
                    sentence[sentenceLen - 2] = 0;
                    if (calcCRC == rxCRC)
                    {
                        log_debug("NMEA Sentence: %s", sentence);
                        NMEASentence s(sentenceStartTime, sentence);
                        bool more;
                        bool ok = curData.Parse(s, more);
                        if (!ok)
                        {
                            log_debug("Sentence failed to parse");
                            curData.Clear();
                        }
                        else if (more == false)
                        {
                            if (!mInitialNavValid && (curData.mType == NMEAData::RMC) && curData.rmc.valid && (amount == 1))
                            {
                                //curData.Dump();
                                SetSystemUTC(curData.rmc.utc);
                            }
                            // post it!
                            UpdateNavData(&curData);
                            //curData.Dump();
                            curData.Clear();
                        }
                    }
                    else
                    {
                        log_debug("Dropped sentence %s", sentence);
                    }
                    inSentence  = false;
                    sentenceLen = 0;
                    break;
                }
                sentence[sentenceLen] = buf[pos];
                ++sentenceLen;
                ++pos;
            }
        }
    }

    return NULL;
}

//-------------------------------------------------------------
void NMEAInterface::UpdateNavData(NMEAData *nmea)
{
    switch (nmea->mType)
    {
        case NMEAData::Invalid:
            break;

        case NMEAData::GGA:
            // Only update struct if fix is valid!
            if ((nmea->gga.fix == 1) || (nmea->gga.fix == 2))
            {
                mCurNavData.WriteUTC(nmea->gga.utc);
                mCurNavData.WriteLatLong(nmea->gga.latitude, nmea->gga.longitude);
                mCurNavData.WriteAltGSep(nmea->gga.altitude, nmea->gga.geoidSep);
                mCurNavData.WriteHdop(nmea->gga.hdop);
            }
            break;

        case NMEAData::GLL:
            if (nmea->gll.valid)
            {
                mCurNavData.WriteUTC(nmea->gll.utc);
                mCurNavData.WriteLatLong(nmea->gll.latitude, nmea->gll.longitude);
            }
            break;

        case NMEAData::GSA: // Don't care, no nav data
            break;

        case NMEAData::GSV: // Don't care, no nav data
            break;

        case NMEAData::MSS: // Don't care, no nav data
            break;

        case NMEAData::RMC:
            if (nmea->rmc.valid)
            {
                mCurNavData.WriteUTC(nmea->rmc.utc);
                mCurNavData.WriteLatLong(nmea->rmc.latitude, nmea->rmc.longitude);
                mCurNavData.WriteVelCourse(nmea->rmc.speed, nmea->rmc.course);
            }
            break;

        case NMEAData::VTG:
            mCurNavData.WriteVelCourse(nmea->vtg.speed, nmea->vtg.course);
            break;
    }

    if (mCurNavData.IsComplete())
    {
        mNavBuffer.Push(mCurNavData.utc, mCurNavData);
        mLastNavData = mCurNavData;
        if (!mInitialNavValid)
        {
            mInitialNavData = mCurNavData;
            mInitialNavValid   = true;
        }

        log_debug("GPS sample at %f", mCurNavData.utc);

        DataLogger::UpdateVariable(mLatitudeID,  mCurNavData.utc, mCurNavData.latitude);
        DataLogger::UpdateVariable(mLongitudeID, mCurNavData.utc, mCurNavData.longitude);
        DataLogger::UpdateVariable(mAltitudeID,  mCurNavData.utc, mCurNavData.altitude);
        DataLogger::UpdateVariable(mVelocityID,  mCurNavData.utc, mCurNavData.velocity);
        DataLogger::UpdateVariable(mCourseID,    mCurNavData.utc, mCurNavData.course);
        DataLogger::UpdateVariable(mHdopID,      mCurNavData.utc, mCurNavData.hdop);

        mCurNavData.Clear();
    }
}

//-------------------------------------------------------------
bool NMEAInterface::GetInitialNavData(NMEANavData &out) const
{
    if (!mInitialNavValid) return false;
    out = mInitialNavData;
    return true;
}

//-------------------------------------------------------------
bool NMEAInterface::GetLastNavData(NMEANavData &out) const
{
    if (!mInitialNavValid) return false;
    out = mLastNavData;
    return true;

}

//---------------------------------------------------------
int NMEAInterface::SampleAt(double time, NMEANavData &out) const
{
    NMEANavData early, late;
    int result = mNavBuffer.FindTimestampBrackets(time, early, late);
    if (result != 0) return result;
    early.Interpolate(out, time, late);
    return 0;
}
