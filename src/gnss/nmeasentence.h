#ifndef NEMASENTENCE_H
#define NEMASENTENCE_H

#include <stdint.h>

class NMEASentence
{
  public:
    NMEASentence(void);
    NMEASentence(double systemTime, const char *sentence);
    NMEASentence(const NMEASentence &rhs);
   ~NMEASentence();

    unsigned    NumValues(void) const { return mNumSentenceValues; };
    const char *Value(unsigned index) const;
    bool        ValueChars(unsigned index, char *dest, unsigned first, unsigned len = 0) const;
    int         ValueAsInt(unsigned index) const;
    int         ValueAsInt(unsigned index, unsigned first, unsigned len = 0) const;
    double      ValueAsDouble(unsigned index) const;
    double      ValueAsDouble(unsigned index, unsigned first, unsigned len = 0) const;
    double      ValueAsDegreesDecimal(unsigned index) const;

    double      FirstByteTime(void) const { return mSystemTime; }
  private:

    double           mSystemTime;

    static const int cMaxSentenceLen = 83;
    char             mSentence[cMaxSentenceLen + 1];

    static const int cMaxSentenceValues = 32;
    char            *mValues[cMaxSentenceValues];
    unsigned         mNumSentenceValues;
};

#endif // NEMASENTENCE_H
