#include "led/ledstring.h"

#include "util/util.h"

/*********

These LEDs use a timing based data stream.
             ________________         0.9us +- .15us
a zero bit: | 0.3us +- .15us |_____________________________________



             ____________________________     0.6us +- .15us
a one bit:  |  0.6us +- .15us            |_________________________



Reset:      low for at least 80us


Data is either 24 bits in order: G7 G6 G5 G4 G3 G2 G1 G0 R7 R6 R5 R4 R3 R2 R1 R0
                                 B7 B6 B5 B4 B3 B2 B1 B0

Or 32 bits in order :            G7 G6 G5 G4 G3 G2 G1 G0 R7 R6 R5 R4 R3 R2 R1 R0
                                 B7 B6 B5 B4 B3 B2 B1 B0 W7 W6 W5 W4 W3 W2 W1 W0


LEDs are chained, First LED in the string is the first LED sent

If you run 192KHz stereo 32bit, you get 2300ns per channel with a ~570ns forced zero
 with the last 8 bits of sample data ignored.

Which means we can run two LED bits per 24 bit sample and meet our timing specs.
  HA!
**********/

#define PINMUX_SELECT "/sys/kernel/debug/pinctrl/c8100000.bus:pinctrl@14-pinctrl-meson/pinmux-select"

#define ZERO_BIT 0x00F0
#define ONE_BIT  0x00FF

const uint8_t LEDString::sOrderedMask[8] = { 0x40, 0x80, 0x10, 0x20, 0x04, 0x08, 0x01, 0x02 };


//----------------------------------------------
LEDString::LEDString(const char *device, LEDString::Init *init) :
    mBufferSize(0),
    mBuffer(NULL),
    mDimmer(256),
    mDirty(false),
    mAlsaPos(0),
    mAlsa(NULL)
{
    mAlsa = new AlsaLEDDriver(device, 192000, 2, &UpdateLEDs, this);

    if (init == NULL) return;

    // run through the init struct and add the LEDs to the string
    unsigned i;
    for (i = 0; init[i].count != 0; i++)
    {
        LEDInfo led;
        led.type = init[i].type;
        unsigned inc = (led.type == RGB) ? 24 : 32;

        unsigned a;
        for (a = 0; a < init[i].count; a++)
        {
            led.offset = mBufferSize;
            mLEDs.push_back(led);
            mBufferSize += inc;
        }
    }

    // We've calculated the number of LEDs and the buffer size, alloc it.
    mBuffer = new uint16_t[mBufferSize];
    // set all to LED zero
    unsigned a;
    for (a = 0; a < mLEDs.size(); a++)
    {
        SetLED(a, 0, 0, 0, 0);
    }

    // Turn on the i2s output
    WriteFileRaw(PINMUX_SELECT, "i2s_out_ch01_ao i2s_out_ao\n", 27);

    mAlsa->Play();
}

//----------------------------------------------
LEDString::~LEDString()
{
    unsigned a;
    for (a = 0; a < mLEDs.size(); a++)
    {
        SetLED(a, 0, 0, 0, 0);
    }
    while (mDirty)
    {
        UpdateDriver();
        usleep(3000);
    }

    usleep(100000);

    if (mAlsa != NULL)
    {
        mAlsa->Stop();
        delete mAlsa;
        mAlsa = NULL;
    }

    if (mBuffer != NULL)
    {
        delete mBuffer;
        mBuffer = NULL;
    }
}

//----------------------------------------------
void LEDString::UpdateDriver(void)
{
    if (mAlsa != NULL) mAlsa->Update();
}

//----------------------------------------------
void LEDString::SetLED(unsigned index, uint8_t r, uint8_t g, uint8_t b, uint8_t w)
{
    if (index >= mLEDs.size()) return;

    if (mDimmer < 256)
    {
        r = ((mDimmer * (unsigned) r) >> 8) & 0xFF;
        g = ((mDimmer * (unsigned) g) >> 8) & 0xFF;
        b = ((mDimmer * (unsigned) b) >> 8) & 0xFF;
        w = ((mDimmer * (unsigned) w) >> 8) & 0xFF;
    }

    uint16_t *bits = &mBuffer[mLEDs[index].offset];

    unsigned a;
    for (a = 0; a < 8; a++)
    {
        bits[a +  0] = ((g & sOrderedMask[a]) != 0) ? ONE_BIT : ZERO_BIT;
        bits[a +  8] = ((r & sOrderedMask[a]) != 0) ? ONE_BIT : ZERO_BIT;
        bits[a + 16] = ((b & sOrderedMask[a]) != 0) ? ONE_BIT : ZERO_BIT;
    }

    if (mLEDs[index].type == RGBW)
    {
        for (a = 0; a < 8; a++) bits[a + 24] = ((w & sOrderedMask[a]) != 0) ? ONE_BIT : ZERO_BIT;
    }
    mDirty = true;
}

//----------------------------------------------
void LEDString::SetDimmer(unsigned dimmer)
{
    mDimmer = dimmer;
    if (mDimmer > 256) mDimmer = 256;
}

//----------------------------------------------
int LEDString::UpdateLEDs(void *context, void *dest, unsigned frames, unsigned numChannels)
{
    if (context == NULL) return 0;
    if (dest == NULL) return 0;
    if (numChannels != 2) return 0;
    LEDString *string = (LEDString *) context;
    return string->UpdateLEDsInternal(dest, frames);
}

//----------------------------------------------
// A frame is a stereo sample at 32 bits per channel
int LEDString::UpdateLEDsInternal(void *dest, unsigned frames)
{
    unsigned copyWords = frames * 4;
    memset(dest, 0x00, copyWords * sizeof(uint16_t));
    //return 0;

    if (mDirty)
    {
        if ((copyWords + mAlsaPos) > mBufferSize) copyWords =  mBufferSize - mAlsaPos;

        //printf("Copying %d words from %d\n", copySamples, mAlsaPos);
        memcpy(dest, &mBuffer[mAlsaPos], copyWords * sizeof(uint16_t));
        mAlsaPos += copyWords;
        if (mAlsaPos >= mBufferSize)
        {
            mAlsaPos = 0;
            mDirty   = false;
        }
    }

    return 0;
}
