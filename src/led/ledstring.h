#ifndef LED_STRING_H
#define LED_STRING_H

#include <stdint.h>

#include "audio/alsaled.h"

#include <vector>

class LEDString
{
  public:

    enum LEDType { RGB, RGBW };

    struct Init
    {
      unsigned count;
      LEDType  type;
    };

    LEDString(const char *device, Init *leds);
   ~LEDString();

    void SetLED(unsigned index, uint8_t r, uint8_t g, uint8_t b, uint8_t w);

    // Uses the same format as display colors except that Alpha is ignored and white is turned off
    void SetLED(unsigned index, uint32_t color)
    {
        SetLED(index, (color >> 16) & 0xFF, (color >> 8) & 0xFF, (color & 0xFF), 0);
    }

    void SetDimmer(unsigned dim); // 256 is full brightness

    void UpdateDriver(void);

  private:

    static int UpdateLEDs(void *context, void *dest, unsigned frames, unsigned numChannels);
    int        UpdateLEDsInternal(void *dest, unsigned frames);

    struct LEDInfo
    {
        uint32_t offset : 24;
        uint32_t type   : 8;
    };

    std::vector<LEDInfo> mLEDs;


    unsigned  mBufferSize;
    uint16_t *mBuffer;

    unsigned mDimmer;

    bool     mDirty;

    unsigned       mAlsaPos;
    AlsaLEDDriver *mAlsa;

    // It's easier to do the ordered mask than swap L R on samples.
    static const uint8_t sOrderedMask[8];
};

#endif // LED_STRING_H