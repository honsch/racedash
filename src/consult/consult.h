#ifndef CONSULT_H
#define CONSULT_H

#include <stdint.h>

#include <vector>
#include <queue>
#include "consult_register.h"
#include "consult_faultcode.h"



//-----------------------------------------------------------------------------
enum ConsultCommands
{
    SelfDiagnostic  = 0xD1,
    EraseErrorCodes = 0xC1,
    ECUInfo         = 0xD0,
    ReadRegister    = 0x5A,
    ReadROM         = 0xC9,
    ReadStop        = 0x30,
    ActivateTest    = 0x0A
};


//-----------------------------------------------------------------------------
enum ConsultTests
{
    SetCoolantTemp                 = 0x80, // VAL=0x50 = 30*C, 0x32 = 0*C, 0x00=-50*C
    SetFuelInjection               = 0x81, // VAL=0x64 = 0%, 0x6E=+10%, 0x5A=-10%
    SetIgnitionTiming              = 0x82, // VAL=0x05=+5 BTDC, 0xFB=-5 BTDC
    SetIACV_AAC                    = 0x84, // VAL=0x00 =0%, 0x8C=100%
    SetIACVOpening                 = 0x85, // VAL=0x00 =0%, 0x8C=100%
    SetIACV_FICDSolenoid           = 0x87, // VAL=0x00 =OFF
    SetPowerBalance                = 0x88, // DIGITAL (see table below), Some ECU types use 0x01 to 0x08 to select cylinder (SR20,?)
    SetFuelPumpRelay               = 0x89, // VAL=0x00 =OFF (0x00 = ON for some ECU, SR20 etc)
    SetPREGSolenoid                = 0x8A, // VAL=0x00 =OFF
    SetSelfLearn                   = 0x8B, // VAL=0x00 = CLEAR
    SetIACV_AACControl             = 0x8C, // VAL=0x00 =OFF
    SetValveTiming                 = 0x8F, // VAL=0x00 =OFF
    SetMAPSolenoid                 = 0x90, // VAL=0x00 =OFF
    SetCoolantFanLow               = 0x93, // VAL=0x00 =OFF
    SetCoolantFanHigh              = 0x94  // VAL=0x00 =OFF
};


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------


class ConsultInterface
{
  public:
    ConsultInterface(const char *dev);
   ~ConsultInterface();

    uint32_t Update(void);
    double   LastRegisterFrameTime(void);

    // These are for specifying and retrieving the order of consult registers to read
    void     RegistersClear(void);
    int      RegistersAdd(unsigned index);
    int      RegistersReadStart(void);
    int      RegistersReadStop(void);
    unsigned RegistersCount(void);
    uint8_t  RegistersRegister(uint8_t order);

    int      FaultsReadStart(void);
    int      FaultsReadStop(void);
    unsigned FaultsCount(void);
    int      FaultsClear(void);
    const ConsultFaultState &FaultsGet(uint8_t index);

    int  TestStart(ConsultTests test, uint8_t value);
    int  TestStop(void);

    // These return the actual consult registers.  The index is NOT from the order.
    unsigned    RegisterCount(void);
    const char *RegisterName(unsigned index);
    float       RegisterValue(unsigned index);
    float       RegisterValidMin(unsigned index);
    float       RegisterValidMax(unsigned index);
    void        RegisterFormatValue(unsigned index, char *dest, unsigned destLen);

  private:

    void InitDatabase(void);
    int  InitCommunications(const char *dev);

    bool LoadConfig(const char *fname);
    bool SaveConfig(const char *fname);

    int SendConsultCommand(uint8_t *data, uint8_t *echo, uint8_t count);
    int SendReadStop(void);

    char mConsultDevice[256];

    // All of the registers available
    std::vector<ConsultRegister *> mConsultRegisters;
    std::vector<ConsultFaultCode*> mConsultFaultCodes;
    std::vector<ConsultFaultState> mFaultState;

    std::queue<uint8_t> mFifo;

    // Consult data receive framing
    bool mFoundReadStopEcho;
    bool mFoundFrameStart;
    bool mFoundFrameLen;
    uint8_t mFrameLen;

    #define READ_FRAME_LEN_MAX 32
    uint8_t mFrame[READ_FRAME_LEN_MAX];

    double   mFrameStartTime;
    double   mConsultLastRegisterFrameTime;
    uint32_t mConsultFramesProcessed;
    uint32_t mConsultBytesProcessed;
    uint32_t mConsultProcessCount;

    #define COMMAND_BUFFER_SIZE 32
    int  mComDevice;
    bool mConsultOK;

    enum ConsultActivityMode { NoActivity, ReadRegisters, ReadFaults, RunTest } ;
    ConsultActivityMode mActivityMode;

    uint8_t mReadCommand[COMMAND_BUFFER_SIZE + 4];
    uint8_t mReadEcho[COMMAND_BUFFER_SIZE + 4];
    unsigned mReadCommandLen;

    // The array of registers in the read command in order for processing
    #define  MAX_REGISTERS_IN_INDEX  32
    unsigned mRegisterIndexCount;
    uint8_t  mRegisterIndex[MAX_REGISTERS_IN_INDEX];


};

#endif // CONSULT_H
