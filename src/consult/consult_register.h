#ifndef CONSULT_REGISTER_H
#define CONSULT_REGISTER_H

#include <stdint.h>
#include <vector>
#include "consult_faultcode.h"

//-----------------------------------------------------------------------------
#define REGISTER_FLAG_SIZE_MASK     0x07
#define REGISTER_FLAG_PULL_DATA     0x80
#define REGISTER_FLAG_SPECIAL_VALID 0x40

class ConsultRegister
{
  public:

    ConsultRegister(const char *name, uint8_t size, uint8_t addr, uint16_t mask, uint8_t shift, float scale, float offset, const char *format, const char *unit, float validMin, float validMax);

    uint8_t AddCommandSpecial(uint8_t *dest, uint8_t *echoDest);
    uint8_t AddCommand(uint8_t *dest, uint8_t *echoDest);
    uint8_t ProcessResultsSpecial(uint8_t *data, uint32_t currentTime);
    uint8_t ProcessResults(uint8_t *data, uint32_t currentTime);
    void    FormatValue(char *string, unsigned inputLength);

    const char *Name(void) { return mName; }
    float CurrentValue(void) { return mCurrentValue; }
    float ValidMin(void) { return mValidMin; }
    float ValidMax(void) { return mValidMax; }

    static void ClearUsedRegisters(void);

  private:
    char     mName[64];     // name of the field
    char     mUnit[8];      // like K/h or C
    float    mCurrentValue;
    float    mMinimum;
    float    mMaximum;
    uint32_t mLastUpdateTime;

    uint8_t  mFlags;     // 1 or 2 bytes
    uint8_t  mAddress;   // MSB or single address
    uint16_t mMask;      // bit mask
    uint8_t  mShift;     // right shift amount to move to the LSB
    float    mScale;     //
    float    mOffset;    // (value * scale) + offset
    char     mFormat[8]; // for sprinf
    float    mValidMin;  // Values less then this are bad readings from the ecu
    float    mValidMax;  // Values more then this are bad readings from the ecu

    // Which addresses are in use in the command list so we don't query the same one twice
    static uint8_t sRegisterOrder[32];

    // The memory map of consult registers
    static uint8_t sRegisterMemory[256]; // all of the possible consult registers

};


//-----------------------------------------------------------------------------
enum ConsultRegisters
{
    RPMCoarseMSB                = 0x00,
    RPMCoarseLSB                = 0x01, // Value * 12.5 (RPM)
    RPMFineMSB                  = 0x02,
    RPMFineLSB                  = 0x03, // Value * 8 (RPM) ??
    MAFVoltageMSB               = 0x04,
    MAFVoltageLSB               = 0x05, // Value * 5 (mV)
    RHMAFVoltageMSB             = 0x06,
    RHMAFVoltageLSB             = 0x07, // Value * 5 (mV)
    CoolantTemp                 = 0x08, // Value-50 (deg C)
    O2Voltage                   = 0x09, // Value * 10 (mV)
    RHO2Voltage                 = 0x0a, // Value * 10 (mV)
    VehicleSpeed                = 0x0b, // Value * 2 (kph)
    BatteryVoltage              = 0x0c, // Value * 80 (mV)
    TPSVoltage                  = 0x0d, // Value * 20 (mV)
    FuelTemp                    = 0x0f, // Value-50 (deg C)
    IntakeAirTemp               = 0x11, // Value -50 (deg C)
    EGTVoltage                  = 0x12, // Value * 20 (mV)
    DigitalBitRegister          = 0x13, // See digital register table
    InjectionTimeMSB            = 0x14,
    InjectionTimeLSB            = 0x15, // Value / 100 (mS)
    IgnitionTiming              = 0x16, // 110 - Value (Deg BTDC)
    AACValve                    = 0x17, // Value / 2 (Idle Air %)
    Alpha                       = 0x1a, // Value (%)
    RHAlpha                     = 0x1b, // Value (%)
    AlphaSelfLearn              = 0x1c, // Value (%)
    RHAlphaSelfLearn            = 0x1d, // Value (%)
    DigitalControlRegister1     = 0x1e, // See digital register table
    DigitalControlRegister2     = 0x1f, // See digital register table

    // My own Hacky Injector duty cycle
    InjectorDutyCycle           = 0xF0

    // newer stuff possible 300Z
    /***  Ignore for now
    M/R F/C MNT                 = 0x21, // See digital register table
    Injector time (RH) MSB      = 0x22,
    Injector time (RH) LSB      = 0x23, // Value / 100 (mS)
    Waste Gate Solenoid %       = 0x28,
    Turbo Boost Sensor, Voltage = 0x29,
    Engine Mount On/Off         = 0x2a,
    Position Counter            = 0x2e,
    Purg. Control Valve, Step   = 0x25,
    Tank Fuel Temperature, C    = 0x26,
    FPCM DR, Voltage            = 0x27,
    Fuel Gauge, Voltage         = 0x2f,
    FR O2 Heater-B1             = 0x30, // Bank 1?
    FR O2 Heater-B2             = 0x31, // Bank 2?
    Ignition Switch             = 0x32,
    CAL/LD Value, %             = 0x33,
    B/Fuel Schedule, mS         = 0x34,
    RR O2 Sensor Voltage        = 0x35,
    RR O2 Sensor-B2 Voltage     = 0x36, // Bank 2?
    Absolute Throttle Position,
    Voltage                     = 0x37,
    MAF gm/S                    = 0x38,
    Evap System Pressure, Voltage = 0x39,
    Absolute Pressure Sensor,
    Voltage Dual                = 0x3a, = 0x4a,
    FPCM F/P Voltage Dual       = 0x52, = 0x53,
    ***/
};

//-----------------------------------------------------------------------------
enum ConsultDigitalBitRegisterBits
{
    TPSClosed           = 0x01,
    StartSignal         = 0x02,
    NeutralSwitch       = 0x04,
    PowerSteeringSwitch = 0x08,
    AirConditionOn      = 0x10
};


//-----------------------------------------------------------------------------
enum ConsultDigitalControlRegister1Bits
{
    CoolantFanLow       = 0x01,
    CoolantFanHigh      = 0x02,
    VTCSolenoid         = 0x20,
    FuelPumpRelay       = 0x40,
    AirConditionRelay   = 0x80
};

//-----------------------------------------------------------------------------
enum ConsultDigitalControlRegister2Bits
{
    EGRSolenoid         = 0x01,
    IACV_FICDSolenoid   = 0x08,
    WastegateSolenoid   = 0x20,
    PressureRegulatorValve = 0x40
};

/**
enum M/R F/C MNT Bits ???
{
    RHLean = 0x40,
    LHLean = 0x80
};

**/


#endif // CONSULT_REGISTER_H
