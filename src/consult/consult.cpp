#include "consult.h"
#include <string.h>


#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>

//#include <inttypes.h>
//#include <syslog.h>


#include "display.h"
#include "util/util.h"

#include "log/log.h"

#define INACTIVE_TIME   100
#define ACTIVE_TIME     5


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
ConsultInterface::ConsultInterface(const char *dev)
{
    strncpy(mConsultDevice, dev, 255);
}

//-------------------------------------------------------------------------------------------------
void ConsultInterface::InitDatabase(void)
{
    mActivityMode = NoActivity;

    // Regular values
    mConsultRegisters.push_back(new ConsultRegister("RPM",             2, RPMCoarseMSB,        0xFFFF, 0, 12.5F,    0.0F, "%4.0f", "RPM",  0.0F, 8000.0F));
    mConsultRegisters.push_back(new ConsultRegister("MAF Voltage",     2, MAFVoltageMSB,       0xFFFF, 0, 0.005F,   0.0F, "%1.3f", "V",    0.0F, 5.2F));
    mConsultRegisters.push_back(new ConsultRegister("Coolant Temp",    1, CoolantTemp,         0xFF,   0, 1.0F,   -50.0F, "%3.0f", "*C", -50.0F, 150.0F));
    mConsultRegisters.push_back(new ConsultRegister("Speed",           1, VehicleSpeed,        0xFF,   0, 2.0F,     0.0F, "%3.0f", "Km/h", 0.0F, 270.0F));
    mConsultRegisters.push_back(new ConsultRegister("Battery Voltage", 1, BatteryVoltage,      0xFF,   0, 0.080F,   0.0F, "%2.2f", "V",    0.0F, 16.0F));
    mConsultRegisters.push_back(new ConsultRegister("TPS Voltage",     1, TPSVoltage,          0xFF,   0, 0.020F,   0.0F, "%1.3f", "V",    0.0F, 5.2F));
    mConsultRegisters.push_back(new ConsultRegister("Injection Time",  2, InjectionTimeMSB,    0xFFFF, 0, 0.01F,    0.0F, "%2.2f", "ms",   0.0F, 30.0F));
    mConsultRegisters.push_back(new ConsultRegister("Injector Duty",   4, InjectorDutyCycle,   0,      0, 0.0F,     0.0F, "%3.1f", "%",    0.0F, 110.0F));
    mConsultRegisters.push_back(new ConsultRegister("Ignition Timing", 1, IgnitionTiming,      0xFF,   0,-1.0F,   110.0F, "%2.0f", "*BTDC", -10.0F, 50.0F));
    mConsultRegisters.push_back(new ConsultRegister("AAC Valve",       1, AACValve,            0xFF,   0, 0.5F,     0.0F, "%3.1f", "%",    0.0F, 100.0F));

    // Digital bits
    mConsultRegisters.push_back(new ConsultRegister("TPS Closed",          1, DigitalBitRegister, TPSClosed,           0, 1.0F, 0.0F, "%1.0f", "", 0.0F, 1.0F));
    mConsultRegisters.push_back(new ConsultRegister("Start Signal",        1, DigitalBitRegister, StartSignal,         1, 1.0F, 0.0F, "%1.0f", "", 0.0F, 1.0F));
    mConsultRegisters.push_back(new ConsultRegister("Neutral Switch",      1, DigitalBitRegister, NeutralSwitch,       2, 1.0F, 0.0F, "%1.0f", "", 0.0F, 1.0F));
    mConsultRegisters.push_back(new ConsultRegister("PS Pressure Switch",  1, DigitalBitRegister, PowerSteeringSwitch, 3, 1.0F, 0.0F, "%1.0f", "", 0.0F, 1.0F));
    mConsultRegisters.push_back(new ConsultRegister("A/C On",              1, DigitalBitRegister, AirConditionOn,      4, 1.0F, 0.0F, "%1.0f", "", 0.0F, 1.0F));


    // Setup the fault code database
    mConsultFaultCodes.push_back(new ConsultFaultCode( 11, "Crank angle sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 12, "MAF sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 13, "Engine temperature sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 14, "Vehicle speed sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 21, "Ignition signal"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 31, "ECU error"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 32, "EGR function"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 33, "Oxygen sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 34, "Knock sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 35, "EGT (CA only)"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 43, "TPS sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 45, "Injector leak"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 54, "A/T control signal missing"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 55, "No faults"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 65, "Cyl 4 misfire"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 66, "Cyl 3 misfire"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 67, "Cyl 2 misfire"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 68, "Cyl 1 misfire"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 71, "Random misfire"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 72, "TW Catlyst system"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 76, "Fuel injection system"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 77, "Rear oxygen sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 82, "Crank position sensor (OBD)"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 84, "A/T Diagnostic comm line"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 95, "Crank P/S cog (OBD)"));
    mConsultFaultCodes.push_back(new ConsultFaultCode( 98, "Coolant temperature sensor"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(103, "Park/neutral switch"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(105, "EGR solenoid"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(111, "Inhibitor switch"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(112, "Vehicle speed sensor (A/T)"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(113, "A/T 1st signal"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(114, "A/T 2nd signal"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(115, "A/T 3rd signal"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(116, "A/T 4th or TCC signal"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(118, "Shift solenoid/V A"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(121, "Shift solenoid/V B"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(123, "Overrun Clutch S/V"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(124, "Torque convertor Clutch S/V"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(125, "Line pressure S/V"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(126, "TPS (A/T)"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(127, "Engine speed signal"));
    mConsultFaultCodes.push_back(new ConsultFaultCode(128, "Fluid temperature sensor"));
}


//-------------------------------------------------------------------------------------------------
int ConsultInterface::InitCommunications(const char *dev)
{
    //SetStatusText("Initializing Consult...");


    mComDevice = open(dev, O_RDWR | O_NDELAY);
    if (mComDevice < 0) { return false; }

    fcntl(mComDevice, F_SETFL, 0);

    // Get the current options for the port...
    struct termios options;
    tcgetattr(mComDevice, &options);

    // Set the baud rates
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);

    // Enable the receiver and set local mode...
    // Turn off charachter processing, set to 8N1
    options.c_cflag &= ~(CSIZE | PARENB);
    options.c_cflag |= (CLOCAL | CREAD | CS8);

    // turn off output processing
    options.c_oflag = 0;

    // Turn off input processing
    options.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

    // Turn off flow control
    options.c_iflag &= ~(IXON | IXOFF | IXANY);

    // One byte is enough to read, no inter-character timer
    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 0;

    // Set the new options for the port...
    tcsetattr(mComDevice, TCSANOW, &options);


    mConsultOK = false;
    mActivityMode = NoActivity;

    uint8_t initCommand[3] = { 0xFF, 0xFF, 0xEF };
    uint8_t response;
    unsigned sanity = 0;
    const unsigned tries = 10;
    // loop up to 'tries' times trying to connect to the ECU
    do
    {
        ++sanity;
        write(mComDevice, initCommand, 3);
        uint16_t count;
        // If the ECU's consult port hasn't been initialized it will echo one byte only (0x10)
        // If it has been intilized it will echo the entire command
        do
        {
            response = 0;
            //count = ComPort_receive(&mComDevice, &response, 1, 10);

            if ((count == 1) && (response == 0x10))
            {
                mConsultOK = true;
            }
        } while ((count == 1) && (mConsultOK == false));

    } while ((mConsultOK == false) && (sanity < tries));

    if (mConsultOK == false)
    {
        // If something weird happened and the engine is spewing register read results
        // so we don;t get the answer we want, send a stop command
        response = ReadStop;
        write(mComDevice, &response, 1);

        //ComPort_close(&mComDevice);
        return -1;
    }

    return 0;
}

//----------------------------------------------------------------
int ConsultInterface::SendConsultCommand(uint8_t *data, uint8_t *echo, uint8_t count)
{
    if (!mConsultOK) return -1;
    if (count == 0)  return -1;
    if (count > 64)  return -1; // size of echo buffer

    uint8_t echoBuffer[64];
    write(mComDevice, data, count);

    int recvCount = 0; //ComPort_receive(&mComDevice, echoBuffer, count, 5 + count);
    if (recvCount != count) return -999;

    bool echoOK = true;
    unsigned a;
    for (a = 0; a < count; a++)
    {
        if (echoBuffer[a] != echo[a])
        {
            echoOK = false;
            break;
        }
    }

    if (echoOK)
    {
        echoBuffer[0] = 0xF0;
        write(mComDevice, echoBuffer, 1);
        return 0;
    }

    mConsultOK = false;
    return -echoBuffer[0];
}


//----------------------------------------------------------------
int ConsultInterface::SendReadStop(void)
{
    if (!mConsultOK) return -1;

    uint8_t command = ReadStop;

    mFoundReadStopEcho = false;
    write(mComDevice, &command, 1);

    // Wait for the read stop echo
    //uint32_t end = gSysTickCount + 25;
    //while ((gSysTickCount < end) && !mFoundReadStopEcho)
    {
        Update();
    }

    if (!mFoundReadStopEcho)
    {
        mConsultOK = false;
        return -98;
    }

    return 0;
}

//-------------------------------------------------------------------------------------------------
unsigned ConsultInterface::RegisterCount(void)
{
    return mConsultRegisters.size();
}

//-------------------------------------------------------------------------------------------------
const char *ConsultInterface::RegisterName(unsigned index)
{
    if (index >= RegisterCount()) return "Bad index N";
    return mConsultRegisters[index]->Name();
}

//-------------------------------------------------------------------------------------------------
float ConsultInterface::RegisterValue(unsigned index)
{
    if (index >= RegisterCount()) return 0.0F;
    return mConsultRegisters[index]->CurrentValue();
}

//-------------------------------------------------------------------------------------------------
void ConsultInterface::RegisterFormatValue(unsigned index, char *dest, unsigned destLen)
{
    if (dest == NULL) return;
    *dest = 0;
    if (index >= RegisterCount()) return;
    mConsultRegisters[index]->FormatValue(dest, destLen);
}

//-------------------------------------------------------------------------------------------------
float ConsultInterface::RegisterValidMin(unsigned index)
{
    if (index >= RegisterCount()) return 0.0F;
    return mConsultRegisters[index]->ValidMin();
}

//-------------------------------------------------------------------------------------------------
float ConsultInterface::RegisterValidMax(unsigned index)
{
    if (index >= RegisterCount()) return 0.0F;
    return mConsultRegisters[index]->ValidMax();
}

//-------------------------------------------------------------------------------------------------
void ConsultInterface::RegistersClear(void)
{
    RegistersReadStop();
    ConsultRegister::ClearUsedRegisters();
    mReadCommandLen = 0;
    mRegisterIndexCount = 0;
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::RegistersAdd(unsigned index)
{
    if (index >= RegisterCount()) return -1;
    if (mActivityMode == ReadRegisters) return -1;
    if (mRegisterIndexCount >= MAX_REGISTERS_IN_INDEX) return -1;

    uint8_t count = mConsultRegisters[index]->AddCommand(mReadCommand + mReadCommandLen, mReadEcho + mReadCommandLen);

    if ((count + mReadCommandLen) > COMMAND_BUFFER_SIZE)
    {
        //assert(0 && "Consult command frame too long");
        return -1;
    }

    mRegisterIndex[mRegisterIndexCount++] = index;
    mReadCommandLen += count;
    return 0;
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::RegistersReadStart(void)
{
    if (!mConsultOK)                 return -1;
    if (mActivityMode != NoActivity) return -2;
    if (mReadCommandLen == 0)        return -3;

    int err = SendConsultCommand(mReadCommand, mReadEcho, mReadCommandLen);
    if (err == 0) mActivityMode = ReadRegisters;
    return err;
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::RegistersReadStop(void)
{
    if (!mConsultOK)                    return -1;
    if (mActivityMode != ReadRegisters) return -2;

    int err = SendReadStop();
    if (err == 0) mActivityMode = NoActivity;
    return err;
}

//-------------------------------------------------------------------------------------------------
unsigned ConsultInterface::RegistersCount(void)
{
    return mRegisterIndexCount;
}

//-------------------------------------------------------------------------------------------------
uint8_t ConsultInterface::RegistersRegister(uint8_t index)
{
    if (index >= RegisterCount()) return 0;
    return mRegisterIndex[index];
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
int ConsultInterface::FaultsReadStart(void)
{
    if (!mConsultOK)                 return -1;
    if (mActivityMode != NoActivity) return -2;

    mFaultState.clear();

    // send faults read command
    uint8_t request = SelfDiagnostic;
    uint8_t echo    = request ^ 0xFF;
    int err = SendConsultCommand(&request, &echo, 1);
    if (err == 0) mActivityMode = ReadFaults;
    return err;
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::FaultsReadStop(void)
{
    if (!mConsultOK)                 return -1;
    if (mActivityMode != ReadFaults) return -2;

    int err = SendReadStop();
    if (err == 0) mActivityMode = NoActivity;
    return err;
}

//-------------------------------------------------------------------------------------------------
unsigned ConsultInterface::FaultsCount(void)
{
    return mFaultState.size();
}

//-------------------------------------------------------------------------------------------------
const ConsultFaultState &ConsultInterface::FaultsGet(uint8_t index)
{
//    if (index >= FaultsCount()) return -1;
    return mFaultState[index];
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::FaultsClear(void)
{
    if (!mConsultOK)                 return -1;
    if (mActivityMode != ReadFaults) return -2;

    FaultsReadStop();

    // send the clear command
    uint8_t request = EraseErrorCodes;
    uint8_t echo    = EraseErrorCodes ^ 0xFF;
    SendConsultCommand(&request, &echo, 1);

    SendReadStop();
    FaultsReadStart();

    return 0;
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::TestStart(ConsultTests test, uint8_t value)
{
    if (!mConsultOK)                 return -1;
    if (mActivityMode != NoActivity) return -2;

    unsigned char command[3], echo[3];

    command[0] = ActivateTest;
    command[1] = test;
    command[2] = value;

    echo[0] = 0xFF ^ command[0];
    echo[1] = command[1];
    echo[2] = command[2];

    int err = SendConsultCommand(command, echo, 3);
    if (err == 0) mActivityMode = RunTest;
    return err;
}

//-------------------------------------------------------------------------------------------------
int ConsultInterface::TestStop(void)
{
    if (!mConsultOK)              return -1;
    if (mActivityMode != RunTest) return -2;

    int err = SendReadStop();
    if (err == 0) mActivityMode = NoActivity;
    return err;
}

//-------------------------------------------------------------------------------------------------
uint32_t ConsultInterface::Update(void)
{
    if (!mConsultOK)
    {
        int err = InitCommunications(mConsultDevice);
        if (err < 0) return 100;  // Don't check again for 100ms
    }

    if (mActivityMode == NoActivity) return INACTIVE_TIME;

    mConsultProcessCount++;

    while (!mFifo.empty())
    {
        // Find the start of frame
        while (!mFoundFrameStart)
        {
            if (mFifo.empty()) goto doneActive;
            uint8_t fs = mFifo.front();
            mFifo.pop();

            if (fs == 0xFF)
            {
                mFoundFrameStart = true;
                mFrameStartTime = SystemTimeUTC();
                mFrameLen = 0;
                memset(mFrame, 0, sizeof(mFrame));
            }
            if (fs == 0xCF)
            {
                mFoundReadStopEcho = true;
            }
        }

        // read the frame length
        if (!mFoundFrameLen)
        {
            if (mFifo.empty()) goto doneActive;
            mFrameLen = mFifo.front();
            mFifo.pop();
            mFoundFrameLen = true;
            //assert(mFrameLen < READ_FRAME_LEN_MAX);
            ++mConsultBytesProcessed;
        }

        // read the frame
        if (mFifo.size() < mFrameLen) goto doneActive;
        unsigned a;
        for (a = 0; a < mFrameLen; a++)
        {
            mFrame[a] = mFifo.front();
            mFifo.pop();
        }

        // Clear the 'in frame' markers
        mFoundFrameStart = false;
        mFoundFrameLen   = false;

        if (mActivityMode == ReadRegisters)
        {
            uint8_t *frame = mFrame;
            unsigned a;
            for (a = 0; a < mRegisterIndexCount; a++)
            {
                ConsultRegister *reg = mConsultRegisters[mRegisterIndex[a]];
                uint8_t used = reg->ProcessResults(frame, mFrameStartTime);
                frame += used;
            }
            mConsultLastRegisterFrameTime = mFrameStartTime;
        }
        else if (mActivityMode == ReadFaults)
        {
            // All fault code frames are even in length!
            //assert((mFrameLen & 0x01) == 0);
            uint8_t *frame = mFrame;
            unsigned a;
            for (a = 0; a < mFrameLen / 2; a++)
            {
                uint8_t code = ((frame[0] >> 4) * 10) + (frame[0]  & 0x0F);
                uint8_t starts = frame[1];
                frame += 2;
                unsigned b;
                for (b = 0; b < mConsultFaultCodes.size(); b++)
                {
                    if (code == mConsultFaultCodes[b]->Code())
                    {
                        ConsultFaultState fs(mConsultFaultCodes[b]->Info(), code, b, starts);
                        mFaultState.push_back(fs);
                        break;
                    }
                }
            }
        }

        ++mConsultFramesProcessed;
    }

doneActive:
    return ACTIVE_TIME;
}


//-------------------------------------------------------------------------------------------------
double ConsultInterface::LastRegisterFrameTime(void)
{
    return mConsultLastRegisterFrameTime;
}

//-------------------------------------------------------------------------------------------------
bool ConsultInterface::SaveConfig(const char *fname)
{
    FILE *outf = fopen(fname, "wt");
    if (outf == NULL) return false;

    fprintf(outf, "# Consult Register Config\n");

    unsigned a;
    for (a = 0; a < mRegisterIndexCount; a++)
    {
        fprintf(outf, "%s\n", RegisterName(mRegisterIndex[a]));
    }

    fclose(outf);
    return true;
}

//-------------------------------------------------------------------------------------------------
bool ConsultInterface::LoadConfig(const char *fname)
{
    FILE *inf = fopen(fname, "rt");
    if (inf == NULL) return false;

    uint8_t newIndex[MAX_REGISTERS_IN_INDEX];
    uint8_t newIndexCount = 0;

    char line[100];
    while (feof(inf) == 0)
    {
        char *buf = fgets(line, sizeof(line), inf);
        if (buf != line)
        {
            fclose(inf);
            return false;
        }

        // strip termination cr/lf
        line[99] = 0;
        unsigned len = strlen(line) - 1;
        while ((line[len] < 32) && (len > 0))
        {
            line[len] = 0;
            --len;
        }

        unsigned a;
        for (a = 0; a < RegisterCount(); a++)
        {
            if (line[0] == '#') continue;
            if (0 == strcmp(line, RegisterName(a)))
            {
                newIndex[newIndexCount++] = a;
                break;
            }
        }
    }

    fclose(inf);

    // If there wasn't anything valid, don't clear what we already have
    if (newIndexCount == 0) return true;

    RegistersReadStop();
    RegistersClear();
    //assert(newIndexCount < MAX_REGISTERS_IN_INDEX);

    unsigned a;
    for (a = 0; a < newIndexCount; a++)
    {
        RegistersAdd(newIndex[a]);
    }

    return true;
}








