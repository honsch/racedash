#include "consult_register.h"
#include <string.h>

#include "log/log.h"

#define INACTIVE_TIME   100
#define ACTIVE_TIME     5

uint8_t ConsultRegister::sRegisterOrder[32];
uint8_t ConsultRegister::sRegisterMemory[256];

//-----------------------------------------------------------------------------
void ConsultRegister::ConsultRegister(const char *name, uint8_t size, uint8_t addr, uint16_t mask, uint8_t shift, float scale, float offset, const char *format, const char *unit, float validMin, float validMax)
{
    strncpy(mName,   name,   sizeof(mName) - 1);
    strncpy(mUnit,   unit,   sizeof(mUnit) - 1);
    strncpy(mFormat, format, sizeof(mFormat) - 1);

    mFlags    = 0;
    mAddress  = addr;
    mMask     = mask;
    mShift    = shift;
    mScale    = scale;
    mOffset   = offset;
    mValidMin = validMin;
    mValidMax = validMax;

    //assert(size <= REGISTER_FLAG_SIZE_MASK);
    mFlags |= (size & REGISTER_FLAG_SIZE_MASK);
}

//-------------------------------------------------------------------------------------------------
void ConsultRegister::ClearUsedRegisters(void)
{
    memset(sRegisterOrder, 0xFF, sizeof(sRegisterOrder));
}

//-------------------------------------------------------------------------------------------------
uint8_t ConsultRegister::AddCommandSpecial(uint8_t *dest, uint8_t *echoDest)
{
    if (mAddress == InjectorDutyCycle)
    {
        uint8_t valid = (sConsultRegisters[0].mFlags & sConsultRegisters[6].mFlags) & REGISTER_FLAG_PULL_DATA;
        if (valid != 0) mFlags |=  REGISTER_FLAG_SPECIAL_VALID;
        else            mFlags &= ~REGISTER_FLAG_SPECIAL_VALID;
    }

    return 0;
}

//-------------------------------------------------------------------------------------------------
uint8_t ConsultRegister::AddCommand(uint8_t *dest, uint8_t *echoDest)
{
    unsigned size = mFlags & REGISTER_FLAG_SIZE_MASK;

    // Handle composite registers
    if (size > 2) return AddCommandSpecial(reg, dest, echoDest);

    uint8_t needPull = 1;
    unsigned index;
    for (index = 0; (index < MAX_REGISTER_ORDER) && (sRegisterOrder[index]) != 0xFF; index++)
    {
        if (sRegisterOrder[index] == mAddress)
        {
            if ((size == 1) || ((size == 2) && (sRegisterOrder[index + 1] == (mAddress + 1))))
            {
                needPull = 0;
                break;
            }
        }
    }

    if (needPull != 0)
    {
        mFlags |= REGISTER_FLAG_PULL_DATA;
    }
    else
    {
        return 0;
    }

    dest[0] = ReadRegister;
    dest[1] = mAddress;

    echoDest[0] = ReadRegister ^ 0xFF;
    echoDest[1] = mAddress;

    sRegisterOrder[index] = mAddress;

    if (size == 2)
    {
        dest[2] = ReadRegister;
        dest[3] = mAddress + 1;

        echoDest[2] = ReadRegister ^ 0xFF;
        echoDest[3] = mAddress + 1;

        sRegisterOrder[index + 1] = mAddress + 1;

        return 4;
    }

    return 2;
}


//-----------------------------------------------------------------------------
uint8_t ConsultRegister::ProcessResultsSpecial(uint8_t *data, uint32_t currentTime)
{
    if ((mFlags & REGISTER_FLAG_SPECIAL_VALID) == 0) return 0;

    float newValue = 0.0F;

    if (mAddress == InjectorDutyCycle)
    {
        float peroid = 1000.0F / (sConsultRegisters[0].mCurrentValue / 120.0F);
        newValue = 100.0F * (sConsultRegisters[6].mCurrentValue / peroid);
    }

    // Only use valid data to range
    if ((newValue >= mValidMin) && (newValue <= mValidMax))
    {
        mCurrentValue = newValue;
        if (mCurrentValue < mMinimum) mMinimum = mCurrentValue;
        if (mCurrentValue > mMaximum) mMaximum = mCurrentValue;
        mLastUpdateTime = currentTime;
    }

    return 0;
}

//-----------------------------------------------------------------------------
uint8_t ConsultRegister::ProcessResults(uint8_t *data, uint32_t currentTime)
{
    unsigned size = mFlags & REGISTER_FLAG_SIZE_MASK;

    if (size > 2) return ProcessResultsSpecial(reg, data, currentTime);

    unsigned bits;

    uint8_t usedSize = 0;
    if ((mFlags & REGISTER_FLAG_PULL_DATA) != 0)
    {
        assert(mAddress < 255);
        sRegisterMemory[mAddress] = data[0];
        ++usedSize;
        if (size == 2)
        {
            sRegisterMemory[mAddress + 1] = data[1];
            ++usedSize;
        }
    }

    bits = (unsigned) sRegisterMemory[mAddress];

    if (size == 2)
    {
        bits = (bits << 8) + (unsigned) sRegisterMemory[mAddress + 1];
    }

    bits = (bits & (unsigned)mMask) >> mShift;
    float val = (float) bits;
    float newValue = (val * mScale) + mOffset;

    if ((newValue >= mValidMin) && (newValue <= mValidMax))
    {
        mCurrentValue = newValue;
        if (mCurrentValue < mMinimum) mMinimum = mCurrentValue;
        if (mCurrentValue > mMaximum) mMaximum = mCurrentValue;
        mLastUpdateTime = currentTime;
    }

    return usedSize;
}

//-----------------------------------------------------------------------------
void ConsultRegister::FormatValue(char *string, unsigned inputLength)
{
    if (reg    == NULL) return;
    if (string == NULL) return;

    sprintf(string, mFormat, mCurrentValue);
    strcat(string, mUnit);
}













