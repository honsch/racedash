#ifndef CONSULT_FAULTCODE_H
#define CONSULT_FAULTCODE_H

#include <stdint.h>

//-----------------------------------------------------------------------------
class ConsultFaultCode
{
  public:
    ConsultFaultCode(uint8_t code, const char *info) :
        mCode(code),
        mInfo(info)
    {  }

    uint8_t     Code(void) const { return mCode; }
    const char *Info(void) const { return mInfo; }

  private:
    uint8_t       mCode;
    const char   *mInfo;

};

//-----------------------------------------------------------------------------
class ConsultFaultState
{
  public:
    ConsultFaultState(const char *fault, uint8_t id, uint8_t faultIndex, uint8_t starts) :
        mFault(fault),
        mFaultID(id),
        mFaultIndex(faultIndex),
        mStartsSinceDetected(starts)
    {  }

    const char *Fault(void)   const { return mFault; }
    uint8_t     FaultID(void) const { return mFaultID; }
  private:
    const char *mFault;
    uint8_t     mFaultID;
    uint8_t     mFaultIndex;
    uint8_t     mStartsSinceDetected;
};


#endif // CONSULT_FAULTCODE_H
