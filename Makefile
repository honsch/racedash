TARGETS=racedash remotedash 
INSTALLUSER=alarm@alarm
INSTALLUSER2=alarm@alarm2
INSTALLUSER3=alarm@alarm3
INSTALLUSER4=alarm@c4
INSTALLPATH=racedash

ODROID=4

ifeq ($(ODROID),2)
    ODROID_ROOT=~/development/odroid-c2
    TCPREFIX =$(ODROID_ROOT)/gcc-arm-11.2-2022.02-x86_64-aarch64-none-linux-gnu/bin/aarch64-none-linux-gnu-
else
    ODROID_ROOT=../odroid-c4/root
    TCPREFIX=/usr/bin/aarch64-linux-gnu-
endif

CC      = $(TCPREFIX)gcc
CX      = $(TCPREFIX)g++
LD      = $(TCPREFIX)g++
AS      = $(TCPREFIX)as
CP      = $(TCPREFIX)objcopy
OD      = $(TCPREFIX)objdump
GDB     = $(TCPREFIX)gdb
GDBTUI  = $(TCPREFIX)gdbtui
NEMIVER = /usr/bin/nemiver
DDD     = /usr/bin/ddd

INCLUDES =	-I./src \
		-I./src/nanovg \
		-I/usr/include/drm

LIBDIRS = -L $(ODROID_ROOT)/usr/lib

LIBS = -lasound -ldrm -lgbm -lEGL -lGLESv2 -logg -lvorbisfile -lvorbis -lconfig++ -lpthread -lrt -lgpiod

MACHINE_OPTS=
CFLAGS  =  $(INCLUDES) $(DEFINES) -Wall -c -fdata-sections -ffunction-sections \
          -O2 -ggdb $(MACHINE_OPTS) -DNANOVG_GLES2_IMPLEMENTATION -DODROID=$(ODROID) --sysroot=$(ODROID_ROOT)

CXXFLAGS  = -std=c++11 -fno-exceptions $(CFLAGS)

CFLAGS += -DDEBUG

LFLAGS  = -Wl,--gc-sections -ggdb --sysroot=$(ODROID_ROOT)
#LFLAGS  = -ggdb -Wl,-Map $(TARGET).map

OBJDIR=obj
DEPDIR:=deps

DEPFLAGS = -MT $@ -MMD -MP -MF $(subst $(OBJDIR)/,,$(DEPDIR)/$@).Td
POSTCOMPILE = @mv -f $(DEPDIR)/$(subst $(OBJDIR)/,,$@).Td $(DEPDIR)/$(subst $(OBJDIR)/,,$@).d && touch $@

SRCFILES_DASH := src/dashmain.cpp

SRCFILES_REMOTE := src/remotemain.cpp

SRCFILES_SHARED := \
		src/display.cpp \
		src/mouse.cpp \
		src/touchscreen.cpp \
		src/audio/alsa.cpp \
		src/audio/alsaled.cpp \
		src/audio/audio.cpp\
		src/audio/playlist.cpp \
		src/config/config.cpp \
		src/consult/consult.cpp \
		src/datalogger/datalogger.cpp \
		src/filter/biquad.cpp \
		src/filter/butterworth.cpp \
		src/gnss/mtkgps.cpp \
		src/gnss/nmeadata.cpp \
		src/gnss/nmeainterface.cpp \
		src/gnss/nmeasentence.cpp \
		src/gpio/gpio.cpp \
		src/led/ledstring.cpp \
		src/log/log.cpp \
		src/nanovg/nanovg.c \
		src/radio/radio_1276_lora.cpp \
		src/rpn/rpn.cpp \
		src/screens/dashboardgauges.cpp \
		src/screens/debug.cpp \
		src/screens/engine.cpp \
		src/screens/gettext.cpp \
		src/screens/musicplayer.cpp \
                src/screens/pit.cpp \
                src/screens/pitrequest.cpp \
                src/screens/playlist_edit.cpp \
                src/screens/playlist_load.cpp \
		src/screens/remotedash.cpp \
		src/screens/selecttrack.cpp \
		src/screens/touchscreencal.cpp \
		src/sensors/iio-ak8975.cpp \
		src/sensors/iiodevice.cpp \
		src/sensors/iiodevicemanager.cpp \
		src/sensors/iiotrigger.cpp \
		src/sensors/iio-max11616.cpp \
		src/sensors/iio-mpu9250.cpp \
		src/sensors/madgwick-ahrs.cpp \
		src/sensors/sensormanager.cpp \
		src/sensors/speeduino.cpp \
                src/sensors/speeduinocan.cpp \
		src/sensors/steinheartheart.cpp \
                src/spi/spi.cpp \
		src/track/track.cpp \
                src/ui/bargauge.cpp \
                src/ui/button.cpp \
		src/ui/dialgauge.cpp \
                src/ui/gauge.cpp \
                src/ui/numericgauge.cpp \
                src/ui/listbox.cpp \
                src/ui/screen.cpp \
                src/ui/screen_element.cpp \
		src/ui/slider.cpp \
		src/ui/stripgauge.cpp \
		src/ui/ui.cpp \
		src/util/util.cpp

OUTOBJS    := $(addprefix $(OBJDIR)/,$(addsuffix .o,$(SRCFILES_SHARED)))
DASHOBJS   := $(addprefix $(OBJDIR)/,$(addsuffix .o,$(SRCFILES_DASH)))
REMOTEOBJS := $(addprefix $(OBJDIR)/,$(addsuffix .o,$(SRCFILES_REMOTE)))


all: racedash remotedash

deploy: racedash remotedash
	expect -f deploy.exp

deploy-data:
	rsync -r fonts $(INSTALLUSER):$(INSTALLPATH)/
	rsync -r images $(INSTALLUSER):$(INSTALLPATH)/
	rsync -r tracks $(INSTALLUSER):$(INSTALLPATH)/
	rsync -r ../../ogg/Music $(INSTALLUSER):$(INSTALLPATH)/

deploy-source: 
	expect -f deploy-src.exp

deploy2: racedash remotedash
	expect -f deploy2.exp

deploy2-data:
	rsync -r fonts $(INSTALLUSER2):$(INSTALLPATH)/
	rsync -r images $(INSTALLUSER2):$(INSTALLPATH)/
	rsync -r tracks $(INSTALLUSER2):$(INSTALLPATH)/
	rsync -r ../../ogg/Music $(INSTALLUSER2):$(INSTALLPATH)/

deploy2-source:
	expect -f deploy2-src.exp

deploy3: racedash remotedash
	expect -f deploy3.exp

deploy3-data:
	rsync -r fonts $(INSTALLUSER3):$(INSTALLPATH)/
	rsync -r images $(INSTALLUSER3):$(INSTALLPATH)/
	rsync -r tracks $(INSTALLUSER3):$(INSTALLPATH)/
	rsync -r ../../ogg/Music $(INSTALLUSER3):$(INSTALLPATH)/

deploy3-source:
	expect -f deploy3-src.exp

deploy4: racedash remotedash
	expect -f deploy4.exp

deploy4-data:
	rsync -r fonts $(INSTALLUSER4):$(INSTALLPATH)/
	rsync -r images $(INSTALLUSER4):$(INSTALLPATH)/
	rsync -r tracks $(INSTALLUSER4):$(INSTALLPATH)/
	rsync -r ../../ogg/Music $(INSTALLUSER4):$(INSTALLPATH)/

deploy4-source:
	expect -f deploy4-src.exp

get-logs:
	rsync -r $(INSTALLUSER):$(INSTALLPATH)/logs/ logs/

get2-logs:
	rsync -r $(INSTALLUSER2):$(INSTALLPATH)/logs/ logs/

get3-logs:
	rsync -r $(INSTALLUSER3):$(INSTALLPATH)/logs/ logs/

clean:
	@echo Cleaning...
	@rm -f $(TARGET)
	@rm -rf $(OBJDIR)
	@rm -rf $(DEPDIR)

racedash: $(OUTOBJS) $(DASHOBJS)
	@echo Linking racedash...
	@$(LD) $(LFLAGS) -o racedash $(OUTOBJS) $(DASHOBJS) $(LIBDIRS) $(LIBS)

remotedash: $(OUTOBJS) $(REMOTEOBJS)
	@echo Linking remotedash...
	@$(LD) $(LFLAGS) -o remotedash $(OUTOBJS) $(REMOTEOBJS) $(LIBDIRS) $(LIBS)

$(OUTOBJS): $(OBJDIR)/%.o : $(basename %) Makefile $(DEPDIR)/%.o.d
	@mkdir -p $(@D)
	@mkdir -p $(DEPDIR)/$(subst $(OBJDIR)/,,$(@D))
	@echo Compiling $<
	@$(CX) $(DEPFLAGS) $(CXXFLAGS) -o $@ $<
	$(POSTCOMPILE)

$(DASHOBJS): $(OBJDIR)/%.o : $(basename %) Makefile $(DEPDIR)/%.o.d
	@mkdir -p $(@D)
	@mkdir -p $(DEPDIR)/$(subst $(OBJDIR)/,,$(@D))
	@echo Compiling $<
	@$(CX) $(DEPFLAGS) $(CXXFLAGS) -o $@ $<
	$(POSTCOMPILE)

$(REMOTEOBJS): $(OBJDIR)/%.o : $(basename %) Makefile $(DEPDIR)/%.o.d
	@mkdir -p $(@D)
	@mkdir -p $(DEPDIR)/$(subst $(OBJDIR)/,,$(@D))
	@echo Compiling $<
	@$(CX) $(DEPFLAGS) $(CXXFLAGS) -o $@ $<
	$(POSTCOMPILE)


$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(patsubst %,$(DEPDIR)/%.o.d,$(SRCFILES_SHARED))
include $(patsubst %,$(DEPDIR)/%.o.d,$(SRCFILES_DASH))
include $(patsubst %,$(DEPDIR)/%.o.d,$(SRCFILES_REMOTE))
